package id.wika.cpptkponline.presenter;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.endpoint.KeputusanJangkaPendekEndpointInterface;
import id.wika.cpptkponline.data.endpoint.LingkupTemuanEndpointInterface;
import id.wika.cpptkponline.data.endpoint.LoginEndpointInterface;
import id.wika.cpptkponline.data.endpoint.PieEndpointInterface;
import id.wika.cpptkponline.data.endpoint.RootCauseEndpointInterface;
import id.wika.cpptkponline.data.endpoint.TokenWrapper;
import id.wika.cpptkponline.data.endpoint.UnitKerjaEndpointInterface;
import id.wika.cpptkponline.data.endpoint.response.KeputusanJangkaPendekResponse;
import id.wika.cpptkponline.data.endpoint.response.LingkupTemuanJenisResponse;
import id.wika.cpptkponline.data.endpoint.response.LingkupTemuanResponse;
import id.wika.cpptkponline.data.endpoint.response.PieCpResponse;
import id.wika.cpptkponline.data.endpoint.response.PieRootCauseResponse;
import id.wika.cpptkponline.data.endpoint.response.ProyekResponse;
import id.wika.cpptkponline.data.endpoint.response.RoleUnitKerjaResponse;
import id.wika.cpptkponline.data.endpoint.response.RootCauseJenisResponse;
import id.wika.cpptkponline.data.endpoint.response.RootCauseResponse;
import id.wika.cpptkponline.data.endpoint.response.UserDevisiResponse;
import id.wika.cpptkponline.data.model.KeputusanJangkaPendek;
import id.wika.cpptkponline.data.model.LingkupTemuan;
import id.wika.cpptkponline.data.model.LingkupTemuanJenis;
import id.wika.cpptkponline.data.model.RoleUnitKerja;
import id.wika.cpptkponline.data.model.RootCauseJenis;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by apple on 9/10/17.
 */

public class DictionaryViewModel {

    private LoginEndpointInterface loginAPI;
    private LingkupTemuanEndpointInterface lingkupTemuanAPI;
    private RootCauseEndpointInterface rootCauseAPI;
    private KeputusanJangkaPendekEndpointInterface keputusanJangkaPendekAPI;
    private UnitKerjaEndpointInterface unitKerjaAPI;
    private PieEndpointInterface pieAPI;

    List<String> listProyek;
    List<User> listUser;
    List<RoleUnitKerjaResponse> listDivisiAll;
    List<KeputusanJangkaPendekResponse> listKeputusanJangkaPendek;

    List<String> listLingkupTemuan;
    List<LingkupTemuan> listLingkupTemuanJenis;

    PieCpResponse listPieCP;
    PieRootCauseResponse listPieRootCause;

    List<String> listDivisi;


    public  PieCpResponse getPieCPPTKP(User user, String pie, int year) {

        listPieCP = new PieCpResponse();

        pieAPI = ServiceGenerator.createService(PieEndpointInterface.class);
        pieAPI.getPie(TokenWrapper.getWrappedToken(user.getToken()),pie, year)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<PieCpResponse>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onNext(PieCpResponse response) {
                        listPieCP=response;
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Error",e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
        return  listPieCP;

    }

    public  PieRootCauseResponse getPieRootCause(User user, String pie, int year) {

        listPieRootCause = new PieRootCauseResponse();

        pieAPI = ServiceGenerator.createService(PieEndpointInterface.class);
        pieAPI.getPieRootCause(TokenWrapper.getWrappedToken(user.getToken()),pie, year)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<PieRootCauseResponse>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onNext(PieRootCauseResponse response) {
                            listPieRootCause=response;
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Error",e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
        return  listPieRootCause;
    }

    public  List<String> getProyekRemote(User user) {
        listProyek = new ArrayList<String>();

        loginAPI = ServiceGenerator.createService(LoginEndpointInterface.class);
        loginAPI.getProyek(TokenWrapper.getWrappedToken(user.getToken()), user.getkodedivisi())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<ProyekResponse>>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onNext(List<ProyekResponse> response) {
                        for (ProyekResponse a: response
                             ) {
                            listProyek.add(a.getNama());
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Error",e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });

        return listProyek;
    }

    public  List<String> getDivisiRemote(User user) {
        listDivisi = new ArrayList<String>();

        loginAPI = ServiceGenerator.createService(LoginEndpointInterface.class);
        loginAPI.getUnitKerja(TokenWrapper.getWrappedToken(user.getToken()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<RoleUnitKerjaResponse>>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onNext(List<RoleUnitKerjaResponse> response) {
                        for (RoleUnitKerjaResponse a: response
                                ) {
                            listDivisi.add(a.getNama());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

        return null;
    }


    public  List<RoleUnitKerjaResponse> getDivisiAllRemote(User user) {

        listDivisiAll = new ArrayList<RoleUnitKerjaResponse>();

        unitKerjaAPI = ServiceGenerator.createService(UnitKerjaEndpointInterface.class);
         unitKerjaAPI.getDivisi(TokenWrapper.getWrappedToken(user.getToken()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<RoleUnitKerjaResponse>>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onNext(List<RoleUnitKerjaResponse> response) {
                        for (RoleUnitKerjaResponse a: response
                                ) {
                            RoleUnitKerjaResponse l =  new RoleUnitKerjaResponse();
                            l.setKode(a.getKode());
                            l.setNama(a.getNama());
                            listDivisiAll.add(l);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

        return listDivisiAll;
    }

    public  List<User> getUserByDivisiRemote(User user, String divisi, String proyek) {

        listUser = new ArrayList<User>();

        unitKerjaAPI = ServiceGenerator.createService(UnitKerjaEndpointInterface.class);
        unitKerjaAPI.getUserByDivisi(TokenWrapper.getWrappedToken(user.getToken()), divisi, proyek)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<UserDevisiResponse>>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onNext(List<UserDevisiResponse> response) {
                        for (UserDevisiResponse a: response
                                ) {
                            User user = new User();
                            user.setFullname(a.getName());
                            user.setUsername(a.getUsername());
                            user.setJabatan(a.getJabatan());
                            listUser.add(user);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

        return listUser;
    }

    public  List<String> getLingkupTemuanRemote(User user, String jenis) {

        listLingkupTemuan = new ArrayList<String>();
        lingkupTemuanAPI = ServiceGenerator.createService(LingkupTemuanEndpointInterface.class);
        lingkupTemuanAPI.getLingkupTemuan(TokenWrapper.getWrappedToken(user.getToken()), jenis)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<LingkupTemuanResponse>>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onNext(List<LingkupTemuanResponse> response) {
                        for (LingkupTemuanResponse a: response
                                ) {
                            listLingkupTemuan.add(a.getNama());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

        return listLingkupTemuan;
    }

    public  List<LingkupTemuan> getLingkupTemuanJenisRemote(User user) {

        listLingkupTemuanJenis = new ArrayList<LingkupTemuan>();
        lingkupTemuanAPI = ServiceGenerator.createService(LingkupTemuanEndpointInterface.class);
        lingkupTemuanAPI.getLingkupTemuanJenis(TokenWrapper.getWrappedToken(user.getToken()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<LingkupTemuanJenisResponse>>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onNext(List<LingkupTemuanJenisResponse> response) {
                        for (LingkupTemuanJenisResponse a: response
                                ) {
                            LingkupTemuan l =  new LingkupTemuan();
                            l.setTitle(a.getJenis());
                            listLingkupTemuanJenis.add(l);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

        return listLingkupTemuanJenis;
    }


    public  List<KeputusanJangkaPendekResponse> getKeputusanJangkaPendekRemote(User user) {
        listKeputusanJangkaPendek = new ArrayList<KeputusanJangkaPendekResponse>();

        keputusanJangkaPendekAPI = ServiceGenerator.createService(KeputusanJangkaPendekEndpointInterface.class);
        keputusanJangkaPendekAPI.getKeputusanJangkaPendek(TokenWrapper.getWrappedToken(user.getToken()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<KeputusanJangkaPendekResponse>>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onNext(List<KeputusanJangkaPendekResponse> response) {
                        for (KeputusanJangkaPendekResponse a: response
                                ) {
                            listKeputusanJangkaPendek.add(a);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

        return listKeputusanJangkaPendek;
    }

    public  List<RootCauseResponse> getRootCauseRemote(User user, String jenis) {
        rootCauseAPI = ServiceGenerator.createService(RootCauseEndpointInterface.class);
        rootCauseAPI.getRootCause(TokenWrapper.getWrappedToken(user.getToken()), jenis)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<RootCauseResponse>>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onNext(List<RootCauseResponse> loginResponse) {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

        return null;
    }

    public  List<RootCauseJenisResponse> getRootCauseJenisRemote(User user) {
        rootCauseAPI = ServiceGenerator.createService(RootCauseEndpointInterface.class);
        rootCauseAPI.getRootCauseJenis(TokenWrapper.getWrappedToken(user.getToken()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<RootCauseJenisResponse>>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onNext(List<RootCauseJenisResponse> loginResponse) {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

        return null;
    }


}
