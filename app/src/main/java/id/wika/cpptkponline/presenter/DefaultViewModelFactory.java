package id.wika.cpptkponline.presenter;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.content.SharedPreferences;

import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.data.db.AppDatabase;
import id.wika.cpptkponline.data.endpoint.FormEndpointInterface;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public class DefaultViewModelFactory implements ViewModelProvider.Factory {

    private Context context;
    private SharedPreferences sharedPreferences;

    public DefaultViewModelFactory(Context context, SharedPreferences sharedPreferences) {
        this.context = context;
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        String token = sharedPreferences.getString(ServiceGenerator.TOKEN_KEY, "");
        AppDatabase dbInstance = AppDatabase.getInstance(context);
        if (modelClass.isAssignableFrom(FormViewModel.class)) {
            return (T) new FormViewModel(dbInstance, token);
        } else if (modelClass.isAssignableFrom(HomeViewModel.class)) {
            return (T) new HomeViewModel(
                    dbInstance.formDao(),
                    dbInstance.formPelaporanDao(),
                    ServiceGenerator.createService(FormEndpointInterface.class),
                    token);
        } else if (modelClass.isAssignableFrom(ReviewViewModel.class)) {
            return (T) new ReviewViewModel(
                    dbInstance,
                    ServiceGenerator.createService(FormEndpointInterface.class),
                    token);
        } else if (modelClass.isAssignableFrom(HistoryViewModel.class)) {
            return (T) new HistoryViewModel(dbInstance.formDao(),
                    dbInstance.formPelaporanDao(),
                    ServiceGenerator.createService(FormEndpointInterface.class),
                    token);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
