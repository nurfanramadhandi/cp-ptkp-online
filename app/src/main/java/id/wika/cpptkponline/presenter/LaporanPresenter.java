package id.wika.cpptkponline.presenter;

import id.wika.cpptkponline.view.LaporanView;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 28, 2017 8:33:21 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public interface LaporanPresenter extends BasePresenter<LaporanView> {
    void getListData();
}