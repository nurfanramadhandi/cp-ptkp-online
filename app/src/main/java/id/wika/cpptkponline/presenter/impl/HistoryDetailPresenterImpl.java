package id.wika.cpptkponline.presenter.impl;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import id.wika.cpptkponline.data.FormSummary;
import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.db.AppDatabase;
import id.wika.cpptkponline.data.db.FormDao;
import id.wika.cpptkponline.data.db.FormPelaporanDao;
import id.wika.cpptkponline.data.endpoint.FormEndpointInterface;
import id.wika.cpptkponline.data.endpoint.TokenWrapper;
import id.wika.cpptkponline.data.endpoint.response.PostTemuanListResponse;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.presenter.HistoryDetailPresenter;
import id.wika.cpptkponline.view.HistoryDetailView;
import id.wika.cpptkponline.view.home.list.FormListItem;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:54:07 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil
 * All Rights Reserved. 
 ******************************************************************************/


public final class HistoryDetailPresenterImpl extends BasePresenterImpl<HistoryDetailView> implements HistoryDetailPresenter {

    // The view is available using the mView variable
    // The view is available using the mView variable
    private final CompositeDisposable mDisposable = new CompositeDisposable();
    private FormDao formDao;
    private FormPelaporanDao formPelaporanDao;
    private FormEndpointInterface formAPI;
    private DateTimeHelper helper = new DateTimeHelper();

    private String token;
    private User user;
    @Inject
    public HistoryDetailPresenterImpl(Context context) {
        AppDatabase dbInstance = AppDatabase.getInstance(context);
        this.formDao = dbInstance.formDao();
        this.formPelaporanDao = dbInstance.formPelaporanDao();
        this.formAPI = ServiceGenerator.createService(FormEndpointInterface.class);
        this.user = User.load(context);
        this.token = user.getToken();
    }

    @Override
    public void onStart(boolean viewCreated) {
        super.onStart(viewCreated);

        // Your code here. Your view is available using mView and will not be null until next onStop()
    }

    @Override
    public void onStop() {
        // Your code here, mView will be null after this method until next onStart()

        super.onStop();
    }

    @Override
    public void onPresenterDestroyed() {
        /*
         * Your code here. After this method, your presenter (and view) will be completely destroyed
         * so make sure to cancel any HTTP call or database connection
         */

        super.onPresenterDestroyed();
        mDisposable.dispose();
        mDisposable.clear();
    }

    @Override
    public void getListData(int id) {

            Observable<List<PostTemuanListResponse.Datum>> observable = formAPI.postTemuanList(TokenWrapper.getWrappedToken(token), String.valueOf(id))
                    .map(new Function<PostTemuanListResponse, List<PostTemuanListResponse.Datum>>() {
                        @Override
                        public List<PostTemuanListResponse.Datum> apply(@io.reactivex.annotations.NonNull PostTemuanListResponse postTemuanListResponse) throws Exception {
                            return postTemuanListResponse.getData();
                        }
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        mDisposable.add(
                observable.subscribeWith(new DisposableObserver<List<PostTemuanListResponse.Datum>>(){

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<PostTemuanListResponse.Datum> postTemuanListResponse) {
                        if (mView != null) {
                            if(postTemuanListResponse.size()!=0)
                                mView.renderData(postTemuanListResponse);
                            else{
                                List<PostTemuanListResponse.Datum> data = new ArrayList<PostTemuanListResponse.Datum>();
                                data.add(new PostTemuanListResponse.Datum("No Data","","",0000000000L,0000000000L,0,"",""));
                                mView.renderData(data);
                                }
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable throwable) {
                        if (mView != null) {
                            mView.showError(throwable.getMessage());
                        }
//                        if (mView != null) {
//                            List<PostTemuanListResponse.Datum> data = new ArrayList<PostTemuanListResponse.Datum>();
//                            data.add(new PostTemuanListResponse.Datum("aaa","bbb","ccc",123123123L,1231231233L,1,"ggg","hhh"));
//                            data.add(new PostTemuanListResponse.Datum("aaa","bbb","ccc",123123123L,1231231233L,2,"ggg","hhh"));
//                            data.add(new PostTemuanListResponse.Datum("aaa","bbb","ccc",123123123L,1231231233L,4,"ggg","hhh"));
//                            mView.renderData(data);
//                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                })
        );
    }
}