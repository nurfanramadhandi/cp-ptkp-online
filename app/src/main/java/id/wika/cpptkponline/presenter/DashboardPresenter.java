package id.wika.cpptkponline.presenter;

import java.util.List;

import id.wika.cpptkponline.data.PieChartData;
import id.wika.cpptkponline.data.endpoint.response.PieCpResponse;
import id.wika.cpptkponline.data.endpoint.response.PieRootCauseResponse;
import id.wika.cpptkponline.view.DashboardView;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 2:39:34 PM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public interface DashboardPresenter extends BasePresenter<DashboardView> {
    void renderChart1(int year);
    void renderChart2(int year);
    void renderChart3(int year);
    void renderChart4(int year);
}