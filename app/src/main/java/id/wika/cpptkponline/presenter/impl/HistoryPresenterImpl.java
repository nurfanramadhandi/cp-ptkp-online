package id.wika.cpptkponline.presenter.impl;

import android.content.Context;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import id.wika.cpptkponline.data.FormSummary;
import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.db.AppDatabase;
import id.wika.cpptkponline.data.db.FormDao;
import id.wika.cpptkponline.data.db.FormPelaporanDao;
import id.wika.cpptkponline.data.endpoint.FormEndpointInterface;
import id.wika.cpptkponline.data.endpoint.TokenWrapper;
import id.wika.cpptkponline.data.filter.ResumeFillterType;
import id.wika.cpptkponline.data.filter.ResumeFilter;
import id.wika.cpptkponline.data.filter.ResumeFilterFactory;
import id.wika.cpptkponline.data.mapper.HistoryToFormSummary;
import id.wika.cpptkponline.data.model.HistoryModel;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.presenter.HistoryPresenter;
import id.wika.cpptkponline.view.HistoryView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import static id.wika.cpptkponline.helper.FormSummarySortComparatorFactory.SORT_TYPE;
import static id.wika.cpptkponline.helper.FormSummarySortComparatorFactory.createComparator;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:51:54 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/


public final class HistoryPresenterImpl extends BasePresenterImpl<HistoryView> implements HistoryPresenter {

    // The view is available using the mView variable
    private final CompositeDisposable mDisposable = new CompositeDisposable();
    private FormDao formDao;
    private FormPelaporanDao formPelaporanDao;
    private FormEndpointInterface formAPI;
    private HistoryToFormSummary historyToFormSummary;

    private String token;
    private User user;

    private ResumeFillterType resumeFillterType = ResumeFillterType.ALL;
    private SORT_TYPE sortType = SORT_TYPE.DATE_DESC;

    public ResumeFillterType getResumeFillterType() {
        return resumeFillterType;
    }

    @Override
    public void setResumeFillterType(ResumeFillterType resumeFillterType) {
        this.resumeFillterType = resumeFillterType;
    }

    @Override
    public void setSortType(SORT_TYPE sortType) {
        this.sortType = sortType;
    }

    @Inject
    public HistoryPresenterImpl(Context context) {
        historyToFormSummary = new HistoryToFormSummary();
        AppDatabase dbInstance = AppDatabase.getInstance(context);
        this.formDao = dbInstance.formDao();
        this.formPelaporanDao = dbInstance.formPelaporanDao();
        this.formAPI = ServiceGenerator.createService(FormEndpointInterface.class);
        this.user = User.load(context);
        this.token = user.getToken();
    }

    @Override
    public void onStart(boolean viewCreated) {
        super.onStart(viewCreated);
        getListData();
        // Your code here. Your view is available using mView and will not be null until next onStop()
    }

    @Override
    public void onStop() {
        // Your code here, mView will be null after this method until next onStart()

        super.onStop();
    }

    @Override
    public void onPresenterDestroyed() {
        /*
         * Your code here. After this method, your presenter (and view) will be completely destroyed
         * so make sure to cancel any HTTP call or database connection
         */

        super.onPresenterDestroyed();
        mDisposable.dispose();
        mDisposable.clear();
    }

    @Override
    public void getListData() {
        Observable<List<HistoryModel.Datum>> observable = formAPI.getFormHistory(TokenWrapper.getWrappedToken(token))
                .map(new Function<HistoryModel, List<HistoryModel.Datum>>() {
                    @Override
                    public List<HistoryModel.Datum> apply(@io.reactivex.annotations.NonNull HistoryModel postTemuanListResponse) throws Exception {
                        return postTemuanListResponse.getData();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        mDisposable.add(
                observable.subscribeWith(new DisposableObserver<List<HistoryModel.Datum>>() {

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<HistoryModel.Datum> postTemuanListResponse) {
                        ResumeFilterFactory resumeFilterFactory = new ResumeFilterFactory();
                        ResumeFilter resumeFilter = resumeFilterFactory.create(getResumeFillterType());
                        if (mView != null) {
                            if (postTemuanListResponse.size() != 0) {
                                List<FormSummary> filteredFormSummaries = resumeFilter.filter(historyToFormSummary.map1(postTemuanListResponse));

                                Collections.sort(filteredFormSummaries, createComparator(sortType));

                                mView.loadData(FormHelper.convertToFormListItems(filteredFormSummaries, sortType));
                            } else {
                                mView.showError("No Data ");
                            }
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable throwable) {
                        if (mView != null) {
                            mView.showError(throwable.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                })
        );
    }
}