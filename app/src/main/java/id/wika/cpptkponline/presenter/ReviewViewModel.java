package id.wika.cpptkponline.presenter;

import android.arch.lifecycle.ViewModel;

import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataApproval;
import id.wika.cpptkponline.data.DataFormPelaporan;
import id.wika.cpptkponline.data.DataRealisasiTindakLanjut;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import id.wika.cpptkponline.data.DataVerifikasiHasilTindakLanjut;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.db.AppDatabase;
import id.wika.cpptkponline.data.db.FormApprovalDao;
import id.wika.cpptkponline.data.db.FormDao;
import id.wika.cpptkponline.data.db.FormPelaporanDao;
import id.wika.cpptkponline.data.db.FormRealisasiTindakLanjutDao;
import id.wika.cpptkponline.data.db.FormRencanaTindakLanjutDao;
import id.wika.cpptkponline.data.db.FormVerifikasiHasilDao;
import id.wika.cpptkponline.data.endpoint.FormEndpointInterface;
import id.wika.cpptkponline.data.endpoint.TokenWrapper;
import id.wika.cpptkponline.data.endpoint.response.GetFormByIdResponse;
import id.wika.cpptkponline.helper.FormConverter;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Function;

/**
 * Created by mamang_kompii on 9/12/17.
 */

public class ReviewViewModel extends ViewModel {

    private FormDao formDao;
    private FormPelaporanDao formPelaporanDao;
    private FormApprovalDao formApprovalDao;
    private FormRencanaTindakLanjutDao rencanaTindakLanjutDao;
    private FormRealisasiTindakLanjutDao realisasiTindakLanjutDao;
    private FormVerifikasiHasilDao formVerifikasiHasilDao;
    private final FormEndpointInterface formAPI;
    private final String token;

    public ReviewViewModel(AppDatabase dbInstance, FormEndpointInterface formAPI, String token) {
        formDao = dbInstance.formDao();
        formPelaporanDao = dbInstance.formPelaporanDao();
        formApprovalDao = dbInstance.formApprovalDao();
        rencanaTindakLanjutDao = dbInstance.formRencanaTindakLanjutDao();
        realisasiTindakLanjutDao = dbInstance.formRealisasiTindakLanjutDao();
        formVerifikasiHasilDao = dbInstance.formVerifikasiHasilDao();
        this.formAPI = formAPI;
        this.token = token;
    }

    public Observable<CPPTKPForm> getFormReviewData(int formId, int formPartStatus, boolean isUsingRemoteData) {
        if (isUsingRemoteData) {
            return getFormRemoteData(formId);
        } else {
            return getFormLocalData(formId, formPartStatus);
        }
    }

    private Observable<CPPTKPForm> getFormRemoteData(final int formId) {
        return formAPI.getFormById(TokenWrapper.getWrappedToken(token), formId)
                .map(new Function<GetFormByIdResponse, CPPTKPForm>() {
                    @Override
                    public CPPTKPForm apply(GetFormByIdResponse getFormByIdResponse) throws Exception {
                        GetFormByIdResponse.Data data = getFormByIdResponse.data;
                        CPPTKPForm cpptkpForm = FormConverter.convertResponseDataToCpPtkpForm(data);

                        cpptkpForm.setFormPelaporan(
                                FormConverter.convertResponseDataToFormPelaporan(data, cpptkpForm.getFormId())
                        );

                        cpptkpForm.setRencanaTindakLanjut(
                                FormConverter.convertResponseDataToRencanaTindakLanjut(data, cpptkpForm.getFormId())
                        );

                        cpptkpForm.setApproval(
                                FormConverter.convertResponseDataToApproval(data, cpptkpForm.getFormId())
                        );

                        cpptkpForm.setRealisasiTindakLanjut(
                                FormConverter.convertResponseDataToRealisasiTindakLanjut(data, cpptkpForm.getFormId())
                        );

                        cpptkpForm.setVerifikasiHasilTindakLanjut(
                                FormConverter.convertResponseDataToVerifikasiTindakLanjut(data, cpptkpForm.getFormId())
                        );


                        return cpptkpForm;
                    }
                }).toObservable();
    }

    private Observable<CPPTKPForm> getFormLocalData(int formId, int formPartStatus) {
        switch (formPartStatus) {
            case FormStatus.FORM_AWAL:
            case FormStatus.FORM_PENGECEKAN:
                return getDataPelaporan(formId);
            case FormStatus.FORM_RENCANA_TINDAK_LANJUT:
                return getDataRencanaTindakLanjut(formId);
            case FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI:
                return getDataApproval(formId);
            case FormStatus.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN:
                return getDataApproval(formId);
            case FormStatus.FORM_REALISASI_TINDAK_LANJUT:
                return getDataRealisasiTindakLanjut(formId);
            case FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN:
                return getDataVerifikasiHasilTindakLanjut(formId);
            case FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED:
                return getDataVerifikasiHasilTindakLanjut(formId);
            default:
                return Observable.just(new CPPTKPForm());
        }
    }

    private Observable<CPPTKPForm> getDataPelaporan(final int formId) {
        return Observable
                .create(new ObservableOnSubscribe<CPPTKPForm>() {
                    @Override
                    public void subscribe(ObservableEmitter<CPPTKPForm> e) throws Exception {
                        e.onNext(formDao.getFormById(formId));
                        e.onComplete();
                    }
                })
                .map(new Function<CPPTKPForm, CPPTKPForm>() {
                    @Override
                    public CPPTKPForm apply(CPPTKPForm cpptkpForm) throws Exception {
                        DataFormPelaporan dataPelaporan =
                                formPelaporanDao.getFormDataByRelatedFormId(formId);
                        cpptkpForm.setFormPelaporan(dataPelaporan);
                        return cpptkpForm;
                    }
                });
    }

    private Observable<CPPTKPForm> getDataRencanaTindakLanjut(final int formId) {
        return Observable
                .create(new ObservableOnSubscribe<CPPTKPForm>() {
                    @Override
                    public void subscribe(ObservableEmitter<CPPTKPForm> e) throws Exception {
                        e.onNext(formDao.getFormById(formId));
                        e.onComplete();
                    }
                })
                .map(new Function<CPPTKPForm, CPPTKPForm>() {
                    @Override
                    public CPPTKPForm apply(CPPTKPForm cpptkpForm) throws Exception {
                        DataRencanaTindakLanjut rencanaTindakLanjut =
                                rencanaTindakLanjutDao.getFormDataByRelatedFormId(formId);
                        cpptkpForm.setRencanaTindakLanjut(rencanaTindakLanjut);
                        return cpptkpForm;
                    }
                });
    }

    private Observable<CPPTKPForm> getDataApproval(final int formId) {
        return Observable
                .create(new ObservableOnSubscribe<CPPTKPForm>() {
                    @Override
                    public void subscribe(ObservableEmitter<CPPTKPForm> e) throws Exception {
                        e.onNext(formDao.getFormById(formId));
                        e.onComplete();
                    }
                })
                .map(new Function<CPPTKPForm, CPPTKPForm>() {
                    @Override
                    public CPPTKPForm apply(CPPTKPForm cpptkpForm) throws Exception {
                        DataApproval dataApproval
                                = formApprovalDao.getFormDataByRelatedFormId(formId);
                        cpptkpForm.setApproval(dataApproval);
                        return cpptkpForm;
                    }
                });
    }

    private Observable<CPPTKPForm> getDataRealisasiTindakLanjut(final int formId) {
        return Observable
                .create(new ObservableOnSubscribe<CPPTKPForm>() {
                    @Override
                    public void subscribe(ObservableEmitter<CPPTKPForm> e) throws Exception {
                        e.onNext(formDao.getFormById(formId));
                        e.onComplete();
                    }
                })
                .map(new Function<CPPTKPForm, CPPTKPForm>() {
                    @Override
                    public CPPTKPForm apply(CPPTKPForm cpptkpForm) throws Exception {
                        DataRealisasiTindakLanjut rencanaTindakLanjut =
                                realisasiTindakLanjutDao.getFormDataByRelatedFormId(formId);
                        cpptkpForm.setRealisasiTindakLanjut(rencanaTindakLanjut);

                        return cpptkpForm;
                    }
                });
    }

    private Observable<CPPTKPForm> getDataVerifikasiHasilTindakLanjut(final int formId) {
        return Observable
                .create(new ObservableOnSubscribe<CPPTKPForm>() {
                    @Override
                    public void subscribe(ObservableEmitter<CPPTKPForm> e) throws Exception {
                        e.onNext(formDao.getFormById(formId));
                        e.onComplete();
                    }
                })
                .map(new Function<CPPTKPForm, CPPTKPForm>() {
                    @Override
                    public CPPTKPForm apply(CPPTKPForm cpptkpForm) throws Exception {
                        DataVerifikasiHasilTindakLanjut dataVerifikasiHasilTindakLanjut =
                                formVerifikasiHasilDao.getFormDataByRelatedFormId(formId);

                        cpptkpForm.setVerifikasiHasilTindakLanjut(dataVerifikasiHasilTindakLanjut);

                        return cpptkpForm;
                    }
                });
    }
}
