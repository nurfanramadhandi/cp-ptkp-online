package id.wika.cpptkponline.presenter;

import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataFormPelaporan;
import id.wika.cpptkponline.data.FormSummary;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.db.FormDao;
import id.wika.cpptkponline.data.db.FormPelaporanDao;
import id.wika.cpptkponline.data.endpoint.FormEndpointInterface;
import id.wika.cpptkponline.data.endpoint.TokenWrapper;
import id.wika.cpptkponline.data.endpoint.response.GetFormResponse;
import id.wika.cpptkponline.data.endpoint.response.RemoteFormSummary;
import id.wika.cpptkponline.data.filter.ResumeFillterType;
import id.wika.cpptkponline.data.filter.ResumeFilter;
import id.wika.cpptkponline.data.filter.ResumeFilterFactory;
import id.wika.cpptkponline.data.model.RemoteFormState;
import id.wika.cpptkponline.helper.FormConverter;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.helper.FormSummarySortComparatorFactory;
import id.wika.cpptkponline.view.home.list.FormListItem;
import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Flowable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;

import static id.wika.cpptkponline.helper.FormSummarySortComparatorFactory.SORT_TYPE;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public class HomeViewModel extends ViewModel {

    private FormDao formDao;
    private FormPelaporanDao formPelaporanDao;
    private FormEndpointInterface formAPI;

    private String token;

    private ResumeFillterType resumeFillterType = ResumeFillterType.ALL;
    private SORT_TYPE sortType = SORT_TYPE.DATE_DESC;

    public ResumeFillterType getResumeFillterType() {
        return resumeFillterType;
    }

    public void setResumeFillterType(ResumeFillterType resumeFillterType) {
        this.resumeFillterType = resumeFillterType;
    }

    public HomeViewModel(FormDao formDao, FormPelaporanDao formPelaporanDao, FormEndpointInterface service, String token) {
        this.formDao = formDao;
        this.formPelaporanDao = formPelaporanDao;
        this.formAPI = service;
        this.token = token;
    }

    public Flowable<List<FormListItem>> getLocalFormListItems(User user) {

        String roleId = user.getRoleId();
        return formDao.getFormSummary().map(new Function<List<FormSummary>, List<FormListItem>>() {
            @Override
            public List<FormListItem> apply(List<FormSummary> formSummaries) throws Exception {
                ResumeFilterFactory resumeFilterFactory = new ResumeFilterFactory();
                ResumeFilter resumeFilter = resumeFilterFactory.create(getResumeFillterType());
                List<FormSummary> filteredFormSummaries = resumeFilter.filter(formSummaries);
                Collections.sort(filteredFormSummaries, FormSummarySortComparatorFactory.createComparator(sortType));
                return FormHelper.convertToFormListItems(filteredFormSummaries, sortType);
            }
        });
    }

    public Flowable syncDataFromRemote(final User user) {
        return formAPI.getForm(TokenWrapper.getWrappedToken(token), user.proyek)
                .filter(new Predicate<GetFormResponse>() {
                    @Override
                    public boolean test(GetFormResponse getFormResponse) throws Exception {
                        return getFormResponse.getStatus().equals("success");
                    }
                })
                .map(new Function<GetFormResponse, List<RemoteFormSummary>>() {
                    @Override
                    public List<RemoteFormSummary> apply(GetFormResponse getFormResponse) throws Exception {
                        return getFormResponse.getData();
                    }
                })
                .doOnNext(new Consumer<List<RemoteFormSummary>>() {
                    @Override
                    public void accept(List<RemoteFormSummary> remoteFormSummaries) throws Exception {
                        Map<Integer, RemoteFormState> remoteFormStatesMap =
                                convertToMap(formDao.getSavedRemoteFormState().blockingFirst());

                        List<CPPTKPForm> newCpptkpForms = new ArrayList<>();

                        for (RemoteFormSummary remoteFormSummary : remoteFormSummaries) {
                            if (user.getRoleId().equals(remoteFormSummary.getAlertTo())) {
                                if (remoteFormStatesMap.containsKey(remoteFormSummary.getFormId())) {
                                    RemoteFormState oldRemoteFormState = remoteFormStatesMap.get(remoteFormSummary.getFormId());

                                    boolean isNewerStatus = FormHelper.isRemoteStatusNewer(
                                            oldRemoteFormState.getFormStatus(),
                                            remoteFormSummary.getStatus()
                                    );

                                    if (isNewerStatus) {
                                        formDao.updateFormStatus(
                                                oldRemoteFormState.getLocalFormId(),
                                                FormHelper.parseRemoteStatusToLocalStatus(remoteFormSummary.getStatus()),
                                                remoteFormSummary.getStatus());
                                    }
                                } else {
                                    newCpptkpForms.add(
                                            FormConverter.convertRemoteFormSummaryToCpPtkpForm(remoteFormSummary));
                                }
                            }
                        }

                        if (newCpptkpForms.size() > 0) {
                            for (CPPTKPForm newForm : newCpptkpForms) {
                                long id = formDao.insertForm(newForm);
                                DataFormPelaporan formPelaporan = newForm.getFormPelaporan();
                                formPelaporan.setRelatedFormId((int) id);
                                formPelaporanDao.insertForm(formPelaporan);
                            }
                        }
                    }

                    private Map<Integer, RemoteFormState> convertToMap(List<RemoteFormState> remoteFormStates) {
                        Map<Integer, RemoteFormState> map = new HashMap<>();
                        for (RemoteFormState state : remoteFormStates) {
                            map.put(state.getFormId(), state);
                        }
                        return map;
                    }
                });

    }

    public Completable clearData(final Context context, final User user) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter e) throws Exception {
                formDao.clearData();
                User.clear(context);

                FirebaseMessaging.getInstance().unsubscribeFromTopic(user.getRoleId());

                e.onComplete();
            }
        });
    }

    public void setSortType(SORT_TYPE sortType) {
        this.sortType = sortType;
    }
}
