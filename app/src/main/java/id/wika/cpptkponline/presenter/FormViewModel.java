package id.wika.cpptkponline.presenter;

import android.arch.lifecycle.ViewModel;

import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataApproval;
import id.wika.cpptkponline.data.DataFormPelaporan;
import id.wika.cpptkponline.data.DataRealisasiTindakLanjut;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import id.wika.cpptkponline.data.DataVerifikasiHasilTindakLanjut;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.data.db.AppDatabase;
import id.wika.cpptkponline.data.db.FormApprovalDao;
import id.wika.cpptkponline.data.db.FormDao;
import id.wika.cpptkponline.data.db.FormPelaporanDao;
import id.wika.cpptkponline.data.db.FormRealisasiTindakLanjutDao;
import id.wika.cpptkponline.data.db.FormRencanaTindakLanjutDao;
import id.wika.cpptkponline.data.db.FormVerifikasiHasilDao;
import id.wika.cpptkponline.data.endpoint.FormEndpointInterface;
import id.wika.cpptkponline.data.endpoint.TokenWrapper;
import id.wika.cpptkponline.data.endpoint.request.PostFormRequestGenerator;
import id.wika.cpptkponline.data.endpoint.response.GetFormByIdResponse;
import id.wika.cpptkponline.helper.FormConverter;
import id.wika.cpptkponline.helper.StringHelper;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public class FormViewModel extends ViewModel {

    private FormDao formDao;
    private FormPelaporanDao formPelaporanDao;
    private FormApprovalDao formApprovalDao;
    private FormRencanaTindakLanjutDao formrencanaTindakLanjutDao;
    private FormRealisasiTindakLanjutDao formrealisasiTindakLanjutDao;
    private FormVerifikasiHasilDao formVerifikasiHasilDao;
    private FormEndpointInterface formEndpointInterface;
    private String token;

    private PostFormRequestGenerator postFormRequestGenerator;

    public FormViewModel(AppDatabase instance, String token) {
        this(instance.formDao(),
                instance.formPelaporanDao(),
                instance.formRencanaTindakLanjutDao(),
                instance.formRealisasiTindakLanjutDao(),
                instance.formVerifikasiHasilDao(),
                instance.formApprovalDao(),
                ServiceGenerator.createService(FormEndpointInterface.class),
                token);
    }

    private FormViewModel(FormDao formDao,
                          FormPelaporanDao formPelaporanDao,
                          FormRencanaTindakLanjutDao formRencanaTindakLanjutDao,
                          FormRealisasiTindakLanjutDao formRealisasiTindakLanjutDao,
                          FormVerifikasiHasilDao formVerifikasiHasilDao,
                          FormApprovalDao formApprovalDao,
                          FormEndpointInterface formEndpointInterface,
                          String token) {

        this.formDao = formDao;
        this.formPelaporanDao = formPelaporanDao;
        this.formrencanaTindakLanjutDao = formRencanaTindakLanjutDao;
        this.formApprovalDao = formApprovalDao;
        this.formEndpointInterface = formEndpointInterface;
        this.token = token;
        this.formrealisasiTindakLanjutDao = formRealisasiTindakLanjutDao;
        this.formVerifikasiHasilDao = formVerifikasiHasilDao;
        this.postFormRequestGenerator = new PostFormRequestGenerator(
                token,
                formEndpointInterface,
                formDao,
                formApprovalDao,
                formPelaporanDao,
                formRencanaTindakLanjutDao,
                formRealisasiTindakLanjutDao,
                formVerifikasiHasilDao
        );
    }

    public Observable sendForm(final CPPTKPForm cpptkpForm, PostFormRequestGenerator.Strategy strategy) {
        return postFormRequestGenerator.createSendFormRequest(cpptkpForm, strategy)
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                    }
                })
                .subscribeOn(Schedulers.io());
    }

    public Observable<Boolean> checkIfFormDataIsAvailable(final int localFormId, final int remoteFormId, final int status) {
        return Observable
                .create(new ObservableOnSubscribe<Boolean>() {
                    @Override
                    public void subscribe(ObservableEmitter<Boolean> e) throws Exception {
                        boolean isDataAvailable;

                        CPPTKPForm cpptkpForm = formDao.getFormById(localFormId);
                        if (cpptkpForm != null) {
                            switch (status) {
                                case FormStatus.FORM_AWAL:
                                    isDataAvailable = checkIfDataPelaporanAvailable(localFormId);
                                    break;
                                case FormStatus.FORM_PENGECEKAN:
                                    isDataAvailable = checkIfDataPelaporanLengkapAvailable(localFormId);
                                    break;
                                case FormStatus.FORM_RENCANA_TINDAK_LANJUT:
                                case FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI:
                                    isDataAvailable = checkIfDataPelaporanLengkapAvailable(localFormId)
                                            && checkIfDataRencanaTindakLanjutAvailable(localFormId);
                                    break;
                                case FormStatus.FORM_REALISASI_TINDAK_LANJUT:
                                    isDataAvailable = checkIfDataPelaporanLengkapAvailable(localFormId)
                                            && checkIfDataRencanaTindakLanjutAvailable(localFormId)
                                            && checkIfDataApprovalAvailable(localFormId)
                                            && checkIfDataRealisasiTindakLanjutAvailable(localFormId);
                                    break;
                                case FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN:
                                    isDataAvailable = checkIfDataPelaporanLengkapAvailable(localFormId)
                                            && checkIfDataRencanaTindakLanjutAvailable(localFormId)
                                            && checkIfDataApprovalAvailable(localFormId)
                                            && checkIfDataRealisasiTindakLanjutAvailable(localFormId)
                                            && checkIfDataVerifikasiTindakLanjutAvailable(localFormId);
                                    break;
                                default:
                                    isDataAvailable = false;
                                    break;
                            }
                        } else {
                            isDataAvailable = false;
                        }

                        e.onNext(isDataAvailable);
                        e.onComplete();
                    }
                })
                .map(new Function<Boolean, Boolean>() {
                    @Override
                    public Boolean apply(Boolean isDataAvailable) throws Exception {
                        if (isDataAvailable) {
                            return true;
                        }

                        GetFormByIdResponse getFormByIdResponse =
                                formEndpointInterface
                                        .getFormById(TokenWrapper.getWrappedToken(token), remoteFormId)
                                        .blockingFirst();

                        if (getFormByIdResponse.status.equals("success")) {
                            saveDataToLocal(getFormByIdResponse.data, localFormId, status);
                            return true;
                        } else {
                            throw new Exception("Data not available!");
                        }
                    }
                })
                .subscribeOn(Schedulers.io());
    }

    private boolean checkIfDataPelaporanAvailable(int formId) {
        return formPelaporanDao.getFormDataByRelatedFormId(formId) != null;
    }

    private boolean checkIfDataPelaporanLengkapAvailable(int formId) {
        DataFormPelaporan formPelaporan = formPelaporanDao.getFormDataByRelatedFormId(formId);
        return formPelaporan != null && StringHelper.isNotBlank(formPelaporan.getKategoriForm());
    }

    private boolean checkIfDataRencanaTindakLanjutAvailable(int formId) {
        return formrencanaTindakLanjutDao.getFormDataByRelatedFormId(formId) != null;
    }

    private boolean checkIfDataVerifikasiTindakLanjutAvailable(int formId) {
        return formVerifikasiHasilDao.getFormDataByRelatedFormId(formId) != null;
    }

    private boolean checkIfDataApprovalAvailable(int formId) {
        return formApprovalDao.getFormDataByRelatedFormId(formId) != null;
    }

    private boolean checkIfDataRealisasiTindakLanjutAvailable(int formId) {
        return formrealisasiTindakLanjutDao.getFormDataByRelatedFormId(formId) != null;
    }

    private void saveDataToLocal(GetFormByIdResponse.Data data, int localFormId, int statusToEvaluate) {
        if (statusToEvaluate == FormStatus.FORM_AWAL) {
            formPelaporanDao.insertForm(FormConverter.convertResponseDataToFormPelaporan(data, localFormId));
        } else if (statusToEvaluate == FormStatus.FORM_PENGECEKAN) {
            insertOrUpdateFormPelaporanData(data, localFormId);
        } else if (statusToEvaluate == FormStatus.FORM_RENCANA_TINDAK_LANJUT) {
            insertOrUpdateFormPelaporanData(data, localFormId);
            formrencanaTindakLanjutDao.insertForm(FormConverter.convertResponseDataToRencanaTindakLanjut(data, localFormId));
        } else if (statusToEvaluate == FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI
                || statusToEvaluate == FormStatus.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN) {
            insertOrUpdateFormPelaporanData(data, localFormId);
            formrencanaTindakLanjutDao.insertForm(FormConverter.convertResponseDataToRencanaTindakLanjut(data, localFormId));
            formApprovalDao.insertForm(FormConverter.convertResponseDataToApproval(data, localFormId));
        } else if (statusToEvaluate == FormStatus.FORM_REALISASI_TINDAK_LANJUT) {
            insertOrUpdateFormPelaporanData(data, localFormId);
            formrencanaTindakLanjutDao.insertForm(FormConverter.convertResponseDataToRencanaTindakLanjut(data, localFormId));
            formApprovalDao.insertForm(FormConverter.convertResponseDataToApproval(data, localFormId));
            formrealisasiTindakLanjutDao.insertForm(FormConverter.convertResponseDataToRealisasiTindakLanjut(data, localFormId));
        } else if (statusToEvaluate == FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN || statusToEvaluate == FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED) {
            insertOrUpdateFormPelaporanData(data, localFormId);
            formrencanaTindakLanjutDao.insertForm(FormConverter.convertResponseDataToRencanaTindakLanjut(data, localFormId));
            formApprovalDao.insertForm(FormConverter.convertResponseDataToApproval(data, localFormId));
            formrealisasiTindakLanjutDao.insertForm(FormConverter.convertResponseDataToRealisasiTindakLanjut(data, localFormId));
            formVerifikasiHasilDao.insertForm(FormConverter.convertResponseDataToVerifikasiTindakLanjut(data, localFormId));
        }
    }

    private void insertOrUpdateFormPelaporanData(GetFormByIdResponse.Data data, int localFormId) {
        DataFormPelaporan existingData = formPelaporanDao.getFormDataByRelatedFormId(localFormId);
        DataFormPelaporan incomingData = FormConverter.convertResponseDataToFormPelaporan(data, localFormId);
        if (existingData != null) {
            existingData.setKategoriForm(incomingData.getKategoriForm());
            existingData.setLingkupTemuan(incomingData.getLingkupTemuan());
            existingData.setDetailLingkupTemuan(incomingData.getDetailLingkupTemuan());
            formPelaporanDao.updateForm(existingData);
        } else {
            formPelaporanDao.insertForm(incomingData);
        }
    }

    public Observable<CPPTKPForm> getFormData(final int formId) {
        return Observable
                .create(new ObservableOnSubscribe<CPPTKPForm>() {
                    @Override
                    public void subscribe(ObservableEmitter<CPPTKPForm> e) throws Exception {
                        CPPTKPForm cpptkpForm = formDao.getFormById(formId);
                        e.onNext(cpptkpForm);
                        e.onComplete();
                    }
                })
                .map(new Function<CPPTKPForm, CPPTKPForm>() {
                    @Override
                    public CPPTKPForm apply(CPPTKPForm cpptkpForm) throws Exception {
                        DataFormPelaporan formPelaporan = formPelaporanDao.getFormDataByRelatedFormId(formId);
                        DataRencanaTindakLanjut rencanaTindakLanjut = formrencanaTindakLanjutDao.getFormDataByRelatedFormId(formId);
                        DataApproval approval = formApprovalDao.getFormDataByRelatedFormId(formId);
                        DataRealisasiTindakLanjut realisasiTindakLanjut = formrealisasiTindakLanjutDao.getFormDataByRelatedFormId(formId);
                        DataVerifikasiHasilTindakLanjut verifikasiHasilTindakLanjut = formVerifikasiHasilDao.getFormDataByRelatedFormId(formId);


                        cpptkpForm.setFormPelaporan(formPelaporan);
                        cpptkpForm.setRencanaTindakLanjut(rencanaTindakLanjut);
                        cpptkpForm.setApproval(approval);
                        cpptkpForm.setRealisasiTindakLanjut(realisasiTindakLanjut);
                        cpptkpForm.setVerifikasiHasilTindakLanjut(verifikasiHasilTindakLanjut);

                        return cpptkpForm;
                    }
                });
    }
}
