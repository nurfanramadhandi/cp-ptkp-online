package id.wika.cpptkponline.presenter;

import id.wika.cpptkponline.data.filter.ResumeFillterType;
import id.wika.cpptkponline.helper.FormSummarySortComparatorFactory;
import id.wika.cpptkponline.view.HistoryView;


/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:51:54 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public interface HistoryPresenter extends BasePresenter<HistoryView> {
    void getListData();

    void setResumeFillterType(ResumeFillterType all);

    void setSortType(FormSummarySortComparatorFactory.SORT_TYPE dateAsc);
}