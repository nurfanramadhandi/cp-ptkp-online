package id.wika.cpptkponline.presenter;

import id.wika.cpptkponline.view.HistoryDetailView;


/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:54:07 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil
 * All Rights Reserved. 
 ******************************************************************************/

public interface HistoryDetailPresenter extends BasePresenter<HistoryDetailView> {
    void getListData(int id);
}