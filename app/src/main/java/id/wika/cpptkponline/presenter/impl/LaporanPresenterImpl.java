package id.wika.cpptkponline.presenter.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.widget.Toast;

import java.util.List;

import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.data.endpoint.FormEndpointInterface;
import id.wika.cpptkponline.data.endpoint.TokenWrapper;
import id.wika.cpptkponline.data.endpoint.response.GetListLaporan;
import id.wika.cpptkponline.presenter.LaporanPresenter;
import id.wika.cpptkponline.view.LaporanView;
import id.wika.cpptkponline.view.attachment.PDFViewActivity;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 28, 2017 8:33:21 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public final class LaporanPresenterImpl extends BasePresenterImpl<LaporanView> implements LaporanPresenter {
    // The view is available using the mView variable
    private SharedPreferences sharedPreferences;
    private FormEndpointInterface formAPI;
    private final CompositeDisposable mDisposable = new CompositeDisposable();

    @Inject
    public LaporanPresenterImpl(Context context) {
        sharedPreferences = context.getSharedPreferences(ServiceGenerator.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        formAPI = ServiceGenerator.createService(FormEndpointInterface.class);
    }

    @Override
    public void onStart(boolean viewCreated) {
        super.onStart(viewCreated);

        // Your code here. Your view is available using mView and will not be null until next onStop()
    }

    @Override
    public void onStop() {
        // Your code here, mView will be null after this method until next onStart()

        super.onStop();
        mDisposable.dispose();
        mDisposable.clear();
    }

    @Override
    public void onPresenterDestroyed() {
        /*
         * Your code here. After this method, your presenter (and view) will be completely destroyed
         * so make sure to cancel any HTTP call or database connection
         */

        super.onPresenterDestroyed();
    }

    @Override
    public void getListData() {
        String token = sharedPreferences.getString(ServiceGenerator.TOKEN_KEY, "");
        Observable<List<GetListLaporan.Datum>> observable = formAPI.getLaporanList(TokenWrapper.getWrappedToken(token))
                .filter(new Predicate<GetListLaporan>() {
                    @Override
                    public boolean test(@io.reactivex.annotations.NonNull GetListLaporan postTemuanListResponse) throws Exception {
                        return postTemuanListResponse.getStatus().equals("success");
                    }
                }).map(new Function<GetListLaporan, List<GetListLaporan.Datum>>() {
                    @Override
                    public List<GetListLaporan.Datum> apply(@io.reactivex.annotations.NonNull GetListLaporan getListLaporan) throws Exception {
                        return getListLaporan.getData();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        mDisposable.add(
                observable.subscribeWith(new DisposableObserver<List<GetListLaporan.Datum>>() {

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<GetListLaporan.Datum> getDatumList) {
//                        mProgressDialog.dismiss();
                        if (mView != null) {
                            mView.renderData(getDatumList);
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable throwable) {
//                        mProgressDialog.dismiss();
                        if (mView != null) {
                            mView.showError(throwable.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                })
        );
    }
}