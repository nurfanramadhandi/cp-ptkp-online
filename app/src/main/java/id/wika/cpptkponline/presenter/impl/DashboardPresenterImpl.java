package id.wika.cpptkponline.presenter.impl;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import id.wika.cpptkponline.data.PieChartData;
import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.endpoint.FormEndpointInterface;
import id.wika.cpptkponline.data.endpoint.TokenWrapper;
import id.wika.cpptkponline.data.endpoint.response.GetGrafikResponse;
import id.wika.cpptkponline.data.endpoint.response.GetGrafikRootCouseResponse;
import id.wika.cpptkponline.data.model.HistoryModel;
import id.wika.cpptkponline.presenter.DashboardPresenter;
import id.wika.cpptkponline.presenter.DictionaryViewModel;
import id.wika.cpptkponline.view.DashboardView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DefaultObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 2:39:34 PM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public final class DashboardPresenterImpl extends BasePresenterImpl<DashboardView> implements DashboardPresenter {
    // The view is available using the mView variable
    private final CompositeDisposable mDisposable = new CompositeDisposable();
    private FormEndpointInterface formAPI;
    private String token;
    private User user;

    @Inject
    public DashboardPresenterImpl(Context context) {
        this.formAPI = ServiceGenerator.createService(FormEndpointInterface.class);
        this.user = User.load(context);
        this.token = user.getToken();
    }

    @Override
    public void onStart(boolean viewCreated) {
        super.onStart(viewCreated);

        // Your code here. Your view is available using mView and will not be null until next onStop()
    }

    @Override
    public void onStop() {
        // Your code here, mView will be null after this method until next onStart()

        super.onStop();
        mDisposable.dispose();
        mDisposable.clear();
    }

    @Override
    public void onPresenterDestroyed() {
        /*
         * Your code here. After this method, your presenter (and view) will be completely destroyed
         * so make sure to cancel any HTTP call or database connection
         */

        super.onPresenterDestroyed();
    }

    @Override
    public void renderChart1(final int year) {
        if (mView != null) {
            mView.setChart1Title("Distribusi Analisis Penyebab CP dan PTKP Tahun " + year);
        }

        Observable<List<GetGrafikRootCouseResponse.Datum>> observable = formAPI.getGrafikRoot(TokenWrapper.getWrappedToken(token), "root_cause", year)
                .filter(new Predicate<GetGrafikRootCouseResponse>() {
                    @Override
                    public boolean test(@io.reactivex.annotations.NonNull GetGrafikRootCouseResponse getGrafikResponse) throws Exception {
                        return getGrafikResponse.getStatus().equals("success");
                    }
                })
                .map(new Function<GetGrafikRootCouseResponse, List<GetGrafikRootCouseResponse.Datum>>() {
                    @Override
                    public List<GetGrafikRootCouseResponse.Datum> apply(@io.reactivex.annotations.NonNull GetGrafikRootCouseResponse postTemuanListResponse) throws Exception {
                        return postTemuanListResponse.getData();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        mDisposable.add(
                observable.subscribeWith(new DisposableObserver<List<GetGrafikRootCouseResponse.Datum>>() {

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<GetGrafikRootCouseResponse.Datum> postTemuanListResponse) {
                        if (mView != null) {
                            if (postTemuanListResponse.size() != 0)
                                mView.renderChart1(postTemuanListResponse);
                            else
                                mView.clearChart1();
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable throwable) {
                        if (mView != null) {
                            mView.showError(throwable.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                })
        );

    }

    @Override
    public void renderChart2(int year) {
        if (mView != null) {
            mView.setChart2Title("Distribusi Masalah CP Tahun " + year);
        }

        Observable<List<GetGrafikResponse.Datum>> observable = formAPI.getGrafik(TokenWrapper.getWrappedToken(token), "cp", year)
                .filter(new Predicate<GetGrafikResponse>() {
                    @Override
                    public boolean test(@io.reactivex.annotations.NonNull GetGrafikResponse getGrafikResponse) throws Exception {
                        return getGrafikResponse.getStatus().equals("success");
                    }
                })
                .map(new Function<GetGrafikResponse, List<GetGrafikResponse.Datum>>() {
                    @Override
                    public List<GetGrafikResponse.Datum> apply(@io.reactivex.annotations.NonNull GetGrafikResponse postTemuanListResponse) throws Exception {
                        return postTemuanListResponse.getData();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        mDisposable.add(
                observable.subscribeWith(new DisposableObserver<List<GetGrafikResponse.Datum>>() {

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<GetGrafikResponse.Datum> postTemuanListResponse) {
                        if (mView != null) {
                            if (postTemuanListResponse.size() != 0)
                                mView.renderChart2(postTemuanListResponse);
                            else
                                mView.clearChart2();
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable throwable) {
                        if (mView != null) {
                            mView.showError(throwable.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                })
        );
    }

    @Override
    public void renderChart3(int year) {
        if (mView != null) {
            mView.setChart3Title("Distribusi Masalah PTKP Intern Tahun " + year);
        }

        Observable<List<GetGrafikResponse.Datum>> observable = formAPI.getGrafik(TokenWrapper.getWrappedToken(token), "ptkp", year)
                .filter(new Predicate<GetGrafikResponse>() {
                    @Override
                    public boolean test(@io.reactivex.annotations.NonNull GetGrafikResponse getGrafikResponse) throws Exception {
                        return getGrafikResponse.getStatus().equals("success");
                    }
                })
                .map(new Function<GetGrafikResponse, List<GetGrafikResponse.Datum>>() {
                    @Override
                    public List<GetGrafikResponse.Datum> apply(@io.reactivex.annotations.NonNull GetGrafikResponse postTemuanListResponse) throws Exception {
                        return postTemuanListResponse.getData();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        mDisposable.add(
                observable.subscribeWith(new DisposableObserver<List<GetGrafikResponse.Datum>>() {

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<GetGrafikResponse.Datum> postTemuanListResponse) {
                        if (mView != null) {
                            if (postTemuanListResponse.size() != 0)
                                mView.renderChart3(postTemuanListResponse);
                            else
                                mView.clearChart3();
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable throwable) {
                        if (mView != null) {
                            mView.showError(throwable.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                })
        );
    }

    @Override
    public void renderChart4(int year) {
        if (mView != null) {
            mView.setChart4Title("Distribusi Masalah PTKP Ekstern Tahun " + year);
        }

        Observable<List<GetGrafikResponse.Datum>> observable = formAPI.getGrafik(TokenWrapper.getWrappedToken(token), "ptkp_keluhan", year)
                .filter(new Predicate<GetGrafikResponse>() {
                    @Override
                    public boolean test(@io.reactivex.annotations.NonNull GetGrafikResponse getGrafikResponse) throws Exception {
                        return getGrafikResponse.getStatus().equals("success");
                    }
                })
                .map(new Function<GetGrafikResponse, List<GetGrafikResponse.Datum>>() {
                    @Override
                    public List<GetGrafikResponse.Datum> apply(@io.reactivex.annotations.NonNull GetGrafikResponse postTemuanListResponse) throws Exception {
                        return postTemuanListResponse.getData();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        mDisposable.add(
                observable.subscribeWith(new DisposableObserver<List<GetGrafikResponse.Datum>>() {

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<GetGrafikResponse.Datum> postTemuanListResponse) {
                        if (mView != null) {
                            if (postTemuanListResponse.size() != 0)
                                mView.renderChart4(postTemuanListResponse);
                            else
                                mView.clearChart4();
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable throwable) {
                        if (mView != null) {
                            mView.showError(throwable.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                })
        );
    }

}