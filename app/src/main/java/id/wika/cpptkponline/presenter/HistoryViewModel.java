package id.wika.cpptkponline.presenter;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataFormPelaporan;
import id.wika.cpptkponline.data.FormSummary;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.db.FormDao;
import id.wika.cpptkponline.data.db.FormPelaporanDao;
import id.wika.cpptkponline.data.endpoint.FormEndpointInterface;
import id.wika.cpptkponline.data.endpoint.TokenWrapper;
import id.wika.cpptkponline.data.endpoint.response.GetFormResponse;
import id.wika.cpptkponline.data.endpoint.response.PostTemuanListResponse;
import id.wika.cpptkponline.data.endpoint.response.RemoteFormSummary;
import id.wika.cpptkponline.data.filter.ResumeFilter;
import id.wika.cpptkponline.data.filter.ResumeFilterFactory;
import id.wika.cpptkponline.data.model.RemoteFormState;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FormConverter;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.view.home.list.FormListItem;
import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 19/09/17 - 23:30
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/
public class HistoryViewModel extends ViewModel {


    private FormDao formDao;
    private FormPelaporanDao formPelaporanDao;
    private FormEndpointInterface formAPI;
    private DateTimeHelper helper = new DateTimeHelper();

    private String token;

    public HistoryViewModel(FormDao formDao, FormPelaporanDao formPelaporanDao, FormEndpointInterface service, String token) {
        this.formDao = formDao;
        this.formPelaporanDao = formPelaporanDao;
        this.formAPI = service;
        this.token = token;
    }

    public Flowable<List<FormListItem>> getLocalFormListItems(User user) {

        String roleId = user.getRoleId();
        return formDao.getFormSummary().map(new Function<List<FormSummary>, List<FormListItem>>() {
            @Override
            public List<FormListItem> apply(List<FormSummary> formSummaries) throws Exception {
                return convertToFormListItems(formSummaries);
            }
        });
    }

    public Flowable syncDataFromRemote(final User user, String proyek) {

        return formAPI.getForm(TokenWrapper.getWrappedToken(token), proyek)
                .filter(new Predicate<GetFormResponse>() {
                    @Override
                    public boolean test(GetFormResponse getFormResponse) throws Exception {
                        return getFormResponse.getStatus().equals("success");
                    }
                })
                .map(new Function<GetFormResponse, List<RemoteFormSummary>>() {
                    @Override
                    public List<RemoteFormSummary> apply(GetFormResponse getFormResponse) throws Exception {
                        return getFormResponse.getData();
                    }
                })
                .doOnNext(new Consumer<List<RemoteFormSummary>>() {
                    @Override
                    public void accept(List<RemoteFormSummary> remoteFormSummaries) throws Exception {
                        Map<Integer, RemoteFormState> remoteFormStatesMap =
                                convertToMap(formDao.getSavedRemoteFormState().blockingFirst());

                        List<CPPTKPForm> newCpptkpForms = new ArrayList<>();

                        for (RemoteFormSummary remoteFormSummary : remoteFormSummaries) {
                            if (user.getRoleId().equals(remoteFormSummary.getAlertTo())) {
                                if (remoteFormStatesMap.containsKey(remoteFormSummary.getFormId())) {
                                    RemoteFormState oldRemoteFormState = remoteFormStatesMap.get(remoteFormSummary.getFormId());

                                    boolean isNewerStatus = FormHelper.isRemoteStatusNewer(
                                            oldRemoteFormState.getFormStatus(),
                                            remoteFormSummary.getStatus()
                                    );

                                    if (isNewerStatus) {
                                        formDao.updateFormStatus(
                                                oldRemoteFormState.getLocalFormId(),
                                                FormHelper.parseRemoteStatusToLocalStatus(remoteFormSummary.getStatus()),
                                                remoteFormSummary.getStatus());
                                    }
                                } else {
                                    newCpptkpForms.add(
                                            FormConverter.convertRemoteFormSummaryToCpPtkpForm(remoteFormSummary));
                                }
                            }
                        }

                        if (newCpptkpForms.size() > 0) {
                            for (CPPTKPForm newForm : newCpptkpForms) {
                                long id = formDao.insertForm(newForm);
                                DataFormPelaporan formPelaporan = newForm.getFormPelaporan();
                                formPelaporan.setRelatedFormId((int) id);
                                formPelaporanDao.insertForm(formPelaporan);
                            }
                        }
                    }

                    private Map<Integer, RemoteFormState> convertToMap(List<RemoteFormState> remoteFormStates) {
                        Map<Integer, RemoteFormState> map = new HashMap<>();
                        for (RemoteFormState state : remoteFormStates) {
                            map.put(state.getFormId(), state);
                        }
                        return map;
                    }
                });

    }

    @NonNull
    private List<FormListItem> convertToFormListItems(List<FormSummary> formSummaries) {
        List<FormListItem> listItems = new ArrayList<>();

        DateTime today = DateTime.now().withTime(0, 0, 0, 0);
        Set<Integer> integers = new HashSet<>();

        for (FormSummary formSummary : formSummaries) {
            DateTime formTs = formSummary.getTimestamp();
            int formDayOfMonth = formTs.getDayOfMonth();
            if (!integers.contains(formDayOfMonth)) {
                integers.add(formDayOfMonth);

                if (formDayOfMonth == today.getDayOfMonth()) {
                    listItems.add(new FormListItem(FormListItem.HEADER_ITEM_TYPE, "Hari Ini"));
                } else if (formDayOfMonth == today.getDayOfMonth() - 1) {
                    listItems.add(new FormListItem(FormListItem.HEADER_ITEM_TYPE, "Kemarin"));
                } else {
                    listItems.add(new FormListItem(FormListItem.HEADER_ITEM_TYPE, helper.printInIndonesian(formTs)));
                }
            }

            listItems.add(new FormListItem(FormListItem.RECORD_ITEM_TYPE, formSummary));
        }
        return listItems;
    }

    Observable<List<PostTemuanListResponse.Datum>> getHistory(String id) {
        return formAPI.postTemuanList(TokenWrapper.getWrappedToken(token), id)
                .map(new Function<PostTemuanListResponse, List<PostTemuanListResponse.Datum>>() {
                    @Override
                    public List<PostTemuanListResponse.Datum> apply(@io.reactivex.annotations.NonNull PostTemuanListResponse postTemuanListResponse) throws Exception {
                        List<PostTemuanListResponse.Datum> data = new ArrayList<>();
                        for (PostTemuanListResponse.Datum item : postTemuanListResponse.getData()) {
                            data.add(item);
                        }

                        return data;
                    }
                });
    }
}
