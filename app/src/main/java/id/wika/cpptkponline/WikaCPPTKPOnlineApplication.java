package id.wika.cpptkponline;

import android.app.Application;
import com.facebook.drawee.backends.pipeline.Fresco;
import net.danlew.android.joda.JodaTimeAndroid;
import id.wika.cpptkponline.data.db.AppDatabase;

/**
 * Created by mamang_kompii on 8/30/17.
 */

public class WikaCPPTKPOnlineApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
        Fresco.initialize(this);
        AppDatabase database = AppDatabase.getInstance(this);
    }
}
