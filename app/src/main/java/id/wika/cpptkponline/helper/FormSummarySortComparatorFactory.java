package id.wika.cpptkponline.helper;

import java.util.Comparator;

import id.wika.cpptkponline.data.FormSummary;

/**
 * Created by mamang_kompii on 20/10/17.
 */

public class FormSummarySortComparatorFactory {

    public enum SORT_TYPE {
        DATE_ASC,
        DATE_DESC,
        FORM_ID_ASC,
        FORM_ID_DESC,
        REMOTE_FORM_ID_ASC,
        REMOTE_FORM_ID_DESC
    }

    public static Comparator<FormSummary> createComparator(SORT_TYPE sort_type) {
        switch (sort_type) {
            case DATE_ASC:
                return new DateAscendingComparator();
            case DATE_DESC:
                return new DateDescendingComparator();
            case FORM_ID_ASC:
                return new FormIDAscendingComparator();
            case FORM_ID_DESC:
                return new FormIDDescendingComparator();
            case REMOTE_FORM_ID_ASC:
                return new RemoteFormIDAscendingComparator();
            case REMOTE_FORM_ID_DESC:
                return new RemoteFormIDDescendingComparator();
            default:
                return new DateDescendingComparator();
        }
    }

    private static class DateAscendingComparator implements Comparator<FormSummary> {
        @Override
        public int compare(FormSummary summary1, FormSummary summary2) {
            return summary1.getTimestamp().compareTo(summary2.getTimestamp());
        }
    }

    private static class DateDescendingComparator implements Comparator<FormSummary> {
        @Override
        public int compare(FormSummary summary1, FormSummary summary2) {
            return summary2.getTimestamp().compareTo(summary1.getTimestamp());
        }
    }

    private static class FormIDAscendingComparator implements Comparator<FormSummary> {
        @Override
        public int compare(FormSummary summary1, FormSummary summary2) {
            return getNomorFormOrEmptyString(summary1)
                    .compareTo(getNomorFormOrEmptyString(summary2));
        }
    }

    private static class FormIDDescendingComparator implements Comparator<FormSummary> {
        @Override
        public int compare(FormSummary summary1, FormSummary summary2) {
            return getNomorFormOrEmptyString(summary2)
                    .compareTo(getNomorFormOrEmptyString(summary1));
        }
    }

    private static String getNomorFormOrEmptyString(FormSummary summary) {
        String noTemuan = summary.getNoTemuan();
        if (StringHelper.isNotBlank(noTemuan)) {
            return noTemuan;
        } else {
            return "";
        }
    }

    private static class RemoteFormIDAscendingComparator implements Comparator<FormSummary> {
        @Override
        public int compare(FormSummary summary1, FormSummary summary2) {
            return summary1.getRemoteFormId() - summary2.getRemoteFormId();
        }
    }

    private static class RemoteFormIDDescendingComparator implements Comparator<FormSummary> {
        @Override
        public int compare(FormSummary summary1, FormSummary summary2) {
            return summary2.getRemoteFormId() - summary1.getRemoteFormId();
        }
    }
}
