package id.wika.cpptkponline.helper;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.Locale;

public class DateTimeHelper {

    private DateTimeFormatter dateTimeFormatter;
    private DateTimeFormatter dateTimePrinter;
    private DateTimeFormatter indonesianDateTimePrinter;

    public DateTimeHelper() {
        dateTimeFormatter = ISODateTimeFormat.dateTimeParser();
        dateTimePrinter = ISODateTimeFormat.dateTime();
        indonesianDateTimePrinter = DateTimeFormat
                .forPattern("EEEE, dd MMMM yyyy").withLocale(new Locale("in"));
    }

    public DateTime parseDateTime(String text) {
        return dateTimeFormatter.parseDateTime(text);
    }

    public String print(DateTime dateTime) {
        return dateTimePrinter.print(dateTime);
    }

    public String printInIndonesian(DateTime dateTime) {
        return indonesianDateTimePrinter.print(dateTime);
    }
}
