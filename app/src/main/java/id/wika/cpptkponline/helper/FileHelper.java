package id.wika.cpptkponline.helper;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mamang_kompii on 8/30/17.
 */

public class FileHelper {

    private static final String PRIMARY_FILE_TYPE = "primary";
    private static final String EXTERNAL_PUBLIC_DOWNLOAD_CONTENT = "content://downloads/public_downloads";
    private static final String CONTENT_SCHEME_NAME = "content";
    private static final String FILE_SCHEME_NAME = "file";
    private static final String IMAGE_FILE_TYPE = "image";
    private static final String VIDEO_FILE_TYPE = "video";
    private static final String AUDIO_FILE_TYPE = "audio";
    private static final String EXTERNAL_STORAGE_DOCUMENT = "com.android.externalstorage.documents";
    private static final String EXTERNAL_DOWNLOAD_DOCUMENT = "com.android.providers.downloads.documents";
    private static final String EXTERNAL_MEDIA_DOCUMENT = "com.android.providers.media.documents";
    private static final String EXTERNAL_GOOGLE_PHOTO_DOCUMENT = "com.google.android.apps.photos.content";

    public static File createImageFile(Context context) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if (PRIMARY_FILE_TYPE.equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            } else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse(EXTERNAL_PUBLIC_DOWNLOAD_CONTENT), Long.valueOf(id));

                return getFilePathFromCursor(context, contentUri, null, null);
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if (IMAGE_FILE_TYPE.equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if (VIDEO_FILE_TYPE.equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if (AUDIO_FILE_TYPE.equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = MediaStore.Images.Media._ID + "=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getFilePathFromCursor(context, contentUri, selection, selectionArgs);
            }
        } else if (CONTENT_SCHEME_NAME.equalsIgnoreCase(uri.getScheme())) {

            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getFilePathFromCursor(context, uri, null, null);
        } else if (FILE_SCHEME_NAME.equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private static boolean isExternalStorageDocument(Uri uri) {
        return EXTERNAL_STORAGE_DOCUMENT.equals(uri.getAuthority());
    }

    private static boolean isDownloadsDocument(Uri uri) {
        return EXTERNAL_DOWNLOAD_DOCUMENT.equals(uri.getAuthority());
    }

    private static boolean isMediaDocument(Uri uri) {
        return EXTERNAL_MEDIA_DOCUMENT.equals(uri.getAuthority());
    }

    private static boolean isGooglePhotosUri(Uri uri) {
        return EXTERNAL_GOOGLE_PHOTO_DOCUMENT.equals(uri.getAuthority());
    }

    private static String getFilePathFromCursor(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = MediaStore.Images.Media.DATA;

        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.getParentFile().exists())
            destFile.getParentFile().mkdirs();

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }
}
