package id.wika.cpptkponline.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.facebook.common.util.UriUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.joda.time.DateTime;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.FormSummary;
import id.wika.cpptkponline.data.endpoint.RemoteFormStatus;
import id.wika.cpptkponline.view.attachment.EditPhotoActivity;
import id.wika.cpptkponline.view.attachment.ViewPhotoActivity;
import id.wika.cpptkponline.view.home.list.FormListItem;
import id.wika.cpptkponline.view.review.ReviewFormActivity;

/**
 * Created by mamang_kompii on 8/29/17.
 */

public class FormHelper {

    public static final String EXTRA_KEY_FORM_ID = "formIdExtra";
    public static final String EXTRA_KEY_REMOTE_FORM_ID = "remoteFormIdExtra";
    public static final String EXTRA_KEY_FORM_DATA = "formDataExtra";
    public static final String EXTRA_KEY_FORM_DATA2 = "formDataExtra2";
    public static final String EXTRA_KEY_FORM_STATUS = "formStatus";
    public static final String EXTRA_KEY_IS_REVIEW_WITH_HISTORY = "isReviewWithHistory";
    public static final String EXTRA_KEY_REVIEW_WITH_REMOTE_DATA = "isReviewWithRemoteData";

    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final int REQUEST_EDIT_IMAGE = 2;
    public static final int REQUEST_VIEW_IMAGE = 3;
    public static final int REQUEST_SELECT_PICTURE_FROM_GALLERY = 4;

    private static DateTimeHelper helper = new DateTimeHelper();

    public static int getFormStatusIcon(int formStatus) {
        switch (formStatus) {
            case FormStatus.FORM_AWAL:
                return R.drawable.form_status_1;
            case FormStatus.FORM_PENGECEKAN:
                return R.drawable.form_status_2;
            case FormStatus.FORM_RENCANA_TINDAK_LANJUT:
                return R.drawable.form_status_2_3;
            case FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI:
                return R.drawable.form_status_3;
            case FormStatus.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN:
                return R.drawable.form_status_4;
            case FormStatus.FORM_REALISASI_TINDAK_LANJUT:
            case FormStatus.FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT:
                return R.drawable.form_status_5;
            case FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN:
                return R.drawable.form_status_6;
            case FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED:
                return R.drawable.form_status_7;
            default:
                return 0;
        }
    }

    public static String getFormCategoryString(String formCategory) {
        switch (formCategory) {
            case CPPTKPForm.FORM_CP:
                return "CP";
            case CPPTKPForm.FORM_PTKP:
                return "PTKP";
            case CPPTKPForm.FORM_PTKP_KELUHAN:
                return "PTKP Keluhan Pelanggan";
            default:
                return "";
        }
    }

    public static String parseKategoriToConstant(String kategori) {
        switch (kategori) {
            case "CP":
                return CPPTKPForm.FORM_CP;
            case "PTKP":
                return CPPTKPForm.FORM_PTKP;
            case "PTKP Keluhan Pelanggan":
                return CPPTKPForm.FORM_PTKP_KELUHAN;
            default:
                return "";
        }
    }

    public static boolean isFormFieldContentValid(TextInputLayout formField) {
        String content = formField.getEditText().getText().toString();

        return StringHelper.isNotBlank(content);
    }

    public static String getTextFromField(TextInputLayout textField) {
        return textField.getEditText().getText().toString();
    }

    public static DateTime getDateTimeFromField(TextInputLayout textField) {
        String datetime = textField.getEditText().toString();
        return DateTime.parse(datetime);
    }

    public static int parseRemoteStatusToLocalStatus(String remoteStatus) {
        switch (remoteStatus) {
            case RemoteFormStatus.DRAFT:
                return FormStatus.FORM_AWAL;
            case RemoteFormStatus.PENGECEKAN:
                return FormStatus.FORM_PENGECEKAN;
            case RemoteFormStatus.RENCANA_TINDAK_LANJUT:
                return FormStatus.FORM_RENCANA_TINDAK_LANJUT;
            case RemoteFormStatus.APPROVAL:
                return FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI;
            case RemoteFormStatus.ARSIP:
                return FormStatus.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN;
            case RemoteFormStatus.REALISASI_TINDAK_LANJUT:
                return FormStatus.FORM_REALISASI_TINDAK_LANJUT;
            case RemoteFormStatus.STATUS_CLOSED:
                return FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED;
            case RemoteFormStatus.STATUS_OPEN:
                return FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN;
            default:
                return -1;
        }
    }

    public static boolean isRemoteStatusNewer(String localStatus, String remoteStatus) {
        return parseRemoteStatusToLocalStatus(localStatus) < parseRemoteStatusToLocalStatus(remoteStatus);
    }

    public static SimpleDraweeView getImageThumbnailViewerDrawee(Fragment fragment, int thumbnailSize, final String path) {
        return getImageThumbnailViewerDrawee(fragment, thumbnailSize, path, false);
    }

    public static SimpleDraweeView getImageThumbnailViewerDrawee(
            final Fragment fragment, int thumbnailSize, final String path, final boolean isDeletable) {
        Context context = fragment.getContext();
        float radius = context.getResources().getDimensionPixelSize(R.dimen.general_rounded_rect_radius);

        SimpleDraweeView simpleDraweeView = new SimpleDraweeView(context);
        Uri photoURI = Uri.parse(path);
        if (!UriUtil.isNetworkUri(photoURI)) {
            photoURI = FileProvider.getUriForFile(context,
                    "id.wika.cppptkponline.fileprovider",
                    new File(path));
        }
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(photoURI)
                .setResizeOptions(new ResizeOptions(thumbnailSize, thumbnailSize))
                .build();
        simpleDraweeView.setController(
                Fresco.newDraweeControllerBuilder()
                        .setOldController(simpleDraweeView.getController())
                        .setImageRequest(request)
                        .build());
        simpleDraweeView.getHierarchy().setPlaceholderImage(R.color.softBlue);
        simpleDraweeView.getHierarchy().setRoundingParams(RoundingParams.fromCornersRadius(radius));
        simpleDraweeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ViewPhotoActivity.class);
                intent.putExtra(ImageHelper.EXTRA_KEY_IMAGE_FILE_PATH, path);
                if (isDeletable) {
                    intent.putExtra(ViewPhotoActivity.EXTRA_KEY_CAN_DELETE, true);
                }
                fragment.startActivityForResult(intent, FormHelper.REQUEST_VIEW_IMAGE);
            }
        });
        return simpleDraweeView;
    }

    public static View getCheckableImageThumbnail(
            final FragmentActivity activity, int thumbnailSize, final String path, final Set<String> pathSet) {
        View view = LayoutInflater.from(activity).inflate(R.layout.thumbnail_with_checkbox, null);

        SimpleDraweeView simpleDraweeView = view.findViewById(R.id.thumbnail);
        Uri photoURI = Uri.parse(path);
        if (!UriUtil.isNetworkUri(photoURI)) {
            photoURI = FileProvider.getUriForFile(activity,
                    "id.wika.cppptkponline.fileprovider",
                    new File(path));
        }
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(photoURI)
                .setResizeOptions(new ResizeOptions(thumbnailSize, thumbnailSize))
                .build();
        simpleDraweeView.setController(
                Fresco.newDraweeControllerBuilder()
                        .setOldController(simpleDraweeView.getController())
                        .setImageRequest(request)
                        .build());
        simpleDraweeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ViewPhotoActivity.class);
                intent.putExtra(ImageHelper.EXTRA_KEY_IMAGE_FILE_PATH, path);
                activity.startActivityForResult(intent, FormHelper.REQUEST_VIEW_IMAGE);
            }
        });

        CheckBox checkBox = view.findViewById(R.id.checkbox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    pathSet.add(path);
                } else {
                    pathSet.remove(path);
                }
            }
        });

        return view;
    }

    public static void launchReviewFormActivityWithNonHistoricalLocalData(
            Activity activity,
            int formId,
            int formStatus) {
        launchReviewFormActivity(activity, formId, formStatus, false, false);
    }

    public static void launchReviewFormActivity(
            Activity activity,
            int formId,
            int formStatus,
            boolean isReviewWithHistory,
            boolean isUsingRemoteData) {

        Intent intent = new Intent(activity, ReviewFormActivity.class);
        intent.putExtra(FormHelper.EXTRA_KEY_FORM_ID, formId);
        intent.putExtra(FormHelper.EXTRA_KEY_FORM_STATUS, formStatus);
        intent.putExtra(FormHelper.EXTRA_KEY_IS_REVIEW_WITH_HISTORY, isReviewWithHistory);
        intent.putExtra(FormHelper.EXTRA_KEY_REVIEW_WITH_REMOTE_DATA, isUsingRemoteData);
        activity.startActivity(intent);
    }

    public static void launchEditPhotoActivity(Fragment fragment, String photoPath) {
        Intent intent = new Intent(fragment.getActivity(), EditPhotoActivity.class);
        intent.putExtra(ImageHelper.EXTRA_KEY_IMAGE_FILE_PATH, photoPath);
        fragment.startActivityForResult(intent, FormHelper.REQUEST_EDIT_IMAGE);
    }

    public static List<FormListItem> convertToFormListItems(
            List<FormSummary> formSummaries,
            FormSummarySortComparatorFactory.SORT_TYPE sortType) {

        switch (sortType) {
            case DATE_ASC:
            case DATE_DESC:
                return convertToDateDividedFormListItems(formSummaries);
            case FORM_ID_ASC:
            case FORM_ID_DESC:
            default:
                return convertToNonDividedFormListItems(formSummaries);
        }
    }

    private static List<FormListItem> convertToDateDividedFormListItems(List<FormSummary> formSummaries) {
        List<FormListItem> listItems = new ArrayList<>();
        DateTime today = DateTime.now().withTime(0, 0, 0, 0);
        Set<Integer> integers = new HashSet<>();

        for (FormSummary formSummary : formSummaries) {
            DateTime formTs = formSummary.getTimestamp();
            int formDayOfMonth = formTs.getDayOfMonth();
            if (!integers.contains(formDayOfMonth)) {
                integers.add(formDayOfMonth);

                if (formDayOfMonth == today.getDayOfMonth()) {
                    listItems.add(new FormListItem(FormListItem.HEADER_ITEM_TYPE, "Hari Ini"));
                } else if (formDayOfMonth == today.getDayOfMonth() - 1) {
                    listItems.add(new FormListItem(FormListItem.HEADER_ITEM_TYPE, "Kemarin"));
                } else {
                    listItems.add(new FormListItem(FormListItem.HEADER_ITEM_TYPE, helper.printInIndonesian(formTs)));
                }
            }

            listItems.add(new FormListItem(FormListItem.RECORD_ITEM_TYPE, formSummary));
        }
        return listItems;
    }

    private static List<FormListItem> convertToNonDividedFormListItems(List<FormSummary> formSummaries) {
        List<FormListItem> listItems = new ArrayList<>();
        for (FormSummary formSummary : formSummaries) {
            listItems.add(new FormListItem(FormListItem.RECORD_ITEM_TYPE, formSummary));
        }
        return listItems;
    }
}
