package id.wika.cpptkponline.helper;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataApproval;
import id.wika.cpptkponline.data.DataFormPelaporan;
import id.wika.cpptkponline.data.DataRealisasiTindakLanjut;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import id.wika.cpptkponline.data.DataVerifikasiHasilTindakLanjut;
import id.wika.cpptkponline.data.FormDataCatalogue;
import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.endpoint.response.GetFormByIdResponse;
import id.wika.cpptkponline.data.endpoint.response.PictureResponse;
import id.wika.cpptkponline.data.endpoint.response.RemoteFormSummary;

/**
 * Created by mamang_kompii on 9/11/17.
 */

public class FormConverter {

    public static CPPTKPForm convertRemoteFormSummaryToCpPtkpForm(RemoteFormSummary remoteFormSummary) {
        CPPTKPForm newForm = new CPPTKPForm();
        newForm.setFormStatus(FormHelper.parseRemoteStatusToLocalStatus(remoteFormSummary.getStatus()));
        newForm.setTimestamp(remoteFormSummary.getTglTemuan());
        newForm.setNeedResponse(true);
        newForm.setRemoteFormId(remoteFormSummary.getFormId());
        newForm.setRemoteFormStatus(remoteFormSummary.getStatus());

        DataFormPelaporan dataPelaporan = new DataFormPelaporan();
        User penemu = new User();
        penemu.setFullname(remoteFormSummary.getNamaPenemu());
        penemu.setDivisi(remoteFormSummary.getDivisi());
        dataPelaporan.setPenemu(penemu);
        dataPelaporan.setNoTemuan(remoteFormSummary.getNoTemuan());
        dataPelaporan.setNamaProyek(remoteFormSummary.getNamaProyek());
        dataPelaporan.setFormDate(remoteFormSummary.getTglTemuan());
        dataPelaporan.setLokasiPenyimpangan(remoteFormSummary.getLokasiPenyimpangan());
        dataPelaporan.setBentukPenyimpangan(remoteFormSummary.getBentukPenyimpangan());

        ArrayList<String> attachments = new ArrayList<>();
        if (StringHelper.isNotBlank(remoteFormSummary.getPic1())) {
            attachments.add(ServiceGenerator.getRemoteUrl(remoteFormSummary.getPic1()));
        }

        if (StringHelper.isNotBlank(remoteFormSummary.getPic2())) {
            attachments.add(ServiceGenerator.getRemoteUrl(remoteFormSummary.getPic2()));
        }

        if (StringHelper.isNotBlank(remoteFormSummary.getPic3())) {
            attachments.add(ServiceGenerator.getRemoteUrl(remoteFormSummary.getPic3()));
        }

        dataPelaporan.setAttachmentPaths(attachments);
        newForm.setFormPelaporan(dataPelaporan);
        return newForm;
    }

    public static CPPTKPForm convertResponseDataToCpPtkpForm(GetFormByIdResponse.Data data) {
        CPPTKPForm cpptkpForm = new CPPTKPForm();
        cpptkpForm.setFormStatus(FormHelper.parseRemoteStatusToLocalStatus(data.status));
        cpptkpForm.setTimestamp(DateTime.now());
        cpptkpForm.setNeedResponse(true);
        cpptkpForm.setRemoteFormId(data.id);
        cpptkpForm.setRemoteFormStatus(data.status);
        return cpptkpForm;
    }

    @NonNull
    public static DataFormPelaporan convertResponseDataToFormPelaporan(GetFormByIdResponse.Data data, int localFormId) {
        DataFormPelaporan dataFormPelaporan = new DataFormPelaporan();
        dataFormPelaporan.setRelatedFormId(localFormId);

        User penemu = new User();
        penemu.setFullname(data.namaPenemu);
        penemu.setDivisi(data.divisiPenemu);
        dataFormPelaporan.setPenemu(penemu);
        dataFormPelaporan.setNamaProyek(data.namaProyek);
        dataFormPelaporan.setFormDate(data.tglTemuan);
        dataFormPelaporan.setLokasiPenyimpangan(data.lokasiPenyimpangan);
        dataFormPelaporan.setBentukPenyimpangan(data.bentukPenyimpangan);
        dataFormPelaporan.setKategoriForm(data.kategoriForm);
        dataFormPelaporan.setLingkupTemuan(data.lingkupTemuan);
        dataFormPelaporan.setDetailLingkupTemuan(data.struktur);
        dataFormPelaporan.setNoTemuan(data.noTemuan);

        if (isAttachmentDataAvailable(data)) {
            dataFormPelaporan.setAttachmentPaths(getAttachmentsByType(data, PictureResponse.ATTACHMENT_TYPE_DRAFT));
        }

        return dataFormPelaporan;
    }

    public static DataRencanaTindakLanjut convertResponseDataToRencanaTindakLanjut(GetFormByIdResponse.Data data, int localFormId) {
        DataRencanaTindakLanjut rencanaTindakLanjut = new DataRencanaTindakLanjut();
        rencanaTindakLanjut.setRelatedFormId(localFormId);

        User penanggungJawab = new User();
        penanggungJawab.setFullname(data.namaPenanggungJawab);
        penanggungJawab.setDivisi(data.divisiPj);
        penanggungJawab.setJabatan(data.jabatan);
        rencanaTindakLanjut.setPenanggungJawab(penanggungJawab);
        rencanaTindakLanjut.setPenyebabUtamaPenyimpangan(data.penyebabUtamaPenyimpangan);
        rencanaTindakLanjut.setKeputusanJangkaPendek(data.keputusanJangkaPendek);

        String detailKeputusanJangkaPendek = data.detailKeputusanJangkaPendek;
        if (StringHelper.isNotBlank(detailKeputusanJangkaPendek)) {
            rencanaTindakLanjut.setTitleDetailKeputusanJangkaPendek(
                    FormDataCatalogue.getDetailKeputusanJangkaPendekTitle(data.keputusanJangkaPendek));
            rencanaTindakLanjut.setDetailKeputusanJangkaPendek(detailKeputusanJangkaPendek);
        }

        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPendek(data.targetWaktuJangkaPendek);
        rencanaTindakLanjut.setKeputusanJangkaPanjang(data.keputusanJangkaPanjang);
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPanjang(data.targetWaktuJangkaPanjang);

        return rencanaTindakLanjut;
    }

    static boolean isApproved(int status) {
        if (status == 1) return true;
        else return false;
    }

    public static DataApproval convertResponseDataToApproval(GetFormByIdResponse.Data data, int localFormId) {
        User penanggungJawab = new User();
        penanggungJawab.setFullname(data.pembuatKeputusan);
        penanggungJawab.setDivisi(data.divisiPembuatKeputusan);
        penanggungJawab.setJabatan("Manager");
        DataApproval approval = new DataApproval(penanggungJawab, data.tglKeputusan, isApproved(data.disetujui));
        approval.setRelatedFormId(localFormId);

        if (isAttachmentDataAvailable(data)) {
            approval.setApprovedAttachmentPaths(
                    getAttachmentsByType(data, PictureResponse.ATTACHMENT_TYPE_APPROVAL));
        }
        return approval;
    }

    public static DataRealisasiTindakLanjut convertResponseDataToRealisasiTindakLanjut(GetFormByIdResponse.Data data, int localFormId) {
        DataRealisasiTindakLanjut realisasiTindakLanjutTindakLanjut = new DataRealisasiTindakLanjut();
        realisasiTindakLanjutTindakLanjut.setRelatedFormId(localFormId);
        realisasiTindakLanjutTindakLanjut.setHasilPencegahanDanPerbaikan(data.hasilPencegahanDanPerbaikan);

        if (isAttachmentDataAvailable(data)) {
            realisasiTindakLanjutTindakLanjut.setListDokumentasiHasil(
                    getAttachmentsByType(data, PictureResponse.ATTACHMENT_TYPE_HASIL)
            );
        }

        return realisasiTindakLanjutTindakLanjut;
    }

    public static DataVerifikasiHasilTindakLanjut convertResponseDataToVerifikasiTindakLanjut(GetFormByIdResponse.Data data, int localFormId) {

        DataVerifikasiHasilTindakLanjut verifikasiHasilTindakLanjut = new DataVerifikasiHasilTindakLanjut();
        verifikasiHasilTindakLanjut.setRelatedFormId(localFormId);
        User verifikator = new User();
        verifikator.setFullname(data.namaVerifikator);
        verifikasiHasilTindakLanjut.setProgres(data.progres);

        verifikator.setDivisi("QA");
        verifikasiHasilTindakLanjut.setVerifikator(verifikator);
        verifikasiHasilTindakLanjut.setStatusOpened(!isApproved(data.isclosed));
        if (data.rootCauseCartegory == "feedback") {
            verifikasiHasilTindakLanjut.setRootCauseCategory(data.rootCauseCartegory);
            verifikasiHasilTindakLanjut.setFeedback(data.rootCause);
        } else {
            verifikasiHasilTindakLanjut.setRootCauseCategory(data.rootCauseCartegory);
            verifikasiHasilTindakLanjut.setRootCauseValue(data.rootCause);
        }

        if (isAttachmentDataAvailable(data)) {
            verifikasiHasilTindakLanjut.setFeedbackAttachments(
                    getAttachmentsByType(data, PictureResponse.ATTACHMENT_TYPE_FEEDBACK));
        }

        return verifikasiHasilTindakLanjut;
    }

    private static boolean isAttachmentDataAvailable(GetFormByIdResponse.Data data) {
        return data.picture != null && data.picture.size() > 0;
    }

    private static ArrayList<String> getAttachmentsByType(GetFormByIdResponse.Data data, String type) {
        ArrayList<String> attachments = new ArrayList<>();

        PictureResponse pictureResponse = getPictureByType(data.picture, type);

        if (StringHelper.isNotBlank(pictureResponse.getPathPic1())) {
            attachments.add(ServiceGenerator.getRemoteUrl(pictureResponse.getPathPic1()));
        }

        if (StringHelper.isNotBlank(pictureResponse.getPathPic2())) {
            attachments.add(ServiceGenerator.getRemoteUrl(pictureResponse.getPathPic2()));
        }

        if (StringHelper.isNotBlank(pictureResponse.getPathPic3())) {
            attachments.add(ServiceGenerator.getRemoteUrl(pictureResponse.getPathPic3()));
        }

        return attachments;
    }

    private static PictureResponse getPictureByType(List<PictureResponse> pictureResponses, String attachmentTypeDraft) {
        for (PictureResponse pictureResponse : pictureResponses) {
            if (attachmentTypeDraft.equals(pictureResponse.getJenis())) {
                return pictureResponse;
            }
        }

        return new PictureResponse();
    }

}
