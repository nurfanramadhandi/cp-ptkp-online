package id.wika.cpptkponline.helper;

import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.UserRole;

/**
 * Created by mamang_kompii on 9/3/17.
 */

public class UserHelper {

    public static final String EXTRA_KEY_USERNAME = "login_username";
    public static final String EXTRA_KEY_FULLNAME = "fullname";
    public static final String EXTRA_KEY_JABATAN = "jabatan";
    public static final String EXTRA_KEY_ROLE_ID = "role_id";

    public static String getUserRoleDescription(User user) {
        return UserRole.getRoleDescription(user.getRoleId());
    }

    public static boolean isQARole(User user) {
        return UserRole.ROLE_QA.equals(user.getRoleId());
    }
}
