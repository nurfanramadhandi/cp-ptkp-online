package id.wika.cpptkponline.helper;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.SparseArray;
import android.util.SparseIntArray;

import io.reactivex.Observer;
import io.reactivex.subjects.AsyncSubject;

public class PermissionHelper {
    public static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 14;

    private SparseArray<AsyncSubject<Boolean>> permissionMap = new SparseArray<>();
    private SparseIntArray permissionCountMap = new SparseIntArray();

    public void requestPermissionFromFragment(Fragment fragment, String[] permissions, int requestCode, Observer<Boolean> observer) {
        if (Build.VERSION.SDK_INT >= 23) {

            if (needRequestPermissions(fragment.getActivity(), permissions)) {
                AsyncSubject<Boolean> permissionObservable = permissionMap.get(requestCode);

                if (permissionObservable != null) {
                    permissionObservable.onComplete();
                }

                permissionObservable = AsyncSubject.create();
                permissionMap.put(requestCode, permissionObservable);
                permissionCountMap.put(requestCode, permissions.length);
                permissionObservable.subscribe(observer);

                fragment.requestPermissions(permissions, requestCode);

            } else {
                observer.onNext(true);
                observer.onComplete();
            }
        } else {
            observer.onNext(true);
            observer.onComplete();
        }
    }

    public boolean requestPermission(Activity activity, String[] permissions, int requestCode, Observer<Boolean> observer) {
        if (Build.VERSION.SDK_INT >= 23) {

            if (needRequestPermissions(activity, permissions)) {
                AsyncSubject<Boolean> permissionObservable = permissionMap.get(requestCode);

                if (permissionObservable != null) {
                    permissionObservable.onComplete();
                }

                permissionObservable = AsyncSubject.create();
                permissionMap.put(requestCode, permissionObservable);
                permissionCountMap.put(requestCode, permissions.length);
                permissionObservable.subscribe(observer);

                ActivityCompat.requestPermissions(activity,
                        permissions,
                        requestCode);

                return false;
            } else {
                observer.onNext(true);
                observer.onComplete();
                return true;
            }
        } else {
            observer.onNext(true);
            observer.onComplete();
            return true;
        }
    }

    private boolean needRequestPermissions(Activity activity, String[] permissions) {
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                return true;
            }
        }
        return false;
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        AsyncSubject<Boolean> permissionObservable = permissionMap.get(requestCode);

        if (isAllPermissionsGranted(requestCode, grantResults)) {
            permissionObservable.onNext(true);
            permissionObservable.onComplete();
        } else {
            permissionObservable.onNext(false);
            permissionObservable.onComplete();
        }

        removePermissionFromMap(requestCode);
    }

    private boolean isAllPermissionsGranted(int requestCode, int[] grantResults) {
        int permissionRequestCount = permissionCountMap.get(requestCode);

        if (grantResults.length == permissionRequestCount) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private void removePermissionFromMap(int requestCode) {
        permissionMap.delete(requestCode);
        permissionCountMap.delete(requestCode);
    }
}
