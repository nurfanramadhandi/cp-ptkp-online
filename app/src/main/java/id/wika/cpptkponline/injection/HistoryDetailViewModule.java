package id.wika.cpptkponline.injection;

import android.support.annotation.NonNull;


import dagger.Module;
import dagger.Provides;
import id.wika.cpptkponline.presenter.HistoryDetailPresenter;
import id.wika.cpptkponline.presenter.PresenterFactory;
import id.wika.cpptkponline.presenter.impl.HistoryDetailPresenterImpl;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:54:07 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil
 * All Rights Reserved. 
 ******************************************************************************/


@Module
public final class HistoryDetailViewModule {
    public HistoryDetailViewModule() {
    }

    @Provides
    public PresenterFactory<HistoryDetailPresenter> providePresenterFactory(@NonNull final HistoryDetailPresenterImpl presenter) {
        return new PresenterFactory<HistoryDetailPresenter>() {
            @NonNull
            @Override
            public HistoryDetailPresenter create() {
                return presenter;
            }
        };
    }
//    @Provides
//    public HistoryDetailInteractor provideInteractor()
//    {
//        return new HistoryDetailInteractorImpl();
//    }

//    @Provides
//    public PresenterFactory<HistoryDetailPresenter> providePresenterFactory(@NonNull final HistoryDetailInteractor interactor)
//    {
//        return new PresenterFactory<HistoryDetailPresenter>()
//        {
//            @NonNull
//            @Override
//            public HistoryDetailPresenter create()
//            {
//                return new HistoryDetailPresenterImpl(interactor);
//            }
//        };
//    }
}
