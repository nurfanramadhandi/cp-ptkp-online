package id.wika.cpptkponline.injection;

import android.support.annotation.NonNull;


import dagger.Module;
import dagger.Provides;
import id.wika.cpptkponline.presenter.HistoryPresenter;
import id.wika.cpptkponline.presenter.PresenterFactory;
import id.wika.cpptkponline.presenter.impl.HistoryPresenterImpl;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:51:54 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/


@Module
public final class HistoryViewModule {
    public HistoryViewModule() {
    }

    @Provides
    public PresenterFactory<HistoryPresenter> providePresenterFactory(@NonNull final HistoryPresenterImpl presenter) {
        return new PresenterFactory<HistoryPresenter>() {
            @NonNull
            @Override
            public HistoryPresenter create() {
                return presenter;
            }
        };
    }
//    @Provides
//    public HistoryInteractor provideInteractor()
//    {
//        return new HistoryInteractorImpl();
//    }

//    @Provides
//    public PresenterFactory<HistoryPresenter> providePresenterFactory(@NonNull final HistoryInteractor interactor)
//    {
//        return new PresenterFactory<HistoryPresenter>()
//        {
//            @NonNull
//            @Override
//            public HistoryPresenter create()
//            {
//                return new HistoryPresenterImpl(interactor);
//            }
//        };
//    }
}
