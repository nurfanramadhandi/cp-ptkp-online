package id.wika.cpptkponline.injection;

import android.content.Context;
import android.support.annotation.NonNull;

import id.wika.cpptkponline.App;

import dagger.Module;
import dagger.Provides;


/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:34:19 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

@Module
public final class AppModule {
    @NonNull
    private final App mApp;

    public AppModule(@NonNull App app) {
        mApp = app;
    }

    @Provides
    public Context provideAppContext() {
        return mApp;
    }

    @Provides
    public App provideApp() {
        return mApp;
    }
}
