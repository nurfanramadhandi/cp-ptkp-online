package id.wika.cpptkponline.injection;

import android.content.Context;

import id.wika.cpptkponline.App;

import javax.inject.Singleton;

import dagger.Component;


/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:34:19 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    Context getAppContext();

    App getApp();
}