package id.wika.cpptkponline.injection;

import id.wika.cpptkponline.view.laporan.LaporanActivity;

import dagger.Component;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 28, 2017 8:33:21 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

@ActivityScope
@Component(dependencies = AppComponent.class, modules = LaporanViewModule.class)
public interface LaporanViewComponent {
    void inject(LaporanActivity activity);
}