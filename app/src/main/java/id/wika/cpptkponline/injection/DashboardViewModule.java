package id.wika.cpptkponline.injection;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import id.wika.cpptkponline.presenter.DashboardPresenter;
import id.wika.cpptkponline.presenter.PresenterFactory;
import id.wika.cpptkponline.presenter.impl.DashboardPresenterImpl;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 2:39:34 PM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

@Module
public final class DashboardViewModule {
    public DashboardViewModule() {
    }

    @Provides
    public PresenterFactory<DashboardPresenter> providePresenterFactory(@NonNull final DashboardPresenterImpl presenter) {
        return new PresenterFactory<DashboardPresenter>() {
            @NonNull
            @Override
            public DashboardPresenter create() {
                return presenter;
            }
        };
    }
}
