package id.wika.cpptkponline.injection;

import id.wika.cpptkponline.view.dashboard.DashboardActivity;

import dagger.Component;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 2:39:34 PM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

@ActivityScope
@Component(dependencies = AppComponent.class, modules = DashboardViewModule.class)
public interface DashboardViewComponent {
    void inject(DashboardActivity activity);
}