package id.wika.cpptkponline.injection;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import id.wika.cpptkponline.presenter.LaporanPresenter;
import id.wika.cpptkponline.presenter.PresenterFactory;
import id.wika.cpptkponline.presenter.impl.LaporanPresenterImpl;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 28, 2017 8:33:21 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

@Module
public final class LaporanViewModule {
    public LaporanViewModule() {
    }

    @Provides
    public PresenterFactory<LaporanPresenter> providePresenterFactory(@NonNull final LaporanPresenterImpl presenter) {
        return new PresenterFactory<LaporanPresenter>() {
            @NonNull
            @Override
            public LaporanPresenter create() {
                return presenter;
            }
        };
    }
}
