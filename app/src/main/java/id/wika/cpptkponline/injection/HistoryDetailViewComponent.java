package id.wika.cpptkponline.injection;

import id.wika.cpptkponline.view.history.HistoryDetailFragment;

import dagger.Component;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:54:07 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil
 * All Rights Reserved. 
 ******************************************************************************/

@FragmentScope
@Component(dependencies = AppComponent.class, modules = HistoryDetailViewModule.class)
public interface HistoryDetailViewComponent {
    void inject(HistoryDetailFragment fragment);
}