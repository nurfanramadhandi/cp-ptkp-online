package id.wika.cpptkponline.injection;

import id.wika.cpptkponline.view.history.HistoryFragment;

import dagger.Component;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:51:54 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

@FragmentScope
@Component(dependencies = AppComponent.class, modules = HistoryViewModule.class)
public interface HistoryViewComponent {
    void inject(HistoryFragment fragment);
}