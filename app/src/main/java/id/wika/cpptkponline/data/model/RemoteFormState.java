package id.wika.cpptkponline.data.model;

/**
 * Created by mamang_kompii on 9/11/17.
 */

public class RemoteFormState {

    private int localFormId;
    private int formId;
    private String formStatus;

    public int getLocalFormId() {
        return localFormId;
    }

    public void setLocalFormId(int localFormId) {
        this.localFormId = localFormId;
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public String getFormStatus() {
        return formStatus;
    }

    public void setFormStatus(String formStatus) {
        this.formStatus = formStatus;
    }
}
