package id.wika.cpptkponline.data;

/**
 * Created by mamang_kompii on 8/25/17.
 */

public class FormStatus {

    public static final int FORM_AWAL = 1;
    public static final int FORM_PENGECEKAN = 2;
    public static final int FORM_RENCANA_TINDAK_LANJUT_DISETUJUI = 4;
    public static final int FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN = 5;
    public static final int FORM_RENCANA_TINDAK_LANJUT = 6;
    public static final int FORM_REALISASI_TINDAK_LANJUT = 7;
    public static final int FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT = 8;
    public static final int LAPORAN_TINDAK_LANJUT_STATUS_OPEN = 9;
    public static final int LAPORAN_TINDAK_LANJUT_STATUS_CLOSED = 10;
}
