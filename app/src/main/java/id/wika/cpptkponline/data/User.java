package id.wika.cpptkponline.data;

import android.arch.persistence.room.Ignore;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import java.io.Serializable;

import id.wika.cpptkponline.data.model.Picture;

/**
 * Created by mamang_kompii on 8/30/17.
 */

public class User implements Serializable {

    String username;
    String fullname;
    public String jabatan;
    public String roleId;
    String token;
    public String kodedivisi;
    public String divisi;
    public String proyek;

    public User() {
    }

    @Ignore
    public User(String username, String fullname, String jabatan, String roleId, String token, String divisi, String kodedivisi, String proyek) {
        this.username = username;
        this.fullname = fullname;
        this.jabatan=   jabatan;
        this.roleId = roleId;
        this.token = token;
        this.divisi= divisi;
        this.kodedivisi=kodedivisi;
        this.proyek=proyek;
    }


    @Ignore
    public User(String username, String fullname, String jabatan, String roleId, String token, String divisi, String kodedivisi, Picture picture, String proyek) {
        this.username = username;
        this.fullname = fullname;
        this.roleId = roleId;
        this.jabatan = jabatan;
        this.token = token;
        this.divisi= divisi;
        this.kodedivisi=kodedivisi;
        this.proyek=proyek;
    }


    public void save(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = pref.edit();

        edit.putString("username", username).putString("token",token).putString("fullname",fullname)
                .putString("jabatan",jabatan).putString("roleId",roleId).putString("divisi",divisi).putString("kodedivisi",kodedivisi)
                .putString("proyek",proyek);
        edit.commit();
    }

    public static void clear(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = pref.edit();
        edit.remove("username").remove("token").remove("fullname").remove("jabatan").remove("roleId").remove("divisi").remove("kodedivisi").remove("proyek");
        edit.commit();
    }

    public static User load(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String uid = pref.getString("username",null);
        String token = pref.getString("token",null);
        if (TextUtils.isEmpty(uid) || TextUtils.isEmpty(token)){
            return null;
        }
        return new User(pref.getString("username",""),pref.getString("fullname",""),
                pref.getString("jabatan",""),pref.getString("roleId",""), pref.getString("token",""),pref.getString("divisi",""),pref.getString("kodedivisi",""),pref.getString("proyek","") );
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public String getkodedivisi() {
        return kodedivisi;
    }

    public void setKodeDivisi(String kodedivisi) {
        this.kodedivisi = kodedivisi;
    }

    public String getDivisi() {
        return divisi;
    }


    public void setDivisi(String divisi) {
        this.divisi = divisi;
    }


    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getProyek() {
        return proyek;
    }

    public void setProyek(String proyek) {
        this.proyek = proyek;
    }

}
