package id.wika.cpptkponline.data.endpoint.response;

import java.util.List;

import id.wika.cpptkponline.data.model.Proyek;
import id.wika.cpptkponline.data.model.RoleUnitKerja;
import id.wika.cpptkponline.data.model.UserProfile;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class LoginResponse {

    private String status;
    private UserProfile user;
    private List<Proyek> proyek;
    private RoleUnitKerja divisi;
    private String token;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserProfile getUser() {
        return user;
    }

    public void setUser(UserProfile user) {
        this.user = user;
    }

    public RoleUnitKerja getDivisi() {
        return divisi;
    }

    public void setDivisi(RoleUnitKerja divisi) {
        this.divisi = divisi;
    }

    public List<Proyek> getProyek() {
        return proyek;
    }

    public void setProyek(List<Proyek> proyek) {
        this.proyek = proyek;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
