package id.wika.cpptkponline.data.db;

import android.arch.persistence.room.TypeConverter;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;

import id.wika.cpptkponline.helper.StringHelper;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public class ArrayOfStringConverter {

    @TypeConverter
    public static ArrayList<String> toArrayListString(String string) {
        return StringHelper.isBlank(string) ? new ArrayList<String>() :
                new ArrayList<String>(Arrays.asList(string.split(";")));
    }

    @TypeConverter
    public static String toString(ArrayList<String> strings) {
        if (strings.size() > 0) {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < strings.size(); i++) {
                if (s.length() > 0) {
                    s.append(";");
                }
                s.append(strings.get(i));
            }
            return s.toString();
        } else {
            return "";
        }
    }
}
