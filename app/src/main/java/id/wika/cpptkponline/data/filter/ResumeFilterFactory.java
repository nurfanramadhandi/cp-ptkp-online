package id.wika.cpptkponline.data.filter;

import java.util.HashMap;
import java.util.Map;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 18/September/2017 - 18:42
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public class ResumeFilterFactory {

    private static final Map<ResumeFillterType, ResumeFilter> mfilter = new HashMap<>();

    public ResumeFilterFactory() {
        mfilter.put(ResumeFillterType.ALL, new FilterAllResumeFilter());
        mfilter.put(ResumeFillterType.FORM_AWAL, new FormAwalResumeFilter());
        mfilter.put(ResumeFillterType.FORM_PENGECEKAN, new FormPengecekanResumeFilter());
        mfilter.put(ResumeFillterType.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI, new FormRTLSetujuResumeFilter());
        mfilter.put(ResumeFillterType.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN, new FormRTLBatalResumeFilter());
        mfilter.put(ResumeFillterType.FORM_RENCANA_TINDAK_LANJUT, new FormRTLRTLResumeFilter());
        mfilter.put(ResumeFillterType.FORM_REALISASI_TINDAK_LANJUT, new FormRealisasiTLResumeFilter());
        mfilter.put(ResumeFillterType.FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT, new FormVerifikasiRTLResumeFilter());
        mfilter.put(ResumeFillterType.LAPORAN_TINDAK_LANJUT_STATUS_OPEN, new LaporanTLSOpenResumeFilter());
        mfilter.put(ResumeFillterType.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED, new LaporanTLSCloseResumeFilter());
    }

    public ResumeFilter create(ResumeFillterType resumeFillterType){
        return mfilter.get(resumeFillterType);
    }
}
