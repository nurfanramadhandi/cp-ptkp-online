package id.wika.cpptkponline.data;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mamang_kompii on 9/4/17.
 */

public class DummyDatabase {

    private static User[] users = {
            new User("fahad", "Fahad Abdullah","Manager", UserRole.ROLE_PROD,"","","",""),
            new User("bambang", "Bambang Fadilla","Manager", UserRole.ROLE_MP,"","","",""),
            new User("jokoatmojo", "Joko Atmojo","Manager", UserRole.ROLE_PROD,"","","",""),
            new User("suginoandi", "Sugino Andi Brotoseno","Manager", UserRole.ROLE_PROD,"","","",""),
            new User("aguscokro", "Agus Cokro Buana","Manager", UserRole.ROLE_QA,"","","",""),
//            new User("andisutoyo", "Andi Sutoyo", UserRole.ROLE_COMMONER)
    };

    public static List<CPPTKPForm> getDummyFormHistory() {
        ArrayList<CPPTKPForm> cpptkpFormList = new ArrayList<>();

        cpptkpFormList.add(createForm1());
        cpptkpFormList.add(createForm2());
        cpptkpFormList.add(createForm3());
        cpptkpFormList.add(createForm4());
        cpptkpFormList.add(createForm5());
        cpptkpFormList.add(createForm6());

        return cpptkpFormList;
    }

    private static CPPTKPForm createForm1() {
        CPPTKPForm cpptkpForm = new CPPTKPForm();
        cpptkpForm.setFormId(1);
        cpptkpForm.setFormStatus(FormStatus.FORM_AWAL);
        cpptkpForm.setNeedResponse(true);

        DataFormPelaporan dataLaporan = new DataFormPelaporan();
        dataLaporan.setPenemu(users[0]);
        dataLaporan.setFormDate(DateTime.now().minusHours(1));
        dataLaporan.setNamaProyek("Apartemen Green Residence Bogor");
        dataLaporan.setLokasiPenyimpangan("Area Pengembangan");
        dataLaporan.setBentukPenyimpangan("Perlengkapan safety kit yang digunakan tidak lengkap");
        cpptkpForm.setFormPelaporan(dataLaporan);

        cpptkpForm.setTimestamp(DateTime.now());
        return cpptkpForm;
    }

    private static CPPTKPForm createForm2() {
        CPPTKPForm cpptkpForm = new CPPTKPForm();
        cpptkpForm.setFormId(2);
        cpptkpForm.setFormStatus(FormStatus.FORM_RENCANA_TINDAK_LANJUT);
        cpptkpForm.setNeedResponse(true);

        DataFormPelaporan dataLaporan = new DataFormPelaporan();
        dataLaporan.setPenemu(users[1]);
        dataLaporan.setFormDate(DateTime.now().minusHours(2));
        dataLaporan.setNamaProyek("Gedung Apartemen DPR");
        dataLaporan.setLokasiPenyimpangan("Area Pengembangan");
        dataLaporan.setBentukPenyimpangan("Penggunaan safety kit tidak sesuai / tidak tepat");
        dataLaporan.setKategoriForm(CPPTKPForm.FORM_CP);
        dataLaporan.setLingkupTemuan("Non Struktur");
        cpptkpForm.setFormPelaporan(dataLaporan);

        DataRencanaTindakLanjut rencanaTindakLanjut = new DataRencanaTindakLanjut();
        rencanaTindakLanjut.setPenanggungJawab(users[2]);
        cpptkpForm.setRencanaTindakLanjut(rencanaTindakLanjut);

        cpptkpForm.setTimestamp(DateTime.now().minusMinutes(5));
        return cpptkpForm;
    }

    private static CPPTKPForm createForm3() {
        CPPTKPForm cpptkpForm = new CPPTKPForm();
        cpptkpForm.setFormId(3);
        cpptkpForm.setFormStatus(FormStatus.FORM_RENCANA_TINDAK_LANJUT);
        cpptkpForm.setNeedResponse(true);
        cpptkpForm.setNomorForm("CP/HPS/05/09/2017");

        DataFormPelaporan dataLaporan = new DataFormPelaporan();
        dataLaporan.setPenemu(users[4]);
        dataLaporan.setFormDate(DateTime.now().minusHours(3));
        dataLaporan.setNamaProyek("Hotel Papaho Setiabudi");
        dataLaporan.setLokasiPenyimpangan("Lantai 1");
        dataLaporan.setBentukPenyimpangan("Terdapat cacat fisik pada kerangka tiang");
        dataLaporan.setKategoriForm(CPPTKPForm.FORM_CP);
        dataLaporan.setLingkupTemuan("Struktur");
        dataLaporan.setDetailLingkupTemuan("Struktur Tiang");
        cpptkpForm.setFormPelaporan(dataLaporan);

        DataRencanaTindakLanjut rencanaTindakLanjut = new DataRencanaTindakLanjut();
        rencanaTindakLanjut.setPenanggungJawab(users[2]);
        rencanaTindakLanjut.setPenyebabUtamaPenyimpangan("Kesalahan metode dalam pembangunan tiang");
        rencanaTindakLanjut.setKeputusanJangkaPendek("Dikerjakan ulang");
        rencanaTindakLanjut.setTitleDetailKeputusanJangkaPendek("Metode rework");
        rencanaTindakLanjut.setDetailKeputusanJangkaPendek("Timpah ulang");
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPendek(DateTime.now().plusWeeks(2));
        rencanaTindakLanjut.setKeputusanJangkaPanjang("Pembinaan praktek pekerja yang bersangkutan dalam 3 - 5 pekerjaan");
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPanjang(DateTime.now().plusWeeks(6));
        cpptkpForm.setRencanaTindakLanjut(rencanaTindakLanjut);

        cpptkpForm.setTimestamp(DateTime.now().minusMinutes(10));
        return cpptkpForm;
    }

    private static CPPTKPForm createForm4() {
        CPPTKPForm cpptkpForm = new CPPTKPForm();
        cpptkpForm.setFormId(4);
        cpptkpForm.setFormStatus(FormStatus.FORM_PENGECEKAN);
        cpptkpForm.setNeedResponse(true);
        cpptkpForm.setNomorForm("CP/MZJ/05/09/2017");

        DataFormPelaporan dataLaporan = new DataFormPelaporan();
        dataLaporan.setPenemu(users[1]);
        dataLaporan.setFormDate(DateTime.now().minusHours(4));
        dataLaporan.setNamaProyek("Museum Zoologi Jakarta");
        dataLaporan.setLokasiPenyimpangan("Lantai 1");
        dataLaporan.setBentukPenyimpangan("Terdapat kesalahan pengerjaan atap sehingga terlihat miring");
        dataLaporan.setKategoriForm(CPPTKPForm.FORM_CP);
        dataLaporan.setLingkupTemuan("Struktur");
        dataLaporan.setDetailLingkupTemuan("Struktur Atap");
        cpptkpForm.setFormPelaporan(dataLaporan);

        DataRencanaTindakLanjut rencanaTindakLanjut = new DataRencanaTindakLanjut();
        rencanaTindakLanjut.setPenanggungJawab(users[3]);
        rencanaTindakLanjut.setPenyebabUtamaPenyimpangan("Kesalahan metode dalam pembangunan atap");
        rencanaTindakLanjut.setKeputusanJangkaPendek("Dikerjakan ulang");
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPendek(DateTime.now().plusWeeks(3));
        rencanaTindakLanjut.setKeputusanJangkaPanjang("Pembinaan praktek pekerja yang bersangkutan dalam 3 - 5 pekerjaan");
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPanjang(DateTime.now().plusWeeks(6));
        cpptkpForm.setRencanaTindakLanjut(rencanaTindakLanjut);

        cpptkpForm.setApproval(new DataApproval(users[1], DateTime.now(), true));

        cpptkpForm.setTimestamp(DateTime.now().minusDays(1));
        return cpptkpForm;
    }

    private static CPPTKPForm createForm5() {
        CPPTKPForm cpptkpForm = new CPPTKPForm();
        cpptkpForm.setFormId(5);
        cpptkpForm.setFormStatus(FormStatus.FORM_RENCANA_TINDAK_LANJUT);
        cpptkpForm.setNeedResponse(true);
        cpptkpForm.setNomorForm("CP/WTB/05/09/2017");

        DataFormPelaporan dataLaporan = new DataFormPelaporan();
        dataLaporan.setPenemu(users[2]);
        dataLaporan.setFormDate(DateTime.now().minusHours(5));
        dataLaporan.setNamaProyek("Wisma Toejoeh Bintang");
        dataLaporan.setLokasiPenyimpangan("Lantai 3");
        dataLaporan.setBentukPenyimpangan("Safety kit tidak dipakai dengan benar");
        dataLaporan.setKategoriForm(CPPTKPForm.FORM_CP);
        dataLaporan.setLingkupTemuan("Non Struktur");
        cpptkpForm.setFormPelaporan(dataLaporan);

        DataRencanaTindakLanjut rencanaTindakLanjut = new DataRencanaTindakLanjut();
        rencanaTindakLanjut.setPenanggungJawab(users[3]);
        rencanaTindakLanjut.setPenyebabUtamaPenyimpangan("Kelalaian pekerja");
        rencanaTindakLanjut.setKeputusanJangkaPendek("Tidak diperlukan tindakan korektif");
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPendek(DateTime.now().plusDays(4));
        rencanaTindakLanjut.setKeputusanJangkaPanjang("Pembinaan pekerja");
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPanjang(DateTime.now().plusWeeks(2));
        cpptkpForm.setRencanaTindakLanjut(rencanaTindakLanjut);

        cpptkpForm.setApproval(new DataApproval(users[1], DateTime.now(), true));

        DataRealisasiTindakLanjut realisasiTindakLanjut = new DataRealisasiTindakLanjut();
        realisasiTindakLanjut.setHasilPencegahanDanPerbaikan(
                "Beberapa pekerja yang dialporkan, selama pemantauan sudah selalu menggunakan safety kit dengan benar");
        cpptkpForm.setRealisasiTindakLanjut(realisasiTindakLanjut);

        cpptkpForm.setTimestamp(DateTime.now().minusDays(1).minusMinutes(20));
        return cpptkpForm;
    }

    private static CPPTKPForm createForm6() {
        CPPTKPForm cpptkpForm = new CPPTKPForm();
        cpptkpForm.setFormId(6);
        cpptkpForm.setFormStatus(FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN);
        cpptkpForm.setNeedResponse(true);
        cpptkpForm.setNomorForm("CP/GMT/05/09/2017");

        DataFormPelaporan dataLaporan = new DataFormPelaporan();
        dataLaporan.setPenemu(users[4]);
        dataLaporan.setFormDate(DateTime.now().minusHours(5));
        dataLaporan.setNamaProyek("Apartemen Graha Madina Tebet");
        dataLaporan.setLokasiPenyimpangan("Lantai 3");
        dataLaporan.setBentukPenyimpangan("Kekurangan bahan & peralatan kurang memadai");
        dataLaporan.setKategoriForm(CPPTKPForm.FORM_CP);
        dataLaporan.setLingkupTemuan("Non Struktur");
        cpptkpForm.setFormPelaporan(dataLaporan);

        DataRencanaTindakLanjut rencanaTindakLanjut = new DataRencanaTindakLanjut();
        rencanaTindakLanjut.setPenanggungJawab(users[3]);
        rencanaTindakLanjut.setPenyebabUtamaPenyimpangan("Kelalaian penyediaan support bahan & peralatan");
        rencanaTindakLanjut.setKeputusanJangkaPendek("Tidak diperlukan tindakan korektif");
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPendek(DateTime.now().plusDays(5));
        rencanaTindakLanjut.setKeputusanJangkaPanjang("Pembinaan support & perbaikan metode pemantauan");
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPanjang(DateTime.now().plusWeeks(3));
        cpptkpForm.setRencanaTindakLanjut(rencanaTindakLanjut);

        cpptkpForm.setApproval(new DataApproval(users[1], DateTime.now(), true));

        DataRealisasiTindakLanjut realisasiTindakLanjut = new DataRealisasiTindakLanjut();
        realisasiTindakLanjut.setHasilPencegahanDanPerbaikan(
                "Selama pemantauan perbaikan, support sudah selalu tersedia dengan baik");
        cpptkpForm.setRealisasiTindakLanjut(realisasiTindakLanjut);

        DataVerifikasiHasilTindakLanjut verifikasiHasilTindakLanjut = new DataVerifikasiHasilTindakLanjut();
        verifikasiHasilTindakLanjut.setVerifikator(users[4]);
        verifikasiHasilTindakLanjut.setStatusOpened(true);
        verifikasiHasilTindakLanjut.setFeedback("Lampiran kurang jelas, perlu ditambahkan lampiran sistem metode pemantauan yang baru");
        cpptkpForm.setVerifikasiHasilTindakLanjut(verifikasiHasilTindakLanjut);

        cpptkpForm.setTimestamp(DateTime.now().minusDays(3));
        return cpptkpForm;
    }

    public static List<User> getUsersByRole(String roleConst) {
        ArrayList<User> filteredUser = new ArrayList<>();

        for (User user : users) {
            if (user.getRoleId() == roleConst) {
                filteredUser.add(user);
            }
        }

        return filteredUser;
    }

    public static CPPTKPForm getFormById(String formId) {

        for (CPPTKPForm cpptkpForm :
                getDummyFormHistory()) {
            if (formId.equals(cpptkpForm.getFormId())) {
                return cpptkpForm;
            }
        }

        return null;
    }

    public static User getUserById(String userId) {
        for (User user : users) {
            if (user.getUsername().equals(userId)) {
                return user;
            }
        }

        return null;
    }

    public static List<FormSummary> getDummyFormHistorySummaries() {
        List<FormSummary> formSummaries = new ArrayList<>();

        for (CPPTKPForm cpptkpForm : getDummyFormHistory()) {
            FormSummary formSummary = new FormSummary(
                    cpptkpForm.getFormId(),
                    cpptkpForm.getRemoteFormId(),
                    cpptkpForm.getFormPelaporan().getNamaProyek(),
                    cpptkpForm.getFormPelaporan().getLokasiPenyimpangan(),
                    cpptkpForm.getFormPelaporan().getBentukPenyimpangan(),
                    cpptkpForm.getNomorForm(),
                    cpptkpForm.getFormStatus(),
                    cpptkpForm.isNeedResponse(),
                    cpptkpForm.getTimestamp()
            );
            formSummaries.add(formSummary);
        }

        return formSummaries;
    }
}
