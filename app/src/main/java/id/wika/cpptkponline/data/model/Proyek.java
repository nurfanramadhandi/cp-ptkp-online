package id.wika.cpptkponline.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class Proyek {

    @SerializedName("kode_proyek")
    private String kode;

    @SerializedName("nama_proyek")
    private String nama;

    public Proyek(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
