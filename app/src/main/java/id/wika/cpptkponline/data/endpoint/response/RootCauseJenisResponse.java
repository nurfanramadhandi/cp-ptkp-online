package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class RootCauseJenisResponse {

    private String status;

    @SerializedName("jenis")
    private String jenis;


    public String getJenis() {
        return jenis;
    }

    public void setJenis(String nama) {
        this.jenis = jenis;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
