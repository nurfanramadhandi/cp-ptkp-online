package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/10/17.
 */

public class PictureResponse {

    public static final String ATTACHMENT_TYPE_DRAFT = "draft";
    public static final String ATTACHMENT_TYPE_APPROVAL = "edit";
    public static final String ATTACHMENT_TYPE_HASIL = "hasil";
    public static final String ATTACHMENT_TYPE_FEEDBACK = "feedback";
    public static final String ATTACHMENT_TYPE_REVISI = "revisi";

    private int id;
    private int idTemuan;
    private String jenis;

    @SerializedName("pic_1")
    private String pathPic1;

    @SerializedName("pic_2")
    private String pathPic2;

    @SerializedName("pic_3")
    private String pathPic3;

    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTemuan() {
        return idTemuan;
    }

    public void setIdTemuan(int idTemuan) {
        this.idTemuan = idTemuan;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getPathPic1() {
        return pathPic1;
    }

    public void setPathPic1(String pathPic1) {
        this.pathPic1 = pathPic1;
    }

    public String getPathPic2() {
        return pathPic2;
    }

    public void setPathPic2(String pathPic2) {
        this.pathPic2 = pathPic2;
    }

    public String getPathPic3() {
        return pathPic3;
    }

    public void setPathPic3(String pathPic3) {
        this.pathPic3 = pathPic3;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
