package id.wika.cpptkponline.data.endpoint;

import java.util.List;

import id.wika.cpptkponline.data.endpoint.response.PostTemuanResponse;
import id.wika.cpptkponline.data.endpoint.response.RootCauseJenisResponse;
import id.wika.cpptkponline.data.endpoint.response.RootCauseResponse;
import id.wika.cpptkponline.data.model.LingkupTemuan;
import id.wika.cpptkponline.data.model.LingkupTemuanJenis;
import id.wika.cpptkponline.data.model.RootCause;
import id.wika.cpptkponline.data.model.RootCauseJenis;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public interface RootCauseEndpointInterface {

    @GET("root_cause_jenis")
    Observable<List<RootCauseJenisResponse>> getRootCauseJenis(
            @Header("Authorization") String auth
    );

    @GET("root_cause")
    Observable<List<RootCauseResponse>> getRootCause(
            @Header("Authorization") String auth,
            @Query("jenis") String jenis
    );
}
