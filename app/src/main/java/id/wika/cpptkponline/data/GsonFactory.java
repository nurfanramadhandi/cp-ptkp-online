package id.wika.cpptkponline.data;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.TimeZone;

import id.wika.cpptkponline.helper.DateTimeHelper;

/**
 * Created by noval on 2/19/15.
 */
public class GsonFactory {

    private static final Type DATE_TIME_TYPE = new TypeToken<DateTime>() {
    }.getType();

    public static Gson createGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        gsonBuilder.registerTypeAdapter(DATE_TIME_TYPE, new DateTimeTypeConverter());
        return gsonBuilder.create();
    }

    private static class DateTimeTypeConverter implements JsonSerializer<DateTime>, JsonDeserializer<DateTime> {

        private DateTimeZone dateTimeZone = DateTimeZone.forTimeZone(TimeZone.getDefault());
        private DateTimeHelper dateTimeHelper = new DateTimeHelper();

        @Override
        public DateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            DateTime dateTime;
            String text = json.getAsString();

            try {
                dateTime = dateTimeHelper.parseDateTime(text);
            } catch (IllegalArgumentException exception) {
                //replacement tanda '***' dengan '000' dilakukan untuk memperbaiki timestamp dengan unknown milisecond part
                //semua timestamp dengan unknown milisecond diubah menjadi 0 milisecond
                dateTime = dateTimeHelper.parseDateTime(text.replace("***", "000"));
            }

            return dateTime.toDateTime(dateTimeZone);
        }

        @Override
        public JsonElement serialize(DateTime src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(dateTimeHelper.print(src));
        }
    }

}