package id.wika.cpptkponline.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class RoleUnitKerja {

    @SerializedName("kode_unit_kerja")
    private String kode;

    @SerializedName("nama_unit_kerja")
    private String nama;

    public RoleUnitKerja(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
