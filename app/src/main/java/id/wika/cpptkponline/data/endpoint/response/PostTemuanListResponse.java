
package id.wika.cpptkponline.data.endpoint.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PostTemuanListResponse {

    @SerializedName("data")
    private List<Datum> mData;
    @SerializedName("status")
    private String mStatus;

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public static class Datum implements Parcelable {

        @SerializedName("activity")
        private String mActivity;
        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("created_by")
        private String mCreatedBy;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_temuan")
        private Long mIdTemuan;
        @SerializedName("status")
        private int mStatus;
        @SerializedName("updated_at")
        private String mUpdatedAt;
        @SerializedName("updated_by")
        private String mUpdatedBy;

        protected Datum(Parcel in) {
            mActivity = in.readString();
            mCreatedAt = in.readString();
            mCreatedBy = in.readString();
            mUpdatedAt = in.readString();
            mUpdatedBy = in.readString();
        }

        public final Creator<Datum> CREATOR = new Creator<Datum>() {
            @Override
            public Datum createFromParcel(Parcel in) {
                return new Datum(in);
            }

            @Override
            public Datum[] newArray(int size) {
                return new Datum[size];
            }
        };

        public Datum(String mActivity, String mCreatedAt, String mCreatedBy, Long mId, Long mIdTemuan, int mStatus, String mUpdatedAt, String mUpdatedBy) {
            this.mActivity = mActivity;
            this.mCreatedAt = mCreatedAt;
            this.mCreatedBy = mCreatedBy;
            this.mId = mId;
            this.mIdTemuan = mIdTemuan;
            this.mStatus = mStatus;
            this.mUpdatedAt = mUpdatedAt;
            this.mUpdatedBy = mUpdatedBy;
        }

        public String getActivity() {
            return mActivity;
        }

        public void setActivity(String activity) {
            mActivity = activity;
        }

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getCreatedBy() {
            return mCreatedBy;
        }

        public void setCreatedBy(String createdBy) {
            mCreatedBy = createdBy;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdTemuan() {
            return mIdTemuan;
        }

        public void setIdTemuan(Long idTemuan) {
            mIdTemuan = idTemuan;
        }

        public int getStatus() {
            return mStatus;
        }

        public void setStatus(int status) {
            mStatus = status;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

        public String getUpdatedBy() {
            return mUpdatedBy;
        }

        public void setUpdatedBy(String updatedBy) {
            mUpdatedBy = updatedBy;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(mActivity);
            parcel.writeString(mCreatedAt);
            parcel.writeString(mCreatedBy);
            parcel.writeString(mUpdatedAt);
            parcel.writeString(mUpdatedBy);
        }
    }
}
