package id.wika.cpptkponline.data.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class DownloadFile extends IntentService {
    public static final int UPDATE_PROGRESS = 8344;
    public static final int UPDATE_PROGRESS_DONE = 8345;
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_BAZ = "id.wika.cpptkponline.data.service.action.BAZ";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "id.wika.cpptkponline.data.service.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "id.wika.cpptkponline.data.service.extra.receiver";
    private static final String EXTRA_PARAM3 = "proyek";

    public DownloadFile() {
        super("DownloadFile");
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String namaProyek, ResultReceiver param2) {
        Intent intent = new Intent(context, DownloadFile.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        intent.putExtra(EXTRA_PARAM3, namaProyek);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String param1 = intent.getStringExtra(EXTRA_PARAM1);
            final String param2 = intent.getStringExtra(EXTRA_PARAM3);
//            ResultReceiver receiver = (ResultReceiver) intent.getParcelableExtra(EXTRA_PARAM2);
            try {
//                handleActionFoo(param1, receiver);
                handleActionFoo(param1,param2, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2, ResultReceiver receiver) throws Exception {
        // TODO: Handle action Foo
        URL url = new URL(param1);
        URLConnection connection = url.openConnection();
        connection.connect();
        // this will be useful so that you can show a typical 0-100% progress bar
        String fileName = "";
        String disposition = connection.getHeaderField("Content-Disposition");
        String contentType = connection.getContentType();
        int contentLength = connection.getContentLength();

        if (disposition != null) {
            // extracts file name from header field
            int index = disposition.indexOf("filename=");
            if (index > 0) {
                fileName = disposition.substring(index + 10,
                        disposition.length() - 1);
            }
        } else {
            // extracts file name from URL
            fileName = param1.substring((param1).lastIndexOf("/") + 1,
                    (param1).length());
        }

        // download the file
        File localFolder = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/WebPageToPDF");
        localFolder.mkdirs();
        InputStream input = new BufferedInputStream(connection.getInputStream());

        if(localFolder.exists()){
            Date date= new Date();
            OutputStream output = new FileOutputStream(localFolder.getAbsolutePath() + "/Laporan_"+param2 + "_"+date.toString() + ".pdf");
            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                Bundle resultData = new Bundle();
                resultData.putInt("progress", (int) total);
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
        }
    }

}
