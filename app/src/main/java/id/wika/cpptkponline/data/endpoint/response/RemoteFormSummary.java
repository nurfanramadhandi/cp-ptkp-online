package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public class RemoteFormSummary {

    @SerializedName("id")
    private int formId;

    private String namaPenemu;

    private String divisi;

    @SerializedName("no_temuan")
    private String noTemuan;

    private String namaProyek;

    private String bentukPenyimpangan;

    private String lokasiPenyimpangan;

    private DateTime tglTemuan;

    private String status;

    private String alertTo;

    @SerializedName("pic_1")
    private String pic1;
    @SerializedName("pic_2")
    private String pic2;
    @SerializedName("pic_3")
    private String pic3;

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public String getNamaPenemu() {
        return namaPenemu;
    }

    public void setNamaPenemu(String namaPenemu) {
        this.namaPenemu = namaPenemu;
    }

    public String getDivisi() {
        return divisi;
    }

    public void setDivisi(String divisi) {
        this.divisi = divisi;
    }

    public String getNamaProyek() {
        return namaProyek;
    }

    public void setNamaProyek(String namaProyek) {
        this.namaProyek = namaProyek;
    }

    public String getLokasiPenyimpangan() {
        return lokasiPenyimpangan;
    }

    public void setLokasiPenyimpangan(String lokasiPenyimpangan) {
        this.lokasiPenyimpangan = lokasiPenyimpangan;
    }

    public String getBentukPenyimpangan() {
        return bentukPenyimpangan;
    }

    public void setBentukPenyimpangan(String bentukPenyimpangan) {
        this.bentukPenyimpangan = bentukPenyimpangan;
    }

    public String getNoTemuan() {
        return noTemuan;
    }

    public void setNoTemuan(String noTemuan) {
        this.noTemuan = noTemuan;
    }

    public DateTime getTglTemuan() {
        return tglTemuan;
    }

    public void setTglTemuan(DateTime tglTemuan) {
        this.tglTemuan = tglTemuan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAlertTo() {
        return alertTo;
    }

    public void setAlertTo(String alertTo) {
        this.alertTo = alertTo;
    }

    public String getPic1() {
        return pic1;
    }

    public void setPic1(String pic1) {
        this.pic1 = pic1;
    }

    public String getPic2() {
        return pic2;
    }

    public void setPic2(String pic2) {
        this.pic2 = pic2;
    }

    public String getPic3() {
        return pic3;
    }

    public void setPic3(String pic3) {
        this.pic3 = pic3;
    }
}
