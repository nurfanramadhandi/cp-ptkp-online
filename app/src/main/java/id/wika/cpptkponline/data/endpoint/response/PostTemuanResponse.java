package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public class PostTemuanResponse {

    @SerializedName("id_temuan")
    private int idTemuan;
    private String status;

    public PostTemuanResponse(int idTemuan, String status) {
        this.idTemuan = idTemuan;
        this.status = status;
    }

    public int getIdTemuan() {
        return idTemuan;
    }

    public void setIdTemuan(int idTemuan) {
        this.idTemuan = idTemuan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
