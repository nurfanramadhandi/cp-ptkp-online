package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class LingkupTemuanJenisResponse {

    private String status;

    @SerializedName("jenis_temuan")
    private String jenis;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }
}
