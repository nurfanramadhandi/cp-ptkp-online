package id.wika.cpptkponline.data.db;

import android.arch.persistence.room.TypeConverter;

import org.joda.time.DateTime;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public class DateTimeConverter {

    @TypeConverter
    public static DateTime toDateTime(Long timestamp) {
        return timestamp == null ? null : new DateTime(timestamp);
    }

    @TypeConverter
    public static Long toTimestamp(DateTime date) {
        return date == null ? null : date.getMillis();
    }
}
