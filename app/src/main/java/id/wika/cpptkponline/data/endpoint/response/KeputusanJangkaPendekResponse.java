package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/7/17.
 */

public class KeputusanJangkaPendekResponse {

    private String status;

    @SerializedName("kode_keputusan")
    private String kode_keputusan;

    @SerializedName("nama")
    private String nama;

    public KeputusanJangkaPendekResponse(String kode_keputusan, String nama) {
        this.kode_keputusan = kode_keputusan;
        this.nama = nama;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKode_keputusan() {
        return kode_keputusan;
    }

    public void setKode_keputusan(String kode_keputusan) {
        this.kode_keputusan = kode_keputusan;
    }


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
