package id.wika.cpptkponline.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class LingkupTemuanJenis {

    @SerializedName("jenis")
    private String jenis;

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String nama) {
        this.jenis = jenis;
    }
}
