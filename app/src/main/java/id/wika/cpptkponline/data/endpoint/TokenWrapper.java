package id.wika.cpptkponline.data.endpoint;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public class TokenWrapper {

    public static String getWrappedToken(String token) {
        return "Bearer " + token;
    }
}
