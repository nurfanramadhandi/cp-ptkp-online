package id.wika.cpptkponline.data;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by mamang_kompii on 9/4/17.
 */

@Entity(tableName = "rencana_tindak_lanjut", foreignKeys = {
        @ForeignKey(entity = CPPTKPForm.class,
                parentColumns = "formId",
                childColumns = "relatedFormId",
                onDelete = ForeignKey.CASCADE)})
public class DataRencanaTindakLanjut implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private int relatedFormId;

    @Embedded
    private User penanggungJawab;

    private String penyebabUtamaPenyimpangan;
    private String keputusanJangkaPendek;
    private String titleDetailKeputusanJangkaPendek;
    private String detailKeputusanJangkaPendek;
    private DateTime targetWaktuKeputusanJangkaPendek;
    private String keputusanJangkaPanjang;
    private DateTime targetWaktuKeputusanJangkaPanjang;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRelatedFormId() {
        return relatedFormId;
    }

    public void setRelatedFormId(int relatedFormId) {
        this.relatedFormId = relatedFormId;
    }

    public User getPenanggungJawab() {
        return penanggungJawab;
    }

    public void setPenanggungJawab(User penanggungJawab) {
        this.penanggungJawab = penanggungJawab;
    }

    public String getPenyebabUtamaPenyimpangan() {
        return penyebabUtamaPenyimpangan;
    }

    public void setPenyebabUtamaPenyimpangan(String penyebabUtamaPenyimpangan) {
        this.penyebabUtamaPenyimpangan = penyebabUtamaPenyimpangan;
    }

    public String getKeputusanJangkaPendek() {
        return keputusanJangkaPendek;
    }

    public void setKeputusanJangkaPendek(String keputusanJangkaPendek) {
        this.keputusanJangkaPendek = keputusanJangkaPendek;
    }

    public String getTitleDetailKeputusanJangkaPendek() {
        return titleDetailKeputusanJangkaPendek;
    }

    public void setTitleDetailKeputusanJangkaPendek(String titleDetailKeputusanJangkaPendek) {
        this.titleDetailKeputusanJangkaPendek = titleDetailKeputusanJangkaPendek;
    }

    public String getDetailKeputusanJangkaPendek() {
        return detailKeputusanJangkaPendek;
    }

    public void setDetailKeputusanJangkaPendek(String detailKeputusanJangkaPendek) {
        this.detailKeputusanJangkaPendek = detailKeputusanJangkaPendek;
    }

    public DateTime getTargetWaktuKeputusanJangkaPendek() {
        return targetWaktuKeputusanJangkaPendek;
    }

    public void setTargetWaktuKeputusanJangkaPendek(DateTime targetWaktuKeputusanJangkaPendek) {
        this.targetWaktuKeputusanJangkaPendek = targetWaktuKeputusanJangkaPendek;
    }

    public String getKeputusanJangkaPanjang() {
        return keputusanJangkaPanjang;
    }

    public void setKeputusanJangkaPanjang(String keputusanJangkaPanjang) {
        this.keputusanJangkaPanjang = keputusanJangkaPanjang;
    }

    public DateTime getTargetWaktuKeputusanJangkaPanjang() {
        return targetWaktuKeputusanJangkaPanjang;
    }

    public void setTargetWaktuKeputusanJangkaPanjang(DateTime targetWaktuKeputusanJangkaPanjang) {
        this.targetWaktuKeputusanJangkaPanjang = targetWaktuKeputusanJangkaPanjang;
    }
}
