package id.wika.cpptkponline.data;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by mamang_kompii on 9/4/17.
 */

@Entity(tableName = "data_approval", foreignKeys = {
        @ForeignKey(entity = CPPTKPForm.class,
                parentColumns = "formId",
                childColumns = "relatedFormId",
                onDelete = ForeignKey.CASCADE)})
public class DataApproval implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private int relatedFormId;

    @Embedded
    private User pembuatKeputusan;

    private DateTime tanggalKeputusan;
    private boolean isApproved = true;
    private ArrayList<String> approvedAttachmentPaths = new ArrayList<>();

    public DataApproval(User pembuatKeputusan, DateTime tanggalKeputusan, boolean isApproved) {
        this.pembuatKeputusan = pembuatKeputusan;
        this.tanggalKeputusan = tanggalKeputusan;
        this.isApproved = isApproved;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRelatedFormId() {
        return relatedFormId;
    }

    public void setRelatedFormId(int relatedFormId) {
        this.relatedFormId = relatedFormId;
    }

    public User getPembuatKeputusan() {
        return pembuatKeputusan;
    }

    public void setPembuatKeputusan(User pembuatKeputusan) {
        this.pembuatKeputusan = pembuatKeputusan;
    }

    public DateTime getTanggalKeputusan() {
        return tanggalKeputusan;
    }

    public void setTanggalKeputusan(DateTime tanggalKeputusan) {
        this.tanggalKeputusan = tanggalKeputusan;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }

    public ArrayList<String> getApprovedAttachmentPaths() {
        return approvedAttachmentPaths;
    }

    public void setApprovedAttachmentPaths(ArrayList<String> approvedAttachmentPaths) {
        this.approvedAttachmentPaths = approvedAttachmentPaths;
    }
}
