package id.wika.cpptkponline.data;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mamang_kompii on 9/3/17.
 */

public class UserRole {

    public static final String ROLE_COMMONER = "PENEMU";
    public static final String ROLE_QA = "QA";
    public static final String ROLE_PROD = "PRODUKSI";
    public static final String ROLE_MP = "MP";

    public static List<String> getRoleList() {
        return Arrays.asList(
                ROLE_COMMONER,
                ROLE_QA,
                ROLE_PROD,
                ROLE_MP);
    }

    public static String getRoleDescription(String roleConst) {
        switch (roleConst) {
            case ROLE_QA:
                return "Divisi QA";
            case ROLE_PROD:
                return "Divisi Produksi";
            case ROLE_MP:
                return "Manajer Produk";
            case ROLE_COMMONER:
                return "Umum";
            default:
                return "";
        }
    }
}
