package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mamang_kompii on 9/7/17.
 */

public class LingkupTemuanResponse {

    @SerializedName("kode_temuan")
    private String kode_temuan;

    @SerializedName("nama")
    private String nama;

    @SerializedName("jenis_temuan")
    private String jenis_temuan;

    private String status;

    public LingkupTemuanResponse() {

    }

    public LingkupTemuanResponse(String kode_temuan, String nama, String jenis_temuan) {
        this.kode_temuan = kode_temuan;
        this.jenis_temuan= jenis_temuan;
        this.nama= nama;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJenis_temuan() {
        return jenis_temuan;
    }

    public void setKode_temuan(String kode_temuan) {
        this.kode_temuan = kode_temuan;
    }

    public String getNama() {
        return nama;
    }

    public String getKode_temuan() {
        return kode_temuan;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setJenis_temuan(String jenis_temuan) {
        this.jenis_temuan = jenis_temuan;
    }
}
