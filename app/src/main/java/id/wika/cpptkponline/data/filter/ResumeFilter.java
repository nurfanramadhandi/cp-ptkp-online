package id.wika.cpptkponline.data.filter;

import java.util.List;

import id.wika.cpptkponline.data.FormSummary;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 18/September/2017 - 18:42
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public interface ResumeFilter {
    List<FormSummary> filter(List<FormSummary> formSummaries);
}
