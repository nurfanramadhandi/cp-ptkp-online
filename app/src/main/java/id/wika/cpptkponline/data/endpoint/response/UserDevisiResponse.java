package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class UserDevisiResponse {

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("name")
    private String name;

    @SerializedName("jabatan")
    private String jabatan;

    @SerializedName("username")
    private String username;


    public UserDevisiResponse(String user_id, String name, String username, String jabatan) {
        this.user_id = user_id;
        this.name = name;
        this.username = username;
        this.jabatan= jabatan;
    }


    public UserDevisiResponse() {
    }

    public String getUsername() {
        return username;
    }

    public String getJabatan() {
        return jabatan;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getName() {
        return name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }


    public void setName(String name) {
        this.name = name;
    }
}
