package id.wika.cpptkponline.data.endpoint;

import java.util.List;

import id.wika.cpptkponline.data.endpoint.response.PieCpResponse;
import id.wika.cpptkponline.data.endpoint.response.PieRootCauseResponse;
import id.wika.cpptkponline.data.endpoint.response.ProyekResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public interface PieEndpointInterface {

    @GET("grafik")
    Observable<PieCpResponse> getPie(
            @Header("Authorization") String auth,
            @Query("pie") String pie,
            @Query("year") int year
    );

    @GET("grafik")
    Observable<PieRootCauseResponse> getPieRootCause(
            @Header("Authorization") String auth,
            @Query("pie") String pie,
            @Query("year") int year

    );


}
