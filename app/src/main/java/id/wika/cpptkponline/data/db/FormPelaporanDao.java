package id.wika.cpptkponline.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import id.wika.cpptkponline.data.DataFormPelaporan;

/**
 * Created by mamang_kompii on 9/9/17.
 */

@Dao
public interface FormPelaporanDao {

    @Insert
    void insertForm(DataFormPelaporan dataFormPelaporan);

    @Update
    void updateForm(DataFormPelaporan dataFormPelaporan);

    @Query("SELECT * FROM form_pelaporan WHERE relatedFormId = :formId")
    DataFormPelaporan getFormDataByRelatedFormId(int formId);
}
