
package id.wika.cpptkponline.data.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class HistoryModel {

    @SerializedName("data")
    private List<Datum> mData;
    @SerializedName("status")
    private String mStatus;

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }
    public class Datum {

        @SerializedName("alert_to")
        private String mAlertTo;
        @SerializedName("bentuk_penyimpangan")
        private String mBentukPenyimpangan;
        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("created_by")
        private String mCreatedBy;
        @SerializedName("divisi")
        private String mDivisi;
        @SerializedName("id")
        private Long mId;
        @SerializedName("lokasi_penyimpangan")
        private String mLokasiPenyimpangan;
        @SerializedName("nama_penemu")
        private String mNamaPenemu;
        @SerializedName("nama_proyek")
        private String mNamaProyek;
        @SerializedName("no_temuan")
        private String mNoTemuan;
        @SerializedName("pic_1")
        private String mPic1;
        @SerializedName("pic_2")
        private String mPic2;
        @SerializedName("pic_3")
        private String mPic3;
        @SerializedName("status")
        private String mStatus;
        @SerializedName("tgl_temuan")
        private String mTglTemuan;
        @SerializedName("updated_at")
        private String mUpdatedAt;
        @SerializedName("updated_by")
        private String mUpdatedBy;

        public String getmNoTemuan() {
            return mNoTemuan;
        }

        public void setmNoTemuan(String mNoTemuan) {
            this.mNoTemuan = mNoTemuan;
        }

        public String getAlertTo() {
            return mAlertTo;
        }

        public void setAlertTo(String alertTo) {
            mAlertTo = alertTo;
        }

        public String getBentukPenyimpangan() {
            return mBentukPenyimpangan;
        }

        public void setBentukPenyimpangan(String bentukPenyimpangan) {
            mBentukPenyimpangan = bentukPenyimpangan;
        }

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getCreatedBy() {
            return mCreatedBy;
        }

        public void setCreatedBy(String createdBy) {
            mCreatedBy = createdBy;
        }

        public String getDivisi() {
            return mDivisi;
        }

        public void setDivisi(String divisi) {
            mDivisi = divisi;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public String getLokasiPenyimpangan() {
            return mLokasiPenyimpangan;
        }

        public void setLokasiPenyimpangan(String lokasiPenyimpangan) {
            mLokasiPenyimpangan = lokasiPenyimpangan;
        }

        public String getNamaPenemu() {
            return mNamaPenemu;
        }

        public void setNamaPenemu(String namaPenemu) {
            mNamaPenemu = namaPenemu;
        }

        public String getNamaProyek() {
            return mNamaProyek;
        }

        public void setNamaProyek(String namaProyek) {
            mNamaProyek = namaProyek;
        }

        public String getPic1() {
            return mPic1;
        }

        public void setPic1(String pic1) {
            mPic1 = pic1;
        }

        public String getPic2() {
            return mPic2;
        }

        public void setPic2(String pic2) {
            mPic2 = pic2;
        }

        public String getPic3() {
            return mPic3;
        }

        public void setPic3(String pic3) {
            mPic3 = pic3;
        }

        public String getStatus() {
            return mStatus;
        }

        public void setStatus(String status) {
            mStatus = status;
        }

        public String getTglTemuan() {
            return mTglTemuan;
        }

        public void setTglTemuan(String tglTemuan) {
            mTglTemuan = tglTemuan;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

        public String getUpdatedBy() {
            return mUpdatedBy;
        }

        public void setUpdatedBy(String updatedBy) {
            mUpdatedBy = updatedBy;
        }

    }
}
