package id.wika.cpptkponline.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import id.wika.cpptkponline.data.DataRealisasiTindakLanjut;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import io.reactivex.Flowable;

/**
 * Created by mamang_kompii on 9/9/17.
 */

@Dao
public interface FormRealisasiTindakLanjutDao {

    @Insert
    void insertForm(DataRealisasiTindakLanjut realisasiTindakLanjut);

    @Update
    void updateForm(DataRealisasiTindakLanjut realisasiTindakLanjut);

    @Query("SELECT * FROM realisasi_tindak_lanjut WHERE relatedFormId = :formId")
    DataRealisasiTindakLanjut getFormDataByRelatedFormId(int formId);
}
