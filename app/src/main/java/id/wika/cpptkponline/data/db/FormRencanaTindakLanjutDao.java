package id.wika.cpptkponline.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataFormPelaporan;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import io.reactivex.Flowable;

/**
 * Created by mamang_kompii on 9/9/17.
 */

@Dao
public interface FormRencanaTindakLanjutDao {

    @Insert
    void insertForm(DataRencanaTindakLanjut rencanaTindakLanjut);

    @Update
    void updateForm(DataRencanaTindakLanjut rencanaTindakLanjut);

    @Query("SELECT * FROM rencana_tindak_lanjut WHERE relatedFormId = :formId")
    DataRencanaTindakLanjut getFormDataByRelatedFormId(int formId);
}
