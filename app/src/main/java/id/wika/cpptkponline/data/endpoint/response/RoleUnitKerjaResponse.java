package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class RoleUnitKerjaResponse {

    public String status;

    @SerializedName("kode_unit_kerja")
    public String kode;

    @SerializedName("nama_unit_kerja")
    public String nama;

    public RoleUnitKerjaResponse(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }
    public RoleUnitKerjaResponse() {
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
