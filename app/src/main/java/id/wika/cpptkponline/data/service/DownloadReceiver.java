package id.wika.cpptkponline.data.service;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 27/September/2017 - 09:12
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public class DownloadReceiver extends ResultReceiver {
    private static final String TAG = DownloadReceiver.class.getSimpleName();
    private ProgressDialog mProgressDialog;
    public DownloadReceiver(Handler handler, ProgressDialog mProgressDialog) {
        super(handler);
        this.mProgressDialog = mProgressDialog;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        Log.i(TAG, "onReceiveResult: "+resultCode+" <>"+resultData.getInt("progress"));
        if (resultCode == DownloadFile.UPDATE_PROGRESS) {
            int progress = resultData.getInt("progress");
            mProgressDialog.setProgress(progress);
        } else if (resultCode == DownloadFile.UPDATE_PROGRESS_DONE){
            mProgressDialog.dismiss();
        }
    }
}
