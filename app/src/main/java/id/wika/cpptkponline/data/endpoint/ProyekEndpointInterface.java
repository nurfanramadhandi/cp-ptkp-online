package id.wika.cpptkponline.data.endpoint;

import java.util.List;

import id.wika.cpptkponline.data.endpoint.response.ProyekResponse;
import id.wika.cpptkponline.data.model.Proyek;
import io.reactivex.Observable;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public interface ProyekEndpointInterface {

    @GET("user/proyek")
    Observable<List<ProyekResponse>> getProyek(
            @Header("Authorization") String auth
    );
}
