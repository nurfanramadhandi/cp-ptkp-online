package id.wika.cpptkponline.data.filter;

import java.util.ArrayList;
import java.util.List;

import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.FormSummary;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 19/September/2017 - 10:35
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

class FormRTLBatalResumeFilter implements ResumeFilter {
    @Override
    public List<FormSummary> filter(List<FormSummary> formSummaries) {
        List<FormSummary> list = new ArrayList<>();
        for (FormSummary fs : formSummaries){
            if (fs.getStatus() == FormStatus.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN){
                list.add(fs);
            }
        }
        return list;
    }
}
