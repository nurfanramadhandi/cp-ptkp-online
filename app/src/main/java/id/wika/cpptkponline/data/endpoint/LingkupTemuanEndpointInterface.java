package id.wika.cpptkponline.data.endpoint;

import java.util.List;

import id.wika.cpptkponline.data.endpoint.response.LingkupTemuanJenisResponse;
import id.wika.cpptkponline.data.endpoint.response.LingkupTemuanResponse;
import id.wika.cpptkponline.data.model.LingkupTemuan;
import id.wika.cpptkponline.data.model.LingkupTemuanJenis;
import id.wika.cpptkponline.data.model.RoleUnitKerja;
import io.reactivex.Observable;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public interface LingkupTemuanEndpointInterface {

    @GET("lingkup_temuan_jenis")
    Observable<List<LingkupTemuanJenisResponse>> getLingkupTemuanJenis(
            @Header("Authorization") String auth
    );

    @GET("lingkup_temuan")
    Observable<List<LingkupTemuanResponse>> getLingkupTemuan(
            @Header("Authorization") String auth,
            @Query("jenis") String jenis
    );



}
