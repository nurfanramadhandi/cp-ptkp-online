package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by mamang_kompii on 9/10/17.
 */

public class GetFormByIdResponse {

    public String status;
    public Data data;

    public class Data {
        public int id;
        public String alertTo;

        // FORM TEMUAN
        public String namaPenemu;
        public String divisiPenemu;
        public DateTime tglTemuan;
        public String namaProyek;
        public String lokasiPenyimpangan;
        public String bentukPenyimpangan;
        public String status;
        public String kategoriForm;
        public String lingkupTemuan;
        public String struktur;
        public List<PictureResponse> picture;
        public String noTemuan;

        // FORM RENCANA TINDAK LANJUT
        @SerializedName("penanggung_jawab")
        public String namaPenanggungJawab;
        @SerializedName("divisi_penanggung_jawab")
        public String divisiPj;
        public String jabatan;
        public String penyebabUtamaPenyimpangan;
        public String keputusanJangkaPendek;
        public String detailKeputusanJangkaPendek;
        public DateTime targetWaktuJangkaPendek;
        public String keputusanJangkaPanjang;
        public DateTime targetWaktuJangkaPanjang;

        // FORM APPROVAL
        public String pembuatKeputusan;
        public DateTime tglKeputusan;
        public String divisiPembuatKeputusan;
        public int disetujui;

        // FORM LAPORAN HASIL TINDAK LANJUT
        @SerializedName("hasil_tindak_lanjut")
        public String hasilPencegahanDanPerbaikan;
        public int progres;

        // FORM VERIFIKASI HASIL
        @SerializedName("nama_verifikator")
        public String namaVerifikator;
        @SerializedName("is_close")
        public int isclosed;
        @SerializedName("root_cuase")
        public String rootCause;
        @SerializedName("root_cause_category")
        public String rootCauseCartegory;
        @SerializedName("tgl_verifikasi")
        public DateTime tglVerifikasi;


    }
}
