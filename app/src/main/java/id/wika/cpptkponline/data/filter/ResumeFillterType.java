package id.wika.cpptkponline.data.filter;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 18/September/2017 - 18:42
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public enum ResumeFillterType {
    ALL,

    FORM_AWAL,

    FORM_PENGECEKAN,

    FORM_RENCANA_TINDAK_LANJUT_DISETUJUI,

    FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN,

    FORM_RENCANA_TINDAK_LANJUT,

    FORM_REALISASI_TINDAK_LANJUT,

    FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT,

    LAPORAN_TINDAK_LANJUT_STATUS_OPEN,

    LAPORAN_TINDAK_LANJUT_STATUS_CLOSED
}
