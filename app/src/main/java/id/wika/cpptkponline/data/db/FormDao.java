package id.wika.cpptkponline.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.FormSummary;
import id.wika.cpptkponline.data.model.RemoteFormState;
import io.reactivex.Flowable;

/**
 * Created by mamang_kompii on 9/9/17.
 */

@Dao
public interface FormDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertForm(CPPTKPForm cpptkpForm);

    @Update
    void updateForm(CPPTKPForm cpptkpForm);

    @Query("SELECT * FROM forms")
    Flowable<List<CPPTKPForm>> getForm();

    @Query("SELECT * FROM forms WHERE formId = :formId")
    CPPTKPForm getFormById(int formId);

    @Query("SELECT formId, " +
            "fp.namaProyek AS title, " +
            "fp.noTemuan AS noTemuan, " +
            "fp.lokasiPenyimpangan AS subTitle, " +
            "fp.bentukPenyimpangan AS summary, " +
            "formStatus as status, " +
            "isNeedResponse as isNeedAttention, " +
            "timestamp, " +
            "remoteFormId " +
            "FROM forms " +
            "LEFT JOIN form_pelaporan AS fp ON forms.formId = fp.relatedFormId")
    Flowable<List<FormSummary>> getFormSummary();

    @Query("SELECT formId as localFormId, remoteFormId as formId, remoteFormStatus as formStatus FROM forms")
    Flowable<List<RemoteFormState>> getSavedRemoteFormState();

    @Query("UPDATE forms SET formStatus = :status, remoteFormStatus = :remoteStatus WHERE formId = :localFormId")
    void updateFormStatus(int localFormId, int status, String remoteStatus);

    @Query("DELETE FROM forms")
    int clearData();
}
