package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class PieRootCauseResponse {

    @SerializedName("data")
    private List<DataRoot> mData;
    @SerializedName("status")
    private String mStatus;

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public List<DataRoot> getmData() {
        return mData;
    }

    public void setmData(List<DataRoot> mData) {
        this.mData = mData;
    }

    public class DataRoot {

        @SerializedName("root_cause_category")
        private String root_cause_category;

        @SerializedName("jumlah")
        private int jumlah;

        public DataRoot(String root_cause_category, int jumlah) {
            this.root_cause_category = root_cause_category;
            this.jumlah = jumlah;
        }

        public String getRoot_cause_category() {
            return root_cause_category;
        }

        public void setRoot_cause_category(String root_cause_category) {
            this.root_cause_category = root_cause_category;
        }

        public int getJumlah() {
            return jumlah;
        }

        public void setJumlah(int jumlah) {
            this.jumlah = jumlah;
        }
    }
}
