package id.wika.cpptkponline.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class RootCause {

    @SerializedName("kode")
    private String kode;

    @SerializedName("jenis")
    private String jenis;

    @SerializedName(value = "nama", alternate = "nama_unit_kerja")
    private String nama;

    public RootCause(String kode, String jenis, String nama) {
        this.kode = kode;
        this.nama = nama;
        this.jenis= jenis;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
