package id.wika.cpptkponline.data;

import android.os.Parcel;
import android.os.Parcelable;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 24/09/17 - 9:51
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/
public class PieChartData implements Parcelable{
    private String lable;
    private float value;

    public PieChartData(String lable, float value) {
        this.lable = lable;
        this.value = value;
    }

    protected PieChartData(Parcel in) {
        lable = in.readString();
        value = in.readFloat();
    }

    public static final Creator<PieChartData> CREATOR = new Creator<PieChartData>() {
        @Override
        public PieChartData createFromParcel(Parcel in) {
            return new PieChartData(in);
        }

        @Override
        public PieChartData[] newArray(int size) {
            return new PieChartData[size];
        }
    };

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(lable);
        parcel.writeFloat(value);
    }
}
