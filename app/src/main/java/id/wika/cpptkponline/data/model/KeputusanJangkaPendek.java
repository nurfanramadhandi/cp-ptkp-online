package id.wika.cpptkponline.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/7/17.
 */

public class KeputusanJangkaPendek {

    private String description;
    private boolean isNeedDetail;
    private String detailDescription;

    public KeputusanJangkaPendek(String description) {
        this.description = description;
        this.isNeedDetail = false;
    }

    public KeputusanJangkaPendek(String description, String detailDescription) {
        this.description = description;
        this.isNeedDetail = true;
        this.detailDescription = detailDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isNeedDetail() {
        return isNeedDetail;
    }

    public void setNeedDetail(boolean needDetail) {
        isNeedDetail = needDetail;
    }

    public String getDetailDescription() {
        return detailDescription;
    }

    public void setDetailDescription(String detailDescription) {
        this.detailDescription = detailDescription;
    }
}
