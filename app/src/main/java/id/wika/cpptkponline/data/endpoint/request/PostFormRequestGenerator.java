package id.wika.cpptkponline.data.endpoint.request;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataApproval;
import id.wika.cpptkponline.data.DataFormPelaporan;
import id.wika.cpptkponline.data.DataRealisasiTindakLanjut;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import id.wika.cpptkponline.data.DataVerifikasiHasilTindakLanjut;
import id.wika.cpptkponline.data.db.FormApprovalDao;
import id.wika.cpptkponline.data.db.FormDao;
import id.wika.cpptkponline.data.db.FormPelaporanDao;
import id.wika.cpptkponline.data.db.FormRealisasiTindakLanjutDao;
import id.wika.cpptkponline.data.db.FormRencanaTindakLanjutDao;
import id.wika.cpptkponline.data.db.FormVerifikasiHasilDao;
import id.wika.cpptkponline.data.endpoint.FormEndpointInterface;
import id.wika.cpptkponline.data.endpoint.TokenWrapper;
import id.wika.cpptkponline.data.endpoint.response.PictureResponse;
import id.wika.cpptkponline.data.endpoint.response.PostTemuanResponse;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by mamang_kompii on 9/11/17.
 */

public class                                                                                                                     PostFormRequestGenerator {

    public enum Strategy {
        POST_TEMUAN,
        POST_REVISI_TINDAK_LANJUT,
        POST_TEMUAN_QA,
        POST_PENGECEKAN,
        POST_STATUS,
        POST_RENCANA_TINDAK_LANJUT,
        POST_REALISASI_TINDAK_LANJUT,
        POST_APPROVAL
    }

    private FormDao formDao;
    private FormPelaporanDao formPelaporanDao;
    private FormRencanaTindakLanjutDao formRencanaTindakLanjutDao;
    private FormRealisasiTindakLanjutDao formRealisasiTindakLanjutDao;
    private FormEndpointInterface formEndpointInterface;
    private FormApprovalDao formApprovalDao;
    private FormVerifikasiHasilDao formVerifikasiHasilDao;

    private String token;

    public PostFormRequestGenerator(
            String token,
            FormEndpointInterface formEndpointInterface,
            FormDao formDao,
            FormApprovalDao formApprovalDao,
            FormPelaporanDao formPelaporanDao,
            FormRencanaTindakLanjutDao formRencanaTindakLanjutDao,
            FormRealisasiTindakLanjutDao formRealisasiTindakLanjutDao,
            FormVerifikasiHasilDao formVerifikasiHasilDao
    ) {
        this.formDao = formDao;
        this.formPelaporanDao = formPelaporanDao;
        this.formApprovalDao = formApprovalDao;
        this.formRencanaTindakLanjutDao = formRencanaTindakLanjutDao;
        this.formEndpointInterface = formEndpointInterface;
        this.token = token;
        this.formRealisasiTindakLanjutDao = formRealisasiTindakLanjutDao;
        this.formVerifikasiHasilDao = formVerifikasiHasilDao;
    }

    public Observable createSendFormRequest(final CPPTKPForm cpptkpForm, Strategy strategy) {
        switch (strategy) {
            case POST_TEMUAN:
                return createPostTemuanRequest(cpptkpForm);
            case POST_TEMUAN_QA:
                return createPostTemuanQARequest(cpptkpForm);
            case POST_PENGECEKAN:
                return createPostPengecekan(cpptkpForm);
            case POST_RENCANA_TINDAK_LANJUT:
                return createPostRencanaTindakLanjut(cpptkpForm);
            case POST_APPROVAL:
                return createPostApproval(cpptkpForm);
            case POST_REALISASI_TINDAK_LANJUT:
                return createPostRealisasiTindakLanjut(cpptkpForm);
            case POST_REVISI_TINDAK_LANJUT:
                return Observable.merge(
                        createPostRevisiRencanaTindakLanjut(cpptkpForm),
                        createPostRevisiRealisasiTindakLanjut(cpptkpForm)
                );
            case POST_STATUS:
                return createPostVerifikasiTindakLanjut(cpptkpForm);
            default:
                return Observable.error(new Exception("Request hasn't implemented yet!"));
        }
    }

    private Observable createPostTemuanRequest(final CPPTKPForm cpptkpForm) {
        DataFormPelaporan formPelaporan = cpptkpForm.getFormPelaporan();

        return formEndpointInterface.postTemuanAwal(
                TokenWrapper.getWrappedToken(token),
                formPelaporan.getPenemu().getFullname(),
                formPelaporan.getBentukPenyimpangan(),
                formPelaporan.getNamaProyek(),
                formPelaporan.getPenemu().getDivisi(),
                formPelaporan.getLokasiPenyimpangan())
                .filter(new Predicate<PostTemuanResponse>() {
                    @Override
                    public boolean test(PostTemuanResponse postTemuanResponse) throws Exception {
                        return postTemuanResponse.getStatus().equals("success");
                    }
                })
                .map(new Function<PostTemuanResponse, CPPTKPForm>() {
                    @Override
                    public CPPTKPForm apply(PostTemuanResponse postTemuanResponse) throws Exception {
                        cpptkpForm.setRemoteFormId(postTemuanResponse.getIdTemuan());
                        return cpptkpForm;
                    }
                })
                .flatMap(new Function<CPPTKPForm, ObservableSource<CPPTKPForm>>() {
                    @Override
                    public ObservableSource<CPPTKPForm> apply(final CPPTKPForm cpptkpForm) throws Exception {
                        int remoteFormId = cpptkpForm.getRemoteFormId();

                        ArrayList<String> attachmentPaths = cpptkpForm.getFormPelaporan().getAttachmentPaths();
                        return formEndpointInterface.uploadFoto(
                                TokenWrapper.getWrappedToken(token),
                                remoteFormId,
                                PictureResponse.ATTACHMENT_TYPE_DRAFT,
                                createRequestBodyList(attachmentPaths)
                        ).map(new Function<Object, CPPTKPForm>() {
                            @Override
                            public CPPTKPForm apply(Object o) throws Exception {
                                return cpptkpForm;
                            }
                        });
                    }
                })
                .doOnNext(new Consumer<CPPTKPForm>() {
                    @Override
                    public void accept(CPPTKPForm cpptkpForm) throws Exception {
                        long formId = formDao.insertForm(cpptkpForm);
                        cpptkpForm.setFormId((int) formId);

                        DataFormPelaporan formPelaporan = cpptkpForm.getFormPelaporan();
                        formPelaporan.setRelatedFormId((int) formId);
                        formPelaporanDao.insertForm(formPelaporan);
                    }
                });
    }

    private List<MultipartBody.Part> createRequestBodyList(ArrayList<String> attachmentPaths) {
        List<MultipartBody.Part> requests = new ArrayList<>();

        for (String path : attachmentPaths) {
            requests.add(createARequestBody(path));
        }

        return requests;
    }

    private MultipartBody.Part createARequestBody(String path) {
        MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
        File file = new File(path);
        RequestBody requestBody = RequestBody.create(MEDIA_TYPE_PNG, file);
        return MultipartBody.Part.createFormData("imgUploader", file.getName(), requestBody);
    }

    private Observable createPostTemuanQARequest(final CPPTKPForm cpptkpForm) {
        DataFormPelaporan formPelaporan = cpptkpForm.getFormPelaporan();
        DataRencanaTindakLanjut rencanaTindakLanjut = cpptkpForm.getRencanaTindakLanjut();
        if (rencanaTindakLanjut.getPenanggungJawab().jabatan == null) {
            rencanaTindakLanjut.getPenanggungJawab().setJabatan("staff");
        }
        return formEndpointInterface.postTemuanQA(
                TokenWrapper.getWrappedToken(token),
                formPelaporan.getPenemu().getFullname(),
                formPelaporan.getBentukPenyimpangan(),
                formPelaporan.getNamaProyek(),
                formPelaporan.getPenemu().getDivisi(),
                formPelaporan.getLokasiPenyimpangan(),
                formPelaporan.getKategoriForm(),
                formPelaporan.getLingkupTemuan(),
                formPelaporan.getDetailLingkupTemuan(),
                rencanaTindakLanjut.getPenanggungJawab().getFullname(),
                rencanaTindakLanjut.getPenanggungJawab().divisi,
                rencanaTindakLanjut.getPenanggungJawab().jabatan)
                .filter(new Predicate<PostTemuanResponse>() {
                    @Override
                    public boolean test(PostTemuanResponse postTemuanResponse) throws Exception {
                        return postTemuanResponse.getStatus().equals("success");
                    }
                })
                .map(new Function<PostTemuanResponse, CPPTKPForm>() {
                    @Override
                    public CPPTKPForm apply(PostTemuanResponse postTemuanResponse) throws Exception {
                        cpptkpForm.setRemoteFormId(postTemuanResponse.getIdTemuan());
                        return cpptkpForm;
                    }
                })
                .flatMap(new Function<CPPTKPForm, ObservableSource<CPPTKPForm>>() {
                    @Override
                    public ObservableSource<CPPTKPForm> apply(final CPPTKPForm cpptkpForm) throws Exception {
                        int remoteFormId = cpptkpForm.getRemoteFormId();

                        ArrayList<String> attachmentPaths = cpptkpForm.getFormPelaporan().getAttachmentPaths();
                        return formEndpointInterface.uploadFoto(
                                TokenWrapper.getWrappedToken(token),
                                remoteFormId,
                                PictureResponse.ATTACHMENT_TYPE_DRAFT,
                                createRequestBodyList(attachmentPaths)
                        ).map(new Function<Object, CPPTKPForm>() {
                            @Override
                            public CPPTKPForm apply(Object o) throws Exception {
                                return cpptkpForm;
                            }
                        });
                    }
                })
                .doOnNext(new Consumer<CPPTKPForm>() {
                    @Override
                    public void accept(CPPTKPForm cpptkpForm) throws Exception {
                        long formId = formDao.insertForm(cpptkpForm);
                        cpptkpForm.setFormId((int) formId);

                        DataFormPelaporan formPelaporan = cpptkpForm.getFormPelaporan();
                        formPelaporan.setRelatedFormId((int) formId);
                        formPelaporanDao.insertForm(formPelaporan);

                        DataRencanaTindakLanjut rencanaTindakLanjut = cpptkpForm.getRencanaTindakLanjut();
                        rencanaTindakLanjut.setRelatedFormId((int) formId);
                        formRencanaTindakLanjutDao.insertForm(rencanaTindakLanjut);
                    }
                });
    }

    private Observable createPostPengecekan(final CPPTKPForm cpptkpForm) {
        DataFormPelaporan formPelaporan = cpptkpForm.getFormPelaporan();
        DataRencanaTindakLanjut rencanaTindakLanjut = cpptkpForm.getRencanaTindakLanjut();
        if (rencanaTindakLanjut.getPenanggungJawab().jabatan == null) {
            rencanaTindakLanjut.getPenanggungJawab().setJabatan("staff");
        }

        return formEndpointInterface.postPengecekan(
                TokenWrapper.getWrappedToken(token),
                cpptkpForm.getRemoteFormId(),
                formPelaporan.getKategoriForm(),
                formPelaporan.getLingkupTemuan(),
                formPelaporan.getDetailLingkupTemuan(),
                rencanaTindakLanjut.getPenanggungJawab().getFullname(),
                rencanaTindakLanjut.getPenanggungJawab().divisi,
                rencanaTindakLanjut.getPenanggungJawab().jabatan)
                .filter(new Predicate<PostTemuanResponse>() {
                    @Override
                    public boolean test(PostTemuanResponse postTemuanResponse) throws Exception {
                        return postTemuanResponse.getStatus().equals("success");
                    }
                })
                .map(new Function<PostTemuanResponse, CPPTKPForm>() {
                    @Override
                    public CPPTKPForm apply(PostTemuanResponse postTemuanResponse) throws Exception {
                        cpptkpForm.setRemoteFormId(postTemuanResponse.getIdTemuan());
                        return cpptkpForm;
                    }
                })
                .doOnNext(new Consumer<CPPTKPForm>() {
                    @Override
                    public void accept(CPPTKPForm cpptkpForm) throws Exception {
                        formDao.updateForm(cpptkpForm);

                        DataFormPelaporan formPelaporan = cpptkpForm.getFormPelaporan();
                        formPelaporanDao.updateForm(formPelaporan);

                        formRencanaTindakLanjutDao.insertForm(cpptkpForm.getRencanaTindakLanjut());
                    }
                });
    }

    private Observable createPostRencanaTindakLanjut(final CPPTKPForm cpptkpForm) {
        DataRencanaTindakLanjut rencanaTindakLanjut = cpptkpForm.getRencanaTindakLanjut();
        return formEndpointInterface.postRencanaTindakLanjut(
                TokenWrapper.getWrappedToken(token),
                cpptkpForm.getRemoteFormId(),
                0,
                rencanaTindakLanjut.getPenyebabUtamaPenyimpangan(),
                rencanaTindakLanjut.getKeputusanJangkaPanjang(),
                rencanaTindakLanjut.getTargetWaktuKeputusanJangkaPanjang(),
                rencanaTindakLanjut.getKeputusanJangkaPendek(),
                rencanaTindakLanjut.getTargetWaktuKeputusanJangkaPendek(),
                rencanaTindakLanjut.getDetailKeputusanJangkaPendek())
                .filter(new Predicate<PostTemuanResponse>() {
                    @Override
                    public boolean test(PostTemuanResponse postTemuanResponse) throws Exception {
                        return postTemuanResponse.getStatus().equals("success");
                    }
                })
                .doOnNext(new Consumer<PostTemuanResponse>() {
                    @Override
                    public void accept(PostTemuanResponse postTemuanResponse) throws Exception {
                        formDao.updateForm(cpptkpForm);

                        formRencanaTindakLanjutDao.insertForm(cpptkpForm.getRencanaTindakLanjut());
                    }
                });
    }

    private Observable createPostVerifikasiTindakLanjut(final CPPTKPForm cpptkpForm) {
        DataVerifikasiHasilTindakLanjut rencanaTindakLanjut = cpptkpForm.getVerifikasiHasilTindakLanjut();
        return formEndpointInterface.postStatus(
                TokenWrapper.getWrappedToken(token),
                cpptkpForm.getRemoteFormId(),
                rencanaTindakLanjut.getProgres(),
                rencanaTindakLanjut.getRootCauseValue(),
                rencanaTindakLanjut.getRootCauseCategory(),
                booltoStatus(rencanaTindakLanjut.isStatusOpened()),
                rencanaTindakLanjut.getVerifikator().getFullname(),
                rencanaTindakLanjut.getTanggalVerifikasi()
        ).filter(new Predicate<PostTemuanResponse>() {
            @Override
            public boolean test(PostTemuanResponse postTemuanResponse) throws Exception {
                return postTemuanResponse.getStatus().equals("success");
            }
        }).doOnNext(new Consumer<PostTemuanResponse>() {
            @Override
            public void accept(PostTemuanResponse postTemuanResponse) throws Exception {
                formDao.updateForm(cpptkpForm);

                formVerifikasiHasilDao.insertForm(cpptkpForm.getVerifikasiHasilTindakLanjut());
            }
        });
    }

    private int booltoInt(boolean status) {
        if (status) return 1;
        else return 0;
    }

    private int booltoStatus(boolean status) {
        if (status) return 0;
        else return 1;
    }

    private Observable createPostApproval(final CPPTKPForm cpptkpForm) {
        DataApproval dataApproval = cpptkpForm.getApproval();

        String[] arrayFileName = new String[]{"", "", ""};

        ArrayList<String> approvedAttachmentPaths = dataApproval.getApprovedAttachmentPaths();
        for (int i = 0; i < approvedAttachmentPaths.size(); i++) {
            String url = approvedAttachmentPaths.get(i);
            String fileName = url.substring(url.lastIndexOf('/') + 1);
            arrayFileName[i] = fileName;
        }

        return formEndpointInterface.postApproval(
                TokenWrapper.getWrappedToken(token),
                cpptkpForm.getRemoteFormId(),
                dataApproval.getPembuatKeputusan().getFullname(),
                dataApproval.getPembuatKeputusan().getDivisi(),
                booltoInt(dataApproval.isApproved()),
                arrayFileName[0],
                arrayFileName[1],
                arrayFileName[2]
        )
                .filter(new Predicate<PostTemuanResponse>() {
                    @Override
                    public boolean test(PostTemuanResponse postTemuanResponse) throws Exception {
                        return postTemuanResponse.getStatus().equals("success");
                    }
                })
                .doOnNext(new Consumer<PostTemuanResponse>() {
                    @Override
                    public void accept(PostTemuanResponse postTemuanResponse) throws Exception {
                        formDao.updateForm(cpptkpForm);
                        formApprovalDao.insertForm(cpptkpForm.getApproval());
                    }
                });
    }

    private Observable createPostRealisasiTindakLanjut(final CPPTKPForm cpptkpForm) {
        DataRealisasiTindakLanjut realisasiTindakLanjut = cpptkpForm.getRealisasiTindakLanjut();
        return formEndpointInterface.postRealisasiTindakLanjut(
                TokenWrapper.getWrappedToken(token),
                cpptkpForm.getRemoteFormId(),
                0,
                realisasiTindakLanjut.getHasilPencegahanDanPerbaikan())
                .filter(new Predicate<PostTemuanResponse>() {
                    @Override
                    public boolean test(PostTemuanResponse postTemuanResponse) throws Exception {
                        return postTemuanResponse.getStatus().equals("success");
                    }
                })
                .flatMap(new Function<PostTemuanResponse, ObservableSource<Object>>() {
                    @Override
                    public ObservableSource<Object> apply(PostTemuanResponse postTemuanResponse) throws Exception {
                        int remoteFormId = postTemuanResponse.getIdTemuan();

                        ArrayList<String> attachmentPaths = cpptkpForm.getRealisasiTindakLanjut().getListDokumentasiHasil();
                        return formEndpointInterface.uploadFoto(
                                TokenWrapper.getWrappedToken(token),
                                remoteFormId,
                                PictureResponse.ATTACHMENT_TYPE_HASIL,
                                createRequestBodyList(attachmentPaths)
                        );
                    }
                })
                .doOnNext(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {
                        formDao.updateForm(cpptkpForm);
                        DataRealisasiTindakLanjut realisasiTindakLanjut = cpptkpForm.getRealisasiTindakLanjut();
                        formRealisasiTindakLanjutDao.insertForm(realisasiTindakLanjut);
                    }
                });
    }

    private Observable createPostRevisiRealisasiTindakLanjut(final CPPTKPForm cpptkpForm) {

        DataRealisasiTindakLanjut realisasiTindakLanjut = cpptkpForm.getRealisasiTindakLanjut();

        return formEndpointInterface.postRealisasiTindakLanjut(
                TokenWrapper.getWrappedToken(token),
                cpptkpForm.getRemoteFormId(), 1,
                realisasiTindakLanjut.getHasilPencegahanDanPerbaikan())
                .filter(new Predicate<PostTemuanResponse>() {
                    @Override
                    public boolean test(PostTemuanResponse postTemuanResponse) throws Exception {
                        return postTemuanResponse.getStatus().equals("success");
                    }
                })
                .flatMap(new Function<PostTemuanResponse, ObservableSource<Object>>() {
                    @Override
                    public ObservableSource<Object> apply(PostTemuanResponse postTemuanResponse) throws Exception {
                        int remoteFormId = postTemuanResponse.getIdTemuan();

                        ArrayList<String> attachmentPaths = cpptkpForm.getRealisasiTindakLanjut().getListDokumentasiHasil();
                        return formEndpointInterface.uploadFoto(
                                TokenWrapper.getWrappedToken(token),
                                remoteFormId,
                                PictureResponse.ATTACHMENT_TYPE_REVISI,
                                createRequestBodyList(attachmentPaths)
                        );
                    }
                })
                .doOnNext(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {
                        formDao.updateForm(cpptkpForm);
                        DataRealisasiTindakLanjut realisasiTindakLanjut = cpptkpForm.getRealisasiTindakLanjut();
                        formRealisasiTindakLanjutDao.updateForm(realisasiTindakLanjut);
                    }
                });
    }

    private Observable createPostRevisiRencanaTindakLanjut(final CPPTKPForm cpptkpForm) {

        DataRencanaTindakLanjut rencanaTindakLanjut = cpptkpForm.getRencanaTindakLanjut();

        return formEndpointInterface.postRencanaTindakLanjut(
                TokenWrapper.getWrappedToken(token),
                cpptkpForm.getRemoteFormId(),
                1,
                rencanaTindakLanjut.getPenyebabUtamaPenyimpangan(),
                rencanaTindakLanjut.getKeputusanJangkaPanjang(),
                rencanaTindakLanjut.getTargetWaktuKeputusanJangkaPanjang(),
                rencanaTindakLanjut.getKeputusanJangkaPendek(),
                rencanaTindakLanjut.getTargetWaktuKeputusanJangkaPendek(),
                rencanaTindakLanjut.getDetailKeputusanJangkaPendek())
                .filter(new Predicate<PostTemuanResponse>() {
                    @Override
                    public boolean test(PostTemuanResponse postTemuanResponse) throws Exception {
                        return postTemuanResponse.getStatus().equals("success");
                    }
                })
                .doOnNext(new Consumer<PostTemuanResponse>() {
                    @Override
                    public void accept(PostTemuanResponse postTemuanResponse) throws Exception {
                        formDao.updateForm(cpptkpForm);
                        formRencanaTindakLanjutDao.updateForm(cpptkpForm.getRencanaTindakLanjut());
                    }
                });

    }
}
