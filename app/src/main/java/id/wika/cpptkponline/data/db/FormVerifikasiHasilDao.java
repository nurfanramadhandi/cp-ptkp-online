package id.wika.cpptkponline.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import id.wika.cpptkponline.data.DataVerifikasiHasilTindakLanjut;

/**
 * Created by mamang_kompii on 9/9/17.
 */

@Dao
public interface FormVerifikasiHasilDao {

    @Insert
    void insertForm(DataVerifikasiHasilTindakLanjut verifikasiHasilTindakLanjut);

    @Update
    void updateForm(DataVerifikasiHasilTindakLanjut verifikasiHasilTindakLanjut);

    @Query("SELECT * FROM data_verifikasi_hasil WHERE relatedFormId = :formId")
    DataVerifikasiHasilTindakLanjut getFormDataByRelatedFormId(int formId);
}
