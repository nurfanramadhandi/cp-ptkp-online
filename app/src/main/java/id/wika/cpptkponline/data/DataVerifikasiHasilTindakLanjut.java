package id.wika.cpptkponline.data;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;

import id.wika.cpptkponline.helper.DateTimeHelper;

/**
 * Created by mamang_kompii on 9/4/17.
 */

@Entity(tableName = "data_verifikasi_hasil", foreignKeys = {
        @ForeignKey(entity = CPPTKPForm.class,
                parentColumns = "formId",
                childColumns = "relatedFormId",
                onDelete = ForeignKey.CASCADE)})
public class DataVerifikasiHasilTindakLanjut implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private int relatedFormId;

    @Embedded
    private User verifikator;

    private DateTime tanggalVerifikasi;
    private boolean isStatusOpened;

    private String feedback;
    @Ignore
    private List<String> feedbackAttachments;

    private String rootCauseCategory;
    private String rootCauseValue;
    private int progres;

    public int getProgres() {
        return progres;
    }

    public void setProgres(int progres) {
        this.progres = progres;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRelatedFormId() {
        return relatedFormId;
    }

    public void setRelatedFormId(int relatedFormId) {
        this.relatedFormId = relatedFormId;
    }

    public User getVerifikator() {
        return verifikator;
    }

    public void setVerifikator(User verifikator) {
        this.verifikator = verifikator;
    }

    public DateTime getTanggalVerifikasi() {
        return tanggalVerifikasi;
    }

    public void setTanggalVerifikasi(DateTime tanggalVerifikasi) {
        this.tanggalVerifikasi = tanggalVerifikasi;
    }

    public boolean isStatusOpened() {
        return isStatusOpened;
    }

    public void setStatusOpened(boolean statusOpened) {
        isStatusOpened = statusOpened;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public List<String> getFeedbackAttachments() {
        return feedbackAttachments;
    }

    public void setFeedbackAttachments(List<String> feedbackAttachments) {
        this.feedbackAttachments = feedbackAttachments;
    }

    public String getRootCauseCategory() {
        return rootCauseCategory;
    }

    public void setRootCauseCategory(String rootCauseCategory) {
        this.rootCauseCategory = rootCauseCategory;
    }

    public String getRootCauseValue() {
        return rootCauseValue;
    }

    public void setRootCauseValue(String rootCauseValue) {
        this.rootCauseValue = rootCauseValue;
    }
}
