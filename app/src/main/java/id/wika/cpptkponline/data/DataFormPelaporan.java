package id.wika.cpptkponline.data;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by mamang_kompii on 9/4/17.
 */

@Entity(tableName = "form_pelaporan", foreignKeys = {
        @ForeignKey(entity = CPPTKPForm.class,
                parentColumns = "formId",
                childColumns = "relatedFormId",
                onDelete = ForeignKey.CASCADE)})
public class DataFormPelaporan implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private int relatedFormId;

    @Embedded
    public User penemu;

    private String namaProyek;
    private String noTemuan;
    private DateTime formDate;
    private String lokasiPenyimpangan;
    private String bentukPenyimpangan;
    private String kategoriForm;
    private String lingkupTemuan;
    private String detailLingkupTemuan;
    private ArrayList<String> attachmentPaths = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getPenemu() {
        return penemu;
    }

    public void setPenemu(User penemu) {
        this.penemu = penemu;
    }

    public String getNamaProyek() {
        return namaProyek;
    }

    public void setNamaProyek(String namaProyek) {
        this.namaProyek = namaProyek;
    }

    public DateTime getFormDate() {
        return formDate;
    }

    public void setFormDate(DateTime formDate) {
        this.formDate = formDate;
    }

    public String getLokasiPenyimpangan() {
        return lokasiPenyimpangan;
    }

    public void setLokasiPenyimpangan(String lokasiPenyimpangan) {
        this.lokasiPenyimpangan = lokasiPenyimpangan;
    }

    public String getBentukPenyimpangan() {
        return bentukPenyimpangan;
    }

    public void setBentukPenyimpangan(String bentukPenyimpangan) {
        this.bentukPenyimpangan = bentukPenyimpangan;
    }

    public String getKategoriForm() {
        return kategoriForm;
    }

    public void setKategoriForm(String kategoriForm) {
        this.kategoriForm = kategoriForm;
    }

    public String getLingkupTemuan() {
        return lingkupTemuan;
    }

    public void setLingkupTemuan(String lingkupTemuan) {
        this.lingkupTemuan = lingkupTemuan;
    }

    public String getDetailLingkupTemuan() {
        return detailLingkupTemuan;
    }

    public void setDetailLingkupTemuan(String detailLingkupTemuan) {
        this.detailLingkupTemuan = detailLingkupTemuan;
    }

    public ArrayList<String> getAttachmentPaths() {
        return attachmentPaths;
    }

    public void setAttachmentPaths(ArrayList<String> attachmentPaths) {
        this.attachmentPaths = attachmentPaths;
    }

    public String getNoTemuan() {
        return noTemuan;
    }

    public void setNoTemuan(String noTemuan) {
        this.noTemuan = noTemuan;
    }

    public int getRelatedFormId() {
        return relatedFormId;
    }

    public void setRelatedFormId(int relatedFormId) {
        this.relatedFormId = relatedFormId;
    }
}
