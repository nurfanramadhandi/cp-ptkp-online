package id.wika.cpptkponline.data.endpoint;

import org.joda.time.DateTime;

import java.sql.Date;
import java.util.List;

import id.wika.cpptkponline.data.endpoint.response.GetFormByIdResponse;
import id.wika.cpptkponline.data.endpoint.response.GetFormResponse;
import id.wika.cpptkponline.data.endpoint.response.GetGrafikResponse;
import id.wika.cpptkponline.data.endpoint.response.GetGrafikRootCouseResponse;
import id.wika.cpptkponline.data.endpoint.response.GetListLaporan;
import id.wika.cpptkponline.data.endpoint.response.PostTemuanListResponse;
import id.wika.cpptkponline.data.endpoint.response.PostTemuanResponse;
import id.wika.cpptkponline.data.endpoint.response.RemoteFormSummary;
import id.wika.cpptkponline.data.model.HistoryModel;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public interface FormEndpointInterface {

    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST("temuan")
    Observable<PostTemuanResponse> postTemuanAwal(
            @Header("Authorization") String auth,
            @Field("nama_penemu") String namaPenemu,
            @Field("bentuk_penyimpangan") String bentukPenyimpangan,
            @Field("nama_proyek") String namaProyek,
            @Field("divisi") String divisi,
            @Field("lokasi_penyimpangan") String lokasiPenyimpangan
    );

    @Multipart
    @POST("upload")
    Observable<Object> uploadFoto(
            @Header("Authorization") String auth,
            @Query("id_temuan") int formId,
            @Query("jenis") String keteranganAttachment,
            @Part List<MultipartBody.Part> photos
    );

    @GET("temuan")
    Flowable<GetFormResponse> getForm(
            @Header("Authorization") String auth,
            @Query("proyek") String proyek
    );

    @GET("temuanhistory")
    Observable<HistoryModel> getFormHistory(@Header("Authorization") String auth);

    @GET("temuan")
    Flowable<GetFormByIdResponse> getFormById(@Header("Authorization") String auth, @Query("id") int formId);

    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST("temuan_qa")
    Observable<PostTemuanResponse> postTemuanQA(
            @Header("Authorization") String auth,
            @Field("nama_penemu") String namaPenemu,
            @Field("bentuk_penyimpangan") String bentukPenyimpangan,
            @Field("nama_proyek") String namaProyek,
            @Field("divisi") String divisi,
            @Field("lokasi_penyimpangan") String lokasiPenyimpangan,
            @Field("kategori_form") String kategoriForm,
            @Field("lingkup_temuan") String lingkupTemuan,
            @Field("struktur") String detailLingkupTemuan,
            @Field("penanggung_jawab") String penanggungJawab,
            @Field("divisi_penanggung_jawab") String divisipenanggungJawab,
            @Field("jabatan_penanggung_jawab") String jabatanpenanggungJawab

    );

    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST("temuan_pengecekan")
    Observable<PostTemuanResponse> postPengecekan(
            @Header("Authorization") String auth,
            @Field("id_temuan") int remoteFormId,
            @Field("kategori_form") String kategoriForm,
            @Field("lingkup_temuan") String lingkupTemuan,
            @Field("struktur") String detailLingkupTemuan,
            @Field("penanggung_jawab") String penanggungJawab,
            @Field("divisi_penanggung_jawab") String divisipenanggungJawab,
            @Field("jabatan_penanggung_jawab") String jabatanpenanggungJawab
    );

    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST("temuan_rencana_tindak_lanjut")
    Observable<PostTemuanResponse> postRencanaTindakLanjut(
            @Header("Authorization") String auth,
            @Field("id_temuan") int remoteFormId,
            @Field("isUpdate") int isUpdated,
            @Field("penyebab_utama_penyimpangan") String penyebabUtamaPenyimpangan,
            @Field("keputusan_jangka_panjang") String keputusanJangkaPanjang,
            @Field("target_waktu_jangka_panjang") DateTime targetWaktuJangkaPanjang,
            @Field("keputusan_jangka_pendek") String keputusanJangkaPendek,
            @Field("target_waktu_jangka_pendek") DateTime targetWaktuJangkaPendek,
            @Field("detail_keputusan_jangka_pendek") String detailJangkaPendek
    );

    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST("temuan_approval")
    Observable<PostTemuanResponse> postApproval(
            @Header("Authorization") String auth,
            @Field("id_temuan") int remoteFormId,
            @Field("pembuat_keputusan") String pembuat_keputusan,
            @Field("divisi") String divisi,
            @Field("disetujui") int disetujui,
            @Field("file1") String file1,
            @Field("file2") String file2,
            @Field("file3") String file3
            );

    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST("temuan_realisasi_tindak_lanjut")
    Observable<PostTemuanResponse> postRealisasiTindakLanjut(
            @Header("Authorization") String auth,
            @Field("id_temuan") int remoteFormId,
            @Field("isUpdate") int update,
            @Field("hasil_tindak_lanjut") String hasil_tindak_lanjut
    );

    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST("temuan_verifikasi_tindak_lanjut")
    Observable<PostTemuanResponse> postStatus(
            @Header("Authorization") String auth,
            @Field("id_temuan") int remoteFormId,
            @Field("progres") int progres,
            @Field("root_cause") String root_cuase,
            @Field("category_root_cause") String category_root_cuase,
            @Field("is_close") int is_close,
            @Field("nama_verifikator") String nama_verifikator,
           @Field("tgl_verifikasi") DateTime tgl_verifikasi
    );

    @GET("temuanlist")
    Observable<PostTemuanListResponse> postTemuanList(
            @Header("Authorization") String auth,
            @Query("id") String id
    );

    @GET("listlaporan")
    Observable<GetListLaporan> getLaporanList(
            @Header("Authorization") String auth
    );

    @GET("grafik")
    Observable<GetGrafikResponse> getGrafik(
            @Header("Authorization") String auth,
            @Query("pie") String id,
            @Query("year") int year

    );

    @GET("grafik")
    Observable<GetGrafikRootCouseResponse> getGrafikRoot(
            @Header("Authorization") String auth,
            @Query("pie") String id,
            @Query("year") int year
    );
}
