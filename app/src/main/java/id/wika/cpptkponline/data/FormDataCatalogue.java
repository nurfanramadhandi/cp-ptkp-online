package id.wika.cpptkponline.data;

import android.content.Intent;
import android.graphics.Movie;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import id.wika.cpptkponline.data.endpoint.FormEndpointInterface;
import id.wika.cpptkponline.data.endpoint.KeputusanJangkaPendekEndpointInterface;
import id.wika.cpptkponline.data.endpoint.LingkupTemuanEndpointInterface;
import id.wika.cpptkponline.data.endpoint.LoginEndpointInterface;
import id.wika.cpptkponline.data.endpoint.RootCauseEndpointInterface;
import id.wika.cpptkponline.data.endpoint.TokenWrapper;
import id.wika.cpptkponline.data.endpoint.response.GetFormResponse;
import id.wika.cpptkponline.data.endpoint.response.KeputusanJangkaPendekResponse;
import id.wika.cpptkponline.data.endpoint.response.LingkupTemuanJenisResponse;
import id.wika.cpptkponline.data.endpoint.response.LingkupTemuanResponse;
import id.wika.cpptkponline.data.endpoint.response.ProyekResponse;
import id.wika.cpptkponline.data.endpoint.response.RemoteFormSummary;
import id.wika.cpptkponline.data.endpoint.response.RoleUnitKerjaResponse;
import id.wika.cpptkponline.data.endpoint.response.RootCauseJenisResponse;
import id.wika.cpptkponline.data.endpoint.response.RootCauseResponse;
import id.wika.cpptkponline.data.model.KeputusanJangkaPendek;
import id.wika.cpptkponline.data.model.LingkupTemuan;
import id.wika.cpptkponline.data.model.Proyek;
import id.wika.cpptkponline.data.model.RoleUnitKerja;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.view.SplashScreen;
import id.wika.cpptkponline.view.home.HomeActivity;
import id.wika.cpptkponline.view.login.LoginActivity;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableObserver;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mamang_kompii on 9/7/17.
 */

public class FormDataCatalogue {

    private static final String TAG = "Data";


    static {
        ArrayList<LingkupTemuan> listLingkup = new ArrayList<>();
        List<String> listStruktur = new ArrayList<>();
        listStruktur.add("Struktur Retak");
        listStruktur.add("Struktur Miring");
        listStruktur.add("Kurang Kokoh");
        listLingkup.add(new LingkupTemuan("Struktur", listStruktur));
        listLingkup.add(new LingkupTemuan("Kelengkapan"));
        listLingkup.add(new LingkupTemuan("Kompetensi"));

        listLingkupTemuan = listLingkup;

        List<KeputusanJangkaPendek> listKeputusan = new ArrayList<>();
        listKeputusan.add(new KeputusanJangkaPendek("Digunakan sebagaimana adanya"));
        listKeputusan.add(new KeputusanJangkaPendek("Diperbaiki (repair)", "Metode repair"));
        listKeputusan.add(new KeputusanJangkaPendek("Dikerjakan ulang (rework)", "Metode rework"));
        listKeputusan.add(new KeputusanJangkaPendek(
                "Tidak diperlukan tindakan korektif",
                "Alasan tidak diperlukannya tindakan korektif"));

        listKeputusanJangkaPendek = listKeputusan;
    }

    private static final String[] namaProyek = new String[]{
            "Apartemen DPR",
            "Pasar Blok M",
            "Pasar Tanah Abang C"
    };


    private static final List<String> listnamaProyek = Arrays.asList(
            "Apartemen DPR",
            "Pasar Blok M",
            "Pasar Tanah Abang C"
    );

    public static final List<String> kategoriForm = Arrays.asList(
            "CP",
            "PTKP",
            "PTKP Keluhan Pelanggan"
    );

    public static final List<String> rootCauseSistem = Arrays.asList(
        "Prosedur belum tersedia",
            "Sistem belum tersedia",
            "Standar tidak sesuai"
    );

    public static final List<String> rootCauseKomitmen = Arrays.asList(
            "Masalah kemauan",
            "Disiplin",
            "Kepedulian",
            "Keterlibatan",
            "Konsultasi",
            "Kelalaian"
    );

    public static final List<String> rootCauseKompetensi = Arrays.asList(
            "Kompetensi tidak sesuai/kurang"
    );

    public static final List<String> rootCausePemahaman = Arrays.asList(
            "Kurang memahami", "Salah dalam memahami"
    );

    public static final List<String> rootCauseLeadership = Arrays.asList(
            "Involvement pimpinan", "Dukungan pimpinan", "Tidak ada keputusan"
    );

    public static final List<String> rootCauseSumberdaya = Arrays.asList(
            "Sumberdaya baru",
            "Sumberdaya tidak tersedia (SDM)",
            "Sumberdaya langka",
            "Sumberdaya finansial"
    );

    public static final List<String> rootCauseEkstern = Arrays.asList(
            "Kenaikan harga BBM",
            "Kenaikan harga pasar",
            "Bencana alam",
            "Gangguan ekstern",
            "Masalah sosial masyarakat",
            "Pembebasan lahan yang menjadi kewenangan owner"
    );

    private static final List<LingkupTemuan> listLingkupTemuan;
    private static final List<KeputusanJangkaPendek> listKeputusanJangkaPendek;

    public static List<String> getListNamaProyek() {
        return Arrays.asList(namaProyek);
    }

    public static List<String> getListNamaProyekNew() {
        return listnamaProyek;
    }

    public static List<String> getListFieldKategori() {
        return kategoriForm;
    }

    public static List<LingkupTemuan> getListLingkupTemuan() {
        return listLingkupTemuan;
    }

    public static List<KeputusanJangkaPendek> getListKeputusanJangkaPendek() {
        return listKeputusanJangkaPendek;
    }

    public static String getDetailKeputusanJangkaPendekTitle(String keputusanJangkaPendekStr) {
        for (KeputusanJangkaPendek keputusanJangkaPendek : listKeputusanJangkaPendek) {
            if (keputusanJangkaPendekStr.equals(keputusanJangkaPendek.getDescription())) {
                return keputusanJangkaPendek.getDetailDescription();
            }
        }

        return "Detail Keputusan Jangka Pendek";
    }

}
