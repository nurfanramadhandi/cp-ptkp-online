package id.wika.cpptkponline.data;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static id.wika.cpptkponline.helper.APIHelper.API_ADDRESS;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class ServiceGenerator {

    public static final String TOKEN_KEY = "apiToken";

    public static final String SHARED_PREFERENCES_NAME = "id.wika.cpptkponline.PREF";
    private static final String BASE_URL = API_ADDRESS; //"http://117.54.15.29:3000/";

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(
                            GsonConverterFactory.create(
                                    GsonFactory.createGson()))
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()));

    private static Retrofit retrofit = builder.build();

    private static HttpLoggingInterceptor logging =
            new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

    private static OkHttpClient.Builder httpClient =
            new OkHttpClient.Builder();

    public static <S> S createService(
            Class<S> serviceClass) {
        if (!httpClient.interceptors().contains(logging)) {
            httpClient.addInterceptor(logging);
            builder.client(httpClient.build());
            retrofit = builder.build();
        }

        return retrofit.create(serviceClass);
    }

    public static String getRemoteUrl(String attachmentFileName) {
        return BASE_URL + "images/" + attachmentFileName;
    }
}
