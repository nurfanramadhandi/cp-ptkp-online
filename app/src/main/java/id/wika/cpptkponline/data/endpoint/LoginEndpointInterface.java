package id.wika.cpptkponline.data.endpoint;

import java.util.List;

import id.wika.cpptkponline.data.endpoint.response.LoginResponse;
import id.wika.cpptkponline.data.endpoint.response.ProyekResponse;
import id.wika.cpptkponline.data.endpoint.response.RoleUnitKerjaResponse;
import id.wika.cpptkponline.data.model.Proyek;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public interface LoginEndpointInterface {

    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST("user/login")
    Observable<LoginResponse> login(@Field("username") String username, @Field("password") String password);

    @GET("user/unitkerja")
    Observable<RoleUnitKerjaResponse> cekToken(
            @Header("Authorization") String auth
    );

    @GET("user/unitkerja")
    Observable<List<RoleUnitKerjaResponse>> getUnitKerja(
            @Header("Authorization") String auth
    );

    @GET("user/proyek")
    Observable<List<ProyekResponse>> getProyek(
            @Header("Authorization") String auth,
            @Query("kode_unit_kerja") String kode_unit_kerja

    );

}
