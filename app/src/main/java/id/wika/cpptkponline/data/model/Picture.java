package id.wika.cpptkponline.data.model;

/**
 * Created by apple on 9/10/17.
 */

import android.content.SharedPreferences;

import com.google.gson.annotations.SerializedName;

/**
 * Created by panca on 1/31/17.
 */

public class Picture {

    public String image;
    public String thumb;

    public Picture(String image,String thumb){
        if(image.contains("https")){
            this.image = image.substring(image.indexOf("https"));
        }
        else{
            this.image = image;
        }
        if(thumb.contains("https")){
            this.thumb = thumb.substring(thumb.indexOf("https"));
        }
        else{
            this.thumb = thumb;
        }
    }

    public SharedPreferences.Editor save(SharedPreferences.Editor edit){
        edit.putString("pic_image",image);
        edit.putString("pic_thumb",image);
        return edit;
    }

    public static SharedPreferences.Editor clear(SharedPreferences.Editor edit){
        edit.remove("pic_image");
        edit.remove("pic_thumb");
        return edit;
    }
}