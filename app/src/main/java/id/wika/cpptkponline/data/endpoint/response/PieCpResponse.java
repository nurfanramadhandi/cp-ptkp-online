package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.wika.cpptkponline.data.model.HistoryModel;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class PieCpResponse {

    @SerializedName("data")
    private List<DataCP> mData;
    @SerializedName("status")
    private String mStatus;

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public List<DataCP> getmData() {
        return mData;
    }

    public void setmData(List<DataCP> mData) {
        this.mData = mData;
    }

    public class DataCP {

        @SerializedName("lingkup_temuan")
        private String lingkup_temuan;

        @SerializedName("jumlah")
        private int jumlah;

        public DataCP(String lingkup_temuan, int jumlah) {
            this.lingkup_temuan = lingkup_temuan;
            this.jumlah = jumlah;
        }

        public String getLingkup_temuan() {
            return lingkup_temuan;
        }

        public void setLingkup_temuan(String lingkup_temuan) {
            this.lingkup_temuan = lingkup_temuan;
        }

        public int getJumlah() {
            return jumlah;
        }

        public void setJumlah(int jumlah) {
            this.jumlah = jumlah;
        }
    }
}
