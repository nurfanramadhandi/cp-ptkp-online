package id.wika.cpptkponline.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by mamang_kompii on 9/4/17.
 */

@Entity(tableName = "forms")
public class CPPTKPForm implements Serializable {

    public static final String FORM_CP = "cp";
    public static final String FORM_PTKP = "ptkp";
    public static final String FORM_PTKP_KELUHAN = "ptkp_keluhan";

    @PrimaryKey(autoGenerate = true)
    private int formId;
    private int formStatus;

    private String nomorForm;
    private DateTime timestamp;
    private boolean isNeedResponse;

    private int remoteFormId;
    private String remoteFormStatus;

    @Ignore
    private DataFormPelaporan formPelaporan;

    @Ignore
    private DataRencanaTindakLanjut rencanaTindakLanjut;

    @Ignore
    private DataApproval approval;

    @Ignore
    private DataRealisasiTindakLanjut realisasiTindakLanjut;

    @Ignore
    private DataVerifikasiHasilTindakLanjut verifikasiHasilTindakLanjut;

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public String getNomorForm() {
        return nomorForm;
    }

    public void setNomorForm(String nomorForm) {
        this.nomorForm = nomorForm;
    }

    public int getFormStatus() {
        return formStatus;
    }

    public void setFormStatus(int formStatus) {
        this.formStatus = formStatus;
    }

    public DataFormPelaporan getFormPelaporan() {
        return formPelaporan;
    }

    public void setFormPelaporan(DataFormPelaporan formPelaporan) {
        this.formPelaporan = formPelaporan;
    }

    public DataRencanaTindakLanjut getRencanaTindakLanjut() {
        return rencanaTindakLanjut;
    }

    public void setRencanaTindakLanjut(DataRencanaTindakLanjut rencanaTindakLanjut) {
        this.rencanaTindakLanjut = rencanaTindakLanjut;
    }

    public DataApproval getApproval() {
        return approval;
    }

    public void setApproval(DataApproval approval) {
        this.approval = approval;
    }

    public DataRealisasiTindakLanjut getRealisasiTindakLanjut() {
        return realisasiTindakLanjut;
    }

    public void setRealisasiTindakLanjut(DataRealisasiTindakLanjut realisasiTindakLanjut) {
        this.realisasiTindakLanjut = realisasiTindakLanjut;
    }

    public DataVerifikasiHasilTindakLanjut getVerifikasiHasilTindakLanjut() {
        return verifikasiHasilTindakLanjut;
    }

    public void setVerifikasiHasilTindakLanjut(DataVerifikasiHasilTindakLanjut verifikasiHasilTindakLanjut) {
        this.verifikasiHasilTindakLanjut = verifikasiHasilTindakLanjut;
    }

    public boolean isNeedResponse() {
        return isNeedResponse;
    }

    public void setNeedResponse(boolean needResponse) {
        isNeedResponse = needResponse;
    }

    public DateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(DateTime timestamp) {
        this.timestamp = timestamp;
    }

    public int getRemoteFormId() {
        return remoteFormId;
    }

    public void setRemoteFormId(int remoteFormId) {
        this.remoteFormId = remoteFormId;
    }

    public String getRemoteFormStatus() {
        return remoteFormStatus;
    }

    public void setRemoteFormStatus(String remoteFormStatus) {
        this.remoteFormStatus = remoteFormStatus;
    }
}
