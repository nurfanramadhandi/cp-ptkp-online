package id.wika.cpptkponline.data.endpoint.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class ProyekResponse {

    private String status;

    @SerializedName("kode_proyek")
    private String kode;

    @SerializedName("nama_proyek")
    private String nama;

    public ProyekResponse(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
