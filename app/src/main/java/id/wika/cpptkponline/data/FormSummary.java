package id.wika.cpptkponline.data;

import org.joda.time.DateTime;

/**
 * Created by mamang_kompii on 8/25/17.
 */

public class FormSummary {

    private int formId;
    private int remoteFormId;
    private String title;
    private String subTitle;
    private String summary;
    public String noTemuan;
    private int status;
    private boolean isNeedAttention;
    private DateTime timestamp;

    public FormSummary(int formId, int remoteFormId, String title, String subTitle, String summary, String noTemuan,  int status, boolean isNeedAttention, DateTime timestamp) {
        this.formId = formId;
        this.remoteFormId = remoteFormId;
        this.title = title;
        this.subTitle = subTitle;
        this.summary = summary;
        this.status = status;
        this.isNeedAttention = isNeedAttention;
        this.timestamp = timestamp;
        this.noTemuan= noTemuan;
    }

    public String getNoTemuan() {
        return noTemuan;
    }

    public void setNoTemuan(String noTemuan) {
        this.noTemuan = noTemuan;
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public int getRemoteFormId() {
        return remoteFormId;
    }

    public void setRemoteFormId(int remoteFormId) {
        this.remoteFormId = remoteFormId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isNeedAttention() {
        return isNeedAttention;
    }

    public void setNeedAttention(boolean needAttention) {
        isNeedAttention = needAttention;
    }

    public DateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(DateTime timestamp) {
        this.timestamp = timestamp;
    }
}
