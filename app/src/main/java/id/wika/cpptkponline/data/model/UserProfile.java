package id.wika.cpptkponline.data.model;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

/**
 * Created by mamang_kompii on 9/8/17.
 */

public class UserProfile {

    private String username;

    private String userId;

    private String name;

    private String email;

    @SerializedName("phone")
    private String phoneNumber;

    private String roleId;

    private DateTime lastLogin;

    private String lastLoginCpptkp;

    @SerializedName("photo_link")
    private String photoUrl;

    private String jabatan;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public DateTime getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(DateTime lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getLastLoginCpptkp() {
        return lastLoginCpptkp;
    }

    public void setLastLoginCpptkp(String lastLoginCpptkp) {
        this.lastLoginCpptkp = lastLoginCpptkp;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }
}
