package id.wika.cpptkponline.data.endpoint;

import java.util.List;

import id.wika.cpptkponline.data.endpoint.response.RoleUnitKerjaResponse;
import id.wika.cpptkponline.data.endpoint.response.UserDevisiResponse;
import id.wika.cpptkponline.data.model.RoleUnitKerja;
import io.reactivex.Observable;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public interface UnitKerjaEndpointInterface {

    @GET("user/unitkerja")
    Observable<List<RoleUnitKerjaResponse>> getUnitKerja(
            @Header("Authorization") String auth
    );

    @GET("unitkerja")
    Observable<List<RoleUnitKerjaResponse>> getDivisi(
            @Header("Authorization") String auth
    );

    @GET("userbydivisi")
    Observable<List<UserDevisiResponse>> getUserByDivisi(
            @Header("Authorization") String auth,
            @Query("divisi") String divisi,
            @Query("kodeproyek") String kodeproyek

    );


}
