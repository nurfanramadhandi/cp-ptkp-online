
package id.wika.cpptkponline.data.endpoint.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GetListLaporan {

    @SerializedName("data")
    private List<Datum> mData;
    @SerializedName("status")
    private String mStatus;

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }
    public class Datum {

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("nama_proyek")
        private String mNamaProyek;
        @SerializedName("status")
        private String mStatus;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getNamaProyek() {
            return mNamaProyek;
        }

        public void setNamaProyek(String namaProyek) {
            mNamaProyek = namaProyek;
        }

        public String getStatus() {
            return mStatus;
        }

        public void setStatus(String status) {
            mStatus = status;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }
}
