package id.wika.cpptkponline.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.util.Log;

import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataApproval;
import id.wika.cpptkponline.data.DataFormPelaporan;
import id.wika.cpptkponline.data.DataRealisasiTindakLanjut;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import id.wika.cpptkponline.data.DataVerifikasiHasilTindakLanjut;

/**
 * Created by mamang_kompii on 9/9/17.
 */

@Database(
        entities = {
            CPPTKPForm.class,
            DataFormPelaporan.class,
            DataRencanaTindakLanjut.class,
            DataApproval.class,
            DataRealisasiTindakLanjut.class,
            DataVerifikasiHasilTindakLanjut.class
        },
        version = 4)
@TypeConverters({DateTimeConverter.class, ArrayOfStringConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract FormDao formDao();
    public abstract FormPelaporanDao formPelaporanDao();
    public abstract FormRencanaTindakLanjutDao formRencanaTindakLanjutDao();
    public abstract FormApprovalDao formApprovalDao();
    public abstract FormRealisasiTindakLanjutDao formRealisasiTindakLanjutDao();
    public abstract FormVerifikasiHasilDao formVerifikasiHasilDao();

    public static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "Sample.db")
                            .fallbackToDestructiveMigration()
                            .build();
                    Log.e("Database", context.getDatabasePath("Sample.db").getPath());
                }
            }
        }
        return INSTANCE;
    }
}
