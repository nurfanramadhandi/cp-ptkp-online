package id.wika.cpptkponline.data.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mamang_kompii on 9/7/17.
 */

public class LingkupTemuan {

    private String title;
    private List<String> detailTemuan = new ArrayList<>();

    public LingkupTemuan(String title) {
        this.title = title;
    }
    public LingkupTemuan() {
    }
    public LingkupTemuan(String title, List<String> detailTemuan) {
        this.title = title;
        this.detailTemuan = detailTemuan;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getDetailTemuan() {
        return detailTemuan;
    }

    public void setDetailTemuan(List<String> detailTemuan) {
        this.detailTemuan = detailTemuan;
    }
}
