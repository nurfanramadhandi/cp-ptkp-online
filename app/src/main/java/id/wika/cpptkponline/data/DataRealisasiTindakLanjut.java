package id.wika.cpptkponline.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by mamang_kompii on 9/4/17.
 */

@Entity(tableName = "realisasi_tindak_lanjut", foreignKeys = {
        @ForeignKey(entity = CPPTKPForm.class,
                parentColumns = "formId",
                childColumns = "relatedFormId",
                onDelete = ForeignKey.CASCADE)})

public class DataRealisasiTindakLanjut implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private int relatedFormId;

    private String hasilPencegahanDanPerbaikan;

    private ArrayList<String> listDokumentasiHasil = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRelatedFormId() {
        return relatedFormId;
    }

    public void setRelatedFormId(int relatedFormId) {
        this.relatedFormId = relatedFormId;
    }

    public String getHasilPencegahanDanPerbaikan() {
        return hasilPencegahanDanPerbaikan;
    }

    public void setHasilPencegahanDanPerbaikan(String hasilPencegahanDanPerbaikan) {
        this.hasilPencegahanDanPerbaikan = hasilPencegahanDanPerbaikan;
    }

    public ArrayList<String> getListDokumentasiHasil() {
        return listDokumentasiHasil;
    }

    public void setListDokumentasiHasil(ArrayList<String> listDokumentasiHasil) {
        this.listDokumentasiHasil = listDokumentasiHasil;
    }
}
