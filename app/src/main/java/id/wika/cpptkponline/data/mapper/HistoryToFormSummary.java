package id.wika.cpptkponline.data.mapper;

import id.wika.cpptkponline.data.FormSummary;
import id.wika.cpptkponline.data.model.HistoryModel;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FormHelper;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 25/September/2017 - 09:29
 * Email   : ardhimaarik2@gmail.com / ardi@bahasakita.co.id
 * Project : KuTulis
 *
 * Copyright (c) 2017 "PT Bahasa Kinerja Utama" - bahasakita.co.id
 * All Rights Reserved. 
 ******************************************************************************/

public class HistoryToFormSummary extends BaseMapper<FormSummary, HistoryModel.Datum> {

    public HistoryToFormSummary() {
    }

    @Override
    public FormSummary map1(HistoryModel.Datum o2) {
        DateTimeHelper dateTimeHelper = new DateTimeHelper();
        return new FormSummary(
                o2.getId().intValue(),
                o2.getId().intValue(),
                o2.getBentukPenyimpangan(),
                o2.getLokasiPenyimpangan(),
                o2.getNamaProyek(),
                o2.getmNoTemuan(),
                FormHelper.parseRemoteStatusToLocalStatus(o2.getStatus()),
                false,
                dateTimeHelper.parseDateTime(o2.getTglTemuan()));
    }

    @Override
    public HistoryModel.Datum map2(FormSummary o1) {
        return null;
    }
}
