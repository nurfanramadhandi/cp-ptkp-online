package id.wika.cpptkponline.data.endpoint;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public class RemoteFormStatus {

    public static final String DRAFT = "draft";
    public static final String PENGECEKAN = "pengecekan";
    public static final String RENCANA_TINDAK_LANJUT = "rencana_tindak_lanjut";
    public static final String REALISASI_TINDAK_LANJUT = "realisasi_tindak_lanjut";
    public static final String APPROVAL = "approval";
    public static final String STATUS_CLOSED = "closed";
    public static final String STATUS_OPEN = "open";
    public static final String ARSIP = "arsip";
    public static final String VERIFIKASI_TINDAK_LANJUT = "verifikasi tindak lanjut";
}
