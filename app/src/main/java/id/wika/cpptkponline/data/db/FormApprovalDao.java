package id.wika.cpptkponline.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import id.wika.cpptkponline.data.DataApproval;
import id.wika.cpptkponline.data.DataRealisasiTindakLanjut;
import io.reactivex.Flowable;

/**
 * Created by mamang_kompii on 9/9/17.
 */

@Dao
public interface FormApprovalDao {

    @Insert
    void insertForm(DataApproval dataApproval);

    @Update
    void updateForm(DataApproval dataApproval);

    @Query("SELECT * FROM data_approval WHERE relatedFormId = :formId")
    DataApproval getFormDataByRelatedFormId(int formId);
}
