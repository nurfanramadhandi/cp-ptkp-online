package id.wika.cpptkponline.data.endpoint.response;

import java.util.List;

/**
 * Created by mamang_kompii on 9/9/17.
 */

public class GetFormResponse {

    private String status;
    private List<RemoteFormSummary> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<RemoteFormSummary> getData() {
        return data;
    }

    public void setData(List<RemoteFormSummary> data) {
        this.data = data;
    }
}
