
package id.wika.cpptkponline.data.endpoint.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GetGrafikResponse {

    @SerializedName("data")
    private List<Datum> mData;
    @SerializedName("status")
    private String mStatus;

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }
    public class Datum {

        @SerializedName("jumlah")
        private Long mJumlah;
        @SerializedName("lingkup_temuan")
        private String mLingkupTemuan;

        public Long getJumlah() {
            return mJumlah;
        }

        public void setJumlah(Long jumlah) {
            mJumlah = jumlah;
        }

        public String getLingkupTemuan() {
            return mLingkupTemuan;
        }

        public void setLingkupTemuan(String lingkupTemuan) {
            mLingkupTemuan = lingkupTemuan;
        }

    }
}
