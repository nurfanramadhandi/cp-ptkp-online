package id.wika.cpptkponline;

import android.app.Application;
import android.support.annotation.NonNull;

import com.facebook.drawee.backends.pipeline.Fresco;

import net.danlew.android.joda.JodaTimeAndroid;

import id.wika.cpptkponline.data.db.AppDatabase;
import id.wika.cpptkponline.injection.AppComponent;
import id.wika.cpptkponline.injection.AppModule;
import id.wika.cpptkponline.injection.DaggerAppComponent;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:34:19 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public final class App extends Application {
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        JodaTimeAndroid.init(this);
        Fresco.initialize(this);

        AppDatabase database = AppDatabase.getInstance(this);
    }

    @NonNull
    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}