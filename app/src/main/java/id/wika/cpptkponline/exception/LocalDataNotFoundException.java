package id.wika.cpptkponline.exception;

import android.support.annotation.NonNull;

/**
 * Created by mamang_kompii on 9/25/17.
 */

public class LocalDataNotFoundException extends RuntimeException {
    public LocalDataNotFoundException(int localFormId) {
        super(getMessage(localFormId));
    }

    @NonNull
    private static String getMessage(int localFormId) {
        return "Local data form not found (formId = " + localFormId + ")";
    }
}
