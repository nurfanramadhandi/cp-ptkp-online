package id.wika.cpptkponline.view.form;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataRealisasiTindakLanjut;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.endpoint.RemoteFormStatus;
import id.wika.cpptkponline.data.endpoint.request.PostFormRequestGenerator;
import id.wika.cpptkponline.helper.FileHelper;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.helper.ImageHelper;
import id.wika.cpptkponline.helper.PermissionHelper;
import id.wika.cpptkponline.presenter.FormViewModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class FormRealisasiTindakLanjutFragment extends Fragment implements CPPTKPFormView {

    private CPPTKPForm formData;

    private String currentPhotoFile;
    private Map<String, View> pictureViewMap = new HashMap<>();
    private ArrayList<String> picturePaths = new ArrayList<>();

    @BindView(R.id.attachment_container)
    LinearLayout attachmentContainer;

    @BindView(R.id.attachment_placeholder)
    View attachmentPlaceholder;

    @BindView(R.id.field_hasil_pencegahan_perbaikan)
    TextInputLayout fieldHasilPencegahanPerbaikan;

    private int formId;
    private PermissionHelper permissionHelper = new PermissionHelper();
    private int thumbnailSize;

    public static FormRealisasiTindakLanjutFragment newInstance(int formId) {
        Bundle args = new Bundle();
        args.putInt(FormHelper.EXTRA_KEY_FORM_ID, formId);

        FormRealisasiTindakLanjutFragment fragment = new FormRealisasiTindakLanjutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public FormRealisasiTindakLanjutFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        formId = getArguments().getInt(FormHelper.EXTRA_KEY_FORM_ID);

        FormViewModel formViewModel = ViewModelProviders
                .of(getActivity())
                .get(FormViewModel.class);
        formViewModel.getFormData(formId)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CPPTKPForm>() {
                    @Override
                    public void accept(CPPTKPForm cpptkpForm) throws Exception {
                        formData = cpptkpForm;
                    }
                });
        thumbnailSize = getResources().getDimensionPixelSize(R.dimen.attachment_thumbnail_size);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_realisasi_tindak_lanjut, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.attachment_placeholder)
    void onClickPlaceholder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pilih pengambilan gambar")
                .setItems(new String[]{"Ambil gambar dari kamera", "Pilih dari galeri"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            dispatchTakePictureIntent();
                        } else {
                            dispatchPickPictureFromGallery();
                        }
                    }
                });
        builder.show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile;

            try {
                photoFile = FileHelper.createImageFile(getActivity());
                currentPhotoFile = photoFile.getAbsolutePath();

                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "id.wika.cppptkponline.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, FormHelper.REQUEST_IMAGE_CAPTURE);
            } catch (IOException e) {
                e.printStackTrace();
                currentPhotoFile = "";
            }
        }
    }

    private void dispatchPickPictureFromGallery() {
        permissionHelper.requestPermissionFromFragment(
                this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                PermissionHelper.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE,
                new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        createGetContentChooser();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        permissionHelper.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    private void createGetContentChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Pilih gambar via galeri"),
                FormHelper.REQUEST_SELECT_PICTURE_FROM_GALLERY);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FormHelper.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            FormHelper.launchEditPhotoActivity(this, currentPhotoFile);
        } else if (requestCode == FormHelper.REQUEST_EDIT_IMAGE && resultCode == Activity.RESULT_OK) {
            final String path = data.getStringExtra(ImageHelper.EXTRA_KEY_IMAGE_FILE_PATH);
            loadAndAddThumbnail(path);
        } else if (requestCode == FormHelper.REQUEST_VIEW_IMAGE && resultCode == Activity.RESULT_OK) {
            final String path = data.getStringExtra(ImageHelper.EXTRA_KEY_IMAGE_FILE_PATH);

            View toBeRemovedView = pictureViewMap.get(path);
            attachmentContainer.removeView(toBeRemovedView);
            pictureViewMap.remove(path);
            picturePaths.remove(path);

            if (picturePaths.size() < 3) {
                attachmentPlaceholder.setVisibility(View.VISIBLE);
            }
        } else if (requestCode == FormHelper.REQUEST_SELECT_PICTURE_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            Uri selectedImageUri = data.getData();
            try {
                File sourceFile = new File(FileHelper.getPath(getActivity(), selectedImageUri));
                File destFile = FileHelper.createImageFile(getActivity());
                FileHelper.copyFile(sourceFile, destFile);
                FormHelper.launchEditPhotoActivity(this, destFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Gagal import gambar dari galeri", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void loadAndAddThumbnail(String path) {
        SimpleDraweeView imageView = FormHelper.getImageThumbnailViewerDrawee(this, thumbnailSize, path, true);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                thumbnailSize,
                thumbnailSize
        );
        params.setMargins(0, 0, getResources().getDimensionPixelSize(R.dimen.attachment_margin), 0);

        attachmentContainer.addView(imageView, attachmentContainer.getChildCount() - 1, params);
        pictureViewMap.put(path, imageView);
        picturePaths.add(path);

        if (picturePaths.size() >= 3) {
            attachmentPlaceholder.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.history_laporan_awal_btn)
    void onClickViewHistoryLaporanAwal() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_PENGECEKAN);
    }

    @OnClick(R.id.history_rencana_tindak_lanjut_btn)
    void onClickViewHistoryRencanaTindakLanjut() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT);
    }

    @OnClick(R.id.history_approval)
    void onClickViewHistoryApproval() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI);
    }

    @Override
    public boolean isValid() {
        return FormHelper.isFormFieldContentValid(fieldHasilPencegahanPerbaikan) && isPictureValid();
    }

    private boolean isPictureValid() {
        return !picturePaths.isEmpty();
    }

    @Override
    public CPPTKPForm getFormData() {

        formData.setFormStatus(FormStatus.FORM_REALISASI_TINDAK_LANJUT);
        formData.setRemoteFormStatus(RemoteFormStatus.REALISASI_TINDAK_LANJUT);
        formData.setNeedResponse(false);

        DataRealisasiTindakLanjut realisasiTindakLanjut = new DataRealisasiTindakLanjut();
        realisasiTindakLanjut.setHasilPencegahanDanPerbaikan(FormHelper.getTextFromField(fieldHasilPencegahanPerbaikan));

        realisasiTindakLanjut.setListDokumentasiHasil(picturePaths);
        realisasiTindakLanjut.setRelatedFormId(formData.getFormId());

        formData.setRealisasiTindakLanjut(realisasiTindakLanjut);

        return formData;
    }

    @Override
    public PostFormRequestGenerator.Strategy getPostFormStrategy(int formStatus) {
        return PostFormRequestGenerator.Strategy.POST_REALISASI_TINDAK_LANJUT;
    }
}
