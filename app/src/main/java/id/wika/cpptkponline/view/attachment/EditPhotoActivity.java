package id.wika.cpptkponline.view.attachment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.helper.ImageHelper;
import id.wika.cpptkponline.view.annotation.FreehandView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class EditPhotoActivity extends AppCompatActivity {

    public static final int PICTURE_SAVE_QUALITY = 80;

    private final int colorList[] = {
            Color.RED,
            Color.GREEN,
            Color.BLUE,
            Color.CYAN,
            Color.MAGENTA,
            Color.YELLOW,
            Color.BLACK
    };

    @BindView(R.id.image_placeholder)
    FreehandView imageViewer;

    @BindView(R.id.color_picker)
    View colorPicker;

    private int selectedColorIndex = 0;

    private String photoPath;
    private ProgressDialog progressDialog;
    private String oldOrientation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_photo);
        ButterKnife.bind(this);

        setColorPicker(selectedColorIndex);

        photoPath = getIntent().getStringExtra(ImageHelper.EXTRA_KEY_IMAGE_FILE_PATH);
        imageViewer.setOrientation(SubsamplingScaleImageView.ORIENTATION_USE_EXIF);
        imageViewer.setImage(ImageSource.uri(photoPath));

        try {
            ExifInterface oldExif = new ExifInterface(photoPath);
            oldOrientation = oldExif.getAttribute(ExifInterface.TAG_ORIENTATION);
        } catch (IOException e) {
            e.printStackTrace();
        }

        initProgressDialog();
    }

    private void setColorPicker(int colorIndex) {
        int selectedColor = colorList[colorIndex];
        colorPicker.setBackgroundColor(selectedColor);
        imageViewer.setStrokeColor(selectedColor);
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle("Menyimpan Gambar");
        progressDialog.setMessage("Sedang menyimpan gambar & anotasi, silakan tunggu beberapa saat..");
        progressDialog.setCanceledOnTouchOutside(false);
    }

    @OnClick(R.id.color_picker)
    void onColorPickerClick() {
        selectedColorIndex = ++selectedColorIndex % colorList.length;
        setColorPicker(selectedColorIndex);
    }

    @OnClick(R.id.cancel_button)
    void onCancelButtonClick() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.undo_button)
    void onUndoButtonClick() {
        imageViewer.undoDrawing();
    }

//    @OnClick(R.id.edit_button)
//    void onEditButtonClick() {
//        Toast.makeText(this, "Pilih antara garis / teks", Toast.LENGTH_SHORT).show();
//    }

    @OnClick(R.id.ok_button)
    void onOkButtonClick() {
        Observable.just(photoPath)
                .map(new Function<String, Bitmap>() {
                    @Override
                    public Bitmap apply(String path) throws Exception {
                        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                        bmOptions.inMutable = true;
                        return BitmapFactory.decodeFile(path, bmOptions);
                    }
                })
                .map(new Function<Bitmap, Bitmap>() {
                    @Override
                    public Bitmap apply(Bitmap bitmap) throws Exception {
                        return imageViewer.drawAnnotationToBitmap(bitmap);
                    }
                })
                .doOnNext(new Consumer<Bitmap>() {
                    @Override
                    public void accept(Bitmap bitmap) throws Exception {
                        saveBitmapToFile(bitmap, photoPath);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Bitmap>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        if (!progressDialog.isShowing()) {
                            progressDialog.show();
                        }
                    }

                    @Override
                    public void onNext(Bitmap bitmap) {
                        bitmap.recycle();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {
                        progressDialog.dismiss();

                        Intent intent = new Intent();
                        intent.putExtras(getIntent().getExtras());
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });
    }

    private void saveBitmapToFile(Bitmap bitmap, String photoPath) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(photoPath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, PICTURE_SAVE_QUALITY, out);

            if (oldOrientation != null) {
                ExifInterface newExif = new ExifInterface(photoPath);
                newExif.setAttribute(ExifInterface.TAG_ORIENTATION, oldOrientation);
                newExif.saveAttributes();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
