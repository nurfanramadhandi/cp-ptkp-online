package id.wika.cpptkponline.view.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;

/**
 * Created by mamang_kompii on 8/30/17.
 */

public abstract class GeneralConfirmationDialogFragment extends DialogFragment {

    @BindView(R.id.dialog_title)
    TextView titleLabel;

    @BindView(R.id.dialog_description)
    TextView descriptionLabel;

    @BindView(R.id.negative_button)
    TextView negativeButton;

    @BindView(R.id.neutral_button)
    TextView neutralButton;

    @BindView(R.id.positive_button)
    TextView positiveButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_confirmation, container, false);
        ButterKnife.bind(this, view);

        setDialogContent();

        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    protected void setDialogContent() {
        titleLabel.setVisibility(isTitleLabelVisible() ? View.VISIBLE : View.GONE);
        titleLabel.setText(getTitleText());

        descriptionLabel.setText(getDescriptionText());

        negativeButton.setText(getNegativeButtonText());

        neutralButton.setVisibility(isNeutralButtonVisible() ? View.VISIBLE : View.GONE);
        neutralButton.setText(getNeutralButtonText());

        positiveButton.setText(getPositiveButtonText());
    }

    protected abstract boolean isTitleLabelVisible();

    protected abstract String getTitleText();

    protected abstract String getDescriptionText();

    protected abstract String getNegativeButtonText();

    protected abstract boolean isNeutralButtonVisible();

    protected abstract String getNeutralButtonText();

    protected abstract String  getPositiveButtonText();

    @OnClick(R.id.negative_button)
    abstract void onNegativeButtonClicked();

    @OnClick(R.id.neutral_button)
    abstract void onNeutralButtonClicked();

    @OnClick(R.id.positive_button)
    abstract void onPositiveButtonClicked();
}
