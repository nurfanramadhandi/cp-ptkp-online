package id.wika.cpptkponline.view.review;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataVerifikasiHasilTindakLanjut;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.presenter.ReviewViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ReviewVerifikasiTindakLanjutFragment extends Fragment {

    private int formId;
    private boolean isReviewWithHistory;

    @BindView(R.id.history_laporan_awal_btn)
    View historyLaporanAwalButton;

    @BindView(R.id.history_rencana_tindak_lanjut_btn)
    View historyRencanaTindakLanjutButton;

    @BindView(R.id.history_realisasi_tindak_lanjut_btn)
    View historyRealisasiTindakLanjutButton;

    @BindView(R.id.user_fullname)
    TextView namaVerifikator;

    @BindView(R.id.verification_date)
    TextView verificationDate;

    @BindView(R.id.field_review_progres)
    TextView fieldProgres;


    @BindView(R.id.field_for_status_open)
    View statusOpenFields;

    @BindView(R.id.field_feedback)
    TextView feedbackField;

    @BindView(R.id.attachment_area_feedback)
    View attachmentArea;

    @BindView(R.id.attachment_container)
    LinearLayout attachmentContainer;

    @BindView(R.id.field_for_status_closed)
    View statusClosedFields;

    @BindView(R.id.field_root_cause)
    TextView rootCause;

    @BindView(R.id.status_open)
    RadioButton statusOpen;

    @BindView(R.id.status_close)
    RadioButton statusClosed;

    public static Fragment newInstance(int formId,  boolean isShowingHistory, boolean isUsingRemoteDat) {
        Bundle args = new Bundle();
        args.putInt(FormHelper.EXTRA_KEY_FORM_ID, formId);
        args.putBoolean(FormHelper.EXTRA_KEY_IS_REVIEW_WITH_HISTORY, isShowingHistory);

        ReviewVerifikasiTindakLanjutFragment fragment = new ReviewVerifikasiTindakLanjutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ReviewVerifikasiTindakLanjutFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        formId = getArguments().getInt(FormHelper.EXTRA_KEY_FORM_ID);
        isReviewWithHistory = getArguments().getBoolean(FormHelper.EXTRA_KEY_IS_REVIEW_WITH_HISTORY);

        ReviewViewModel reviewViewModel = ViewModelProviders
                .of(getActivity())
                .get(ReviewViewModel.class);

        reviewViewModel.getFormReviewData(formId, FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED, false)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CPPTKPForm>() {
                    @Override
                    public void accept(CPPTKPForm cpptkpForm) throws Exception {
                        if (cpptkpForm.getVerifikasiHasilTindakLanjut() != null) {
                            setExistingData(cpptkpForm.getVerifikasiHasilTindakLanjut());
                        }
                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review_verifikasi_tindak_lanjut, container, false);
        ButterKnife.bind(this, view);

        historyLaporanAwalButton.setVisibility(isReviewWithHistory ? View.VISIBLE : View.GONE);
        historyRencanaTindakLanjutButton.setVisibility(isReviewWithHistory ? View.VISIBLE : View.GONE);
        historyRealisasiTindakLanjutButton.setVisibility(isReviewWithHistory ? View.VISIBLE : View.GONE);

        return view;
    }

    private void setExistingData(DataVerifikasiHasilTindakLanjut verifikasiHasilTindakLanjut) {
        DateTimeHelper dateTimeHelper = new DateTimeHelper();

        namaVerifikator.setText(verifikasiHasilTindakLanjut.getVerifikator().getFullname());
        verificationDate.setText(
                dateTimeHelper.printInIndonesian(verifikasiHasilTindakLanjut.getTanggalVerifikasi()));

        boolean isStatusOpened = verifikasiHasilTindakLanjut.isStatusOpened();

        if (isStatusOpened) {
            showStatusOpenFields(verifikasiHasilTindakLanjut);
        } else {
            showStatusClosedFields(verifikasiHasilTindakLanjut);
        }
    }

    private void showStatusClosedFields(DataVerifikasiHasilTindakLanjut verifikasiHasilTindakLanjut) {
        statusClosed.setChecked(true);
        statusClosedFields.setVisibility(View.VISIBLE);
        feedbackField.setText(verifikasiHasilTindakLanjut.getFeedback());

        int progres = verifikasiHasilTindakLanjut.getProgres();
        if (progres == 0)
            fieldProgres.setText("0 %");
        else {
            fieldProgres.setText(String.valueOf(progres) + " %");
        }

        if (verifikasiHasilTindakLanjut.getFeedbackAttachments().size() > 0) {
            attachmentArea.setVisibility(View.VISIBLE);
            int thumbnailSize = getResources().getDimensionPixelSize(R.dimen.attachment_thumbnail_size);

            for (final String path : verifikasiHasilTindakLanjut.getFeedbackAttachments()) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        thumbnailSize,
                        thumbnailSize
                );
                params.setMargins(0, 0, getResources().getDimensionPixelSize(R.dimen.attachment_margin), 0);
                SimpleDraweeView simpleDraweeView = FormHelper.getImageThumbnailViewerDrawee(this, thumbnailSize, path);
                attachmentContainer.addView(simpleDraweeView, attachmentContainer.getChildCount() - 1, params);
            }
        } else {
            attachmentArea.setVisibility(View.GONE);
        }
    }

    private void showStatusOpenFields(DataVerifikasiHasilTindakLanjut verifikasiHasilTindakLanjut) {
        statusOpen.setChecked(true);
        statusOpenFields.setVisibility(View.VISIBLE);
        rootCause.setText(
                new StringBuilder()
                        .append(verifikasiHasilTindakLanjut.getRootCauseCategory())
                        .append(" : ")
                        .append(verifikasiHasilTindakLanjut.getRootCauseValue())
                        .toString());
    }

    @OnClick(R.id.history_laporan_awal_btn)
    void onClickViewHistoryLaporanAwal() {
        FormHelper.launchReviewFormActivity(
                getActivity(), formId, FormStatus.FORM_PENGECEKAN, true, false);
    }

    @OnClick(R.id.history_rencana_tindak_lanjut_btn)
    void onClickViewHistoryRencanaTindakLanjut() {
        FormHelper.launchReviewFormActivity(
                getActivity(), formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT,true, false);
    }

    @OnClick(R.id.history_realisasi_tindak_lanjut_btn)
    void onClickViewHistoryRealisasiTindakLanjut() {
        FormHelper.launchReviewFormActivity(
                getActivity(), formId, FormStatus.FORM_REALISASI_TINDAK_LANJUT, true, false);
    }
}
