package id.wika.cpptkponline.view;

import android.support.annotation.UiThread;

import java.util.List;

import id.wika.cpptkponline.data.endpoint.response.PostTemuanListResponse;


/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:54:07 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil
 * All Rights Reserved. 
 ******************************************************************************/

@UiThread
public interface HistoryDetailView extends BaseView{
    void renderData(List<PostTemuanListResponse.Datum> formListItems);
}