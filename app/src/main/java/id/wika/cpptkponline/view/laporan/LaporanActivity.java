package id.wika.cpptkponline.view.laporan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.endpoint.response.GetListLaporan;
import id.wika.cpptkponline.data.endpoint.response.PostTemuanListResponse;
import id.wika.cpptkponline.injection.AppComponent;
import id.wika.cpptkponline.injection.DaggerLaporanViewComponent;
import id.wika.cpptkponline.injection.LaporanViewModule;
import id.wika.cpptkponline.presenter.LaporanPresenter;
import id.wika.cpptkponline.presenter.PresenterFactory;
import id.wika.cpptkponline.view.LaporanView;
import id.wika.cpptkponline.view.attachment.PDFViewActivity;
import id.wika.cpptkponline.view.history.HistoryAdapter;
import id.wika.cpptkponline.view.impl.BaseActivity;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 28, 2017 8:33:21 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public final class  LaporanActivity extends BaseActivity<LaporanPresenter, LaporanView> implements LaporanView, LaporanAdaptor.OnListClick {
    @Inject
    PresenterFactory<LaporanPresenter> mPresenterFactory;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_bulan)
    TextView tvBulan;
    @BindView(R.id.tv_tahun)
    TextView tvTahun;
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.form_list)
    RecyclerView formList;
    private LaporanAdaptor adapter;

    private String bulan;
    private String tahun;
    private String status;

    // Your presenter is available using the mPresenter variable

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan);
        ButterKnife.bind(this);

        initActionBar();
        initView();
        Date date = new Date();
        bulan = String.valueOf(date.getMonth());
        tahun = "2017";
        status="all";

        // Your code here
        // Do not call mPresenter from here, it will be null! Wait for onStart or onPostCreate.
    }

    private void initView() {
        adapter = new LaporanAdaptor(this);
        formList.setAdapter(adapter);
        formList.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initActionBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Laporan");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void setupComponent(@NonNull AppComponent parentComponent) {
        DaggerLaporanViewComponent.builder()
                .appComponent(parentComponent)
                .laporanViewModule(new LaporanViewModule())
                .build()
                .inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mPresenter != null) {
            mPresenter.getListData();
        }
    }

    @NonNull
    @Override
    protected PresenterFactory<LaporanPresenter> getPresenterFactory() {
        return mPresenterFactory;
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, LaporanActivity.class);
//        starter.putExtra();
        context.startActivity(starter);
    }

    @OnClick({R.id.tv_bulan, R.id.tv_tahun, R.id.tv_status})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_bulan:
                showMonth();
                break;
            case R.id.tv_tahun:
                showYear();
                break;
            case R.id.tv_status:
                showPopUpMenu();
                break;
        }
    }

    private void showYear() {
        View promptsView = LayoutInflater.from(this).inflate(R.layout.item_spinner, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setTitle("Tahun..");
        alertDialogBuilder.setIcon(R.drawable.ic_calendar);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        final Spinner mSpinner = (Spinner) promptsView.findViewById(R.id.spinner);
//        final MaterialSpinner mSpinner = (MaterialSpinner) promptsView.findViewById(R.id.spinner);
        final Button mButton = (Button) promptsView.findViewById(R.id.button);
        ArrayList<String> years = new ArrayList<String>();
        final int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 2016; i <= thisYear + 4; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, years);
        mSpinner.setAdapter(adapter);
//        mSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                tvTahun.setText(mSpinner.getItems().toString());
//                tahun = mSpinner.getItems().toString();
//                alertDialog.dismiss();
//
//            }
//        });
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvTahun.setText(mSpinner.getSelectedItem().toString());
                tahun = mSpinner.getSelectedItem().toString();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
    }

    private void showMonth() {
        View promptsView = LayoutInflater.from(this).inflate(R.layout.item_spinner, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setTitle("Bulan..");
        alertDialogBuilder.setIcon(R.drawable.ic_calendar);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        final Spinner mSpinner = (Spinner) promptsView.findViewById(R.id.spinner);
        final Button mButton = (Button) promptsView.findViewById(R.id.button);
        ArrayList<String> years = new ArrayList<String>();
        years.add("Januari");
        years.add("Februari");
        years.add("Maret");
        years.add("April");
        years.add("Mei");
        years.add("Juni");
        years.add("Juli");
        years.add("Agustus");
        years.add("September");
        years.add("Oktober");
        years.add("November");
        years.add("Desember");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, years);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvBulan.setText(mSpinner.getSelectedItem().toString());
                bulan = mSpinner.getSelectedItem().toString();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
    }

    private void showPopUpMenu() {
        PopupMenu popup = new PopupMenu(LaporanActivity.this, tvStatus);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.item_loporan_status_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                Toast.makeText(
                        LaporanActivity.this,
                        "You Clicked : " + item.getOrder() + "  " + item.getTitleCondensed().toString(),
                        Toast.LENGTH_SHORT
                ).show();
                tvStatus.setText(item.getTitle());
                status = item.getTitleCondensed().toString();
                return true;
            }
        });

        popup.show(); //showing popup menu
    }

    public  String getIntBulan(String bulan){
        switch(bulan) {
            case "Januari":
                return "1";
            case  "Februari":
                return "2";
            case "Maret":
            return "3";
            case "April":
            return "4";

            case "Mei":
            return "5";

            case "Juni":
            return "6";

            case "Juli":
            return "7";

            case "Agustus":
            return "8";

            case "September":
            return "9";

            case "Oktober":
            return "10";

            case "November":
            return "11";

            case "Desember":
            return "12";

            default:
                Date date = new Date();
                bulan = String.valueOf(date.getMonth());
                return bulan;
        }

    }


    @Override
    public void onItemClick(GetListLaporan.Datum item) {
        String bulanInt= getIntBulan(bulan);
        PDFViewActivity.start(this, item.getNamaProyek(), bulanInt, tahun, status);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, ""+message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void renderData(List<GetListLaporan.Datum> formListItems) {
        if (adapter != null)
            adapter.setDatas(formListItems);
    }
}
