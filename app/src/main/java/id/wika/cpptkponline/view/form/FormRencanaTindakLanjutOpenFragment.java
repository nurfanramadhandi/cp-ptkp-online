package id.wika.cpptkponline.view.form;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;

import org.joda.time.DateTime;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataRealisasiTindakLanjut;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.endpoint.RemoteFormStatus;
import id.wika.cpptkponline.data.endpoint.request.PostFormRequestGenerator;
import id.wika.cpptkponline.data.endpoint.response.KeputusanJangkaPendekResponse;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FileHelper;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.helper.ImageHelper;
import id.wika.cpptkponline.helper.PermissionHelper;
import id.wika.cpptkponline.presenter.DictionaryViewModel;
import id.wika.cpptkponline.presenter.FormViewModel;
import id.wika.cpptkponline.view.attachment.EditPhotoActivity;
import id.wika.cpptkponline.view.dialog.DatePickerDialogFragment;
import id.wika.cpptkponline.view.widget.ClickToSelectEditText;
import id.wika.cpptkponline.view.widget.ClickToSelectEditTextListAdapter;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static id.wika.cpptkponline.view.widget.ClickToSelectEditText.OnClickListener;
import static id.wika.cpptkponline.view.widget.ClickToSelectEditText.VISIBLE;

public class FormRencanaTindakLanjutOpenFragment extends Fragment implements CPPTKPFormView {

    public static final String TAG = "FormRencanaTindakLanjutOpenFragment";

    private DateTimeHelper dateTimeHelper = new DateTimeHelper();
    private String userFullname;
    private String userDivision;
    private String userPosition;
    private String currentPhotoFile;
    private CPPTKPForm formData;

    @BindView(R.id.field_penyebab_utama_penyimpangan)
    TextView fieldPenyebabUtamaPenyimpangan;

    @BindView(R.id.field_isian_tindak_lanjut)
    TextInputLayout fieldIsianTindaklanjut;

    @BindView(R.id.field_keputusan_jangka_pendek)
    TextInputLayout fieldKeputusanJangkaPendek;

    @BindView(R.id.field_keputusan_jangka_pendek_extra)
    TextInputLayout fieldKeputusanJangkaPendekExtra;

    @BindView(R.id.field_waktu_keputusan_jangka_pendek)
    TextInputLayout fieldWaktuKeputusanJangkaPendek;

    @BindView(R.id.field_keputusan_jangka_panjang)
    TextInputLayout fieldKeputusanJangkaPanjang;

    @BindView(R.id.field_waktu_keputusan_jangka_panjang)
    TextInputLayout fieldWaktuKeputusanJangkaPanjang;

    @BindView(R.id.attachment_container)
    LinearLayout attachmentContainer;

    @BindView(R.id.attachment_placeholder)
    View attachmentPlaceholder;

    private User user;
    DateTime dateJangkaPendek;
    DateTime dateJangkaPanjang;
    private int formId;
    private DictionaryViewModel dictionaryViewModel;
    private PermissionHelper permissionHelper = new PermissionHelper();
    private int thumbnailSize;
    private Map<String, View> pictureViewMap = new HashMap<>();
    private ArrayList<String> picturePaths = new ArrayList<>();

    public static FormRencanaTindakLanjutOpenFragment newInstance(int formId) {
        Bundle args = new Bundle();
        args.putInt(FormHelper.EXTRA_KEY_FORM_ID, formId);

        FormRencanaTindakLanjutOpenFragment fragment = new FormRencanaTindakLanjutOpenFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = User.load(getActivity());
            userFullname = user.getFullname();
            userDivision = user.getDivisi();
            userPosition = user.getJabatan();
            formId = getArguments().getInt(FormHelper.EXTRA_KEY_FORM_ID);
            dictionaryViewModel = new DictionaryViewModel();

            FormViewModel formViewModel = ViewModelProviders
                    .of(getActivity())
                    .get(FormViewModel.class);
            formViewModel.getFormData(formId)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<CPPTKPForm>() {
                        @Override
                        public void accept(CPPTKPForm cpptkpForm) throws Exception {
                            formData = cpptkpForm;
                            setContentView(formData);
                        }
                    });
        }
        thumbnailSize = getResources().getDimensionPixelSize(R.dimen.attachment_thumbnail_size);
    }

    @OnClick(R.id.attachment_placeholder)
    void onClickPlaceholder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pilih pengambilan gambar")
                .setItems(new String[]{"Ambil gambar dari kamera", "Pilih dari galeri"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            dispatchTakePictureIntent();
                        } else {
                            dispatchPickPictureFromGallery();
                        }
                    }
                });
        builder.show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile;

            try {
                photoFile = FileHelper.createImageFile(getActivity());
                currentPhotoFile = photoFile.getAbsolutePath();

                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "id.wika.cppptkponline.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, FormHelper.REQUEST_IMAGE_CAPTURE);
            } catch (IOException e) {
                e.printStackTrace();
                currentPhotoFile = "";
            }
        }
    }

    private void dispatchPickPictureFromGallery() {
        permissionHelper.requestPermissionFromFragment(
                this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                PermissionHelper.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE,
                new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        createGetContentChooser();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        permissionHelper.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    private void createGetContentChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Pilih gambar via galeri"),
                FormHelper.REQUEST_SELECT_PICTURE_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FormHelper.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            FormHelper.launchEditPhotoActivity(this, currentPhotoFile);

            currentPhotoFile = "";
        } else if (requestCode == FormHelper.REQUEST_EDIT_IMAGE && resultCode == Activity.RESULT_OK) {
            final String path = data.getStringExtra(ImageHelper.EXTRA_KEY_IMAGE_FILE_PATH);
            loadAndAddThumbnail(path);
        } else if (requestCode == FormHelper.REQUEST_VIEW_IMAGE && resultCode == Activity.RESULT_OK) {
            final String path = data.getStringExtra(ImageHelper.EXTRA_KEY_IMAGE_FILE_PATH);

            View toBeRemovedView = pictureViewMap.get(path);
            attachmentContainer.removeView(toBeRemovedView);
            pictureViewMap.remove(path);
            picturePaths.remove(path);

            if (picturePaths.size() < 3) {
                attachmentPlaceholder.setVisibility(View.VISIBLE);
            }
        } else if (requestCode == FormHelper.REQUEST_SELECT_PICTURE_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            Uri selectedImageUri = data.getData();
            try {
                File sourceFile = new File(FileHelper.getPath(getActivity(), selectedImageUri));
                File destFile = FileHelper.createImageFile(getActivity());
                FileHelper.copyFile(sourceFile, destFile);
                FormHelper.launchEditPhotoActivity(this, destFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Gagal import gambar dari galeri", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void loadAndAddThumbnail(String path) {
        SimpleDraweeView imageView = FormHelper.getImageThumbnailViewerDrawee(this, thumbnailSize, path, true);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                thumbnailSize,
                thumbnailSize
        );
        params.setMargins(0, 0, getResources().getDimensionPixelSize(R.dimen.attachment_margin), 0);

        attachmentContainer.addView(imageView, attachmentContainer.getChildCount() - 1, params);
        pictureViewMap.put(path, imageView);
        picturePaths.add(path);

        if (picturePaths.size() >= 3) {
            attachmentPlaceholder.setVisibility(View.GONE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_rencana_tindak_lanjut_open, container, false);
        ButterKnife.bind(this, view);

        setupStaticInfo(view);

        return view;
    }

    private void setContentView(CPPTKPForm formData) {
        if (formData != null) {
            setupFieldKeputusanJangkaPendek();
            setupFieldWaktuKeputusanJangkaPendek();
            setupFieldWaktuKeputusanJangkaPanjang();
            if (formData.getRencanaTindakLanjut() != null)
                fieldPenyebabUtamaPenyimpangan.setText(formData.getRencanaTindakLanjut().getPenyebabUtamaPenyimpangan());
        }
    }

    private void setupStaticInfo(View view) {
        ((TextView) view.findViewById(R.id.user_fullname)).setText(userFullname);
        ((TextView) view.findViewById(R.id.user_division)).setText(userDivision);
        ((TextView) view.findViewById(R.id.user_position)).setText(userPosition);
    }

    private void setupFieldKeputusanJangkaPendek() {

        List<KeputusanJangkaPendekResponse> listProyek = dictionaryViewModel.getKeputusanJangkaPendekRemote(user);
        ClickToSelectEditText editText = (ClickToSelectEditText) fieldKeputusanJangkaPendek.getEditText();
        editText.setAdapter(new ClickToSelectEditTextListAdapter<KeputusanJangkaPendekResponse>(getActivity(), listProyek) {
            @Override
            public String getItemLabelToSet(KeputusanJangkaPendekResponse selectedItem) {
                return selectedItem.getNama();
            }
        });

        editText.setOnItemSelectedListener(new ClickToSelectEditText.OnItemSelectedListener<KeputusanJangkaPendekResponse>() {
            @Override
            public void onItemSelectedListener(KeputusanJangkaPendekResponse item, int selectedIndex) {
                fieldKeputusanJangkaPendekExtra.setHint(item.getNama());
                fieldKeputusanJangkaPendekExtra.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setupFieldWaktuKeputusanJangkaPendek() {
        fieldWaktuKeputusanJangkaPendek.getEditText().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialogFragment newFragment = new DatePickerDialogFragment();
                newFragment.setListener(new DatePickerDialogFragment.Listener() {
                    @Override
                    public void onDateSet(int year, int month, int day) {
                        dateJangkaPendek = new DateTime(year, month, day, 0, 0);
                        fieldWaktuKeputusanJangkaPendek.getEditText().setText(
                                dateTimeHelper.printInIndonesian(dateJangkaPendek)
                        );
                    }
                });
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });
    }

    private void setupFieldWaktuKeputusanJangkaPanjang() {
        fieldWaktuKeputusanJangkaPanjang.getEditText().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialogFragment newFragment = new DatePickerDialogFragment();
                newFragment.setListener(new DatePickerDialogFragment.Listener() {
                    @Override
                    public void onDateSet(int year, int month, int day) {
                        dateJangkaPanjang = new DateTime(year, month, day, 0, 0);
                        fieldWaktuKeputusanJangkaPanjang.getEditText().setText(
                                dateTimeHelper.printInIndonesian(dateJangkaPanjang)
                        );
                    }
                });
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });
    }

    @OnClick(R.id.history_laporan_awal_btn)
    void onClickViewHistoryButton() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_PENGECEKAN);
    }

    @OnClick(R.id.history_rencana_tindak_lanjut_btn)
    void onClickViewHistoryRencanaTindakLanjutButton() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT);
    }

    @OnClick(R.id.history_approval)
    void onClickViewHistoryApprovalButton() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI);
    }

    @OnClick(R.id.history_realisasi_tindak_lanjut_btn)
    void onClickViewHistoryRealisasiTindaklanjutButton() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_REALISASI_TINDAK_LANJUT);
    }

    @OnClick(R.id.history_verifikasi_tindak_lanjut)
    void onClickViewHistoryVerifikasiTindaklanjutButton() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN);
    }

    @Override
    public boolean isValid() {
        return isValidFieldKeputusanJangkaPendek()
                && (dateJangkaPanjang.getDayOfYear() > dateJangkaPendek.getDayOfYear())
                && isValidFieldKeputusanJangkaPanjang();
    }

    private boolean isValidFieldKeputusanJangkaPendek() {
        boolean isValidExtraField =
                fieldKeputusanJangkaPendekExtra.getVisibility() != VISIBLE
                        || FormHelper.isFormFieldContentValid(fieldKeputusanJangkaPendekExtra);
        return FormHelper.isFormFieldContentValid(fieldKeputusanJangkaPendek)
                && isValidExtraField
                && FormHelper.isFormFieldContentValid(fieldWaktuKeputusanJangkaPendek);
    }

    private boolean isValidFieldKeputusanJangkaPanjang() {
        return FormHelper.isFormFieldContentValid(fieldKeputusanJangkaPanjang)
                && FormHelper.isFormFieldContentValid(fieldWaktuKeputusanJangkaPanjang);
    }

    @Override
    public CPPTKPForm getFormData() {
        formData.setFormStatus(FormStatus.FORM_REALISASI_TINDAK_LANJUT);
        formData.setRemoteFormStatus(RemoteFormStatus.REALISASI_TINDAK_LANJUT);
        formData.setNeedResponse(false);

        DataRencanaTindakLanjut rencanaTindakLanjut = new DataRencanaTindakLanjut();
        rencanaTindakLanjut.setId(formData.getRencanaTindakLanjut().getId());
        rencanaTindakLanjut.setPenyebabUtamaPenyimpangan(fieldPenyebabUtamaPenyimpangan.getText().toString());
        rencanaTindakLanjut.setKeputusanJangkaPanjang(FormHelper.getTextFromField(fieldKeputusanJangkaPanjang));
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPanjang(dateJangkaPanjang);

        rencanaTindakLanjut.setKeputusanJangkaPendek(FormHelper.getTextFromField(fieldKeputusanJangkaPendek));
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPendek(dateJangkaPendek);

        CharSequence fieldKeputusanJangkaPendekExtraHint = fieldKeputusanJangkaPendekExtra.getHint();
        if (fieldKeputusanJangkaPendekExtraHint != null) {
            rencanaTindakLanjut.setTitleDetailKeputusanJangkaPendek(fieldKeputusanJangkaPendekExtraHint.toString());
            rencanaTindakLanjut.setDetailKeputusanJangkaPendek(FormHelper.getTextFromField(fieldKeputusanJangkaPendekExtra));
        } else {
            rencanaTindakLanjut.setDetailKeputusanJangkaPendek("");
        }

        User penanggungJawab = user;
        rencanaTindakLanjut.setPenanggungJawab(penanggungJawab);
        rencanaTindakLanjut.setRelatedFormId(formData.getFormId());

        formData.setRencanaTindakLanjut(rencanaTindakLanjut);

        DataRealisasiTindakLanjut realisasiTindakLanjut = new DataRealisasiTindakLanjut();
        realisasiTindakLanjut.setId(formData.getRealisasiTindakLanjut().getId());
        realisasiTindakLanjut.setListDokumentasiHasil(picturePaths);
        realisasiTindakLanjut.setRelatedFormId(formData.getFormId());
        realisasiTindakLanjut.setHasilPencegahanDanPerbaikan(FormHelper.getTextFromField(fieldIsianTindaklanjut));
        formData.setRealisasiTindakLanjut(realisasiTindakLanjut);

        return formData;
    }

    @Override
    public PostFormRequestGenerator.Strategy getPostFormStrategy(int formStatus) {
        return PostFormRequestGenerator.Strategy.POST_REVISI_TINDAK_LANJUT;
    }
}
