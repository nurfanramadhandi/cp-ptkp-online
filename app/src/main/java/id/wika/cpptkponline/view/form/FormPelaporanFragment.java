package id.wika.cpptkponline.view.form;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;

import org.joda.time.DateTime;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataFormPelaporan;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import id.wika.cpptkponline.data.FormDataCatalogue;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.endpoint.RemoteFormStatus;
import id.wika.cpptkponline.data.endpoint.request.PostFormRequestGenerator;
import id.wika.cpptkponline.data.endpoint.response.RoleUnitKerjaResponse;
import id.wika.cpptkponline.data.model.LingkupTemuan;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FileHelper;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.helper.ImageHelper;
import id.wika.cpptkponline.helper.PermissionHelper;
import id.wika.cpptkponline.helper.StringHelper;
import id.wika.cpptkponline.helper.UserHelper;
import id.wika.cpptkponline.presenter.DictionaryViewModel;
import id.wika.cpptkponline.view.widget.ClickToSelectEditText;
import id.wika.cpptkponline.view.widget.ClickToSelectEditText.OnItemSelectedListener;
import id.wika.cpptkponline.view.widget.ClickToSelectEditTextListAdapter;
import id.wika.cpptkponline.view.widget.StringClickToSelectEditTextListAdapter;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class FormPelaporanFragment extends Fragment implements CPPTKPFormView {

    public static final String TAG = "FormPelaporanFragment";

    private DateTime formDate = DateTime.now();

    private String currentPhotoFile;
    private Map<String, View> pictureViewMap = new HashMap<>();
    private ArrayList<String> picturePaths = new ArrayList<>();

    @BindView(R.id.attachment_container)
    LinearLayout attachmentContainer;

    @BindView(R.id.attachment_placeholder)
    View attachmentPlaceholder;

    // FORM FIELDS

    @BindView(R.id.field_project_name)
    TextInputLayout fieldProjectName;

    @BindView(R.id.field_lokasi_penyimpangan)
    TextInputLayout fieldLokasiPenyimpangan;

    @BindView(R.id.field_bentuk_penyimpangan)
    TextInputLayout fieldBentukPenyimpangan;

    @BindView(R.id.extended_form_field)
    View extendedFormField;

    @BindView(R.id.field_kategori)
    TextInputLayout fieldKategori;

    @BindView(R.id.field_lingkup_temuan)
    TextInputLayout fieldLingkupTemuan;

    @BindView(R.id.field_detail_lingkup_temuan)
    TextInputLayout fieldDetailLingkupTemuan;

    @BindView(R.id.field_divisi_penanggungjawab)
    TextInputLayout fieldDivisiPenanggungJawab;

    @BindView(R.id.field_nama_penanggungjawab)
    TextInputLayout fieldNamaPenanggungJawab;

    private User user;
    private DictionaryViewModel dictionaryViewModel;
    private int thumbnailSize;

    private PermissionHelper permissionHelper = new PermissionHelper();

    public FormPelaporanFragment() {
    }

    public static FormPelaporanFragment newInstance() {
        return new FormPelaporanFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = User.load(getActivity());
        dictionaryViewModel = new DictionaryViewModel();

        thumbnailSize = getResources().getDimensionPixelSize(R.dimen.attachment_thumbnail_size);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_pelaporan, container, false);
        ButterKnife.bind(this, view);

        // SET STATIC INFO
        setupStaticInfo(view);

        // SET FORM
        setupFieldNamaProject();
        if (isQaRole()) {
            setupFieldKategori();
            setupFieldLingkupTemuan();
            setupFieldPenanggungJawab();
            extendedFormField.setVisibility(View.VISIBLE);
        } else {
            extendedFormField.setVisibility(View.GONE);
        }

        return view;
    }

    private boolean isQaRole() {
        return UserHelper.isQARole(user);
    }

    private void setupStaticInfo(View view) {
        ((TextView) view.findViewById(R.id.user_fullname)).setText(user.getFullname());
        ((TextView) view.findViewById(R.id.user_position)).setText(user.getJabatan());

        DateTimeHelper dateTimeHelper = new DateTimeHelper();
        ((TextView) view.findViewById(R.id.form_date)).setText(dateTimeHelper.printInIndonesian(formDate));
    }

    private void setupFieldNamaProject() {

        List<String> listProyek = dictionaryViewModel.getProyekRemote(user);

        ((ClickToSelectEditText) fieldProjectName.getEditText()).setAdapter(
                new StringClickToSelectEditTextListAdapter(getActivity(), listProyek)
        );
    }

    private void setupFieldKategori() {
        ((ClickToSelectEditText) fieldKategori.getEditText()).setAdapter(
                new StringClickToSelectEditTextListAdapter(getActivity(), FormDataCatalogue.getListFieldKategori()));
    }

    private void setupFieldLingkupTemuan() {

        List<LingkupTemuan> listProyek = dictionaryViewModel.getLingkupTemuanJenisRemote(user);

        ClickToSelectEditText editText = (ClickToSelectEditText) fieldLingkupTemuan.getEditText();
        editText.setAdapter(new ClickToSelectEditTextListAdapter<LingkupTemuan>(getActivity(), listProyek) {
            @Override
            public String getItemLabelToSet(LingkupTemuan selectedItem) {
                return selectedItem.getTitle();
            }
        });

        editText.setOnItemSelectedListener(new OnItemSelectedListener<LingkupTemuan>() {
            @Override
            public void onItemSelectedListener(LingkupTemuan item, int selectedIndex) {
                if (item.getTitle().length() > 0) {
                    setupFieldDetailLingkupTemuan(item);
                    fieldDetailLingkupTemuan.setVisibility(View.VISIBLE);

                } else {
                    fieldDetailLingkupTemuan.setVisibility(View.GONE);
                }
            }
        });
    }

    private void setupFieldDetailLingkupTemuan(LingkupTemuan item) {

        List<String> listProyek = dictionaryViewModel.getLingkupTemuanRemote(user, item.getTitle());

        ((ClickToSelectEditText) fieldDetailLingkupTemuan.getEditText())
                .setAdapter(new StringClickToSelectEditTextListAdapter(getActivity(), listProyek));
        fieldDetailLingkupTemuan.setVisibility(View.VISIBLE);

    }

    private void setupFieldPenanggungJawab() {
        List<RoleUnitKerjaResponse> listProyek = dictionaryViewModel.getDivisiAllRemote(user);

        ClickToSelectEditText editText = (ClickToSelectEditText) fieldDivisiPenanggungJawab.getEditText();
        editText.setAdapter(new ClickToSelectEditTextListAdapter<RoleUnitKerjaResponse>(getActivity(), listProyek) {
            @Override
            public String getItemLabelToSet(RoleUnitKerjaResponse selectedItem) {
                return selectedItem.nama;
            }
        });

        editText.setOnItemSelectedListener(new ClickToSelectEditText.OnItemSelectedListener<RoleUnitKerjaResponse>() {
            @Override
            public void onItemSelectedListener(RoleUnitKerjaResponse item, int selectedIndex) {

                setupFieldNamaPenanggungJawab(user, item.getKode());
            }
        });
    }

    private void setupFieldNamaPenanggungJawab(User user, String divisi) {
        String proyek = FormHelper.getTextFromField(fieldProjectName);
        List<User> listProyek = dictionaryViewModel.getUserByDivisiRemote(user, divisi, proyek);
        fieldNamaPenanggungJawab.setVisibility(View.VISIBLE);
        fieldNamaPenanggungJawab.getEditText().setText("");

        ((ClickToSelectEditText) fieldNamaPenanggungJawab.getEditText())
                .setAdapter(new ClickToSelectEditTextListAdapter<User>(getActivity(), listProyek) {
                    @Override
                    public String getItemLabelToSet(User selectedItem) {
                        return selectedItem.getFullname() + " - " +selectedItem.getJabatan();
                    }
                });
    }

    @OnClick(R.id.attachment_placeholder)
    void onClickPlaceholder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pilih pengambilan gambar")
                .setItems(new String[]{"Ambil gambar dari kamera", "Pilih dari galeri"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            dispatchTakePictureIntent();
                        } else {
                            dispatchPickPictureFromGallery();
                        }
                    }
                });
        builder.show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile;

            try {
                photoFile = FileHelper.createImageFile(getActivity());
                currentPhotoFile = photoFile.getAbsolutePath();

                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "id.wika.cppptkponline.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, FormHelper.REQUEST_IMAGE_CAPTURE);
            } catch (IOException e) {
                e.printStackTrace();
                currentPhotoFile = "";
            }
        }
    }

    private void dispatchPickPictureFromGallery() {
        permissionHelper.requestPermissionFromFragment(
                this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                PermissionHelper.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE,
                new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        createGetContentChooser();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        permissionHelper.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    private void createGetContentChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Pilih gambar via galeri"),
                FormHelper.REQUEST_SELECT_PICTURE_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FormHelper.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            FormHelper.launchEditPhotoActivity(this, currentPhotoFile);

            currentPhotoFile = "";
        } else if (requestCode == FormHelper.REQUEST_EDIT_IMAGE && resultCode == Activity.RESULT_OK) {
            final String path = data.getStringExtra(ImageHelper.EXTRA_KEY_IMAGE_FILE_PATH);
            loadAndAddThumbnail(path);
        } else if (requestCode == FormHelper.REQUEST_VIEW_IMAGE && resultCode == Activity.RESULT_OK) {
            final String path = data.getStringExtra(ImageHelper.EXTRA_KEY_IMAGE_FILE_PATH);

            View toBeRemovedView = pictureViewMap.get(path);
            attachmentContainer.removeView(toBeRemovedView);
            pictureViewMap.remove(path);
            picturePaths.remove(path);

            if (picturePaths.size() < 3) {
                attachmentPlaceholder.setVisibility(View.VISIBLE);
            }
        } else if (requestCode == FormHelper.REQUEST_SELECT_PICTURE_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            Uri selectedImageUri = data.getData();
            try {
                File sourceFile = new File(FileHelper.getPath(getActivity(), selectedImageUri));
                File destFile = FileHelper.createImageFile(getActivity());
                FileHelper.copyFile(sourceFile, destFile);
                FormHelper.launchEditPhotoActivity(this, destFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Gagal import gambar dari galeri", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void loadAndAddThumbnail(final String path) {
        SimpleDraweeView imageView = FormHelper.getImageThumbnailViewerDrawee(this, thumbnailSize, path, true);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                thumbnailSize,
                thumbnailSize
        );
        params.setMargins(0, 0, getResources().getDimensionPixelSize(R.dimen.attachment_margin), 0);

        attachmentContainer.addView(imageView, attachmentContainer.getChildCount() - 1, params);
        pictureViewMap.put(path, imageView);
        picturePaths.add(path);

        if (picturePaths.size() >= 3) {
            attachmentPlaceholder.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean isValid() {
        return FormHelper.isFormFieldContentValid(fieldProjectName)
                && FormHelper.isFormFieldContentValid(fieldLokasiPenyimpangan)
                && FormHelper.isFormFieldContentValid(fieldBentukPenyimpangan)
                && isValidExtendedField()
                && isPictureValid();

    }

    private boolean isValidExtendedField() {
        if (isQaRole()) {
            return FormHelper.isFormFieldContentValid(fieldKategori)
                    && FormHelper.isFormFieldContentValid(fieldLingkupTemuan)
                    && isLingkupTemuanExtraValid()
                    && isPictureValid();
        } else {
            return true;
        }
    }

    private boolean isLingkupTemuanExtraValid() {
        return fieldDetailLingkupTemuan.getVisibility() != View.VISIBLE
                || FormHelper.isFormFieldContentValid(fieldDetailLingkupTemuan);
    }

    private boolean isPictureValid() {
        return !picturePaths.isEmpty();
    }

    @Override
    public CPPTKPForm getFormData() {
        CPPTKPForm cpptkpForm = new CPPTKPForm();
        cpptkpForm.setTimestamp(DateTime.now());
        cpptkpForm.setFormStatus(FormStatus.FORM_AWAL);
        cpptkpForm.setRemoteFormStatus(RemoteFormStatus.DRAFT);

        DataFormPelaporan formPelaporan = new DataFormPelaporan();
        formPelaporan.setPenemu(user);
        formPelaporan.setNamaProyek(FormHelper.getTextFromField(fieldProjectName));
        formPelaporan.setFormDate(formDate);
        formPelaporan.setLokasiPenyimpangan(FormHelper.getTextFromField(fieldLokasiPenyimpangan));
        formPelaporan.setBentukPenyimpangan(FormHelper.getTextFromField(fieldBentukPenyimpangan));
        formPelaporan.setAttachmentPaths(picturePaths);

        String kategori = FormHelper.getTextFromField(fieldKategori);
        if (StringHelper.isNotBlank(kategori)) {
            cpptkpForm.setFormStatus(FormStatus.FORM_PENGECEKAN);
            cpptkpForm.setRemoteFormStatus(RemoteFormStatus.PENGECEKAN);

            formPelaporan.setKategoriForm(FormHelper.parseKategoriToConstant(kategori));
            formPelaporan.setLingkupTemuan(FormHelper.getTextFromField(fieldLingkupTemuan));
            formPelaporan.setDetailLingkupTemuan(FormHelper.getTextFromField(fieldDetailLingkupTemuan));
        }

        cpptkpForm.setFormPelaporan(formPelaporan);

        String namaPenanggungJawab = FormHelper.getTextFromField(fieldNamaPenanggungJawab);
        if (StringHelper.isNotBlank(namaPenanggungJawab)) {
            DataRencanaTindakLanjut rencanaTindakLanjut = new DataRencanaTindakLanjut();
            User penanggungJawab = new User();
            penanggungJawab.setFullname(namaPenanggungJawab);
            penanggungJawab.setDivisi(FormHelper.getTextFromField(fieldDivisiPenanggungJawab));
            rencanaTindakLanjut.setPenanggungJawab(penanggungJawab);

            cpptkpForm.setRencanaTindakLanjut(rencanaTindakLanjut);
        }

        return cpptkpForm;
    }

    @Override
    public PostFormRequestGenerator.Strategy getPostFormStrategy(int formStatus) {
        if (formStatus == FormStatus.FORM_PENGECEKAN) {
            return PostFormRequestGenerator.Strategy.POST_TEMUAN_QA;
        } else {
            return PostFormRequestGenerator.Strategy.POST_TEMUAN;
        }
    }
}
