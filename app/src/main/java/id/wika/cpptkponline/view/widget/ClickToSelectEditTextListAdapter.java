package id.wika.cpptkponline.view.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.wika.cpptkponline.R;

/**
 * Created by mamang_kompii on 9/7/17.
 */

public abstract class ClickToSelectEditTextListAdapter<T> extends ArrayAdapter<T> {

    public ClickToSelectEditTextListAdapter(Context context, List<T> data) {
        super(context, R.layout.field_dialog_list_item, data);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setText(getItemLabelToSet(getItem(position)));
        return view;
    }

    public abstract String getItemLabelToSet(T selectedItem);
}
