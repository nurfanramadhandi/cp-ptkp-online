package id.wika.cpptkponline.view.widget;

import android.content.Context;

import java.util.List;

/**
 * Created by mamang_kompii on 9/7/17.
 */

public class StringClickToSelectEditTextListAdapter extends ClickToSelectEditTextListAdapter<String> {

    public StringClickToSelectEditTextListAdapter(Context context, List<String> data) {
        super(context, data);
    }

    @Override
    public String getItemLabelToSet(String selectedItem) {
        return selectedItem;
    }
}
