package id.wika.cpptkponline.view.form;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import id.wika.cpptkponline.data.FormDataCatalogue;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.endpoint.RemoteFormStatus;
import id.wika.cpptkponline.data.endpoint.request.PostFormRequestGenerator;
import id.wika.cpptkponline.data.endpoint.response.KeputusanJangkaPendekResponse;
import id.wika.cpptkponline.data.endpoint.response.LingkupTemuanResponse;
import id.wika.cpptkponline.data.model.KeputusanJangkaPendek;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.presenter.DictionaryViewModel;
import id.wika.cpptkponline.presenter.FormViewModel;
import id.wika.cpptkponline.view.dialog.DatePickerDialogFragment;
import id.wika.cpptkponline.view.widget.ClickToSelectEditText;
import id.wika.cpptkponline.view.widget.ClickToSelectEditTextListAdapter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static id.wika.cpptkponline.view.widget.ClickToSelectEditText.OnClickListener;
import static id.wika.cpptkponline.view.widget.ClickToSelectEditText.VISIBLE;

public class FormRencanaTindakLanjutFragment extends Fragment implements CPPTKPFormView {

    public static final String TAG = "FormRencanaTindakLanjutFragment";

    private DateTimeHelper dateTimeHelper = new DateTimeHelper();

    private String userFullname;
    private String userDivision;
    private String userPosition;

    private CPPTKPForm formData;

    @BindView(R.id.field_penyebab_utama_penyimpangan)
    TextInputLayout fieldPenyebabUtamaPenyimpangan;

    @BindView(R.id.field_keputusan_jangka_pendek)
    TextInputLayout fieldKeputusanJangkaPendek;

    @BindView(R.id.field_keputusan_jangka_pendek_extra)
    TextInputLayout fieldKeputusanJangkaPendekExtra;

    @BindView(R.id.field_waktu_keputusan_jangka_pendek)
    TextInputLayout fieldWaktuKeputusanJangkaPendek;

    @BindView(R.id.field_keputusan_jangka_panjang)
    TextInputLayout fieldKeputusanJangkaPanjang;

    @BindView(R.id.field_waktu_keputusan_jangka_panjang)
    TextInputLayout fieldWaktuKeputusanJangkaPanjang;

    private User user;
    DateTime dateJangkaPendek;
    DateTime dateJangkaPanjang;
    private int formId;
    private DictionaryViewModel dictionaryViewModel;

    public static FormRencanaTindakLanjutFragment newInstance(int formId) {
        Bundle args = new Bundle();
        args.putInt(FormHelper.EXTRA_KEY_FORM_ID, formId);

        FormRencanaTindakLanjutFragment fragment = new FormRencanaTindakLanjutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = User.load(getActivity());
            userFullname = user.getFullname();
            userDivision = user.getDivisi();
            userPosition = user.getJabatan();
            formId = getArguments().getInt(FormHelper.EXTRA_KEY_FORM_ID);
            dictionaryViewModel = new DictionaryViewModel();

            FormViewModel formViewModel = ViewModelProviders
                    .of(getActivity())
                    .get(FormViewModel.class);
            formViewModel.getFormData(formId)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<CPPTKPForm>() {
                        @Override
                        public void accept(CPPTKPForm cpptkpForm) throws Exception {
                            formData = cpptkpForm;
                            setContentView(formData);
                        }
                    });
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_rencana_tindak_lanjut, container, false);
        ButterKnife.bind(this, view);

        // SET STATIC INFO
        setupStaticInfo(view);

        // SET FORM


        return view;
    }

    private void setContentView(CPPTKPForm formData) {
        if (formData != null && formData.getRemoteFormStatus().equals("pengecekan")) {
            setupFieldKeputusanJangkaPendek();
            setupFieldWaktuKeputusanJangkaPendek();
            setupFieldWaktuKeputusanJangkaPanjang();
        }
    }

    private void setupStaticInfo(View view) {
        ((TextView) view.findViewById(R.id.user_fullname)).setText(userFullname);
        ((TextView) view.findViewById(R.id.user_division)).setText(userDivision);
        ((TextView) view.findViewById(R.id.user_position)).setText(userPosition);
    }

    private void setupFieldKeputusanJangkaPendek() {

        List<KeputusanJangkaPendekResponse> listProyek = dictionaryViewModel.getKeputusanJangkaPendekRemote(user);
        ClickToSelectEditText editText = (ClickToSelectEditText) fieldKeputusanJangkaPendek.getEditText();
        editText.setAdapter(new ClickToSelectEditTextListAdapter<KeputusanJangkaPendekResponse>(getActivity(), listProyek) {
            @Override
            public String getItemLabelToSet(KeputusanJangkaPendekResponse selectedItem) {
                return selectedItem.getNama();
            }
        });

        editText.setOnItemSelectedListener(new ClickToSelectEditText.OnItemSelectedListener<KeputusanJangkaPendekResponse>() {
            @Override
            public void onItemSelectedListener(KeputusanJangkaPendekResponse item, int selectedIndex) {
                fieldKeputusanJangkaPendekExtra.setHint(item.getNama());
                fieldKeputusanJangkaPendekExtra.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setupFieldWaktuKeputusanJangkaPendek() {
        fieldWaktuKeputusanJangkaPendek.getEditText().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialogFragment newFragment = new DatePickerDialogFragment();
                newFragment.setListener(new DatePickerDialogFragment.Listener() {
                    @Override
                    public void onDateSet(int year, int month, int day) {
                        dateJangkaPendek = new DateTime(year, month, day, 0, 0);
                        fieldWaktuKeputusanJangkaPendek.getEditText().setText(
                                dateTimeHelper.printInIndonesian(dateJangkaPendek)
                        );
                    }
                });
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });
    }

    private void setupFieldWaktuKeputusanJangkaPanjang() {
        fieldWaktuKeputusanJangkaPanjang.getEditText().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialogFragment newFragment = new DatePickerDialogFragment();
                newFragment.setListener(new DatePickerDialogFragment.Listener() {
                    @Override
                    public void onDateSet(int year, int month, int day) {
                        dateJangkaPanjang = new DateTime(year, month, day, 0, 0);
                        fieldWaktuKeputusanJangkaPanjang.getEditText().setText(
                                dateTimeHelper.printInIndonesian(dateJangkaPanjang)
                        );
                    }
                });
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });
    }

    @OnClick(R.id.history_laporan_awal_btn)
    void onClickViewHistoryButton() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_PENGECEKAN);
    }

    @Override
    public boolean isValid() {
        return FormHelper.isFormFieldContentValid(fieldPenyebabUtamaPenyimpangan)
                && isValidFieldKeputusanJangkaPendek()
                && (dateJangkaPendek.getDayOfYear() < dateJangkaPanjang.getDayOfYear())
                && isValidFieldKeputusanJangkaPanjang();
    }

    private boolean isValidFieldKeputusanJangkaPendek() {
        boolean isValidExtraField =
                fieldKeputusanJangkaPendekExtra.getVisibility() != VISIBLE
                        || FormHelper.isFormFieldContentValid(fieldKeputusanJangkaPendekExtra);
        return FormHelper.isFormFieldContentValid(fieldKeputusanJangkaPendek)
                && isValidExtraField
                && FormHelper.isFormFieldContentValid(fieldWaktuKeputusanJangkaPendek);
    }

    private boolean isValidFieldKeputusanJangkaPanjang() {
        return FormHelper.isFormFieldContentValid(fieldKeputusanJangkaPanjang)
                && FormHelper.isFormFieldContentValid(fieldWaktuKeputusanJangkaPanjang);
    }

    @Override
    public CPPTKPForm getFormData() {
        formData.setFormStatus(FormStatus.FORM_RENCANA_TINDAK_LANJUT);
        formData.setRemoteFormStatus(RemoteFormStatus.RENCANA_TINDAK_LANJUT);
        formData.setNeedResponse(false);

        DataRencanaTindakLanjut rencanaTindakLanjut = new DataRencanaTindakLanjut();

        rencanaTindakLanjut.setPenyebabUtamaPenyimpangan(FormHelper.getTextFromField(fieldPenyebabUtamaPenyimpangan));

        rencanaTindakLanjut.setKeputusanJangkaPanjang(FormHelper.getTextFromField(fieldKeputusanJangkaPanjang));
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPanjang(dateJangkaPanjang);

        rencanaTindakLanjut.setKeputusanJangkaPendek(FormHelper.getTextFromField(fieldKeputusanJangkaPendek));
        rencanaTindakLanjut.setTargetWaktuKeputusanJangkaPendek(dateJangkaPendek);
        CharSequence fieldKeputusanJangkaPendekExtraHint = fieldKeputusanJangkaPendekExtra.getHint();
        if (fieldKeputusanJangkaPendekExtraHint != null) {
            rencanaTindakLanjut.setTitleDetailKeputusanJangkaPendek(fieldKeputusanJangkaPendekExtraHint.toString());
            rencanaTindakLanjut.setDetailKeputusanJangkaPendek(FormHelper.getTextFromField(fieldKeputusanJangkaPendekExtra));
        } else {
            rencanaTindakLanjut.setDetailKeputusanJangkaPendek("");
        }

        User penanggungJawab = user;
        rencanaTindakLanjut.setPenanggungJawab(penanggungJawab);
        rencanaTindakLanjut.setRelatedFormId(formData.getFormId());

        formData.setRencanaTindakLanjut(rencanaTindakLanjut);

        return formData;
    }

    @Override
    public PostFormRequestGenerator.Strategy getPostFormStrategy(int formStatus) {
        return PostFormRequestGenerator.Strategy.POST_RENCANA_TINDAK_LANJUT;
    }
}
