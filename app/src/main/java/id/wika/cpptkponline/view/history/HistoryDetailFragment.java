package id.wika.cpptkponline.view.history;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.endpoint.response.PostTemuanListResponse;
import id.wika.cpptkponline.injection.AppComponent;
import id.wika.cpptkponline.injection.DaggerHistoryDetailViewComponent;
import id.wika.cpptkponline.injection.HistoryDetailViewModule;
import id.wika.cpptkponline.presenter.HistoryDetailPresenter;
import id.wika.cpptkponline.presenter.PresenterFactory;
import id.wika.cpptkponline.view.HistoryDetailView;
import id.wika.cpptkponline.view.impl.BaseFragment;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:54:07 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/


public final class HistoryDetailFragment extends BaseFragment<HistoryDetailPresenter, HistoryDetailView> implements HistoryDetailView {
    @Inject
    PresenterFactory<HistoryDetailPresenter> mPresenterFactory;
    @BindView(R.id.form_list)
    RecyclerView formList;
    Unbinder unbinder;
    private int id;
    private String title;
    private String subtitle;
    private HistoryAdapter adapter;
    // Your presenter is available using the mPresenter variable

    public HistoryDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_detail, container, false);

        if (getArguments().getString("title") != null) {
            id = getArguments().getInt("id");
            title = getArguments().getString("title");
            subtitle = getArguments().getString("subtitle");
            ((HistoryActivity) getActivity()).getSupportActionBar().setTitle(title+" - "+subtitle);
        }else{
            id = 0;
            title = "No Data";
            subtitle =  "";
            ((HistoryActivity) getActivity()).getSupportActionBar().setTitle(title+" - "+subtitle);
        }
        unbinder = ButterKnife.bind(this, view);
        initFormList();
        return view;
    }

    private void initFormList() {
        adapter = new HistoryAdapter(new ArrayList<PostTemuanListResponse.Datum>());
        formList.setAdapter(adapter);
        formList.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mPresenter != null) {
            mPresenter.getListData(id);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // TODO Add your menu entries here
        inflater.inflate(R.menu.fragment_history_menu, menu);
        setSearchView(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setSearchView(Menu menu) {
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new HistoryQueryListener(adapter));

        MenuItemCompat.setOnActionExpandListener(item, new HistoryQueryExpandListener(adapter));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_setting:
                break;
        }
        return true;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Your code here
        // Do not call mPresenter from here, it will be null! Wait for onStart
    }

    @Override
    protected void setupComponent(@NonNull AppComponent parentComponent) {
        DaggerHistoryDetailViewComponent.builder()
                .appComponent(parentComponent)
                .historyDetailViewModule(new HistoryDetailViewModule())
                .build()
                .inject(this);
    }

    @NonNull
    @Override
    protected PresenterFactory<HistoryDetailPresenter> getPresenterFactory() {
        return mPresenterFactory;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context context() {
        return null;
    }

    @Override
    public void renderData(List<PostTemuanListResponse.Datum> formListItems) {
        Toast.makeText(getActivity(), formListItems.get(0).getActivity(), Toast.LENGTH_SHORT).show();
        if (adapter != null)
            adapter.setData(formListItems);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
