package id.wika.cpptkponline.view.history;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.endpoint.response.PostTemuanListResponse;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.view.home.list.FormListItem;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 20/September/2017 - 11:23
 * Email   : ardhimaarik2@gmail.com
 * Project : KuTulis
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private List<PostTemuanListResponse.Datum> list;
    private List<PostTemuanListResponse.Datum> listTemp;
    private  DateTimeHelper dateTimeHelper = new DateTimeHelper();
    private final HistoryAdapterFilter adapterFilter;

    public HistoryAdapter(List<PostTemuanListResponse.Datum> list) {
        this.adapterFilter = new HistoryAdapterFilter(this);
        this.list = new ArrayList<PostTemuanListResponse.Datum>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.form_summary_list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        PostTemuanListResponse.Datum item = list.get(i);
        viewHolder.itemTitle.setText(item.getActivity());
        viewHolder.statusImage.setImageResource(FormHelper.getFormStatusIcon(item.getStatus()));
        if(item.getCreatedAt()!="")
            viewHolder.itemSummary.setText("Created At "+item.getCreatedAt().substring(0,10));
        else
            viewHolder.itemSummary.setText("Created At "+item.getCreatedAt());

        viewHolder.itemSummaryStatus.setText("Created By " +item.getCreatedBy());
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setData(List<PostTemuanListResponse.Datum> data) {
        this.list = data;
        this.adapterFilter.setRecordModelList(data);
        this.notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.status_image)
        ImageView statusImage;
        @BindView(R.id.extra_status)
        View extraStatus;
        @BindView(R.id.item_title)
        TextView itemTitle;
        @BindView(R.id.item_summary)
        TextView itemSummary;
        @BindView(R.id.item_summary_status)
        TextView itemSummaryStatus;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void startCollapsingFilter() {
        listTemp = list;
    }

    public void stopCollapsingFilter() {
        if (listTemp != null) {
            setData(listTemp);
        }
    }
    public HistoryAdapterFilter getAdapterFilter() {
        return adapterFilter;
    }

    public void setFilterData(Collection<PostTemuanListResponse.Datum> metaTranscriptModelCollection) {
        this.list = (List<PostTemuanListResponse.Datum>) metaTranscriptModelCollection;
        this.notifyDataSetChanged();
    }
}
