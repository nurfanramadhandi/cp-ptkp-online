package id.wika.cpptkponline.view.history;

import android.support.v4.view.MenuItemCompat;
import android.view.MenuItem;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 24/09/17 - 11:00
 * Email   : ardhimaarik2@gmail.com
 * Project : Sample App
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/
public class HistoryQueryExpandListener implements MenuItemCompat.OnActionExpandListener {
    private HistoryAdapter adapter;

    public HistoryQueryExpandListener(HistoryAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        adapter.startCollapsingFilter();
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        adapter.stopCollapsingFilter();
        return true;
    }
}
