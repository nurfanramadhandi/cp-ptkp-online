package id.wika.cpptkponline.view.laporan;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.endpoint.response.GetListLaporan;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 28/September/2017 - 10:14
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public class LaporanAdaptor extends RecyclerView.Adapter<LaporanAdaptor.ViewHolder> {
    private List<GetListLaporan.Datum> datas;
    private OnListClick listener;

    public LaporanAdaptor(OnListClick listener) {
        this.datas = new ArrayList<GetListLaporan.Datum>();
        this.listener = listener;
    }

    public interface OnListClick{
        void onItemClick(GetListLaporan.Datum item);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_laporan, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final GetListLaporan.Datum item = datas.get(i);
        viewHolder.firstLine.setText(item.getNamaProyek());
        viewHolder.secondLine.setText(item.getStatus());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public List<GetListLaporan.Datum> getDatas() {
        return datas;
    }

    public void setDatas(List<GetListLaporan.Datum> datas) {
        this.datas = datas;
        this.notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.icon)
        ImageView icon;
        @BindView(R.id.secondLine)
        TextView secondLine;
        @BindView(R.id.firstLine)
        TextView firstLine;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
