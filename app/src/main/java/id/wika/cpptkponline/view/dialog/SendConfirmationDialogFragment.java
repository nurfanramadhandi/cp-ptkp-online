package id.wika.cpptkponline.view.dialog;

/**
 * Created by mamang_kompii on 8/30/17.
 */

public class SendConfirmationDialogFragment extends GeneralConfirmationDialogFragment {

    public static final String TAG = "SendConfirmationDialogFragment";
    private Runnable action;

    @Override
    protected boolean isTitleLabelVisible() {
        return true;
    }

    @Override
    protected String getTitleText() {
        return "Kirim Data";
    }

    @Override
    protected String getDescriptionText() {
        return "Apakah Anda ingin mengirim form laporan ini?";
    }

    @Override
    protected String getNegativeButtonText() {
        return "BATAL";
    }

    @Override
    protected boolean isNeutralButtonVisible() {
        return false;
    }

    @Override
    protected String getNeutralButtonText() {
        return null;
    }

    @Override
    protected String getPositiveButtonText() {
        return "KIRIM";
    }

    @Override
    void onNegativeButtonClicked() {
        dismiss();
    }

    @Override
    void onNeutralButtonClicked() {
    }

    @Override
    void onPositiveButtonClicked() {
        dismiss();

        if (action != null) {
            action.run();
        }
    }

    public void setAction(Runnable action) {
        this.action = action;
    }
}
