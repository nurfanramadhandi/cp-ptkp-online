package id.wika.cpptkponline.view.review;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.presenter.DefaultViewModelFactory;
import id.wika.cpptkponline.presenter.FormViewModel;
import id.wika.cpptkponline.presenter.ReviewViewModel;
import id.wika.cpptkponline.view.form.FormActivity;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class ReviewFormActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar actionToolbar;

    private int formId;
    private boolean isReviewWithHistory;
    private FormViewModel formViewModel;
    private boolean isUsingRemoteData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_form);
        ButterKnife.bind(this);

        SharedPreferences sharedPreferences = getSharedPreferences(ServiceGenerator.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        ViewModelProviders
                .of(this, new DefaultViewModelFactory(this, sharedPreferences))
                .get(ReviewViewModel.class);

        formViewModel = ViewModelProviders
                .of(this, new DefaultViewModelFactory(this, sharedPreferences))
                .get(FormViewModel.class);


        initActionBar();
        initFragment(savedInstanceState);
    }

    private void initActionBar() {
        setSupportActionBar(actionToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        actionToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initFragment(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            return;
        }

        formId = getIntent().getIntExtra(FormHelper.EXTRA_KEY_FORM_ID, -1);
        isReviewWithHistory = getIntent().getBooleanExtra(FormHelper.EXTRA_KEY_IS_REVIEW_WITH_HISTORY, false);
        isUsingRemoteData = getIntent().getBooleanExtra(FormHelper.EXTRA_KEY_REVIEW_WITH_REMOTE_DATA, false);
        int itemStatus = getIntent().getIntExtra(FormHelper.EXTRA_KEY_FORM_STATUS, -1);
        setDataToView(itemStatus);
    }

    private void setDataToView(int itemStatus) {
        getSupportActionBar().setTitle(getBarTitle(itemStatus));

        try {
            bindFragmentWithData(itemStatus);

        } catch (Exception e) {
            Toast.makeText(ReviewFormActivity.this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private String getBarTitle(int formStatus) {
        switch (formStatus) {
            case FormStatus.FORM_AWAL:
                return "Form Pengecekan Awal";
            case FormStatus.FORM_PENGECEKAN:
                return "Form Pengecekan";
            case FormStatus.FORM_RENCANA_TINDAK_LANJUT:
                return "Rencana Tindak Lanjut";
            case FormStatus.FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT:
                return "Verifikasi Tindak Lanjut";
            case FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED:
                return "Verifikasi Tindak Lanjut (Closed)";
            case FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN:
                return "Verifikasi Tindak Lanjut (Open)";
            case FormStatus.FORM_REALISASI_TINDAK_LANJUT:
                return "Realisasi Tindak Lanjut";
            case FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI:
                return "Approval Disetujui";
            case FormStatus.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN:
                return "Approval Ditolak";
            default:
                return "";
        }
    }

    private void bindFragmentWithData(int itemStatus) {
        Fragment formFragment = null;

        switch (itemStatus) {
            case FormStatus.FORM_AWAL:
            case FormStatus.FORM_PENGECEKAN:
                formFragment = ReviewFormPelaporanFragment.newInstance(formId, isUsingRemoteData);
                break;
            case FormStatus.FORM_RENCANA_TINDAK_LANJUT:
                formFragment = ReviewRencanaTindakLanjutFragment.newInstance(formId, isReviewWithHistory, isUsingRemoteData);
                break;
            case FormStatus.FORM_REALISASI_TINDAK_LANJUT:
                formFragment = ReviewHasilTindakLanjutFragment.newInstance(formId, isReviewWithHistory, isUsingRemoteData);
                break;
            case FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI:
                formFragment = ReviewFormApprovalFragment.newInstance(formId, isReviewWithHistory, isUsingRemoteData);
                break;
            case FormStatus.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN:
                formFragment = ReviewFormApprovalFragment.newInstance(formId, isReviewWithHistory, isUsingRemoteData);
                break;
            case FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED:
                formFragment = ReviewVerifikasiHasilFragment.newInstance(formId,isReviewWithHistory, isUsingRemoteData);
                break;
            case FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN:
                formFragment = ReviewVerifikasiHasilFragment.newInstance(formId,isReviewWithHistory, isUsingRemoteData);
                break;
            case FormStatus.FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT:
                formFragment = ReviewVerifikasiTindakLanjutFragment.newInstance(formId,isReviewWithHistory, isUsingRemoteData);
                break;

        }

        if (formFragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, formFragment)
                    .commit();
        }
    }
}
