package id.wika.cpptkponline.view.history;

import android.support.v7.widget.SearchView;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 24/09/17 - 11:01
 * Email   : ardhimaarik2@gmail.com
 * Project : Sample App
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/
public class HistoryQueryListener implements SearchView.OnQueryTextListener {
    private HistoryAdapter recordAdapter;

    public HistoryQueryListener(HistoryAdapter recordAdapter) {
        this.recordAdapter = recordAdapter;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        recordAdapter.getAdapterFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        recordAdapter.getAdapterFilter().filter(newText);
        return false;
    }
}