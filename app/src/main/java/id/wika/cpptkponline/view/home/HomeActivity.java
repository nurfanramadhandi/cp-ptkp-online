package id.wika.cpptkponline.view.home;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.FormSummary;
import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.filter.ResumeFillterType;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.helper.FormSummarySortComparatorFactory;
import id.wika.cpptkponline.presenter.DefaultViewModelFactory;
import id.wika.cpptkponline.presenter.HomeViewModel;
import id.wika.cpptkponline.view.HelpActivity;
import id.wika.cpptkponline.view.dashboard.DashboardActivity;
import id.wika.cpptkponline.view.form.FormActivity;
import id.wika.cpptkponline.view.history.HistoryActivity;
import id.wika.cpptkponline.view.home.list.FormListItem;
import id.wika.cpptkponline.view.home.list.FormListSummaryRecyclerViewAdapter;
import id.wika.cpptkponline.view.home.list.RecordQueryExpandListener;
import id.wika.cpptkponline.view.home.list.RecordQueryListener;
import id.wika.cpptkponline.view.laporan.LaporanActivity;
import id.wika.cpptkponline.view.login.LoginActivity;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout navigationDrawer;

    @BindView(R.id.toolbar)
    Toolbar actionToolbar;

    @BindView(R.id.form_list)
    RecyclerView formList;

    private int checkedMenuId;

    private User user;

    private final CompositeDisposable mDisposable = new CompositeDisposable();
    private HomeViewModel homeViewModel;
    private FormListSummaryRecyclerViewAdapter adapter;
    private boolean isRefreshing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        user = User.load(this);

        initActionBar();
        initNavigationDrawer();
        initFormList();

        SharedPreferences sharedPreferences = getSharedPreferences(ServiceGenerator.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        homeViewModel = ViewModelProviders
                .of(this, new DefaultViewModelFactory(this, sharedPreferences))
                .get(HomeViewModel.class);
    }

    private void initActionBar() {
        setSupportActionBar(actionToolbar);
        getSupportActionBar().setTitle("Home");
    }

    private void initNavigationDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, navigationDrawer, actionToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        navigationDrawer.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);
        // TODO REPLACE CODE
        navigationView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                navigationView.removeOnLayoutChangeListener(this);

                TextView usernameLabel = navigationView.findViewById(R.id.user_fullname);
                usernameLabel.setText(user.getFullname());
                TextView userDivisionLabel = navigationView.findViewById(R.id.user_role);
                userDivisionLabel.setText(user.getJabatan() + " - " + user.getRoleId());
            }
        });
        checkedMenuId = R.id.nav_home;
    }

    private void initFormList() {
        adapter = new FormListSummaryRecyclerViewAdapter(new ArrayList<FormListItem>());
        adapter.setListener(createListener());
        formList.setAdapter(adapter);
        formList.setLayoutManager(new LinearLayoutManager(this));
    }

    private FormListSummaryRecyclerViewAdapter.OnItemClickListener createListener() {
        return new FormListSummaryRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                FormListItem item = ((FormListSummaryRecyclerViewAdapter) formList.getAdapter()).getItem(position);
                FormSummary summary = (FormSummary) item.getData();
                if (summary.isNeedAttention()) {
                    handleOpenForm(summary);
                } else {
                    handleOpenDataReview(summary);
                }
            }
        };
    }

    private void handleOpenForm(FormSummary summary) {

        if (summary.getStatus() == FormStatus.FORM_AWAL
                || summary.getStatus() == FormStatus.FORM_PENGECEKAN
                || summary.getStatus() == FormStatus.FORM_RENCANA_TINDAK_LANJUT
                || summary.getStatus() == FormStatus.FORM_REALISASI_TINDAK_LANJUT
                || summary.getStatus() == FormStatus.FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT
                || summary.getStatus() == FormStatus.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN
                || summary.getStatus() == FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI
                || summary.getStatus() == FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN) {

            Intent intent = new Intent(HomeActivity.this, FormActivity.class);
            intent.putExtra(FormHelper.EXTRA_KEY_FORM_STATUS, summary.getStatus());
            intent.putExtra(FormHelper.EXTRA_KEY_FORM_ID, summary.getFormId());
            intent.putExtra(FormHelper.EXTRA_KEY_REMOTE_FORM_ID, summary.getRemoteFormId());
            startActivity(intent);
        }
    }

    private void handleOpenDataReview(FormSummary summary) {
        FormHelper.launchReviewFormActivity(
                this,
                summary.getFormId(),
                summary.getStatus(),
                true, false);
    }

    @Override
    public void onBackPressed() {
        if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
            navigationDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_action_menu, menu);
        setSearchView(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_filter) {
            showMenuFilter();
            return true;
        } else if (id == R.id.action_sort) {
            showSortMenu();
            return true;
        } else if (id == R.id.action_logout) {
            logout();
        } else if (id == R.id.action_refresh) {
            loadData();
            isRefreshing = true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showSortMenu() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String[] sortTypes = {
                "Tanggal Temuan (ASC)",
                "Tanggal Temuan (DESC)",
                "Nomor Form (ASC)",
                "Nomor Form (DESC)"
        };

        builder.setTitle("Urutkan berdasarkan")
                .setItems(sortTypes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        homeViewModel.setSortType(getSelectedSortType(which));
                        requestRefreshLocalData();
                    }

                    private FormSummarySortComparatorFactory.SORT_TYPE getSelectedSortType(int which) {
                        switch (which) {
                            case 0:
                                return FormSummarySortComparatorFactory.SORT_TYPE.DATE_ASC;
                            case 1:
                                return FormSummarySortComparatorFactory.SORT_TYPE.DATE_DESC;
                            case 2:
                                return FormSummarySortComparatorFactory.SORT_TYPE.FORM_ID_ASC;
                            case 3:
                                return FormSummarySortComparatorFactory.SORT_TYPE.FORM_ID_DESC;
                            default:
                                return FormSummarySortComparatorFactory.SORT_TYPE.DATE_DESC;
                        }
                    }
                });

        builder.show();
    }

    private void setSearchView(Menu menu) {
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new RecordQueryListener(adapter));

        MenuItemCompat.setOnActionExpandListener(item, new RecordQueryExpandListener(adapter));

//        SearchManager searchManager = (SearchManager)
//                getSystemService(Context.SEARCH_SERVICE);
//        MenuItem item = menu.findItem(R.id.action_search);
//        searchView = (SearchView) item.getActionView();
//
//        searchView.setSearchableInfo(searchManager.
//                getSearchableInfo(getComponentName()));
//        searchView.setSubmitButtonEnabled(true);
//        searchView.setOnQueryTextListener(this);

//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
//        // Assumes current activity is the searchable activity
////        searchView.setOnQueryTextListener(new RecordQueryListener(adapter));
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
    }

    private void showMenuFilter() {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        String[] sports = {
                "ALL"
                , "FORM_AWAL".replace("_", " ")
                , "FORM_PENGECEKAN".replace("_", " ")
                , "FORM_RENCANA_TINDAK_LANJUT_DISETUJUI".replace("_", " ")
                , "FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN".replace("_", " ")
                , "FORM_RENCANA_TINDAK_LANJUT".replace("_", " ")
                , "FORM_REALISASI_TINDAK_LANJUT".replace("_", " ")
                , "FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT".replace("_", " ")
                , "LAPORAN_TINDAK_LANJUT_STATUS_OPEN".replace("_", " ")
                , "LAPORAN_TINDAK_LANJUT_STATUS_CLOSED".replace("_", " ")
        };
        b.setTitle("Filter Status")
                .setItems(sports, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        switch (which) {
                            case 0:
                                homeViewModel.setResumeFillterType(ResumeFillterType.ALL);
                                break;
                            case 1:
                                homeViewModel.setResumeFillterType(ResumeFillterType.FORM_AWAL);
                                break;
                            case 2:
                                homeViewModel.setResumeFillterType(ResumeFillterType.FORM_PENGECEKAN);
                                break;
                            case 3:
                                homeViewModel.setResumeFillterType(ResumeFillterType.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI);
                                break;
                            case 4:
                                homeViewModel.setResumeFillterType(ResumeFillterType.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN);
                                break;
                            case 5:
                                homeViewModel.setResumeFillterType(ResumeFillterType.FORM_RENCANA_TINDAK_LANJUT);
                                break;
                            case 6:
                                homeViewModel.setResumeFillterType(ResumeFillterType.FORM_REALISASI_TINDAK_LANJUT);
                                break;
                            case 7:
                                homeViewModel.setResumeFillterType(ResumeFillterType.FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT);
                                break;
                            case 8:
                                homeViewModel.setResumeFillterType(ResumeFillterType.LAPORAN_TINDAK_LANJUT_STATUS_OPEN);
                                break;
                            case 9:
                                homeViewModel.setResumeFillterType(ResumeFillterType.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED);
                                break;
                        }

                        requestRefreshLocalData();
                    }
                });

        b.show();

    }

    private void requestRefreshLocalData() {
        mDisposable.add(
                homeViewModel.getLocalFormListItems(user)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<List<FormListItem>>() {
                            @Override
                            public void accept(List<FormListItem> formListItems) throws Exception {
                                adapter.setData(formListItems);
                                adapter.notifyDataSetChanged();
                            }
                        })
        );
    }

    private void logout() {
        homeViewModel.clearData(this, user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onComplete() {
                        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int newId = item.getItemId();

        navigationDrawer.closeDrawer(GravityCompat.START);

        if (checkedMenuId != newId) {
            if (newId == R.id.nav_home) {
                Toast.makeText(this, "Nav : Home", Toast.LENGTH_SHORT).show();
            } else if (newId == R.id.nav_dashboard) {
                DashboardActivity.start(this);
                return false;
            } else if (newId == R.id.nav_history) {
                HistoryActivity.start(this, true);
                return false;
            } else if (newId == R.id.nav_reports) {
                LaporanActivity.start(this);
                return false;
            } else if (newId == R.id.nav_tracking) {
                HistoryActivity.start(this, false);
                return false;
            } else if (newId == R.id.nav_help) {
                startActivity(new Intent(this, HelpActivity.class));
                return false;
            }

            checkedMenuId = newId;
            return true;
        }

        return false;
    }

    @OnClick(R.id.fab)
    void onClickActionButton() {
        Intent intent = new Intent(this, FormActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();

        loadData();
    }

    private void loadData() {
        mDisposable.add(
                homeViewModel.getLocalFormListItems(user)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<List<FormListItem>>() {
                            @Override
                            public void accept(List<FormListItem> formListItems) throws Exception {
                                adapter.setData(formListItems);
                                adapter.notifyDataSetChanged();
                                if (isRefreshing) {
                                    Toast.makeText(HomeActivity.this, "Data Loaded", Toast.LENGTH_SHORT).show();
                                    isRefreshing = false;
                                }
                            }
                        })
        );

        try {
            mDisposable.add(
                    homeViewModel.syncDataFromRemote(user)
                            .subscribeOn(Schedulers.io())
                            .subscribe());

        } catch (Exception e) {
            Toast.makeText(HomeActivity.this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        mDisposable.clear();
    }
}
