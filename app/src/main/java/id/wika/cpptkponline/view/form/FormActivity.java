package id.wika.cpptkponline.view.form;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.data.endpoint.request.PostFormRequestGenerator;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.presenter.DefaultViewModelFactory;
import id.wika.cpptkponline.presenter.FormViewModel;
import id.wika.cpptkponline.view.dialog.DiscardFormDialogFragment;
import id.wika.cpptkponline.view.dialog.SendConfirmationDialogFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class FormActivity extends AppCompatActivity {

    private String activeFragmentTag;

    private FormViewModel formViewModel;

    @BindView(R.id.toolbar)
    Toolbar actionToolbar;

    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        ButterKnife.bind(this);

        SharedPreferences sharedPreferences = getSharedPreferences(ServiceGenerator.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        formViewModel = ViewModelProviders
                .of(this, new DefaultViewModelFactory(this, sharedPreferences))
                .get(FormViewModel.class);

        initActionBar();
        initFragment(savedInstanceState);

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle("Transfer Data");
        progressDialog.setMessage("Sedang mengirim data, silakan tunggu beberapa saat..");
        progressDialog.setCanceledOnTouchOutside(false);
    }

    private void initActionBar() {
        setSupportActionBar(actionToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getActionBarTitle());
        actionToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private String getActionBarTitle() {
        int incomingFormStatus = getIntent().getIntExtra(FormHelper.EXTRA_KEY_FORM_STATUS, 0);

        if (incomingFormStatus == FormStatus.FORM_RENCANA_TINDAK_LANJUT) {
            return "Rencana Tindak Lanjut";
        } else if (incomingFormStatus == FormStatus.FORM_AWAL) {
            return "Cek Form Pelaporan";
        } else if (incomingFormStatus == FormStatus.FORM_REALISASI_TINDAK_LANJUT) {
            return "Form Verifikasi Tindak Lanjut";
        } else if (incomingFormStatus == FormStatus.FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT) {
            return "Form Verifikasi Tindak Lanjut";
        } else if (incomingFormStatus == FormStatus.FORM_PENGECEKAN) {
            return "Form Rencana Tindak Lanjut";
        } else if (incomingFormStatus == FormStatus.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN) {
            return "Form Arsip";
        } else if (incomingFormStatus == FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI) {
            return "Form Realisasi Tindak Lanjut";
        } else if (incomingFormStatus == FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN) {
            return "Form Revisi Tindak Lanjut (Open)";
        } else {
            return "Form Pelaporan";
        }
    }

    private void initFragment(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            return;
        }

        final int status = getIntent().getIntExtra(FormHelper.EXTRA_KEY_FORM_STATUS, 0);
        final int localFormId = getIntent().getIntExtra(FormHelper.EXTRA_KEY_FORM_ID, -1);
        final int remoteFormId = getIntent().getIntExtra(FormHelper.EXTRA_KEY_REMOTE_FORM_ID, -1);

        if (status == 0) {
            attachFragment(status, localFormId);
        } else {
            formViewModel.checkIfFormDataIsAvailable(localFormId, remoteFormId, status)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Boolean aBoolean) {
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(FormActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            attachFragment(status, localFormId);
                        }
                    });
        }
    }

    private void attachFragment(int status, int formId) {
        Fragment formFragment;

        if (status == FormStatus.FORM_AWAL) {
            formFragment = FormPengecekanFragment.newInstance(formId);
        } else if (status == FormStatus.FORM_PENGECEKAN) {
            formFragment = FormRencanaTindakLanjutFragment.newInstance(formId);
        } else if (status == FormStatus.FORM_RENCANA_TINDAK_LANJUT) {
            formFragment = FormApprovalFragment.newInstance(formId);
        } else if (status == FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN) {
            formFragment = FormRencanaTindakLanjutOpenFragment.newInstance(formId);
        } else if (status == FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI) {
            formFragment = FormRealisasiTindakLanjutFragment.newInstance(formId);
        } else if (status == FormStatus.FORM_REALISASI_TINDAK_LANJUT) {
            formFragment = VerifikasiHasilFragment.newInstance(formId);
        } else {
            formFragment = FormPelaporanFragment.newInstance();
        }

        setActiveFragment(formFragment, FormPelaporanFragment.TAG);
    }

    private void setActiveFragment(Fragment formFragment, String fragmentTag) {
        activeFragmentTag = fragmentTag;

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, formFragment, fragmentTag)
                .commit();
    }

    @Override
    public void onBackPressed() {
        new DiscardFormDialogFragment().
                show(getFragmentManager(), DiscardFormDialogFragment.TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.form_action_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_check) {
            final Fragment fragment = getSupportFragmentManager().findFragmentByTag(activeFragmentTag);
            if (fragment instanceof CPPTKPFormView && ((CPPTKPFormView) fragment).isValid()) {
                SendConfirmationDialogFragment sendConfirmationDialogFragment = new SendConfirmationDialogFragment();
                sendConfirmationDialogFragment.setAction(new Runnable() {
                    @Override
                    public void run() {
                        CPPTKPForm cpptkpForm = ((CPPTKPFormView) fragment).getFormData();
                        PostFormRequestGenerator.Strategy strategy = ((CPPTKPFormView) fragment).getPostFormStrategy(cpptkpForm.getFormStatus());

                        sendForm(cpptkpForm, strategy);
                    }
                });

                sendConfirmationDialogFragment
                        .show(getFragmentManager(), SendConfirmationDialogFragment.TAG);
            } else {
                Toast.makeText(this, "Form tidak valid!", Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendForm(CPPTKPForm cpptkpForm, PostFormRequestGenerator.Strategy strategy) {
        formViewModel.sendForm(cpptkpForm, strategy)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        if (!progressDialog.isShowing()) {
                            progressDialog.show();
                        }
                    }

                    @Override
                    public void onNext(Object o) {
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Toast.makeText(FormActivity.this, "Gagal mengirim data. Silakan coba kembali.", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {
                        progressDialog.dismiss();
                        finish();
                    }
                });
    }
}
