package id.wika.cpptkponline.view.history;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import id.wika.cpptkponline.data.endpoint.response.PostTemuanListResponse;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 24/09/17 - 10:53
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/
public class HistoryAdapterFilter extends Filter {
    private HistoryAdapter adapter;
    private List<PostTemuanListResponse.Datum> recordModelList;

    public HistoryAdapterFilter(HistoryAdapter adapter) {
        this.adapter = adapter;
        recordModelList = new ArrayList<>();
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        constraint = constraint.toString();

        final List<PostTemuanListResponse.Datum> filteredModelList = new ArrayList<>();
        for (PostTemuanListResponse.Datum model : recordModelList) {

            final String text = model.getActivity().toLowerCase();
            if (text.contains(constraint)) {
//                        HighlightUtil.highlightPositions(model, constraint.toString());
                filteredModelList.add(model);
            }
        }

        final FilterResults results = new FilterResults();
        results.values = filteredModelList;
        results.count = filteredModelList.size();

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        if (results.count > 0)
            adapter.setFilterData((Collection<PostTemuanListResponse.Datum>) results.values);
    }

    public void setRecordModelList(List<PostTemuanListResponse.Datum> recordModelList) {
        this.recordModelList = recordModelList;
    }
}