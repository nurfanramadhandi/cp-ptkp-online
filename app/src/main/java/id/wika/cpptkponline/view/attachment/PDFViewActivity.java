package id.wika.cpptkponline.view.attachment;

/**
 * Created by apple on 9/26/17.
 */

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.service.DownloadFile;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static id.wika.cpptkponline.helper.APIHelper.API_ADDRESS;

public class PDFViewActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.webview)
    WebView webview;
    private String url_pdf = API_ADDRESS + "laporan.pdf"; //"http://117.54.15.29:3000/laporan.pdf";

    private ProgressDialog mProgressDialog;
    private String proyek;
    private String bulan;
    private String tahun;
    private String status;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pdf);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        progressBar.setVisibility(View.VISIBLE);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                //do your work
            } else {
                requestPermission();
            }
        }

        initActionBar();
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.getSettings().setBuiltInZoomControls(true);
        Intent intent = getIntent();

        proyek = intent.getStringExtra("proyek");
        bulan = intent.getStringExtra("bulan");
        tahun = intent.getStringExtra("tahun");
        status = intent.getStringExtra("status");
        loadWebView(url_pdf,proyek,bulan,tahun,status);

        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            }
        });

    }


    protected boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    protected void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            }
        }
    }
    private void initActionBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("File");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.file_action_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.action_refresh) {
            refresh();
            return true;
        } else return super.onOptionsItemSelected(item);
    }

    private void refresh() {
        loadWebView(url_pdf,proyek,bulan,tahun,status);
    }

    private void downloadFile() {
        DownloadFile.startActionBaz(this, url_pdf,proyek, null);
    }

    public static void start(Context context, String nama, String bulan, String tahun, String status) {
        Intent starter = new Intent(context, PDFViewActivity.class);
        starter.putExtra("proyek", nama);
        starter.putExtra("bulan", bulan);
        starter.putExtra("tahun", tahun);
        starter.putExtra("status", status);
        context.startActivity(starter);
    }

    private void loadWebView(String url, String namaProyek, String bulan, String Tahun, String status){
        progressBar.setVisibility(View.GONE);
        String param="?bulan="+bulan+"&tahun="+Tahun+"&status="+status+"&namaProyek="+namaProyek;
        url+=param;


        try {
            String gabung = URLEncoder.encode(url,"UTF-8");
            webview.loadUrl("https://docs.google.com/viewer?url=" +gabung);
            System.out.println("ckckckckc"+ gabung);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}