package id.wika.cpptkponline.view;

import android.support.annotation.UiThread;

import java.util.List;

import id.wika.cpptkponline.view.home.list.FormListItem;


/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:51:54 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

@UiThread
public interface HistoryView extends BaseView{
    void loadData(List<FormListItem> formListItems);
}