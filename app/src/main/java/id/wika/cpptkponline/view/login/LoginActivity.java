package id.wika.cpptkponline.view.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.endpoint.LoginEndpointInterface;
import id.wika.cpptkponline.data.endpoint.response.LoginResponse;
import id.wika.cpptkponline.data.model.RoleUnitKerja;
import id.wika.cpptkponline.data.model.UserProfile;
import id.wika.cpptkponline.helper.UserHelper;
import id.wika.cpptkponline.view.home.HomeActivity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_username)
    EditText usernameInput;

    @BindView(R.id.login_password)
    EditText passwordInput;

    @BindView(R.id.login_button)
    View loginButton;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnTextChanged(R.id.login_username)
    void onUsernameInputChanged() {
        checkValidInput();
    }

    @OnTextChanged(R.id.login_password)
    void onPasswordInputChanged() {
        checkValidInput();
    }

    private void checkValidInput() {
        boolean isValid = usernameInput.length() > 0 && passwordInput.length() > 0;

        loginButton.setEnabled(isValid);
    }

    @OnClick(R.id.login_button)
    void onLoginButtonClick(View v) {
        LoginEndpointInterface loginAPI = ServiceGenerator.createService(LoginEndpointInterface.class);

        loginAPI.login(usernameInput.getText().toString(), passwordInput.getText().toString())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<LoginResponse>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                        loginButton.setEnabled(false);
                    }

                    @Override
                    public void onNext(LoginResponse loginResponse) {
                        handleResponse(loginResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(LoginActivity.this, e.getMessage().toString()+". Wrong Username or Password!", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                        loginButton.setEnabled(true);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private void handleResponse(LoginResponse responseJson) {
        //saveTokenToSharedPreferences(loginResponse.getToken(), loginResponse.getUser());
        String status = responseJson.getStatus();

        if(status.equals("success")) {
            UserProfile userProfile = responseJson.getUser();
            RoleUnitKerja divisi = responseJson.getDivisi();
            user = new User();
            user.setFullname(userProfile.getName());
            user.setJabatan(userProfile.getJabatan());
            user.setToken(responseJson.getToken());
            user.setUsername(userProfile.getUsername());
            user.setRoleId(userProfile.getRoleId());
            user.setDivisi(divisi.getNama());
            user.setKodeDivisi(divisi.getKode());
            String proyek = "";

            if (responseJson.getProyek().size() != 0){
                proyek = responseJson.getProyek().get(0).getNama();
            }
            user.setProyek(proyek);

            user.save(LoginActivity.this);
            saveTokenToSharedPreferences(responseJson.getToken(),userProfile);
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            finish();
        }
        else{
            String errorMessage = responseJson.getStatus();
            Toast.makeText(LoginActivity.this, errorMessage, Toast.LENGTH_LONG).show();
        }

    }

    private void saveTokenToSharedPreferences(String token, UserProfile user) {
        SharedPreferences sharedPref = getSharedPreferences(ServiceGenerator.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(ServiceGenerator.TOKEN_KEY, token);
        editor.putString(UserHelper.EXTRA_KEY_FULLNAME, user.getName());
        editor.putString(UserHelper.EXTRA_KEY_JABATAN, user.getJabatan());
        editor.putString(UserHelper.EXTRA_KEY_ROLE_ID, user.getRoleId());
        editor.apply();

        FirebaseMessaging.getInstance().subscribeToTopic(user.getRoleId());
    }
}
