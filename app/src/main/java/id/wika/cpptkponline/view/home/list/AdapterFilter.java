package id.wika.cpptkponline.view.home.list;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import id.wika.cpptkponline.data.FormSummary;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 19/September/2017 - 15:58
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public class AdapterFilter extends Filter {
    private FormListSummaryRecyclerViewAdapter adapter;
    private List<FormListItem> recordModelList;

    public AdapterFilter(FormListSummaryRecyclerViewAdapter adapter) {
        this.adapter = adapter;
        recordModelList = new ArrayList<>();
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        constraint = constraint.toString();

        final List<FormListItem> filteredModelList = new ArrayList<>();
        for (FormListItem model : recordModelList) {
            switch (model.getItemType()) {
                case FormListItem.HEADER_ITEM_TYPE:
                    filteredModelList.add(model);
                    break;
                case FormListItem.RECORD_ITEM_TYPE:
                    FormSummary summary = ((FormSummary) model.getData());
                    final String text = summary.getSubTitle().toLowerCase();
                    if (text.contains(constraint)) {
//                        HighlightUtil.highlightPositions(model, constraint.toString());
                        filteredModelList.add(model);
                    }
                    break;
                default:
                    break;
            }
        }

        final FilterResults results = new FilterResults();
        results.values = filteredModelList;
        results.count = filteredModelList.size();

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        if (results.count > 0 )
            adapter.setFilterData((Collection<FormListItem>) results.values);
    }

    public void setRecordModelList(List<FormListItem> recordModelList) {
        this.recordModelList = recordModelList;
    }
}