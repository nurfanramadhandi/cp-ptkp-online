package id.wika.cpptkponline.view.form;

import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.endpoint.request.PostFormRequestGenerator;

/**
 * Created by mamang_kompii on 9/3/17.
 */

public interface CPPTKPFormView {

    boolean isValid();
    CPPTKPForm getFormData();
    PostFormRequestGenerator.Strategy getPostFormStrategy(int formStatus);
}
