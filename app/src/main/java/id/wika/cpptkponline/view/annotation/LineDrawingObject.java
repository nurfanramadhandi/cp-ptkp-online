package id.wika.cpptkponline.view.annotation;

import android.graphics.Paint;
import android.graphics.PointF;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mamang_kompii on 29/09/17.
 */

public class LineDrawingObject {

    private List<PointF> sPoints = new ArrayList<>();
    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public LineDrawingObject(int strokeWidth, int strokeColor) {
        initPaint(strokeWidth, strokeColor);
    }

    private void initPaint(int strokeWidth, int strokeColor) {
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(strokeWidth);
        paint.setColor(strokeColor);
    }

    public boolean isDrawable() {
        return sPoints.size() >= 2;
    }

    public void addDrawingPoint(PointF drawingPoint) {
        sPoints.add(drawingPoint);
    }

    public PointF getDrawingPoint(int index) {
        return sPoints.get(index);
    }

    public PointF getStartingDrawingPoint() {
        return getDrawingPoint(0);
    }

    public int getDrawingPointSize() {
        return sPoints.size();
    }

    public Paint getPaint() {
        return paint;
    }
}
