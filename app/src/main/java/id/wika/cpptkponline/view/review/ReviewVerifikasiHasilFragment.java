package id.wika.cpptkponline.view.review;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataVerifikasiHasilTindakLanjut;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.presenter.ReviewViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ReviewVerifikasiHasilFragment extends Fragment {

    private int formId;
    private boolean isReviewWithHistory;
    private boolean isUsingRemoteData;

    @BindView(R.id.user_fullname)
    TextView userVerifikator;

    @BindView(R.id.verification_date)
    TextView verification_date;

    @BindView(R.id.field_review_progres)
    TextView fieldProgres;

    @BindView(R.id.status_open)
    RadioButton statusOpen;

    @BindView(R.id.status_close)
    RadioButton statusClose;

    @BindView(R.id.review_root_cause)
    TextView rootCause;

    @BindView(R.id.review_root_cause_category)
    TextView rootCauseCategory;

    @BindView(R.id.review_root_cause_label)
    TextView rootCauseLabel;

    @BindView(R.id.review_root_cause_category_label)
    TextView rootCauseCategoryLabel;


    @BindView(R.id.history_laporan_awal_btn)
    View historyLaporanAwalButton;

    @BindView(R.id.history_rencana_tindak_lanjut_btn)
    View historyRencanaTindakLanjutButton;

    @BindView(R.id.history_approval)
    View historyLaporanApprovalButton;

    @BindView(R.id.history_realisasi_tindak_lanjut_btn)
    View historyRealisasiTindakLanjutButton;

    public static Fragment newInstance(int formId, boolean isShowingHistory, boolean isUsingRemoteData) {
        Bundle args = new Bundle();

        args.putInt(FormHelper.EXTRA_KEY_FORM_ID, formId);
        args.putBoolean(FormHelper.EXTRA_KEY_REVIEW_WITH_REMOTE_DATA, isUsingRemoteData);
        args.putBoolean(FormHelper.EXTRA_KEY_IS_REVIEW_WITH_HISTORY, isShowingHistory);

        ReviewVerifikasiHasilFragment fragment = new ReviewVerifikasiHasilFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ReviewVerifikasiHasilFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        formId = getArguments().getInt(FormHelper.EXTRA_KEY_FORM_ID);
        isUsingRemoteData = getArguments().getBoolean(FormHelper.EXTRA_KEY_REVIEW_WITH_REMOTE_DATA);
        isReviewWithHistory = getArguments().getBoolean(FormHelper.EXTRA_KEY_IS_REVIEW_WITH_HISTORY);

        ReviewViewModel reviewViewModel = ViewModelProviders
                .of(getActivity())
                .get(ReviewViewModel.class);
        reviewViewModel.getFormReviewData(formId, FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED, isUsingRemoteData)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CPPTKPForm>() {
                    @Override
                    public void accept(CPPTKPForm cpptkpForm) throws Exception {
                        if (cpptkpForm.getVerifikasiHasilTindakLanjut() != null)
                            setExistingData(cpptkpForm.getVerifikasiHasilTindakLanjut());
                    }
                });
    }

    private void setExistingData(DataVerifikasiHasilTindakLanjut dataRencanaTindakLanjut) {

        int progres = dataRencanaTindakLanjut.getProgres();
        if (progres == 0)
            fieldProgres.setText("0 %");
        else {
            fieldProgres.setText(String.valueOf(progres) + " %");
        }

        if (dataRencanaTindakLanjut.isStatusOpened()) {
            statusOpen.setChecked(true);
            rootCauseLabel.setText("Feedback");
            rootCauseCategoryLabel.setText("Feedback Category");
        } else {
            statusClose.setChecked(true);
        }
        User user = dataRencanaTindakLanjut.getVerifikator();
        if (user != null) {
            userVerifikator.setText(user.getFullname());
        }
        DateTimeHelper dateTimeHelper = new DateTimeHelper();
        verification_date.setText(dateTimeHelper.printInIndonesian(
                dataRencanaTindakLanjut.getTanggalVerifikasi()));

        if (dataRencanaTindakLanjut.getRootCauseValue() != null)
            rootCause.setText(dataRencanaTindakLanjut.getRootCauseValue());
        else
            rootCause.setText("No Data");

        if (dataRencanaTindakLanjut.getRootCauseCategory() != null)
            rootCauseCategory.setText(dataRencanaTindakLanjut.getRootCauseCategory());
        else
            rootCauseCategory.setText("No Data");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_review_verifikasi_hasil, container, false);
        ButterKnife.bind(this, view);

        historyLaporanAwalButton.setVisibility(isReviewWithHistory ? View.VISIBLE : View.GONE);
        historyRencanaTindakLanjutButton.setVisibility(isReviewWithHistory ? View.VISIBLE : View.GONE);
        historyLaporanApprovalButton.setVisibility(isReviewWithHistory ? View.VISIBLE : View.GONE);
        historyRealisasiTindakLanjutButton.setVisibility(isReviewWithHistory ? View.VISIBLE : View.GONE);

        return view;
    }

    @OnClick(R.id.history_laporan_awal_btn)
    void onClickViewHistoryLaporanAwal() {
        FormHelper.launchReviewFormActivity(
                getActivity(), formId, FormStatus.FORM_PENGECEKAN, false, isUsingRemoteData);
    }

    @OnClick(R.id.history_rencana_tindak_lanjut_btn)
    void onClickViewHistoryRencanaTindakLanjut() {
        FormHelper.launchReviewFormActivity(
                getActivity(), formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT, false, isUsingRemoteData);
    }

    @OnClick(R.id.history_approval)
    void onClickViewHistoryApproval() {
        FormHelper.launchReviewFormActivity(
                getActivity(), formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI, false, isUsingRemoteData);
    }

    @OnClick(R.id.history_realisasi_tindak_lanjut_btn)
    void onClickViewHistoryRealisasiTindakLanjut() {
        FormHelper.launchReviewFormActivity(
                getActivity(), formId, FormStatus.FORM_REALISASI_TINDAK_LANJUT, false, isUsingRemoteData);
    }

}
