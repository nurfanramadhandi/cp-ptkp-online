package id.wika.cpptkponline.view.dialog;

import android.app.Activity;

/**
 * Created by mamang_kompii on 8/31/17.
 */

public class PhotoDeleteConfirmationDialogFragment extends GeneralConfirmationDialogFragment {

    public static final String TAG = "PhotoDeleteConfirmationDialogFragment";
    private InteractionListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof InteractionListener) {
            listener = (InteractionListener) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        listener = null;
    }

    @Override
    protected boolean isTitleLabelVisible() {
        return true;
    }

    @Override
    protected String getTitleText() {
        return "Hapus Foto";
    }

    @Override
    protected String getDescriptionText() {
        return "Apakah Anda ingin menghapus foto ini?";
    }

    @Override
    protected String getNegativeButtonText() {
        return "Batal";
    }

    @Override
    protected boolean isNeutralButtonVisible() {
        return false;
    }

    @Override
    protected String getNeutralButtonText() {
        return null;
    }

    @Override
    protected String getPositiveButtonText() {
        return "Hapus";
    }

    @Override
    void onNegativeButtonClicked() {
        dismiss();
    }

    @Override
    void onNeutralButtonClicked() {}

    @Override
    void onPositiveButtonClicked() {
        dismiss();

        if (listener != null) {
            listener.onConfirmDeletePhoto();
        }
    }

    public interface InteractionListener {
        void onConfirmDeletePhoto();
    }
}
