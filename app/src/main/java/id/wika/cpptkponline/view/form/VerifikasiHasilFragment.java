package id.wika.cpptkponline.view.form;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataVerifikasiHasilTindakLanjut;
import id.wika.cpptkponline.data.FormDataCatalogue;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.endpoint.RemoteFormStatus;
import id.wika.cpptkponline.data.endpoint.request.PostFormRequestGenerator;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.helper.StringHelper;
import id.wika.cpptkponline.helper.UserHelper;
import id.wika.cpptkponline.presenter.FormViewModel;
import id.wika.cpptkponline.view.widget.ClickToSelectEditText;
import id.wika.cpptkponline.view.widget.StringClickToSelectEditTextListAdapter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class VerifikasiHasilFragment extends Fragment implements CPPTKPFormView {

    private DateTime formDate = DateTime.now();

    private CPPTKPForm formData;

    private int formId;

    @BindView(R.id.field_progres)
    TextInputLayout fieldProgres;

    @BindView(R.id.field_for_status_open)
    View statusOpenFields;

    @BindView(R.id.field_for_status_closed)
    View statusClosedFields;

    @BindView(R.id.field_root_cause_1)
    TextInputLayout rootCause1;

    @BindView(R.id.field_root_cause_2)
    TextInputLayout rootCause2;

    @BindView(R.id.field_root_cause_3)
    TextInputLayout rootCause3;

    @BindView(R.id.field_root_cause_4)
    TextInputLayout rootCause4;

    @BindView(R.id.field_root_cause_5)
    TextInputLayout rootCause5;

    @BindView(R.id.field_root_cause_6)
    TextInputLayout rootCause6;

    @BindView(R.id.field_root_cause_7)
    TextInputLayout rootCause7;

    @BindView(R.id.field_feedback)
    TextInputLayout feed_back;

    @BindView(R.id.status_open)
    RadioButton radioOpen;

    @BindView(R.id.status_close)
    RadioButton radioClose;

    private User user;
    boolean isClosed;
    private String selectedRootCause;
    private String selectedRootCauseCategory;

    public static VerifikasiHasilFragment newInstance(int formId) {
        Bundle args = new Bundle();
        args.putInt(FormHelper.EXTRA_KEY_FORM_ID, formId);

        VerifikasiHasilFragment fragment = new VerifikasiHasilFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public VerifikasiHasilFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = User.load(getActivity());
        formId = getArguments().getInt(FormHelper.EXTRA_KEY_FORM_ID);
        FormViewModel formViewModel = ViewModelProviders
                .of(getActivity())
                .get(FormViewModel.class);
        formViewModel.getFormData(formId)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CPPTKPForm>() {
                    @Override
                    public void accept(CPPTKPForm cpptkpForm) throws Exception {
                        formData = cpptkpForm;
                        setContentView(formData);
                    }
                });
    }

    private void setContentView(CPPTKPForm formData) {
        if (formData != null && formData.getRemoteFormStatus().equals("status")) {
            setupField();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_verifikasi_hasil, container, false);
        ButterKnife.bind(this, view);

        setupStaticInfo(view);
        setupField();

        fieldProgres.getEditText().setFilters(new InputFilter[]{new MinMaxFilter(0, 100)});
        fieldProgres.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int progress = getProgressNum(s.toString());
                if (progress >= 100) {
                    radioClose.setChecked(true);
                } else {
                    radioOpen.setChecked(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        return view;
    }

    private void setupStaticInfo(View view) {
        isClosed = false;
        ((TextView) view.findViewById(R.id.user_fullname)).setText(user.getFullname());
        DateTimeHelper dateTimeHelper = new DateTimeHelper();
        ((TextView) view.findViewById(R.id.verification_date)).setText(dateTimeHelper.printInIndonesian(formDate));
    }

    private void setupField() {
        setupRootCauseField(rootCause1, FormDataCatalogue.rootCauseSistem);
        setupRootCauseField(rootCause2, FormDataCatalogue.rootCauseKomitmen);
        setupRootCauseField(rootCause3, FormDataCatalogue.rootCauseKompetensi);
        setupRootCauseField(rootCause4, FormDataCatalogue.rootCausePemahaman);
        setupRootCauseField(rootCause5, FormDataCatalogue.rootCauseLeadership);
        setupRootCauseField(rootCause6, FormDataCatalogue.rootCauseSumberdaya);
        setupRootCauseField(rootCause7, FormDataCatalogue.rootCauseEkstern);
    }

    private void setupRootCauseField(final TextInputLayout rootCauseView, List<String> list) {
        final ClickToSelectEditText editText = (ClickToSelectEditText) rootCauseView.getEditText();
        editText.setAdapter(new StringClickToSelectEditTextListAdapter(getActivity(), list));
        editText.setOnItemSelectedListener(new ClickToSelectEditText.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelectedListener(String item, int selectedIndex) {
                selectedRootCause = item;
                if (editText.mHint != null) {
                    CharSequence chars = editText.mHint;
                    selectedRootCauseCategory = chars.toString();
                }
                disableOtherField(rootCauseView);
            }
        });
    }

    private void disableOtherField(TextInputLayout rootCause) {
        List<TextInputLayout> layouts = Arrays.asList(
                rootCause1,
                rootCause2,
                rootCause3,
                rootCause4,
                rootCause5,
                rootCause6,
                rootCause7
        );

        for (TextInputLayout layout : layouts) {
            if (layout == rootCause) {
                continue;
            }

            layout.setEnabled(false);
        }
    }

    @OnCheckedChanged(R.id.status_open)
    void onStatusOpenChecked(boolean checked) {
        if (checked) {
            renderStatusOpenField();
            isClosed = false;
        }
    }

    private void renderStatusOpenField() {
        statusOpenFields.setVisibility(View.VISIBLE);
        statusClosedFields.setVisibility(View.GONE);
    }

    @OnCheckedChanged(R.id.status_close)
    void onStatusCloseChecked(boolean checked) {
        if (checked) {
            renderStatusCloseField();
            isClosed = true;
        }
    }

    private void renderStatusCloseField() {
        statusClosedFields.setVisibility(View.VISIBLE);
        statusOpenFields.setVisibility(View.GONE);
    }

    @OnClick(R.id.history_laporan_awal_btn)
    void onClickViewHistoryLaporanAwal() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_PENGECEKAN);
    }

    @OnClick(R.id.history_rencana_tindak_lanjut_btn)
    void onClickViewHistoryRencanaTindakLanjut() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT);
    }

    @OnClick(R.id.history_approval)
    void onClickViewHistoryApproval() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI);
    }

    @OnClick(R.id.history_realisasi_tindak_lanjut_btn)
    void onClickViewHistoryRealisasiTindakLanjut() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_REALISASI_TINDAK_LANJUT);
    }

    @Override
    public boolean isValid() {
        if (isClosed) {
            return isValidRootCauseField();
        } else {
            return FormHelper.isFormFieldContentValid(feed_back);
        }
    }

    private boolean isValidRootCauseField() {
        return FormHelper.isFormFieldContentValid(rootCause1) || FormHelper.isFormFieldContentValid(rootCause2)
                || FormHelper.isFormFieldContentValid(rootCause3) || FormHelper.isFormFieldContentValid(rootCause4)
                || FormHelper.isFormFieldContentValid(rootCause5) | FormHelper.isFormFieldContentValid(rootCause6)
                || FormHelper.isFormFieldContentValid(rootCause7);
    }

    @Override
    public CPPTKPForm getFormData() {
        formData.setFormStatus(isClosed ?
                FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED :
                FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN);
        formData.setRemoteFormStatus(isClosed ?
                RemoteFormStatus.STATUS_CLOSED :
                RemoteFormStatus.STATUS_OPEN);
        formData.setNeedResponse(false);

        DataVerifikasiHasilTindakLanjut verifikasiHasilTindakLanjut = new DataVerifikasiHasilTindakLanjut();
        verifikasiHasilTindakLanjut.setVerifikator(user);
        verifikasiHasilTindakLanjut.setTanggalVerifikasi(formDate);

        String progress = FormHelper.getTextFromField(fieldProgres);
        verifikasiHasilTindakLanjut.setProgres(Math.min(getProgressNum(progress), 100));

        if (isClosed) {
            verifikasiHasilTindakLanjut.setRootCauseValue(selectedRootCause);
            verifikasiHasilTindakLanjut.setRootCauseCategory(selectedRootCauseCategory);
        } else {
            verifikasiHasilTindakLanjut.setRootCauseValue(FormHelper.getTextFromField(feed_back));
            verifikasiHasilTindakLanjut.setRootCauseCategory("feedback");
        }
        verifikasiHasilTindakLanjut.setStatusOpened(!isClosed);
        verifikasiHasilTindakLanjut.setRelatedFormId(formData.getFormId());

        formData.setVerifikasiHasilTindakLanjut(verifikasiHasilTindakLanjut);

        return formData;
    }

    @Override
    public PostFormRequestGenerator.Strategy getPostFormStrategy(int formStatus) {
        return PostFormRequestGenerator.Strategy.POST_STATUS;
    }

    private Integer getProgressNum(String progress) {
        if (StringHelper.isNotBlank(progress)) {
            return Integer.parseInt(progress);
        } else {
            return 0;
        }
    }
}
