package id.wika.cpptkponline.view.home.list;

/**
 * Created by mamang_kompii on 8/25/17.
 */

public class FormListItem {
    public static final int HEADER_ITEM_TYPE = 0;
    public static final int RECORD_ITEM_TYPE = 1;

    private int itemType;
    private Object data;

    public FormListItem(int itemType, Object data) {
        this.itemType = itemType;
        this.data = data;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
