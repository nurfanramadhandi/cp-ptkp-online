package id.wika.cpptkponline.view;

import android.support.annotation.UiThread;

import java.util.List;

import id.wika.cpptkponline.data.endpoint.response.GetListLaporan;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 28, 2017 8:33:21 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

@UiThread
public interface LaporanView extends BaseView{
    void renderData(List<GetListLaporan.Datum> formListItems);
}