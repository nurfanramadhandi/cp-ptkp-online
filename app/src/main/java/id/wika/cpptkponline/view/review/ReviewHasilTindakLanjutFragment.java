package id.wika.cpptkponline.view.review;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataRealisasiTindakLanjut;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.presenter.ReviewViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ReviewHasilTindakLanjutFragment extends Fragment {

    private int formId;
    private boolean isReviewWithHistory;
    boolean isUsingRemoteData;

    @BindView(R.id.field_hasil_pencegahan_perbaikan_review)
    TextView fieldHasilPencegahanPerbaikan;


    @BindView(R.id.attachment_area_hasil)
    View attachmentArea;

    @BindView(R.id.attachment_container_hasil)
    LinearLayout attachmentContainer;

    @BindView(R.id.history_laporan_awal_btn)
    View historyLaporanAwalButton;

    @BindView(R.id.history_approval)
    View historyLaporanApprovalButton;

    @BindView(R.id.history_rencana_tindak_lanjut_btn)
    View historyRencanaTindakLanjutButton;

    public static Fragment newInstance(int formId, boolean isShowingHistory, boolean isUsingRemoteData) {
        Bundle args = new Bundle();
        args.putInt(FormHelper.EXTRA_KEY_FORM_ID, formId);
        args.putBoolean(FormHelper.EXTRA_KEY_REVIEW_WITH_REMOTE_DATA, isUsingRemoteData);
        args.putBoolean(FormHelper.EXTRA_KEY_IS_REVIEW_WITH_HISTORY, isShowingHistory);

        ReviewHasilTindakLanjutFragment fragment = new ReviewHasilTindakLanjutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        formId = getArguments().getInt(FormHelper.EXTRA_KEY_FORM_ID);
        isUsingRemoteData = getArguments().getBoolean(FormHelper.EXTRA_KEY_REVIEW_WITH_REMOTE_DATA);
        isReviewWithHistory = getArguments().getBoolean(FormHelper.EXTRA_KEY_IS_REVIEW_WITH_HISTORY);

        ReviewViewModel reviewViewModel = ViewModelProviders
                .of(getActivity())
                .get(ReviewViewModel.class);
        reviewViewModel.getFormReviewData(formId, FormStatus.FORM_REALISASI_TINDAK_LANJUT, isUsingRemoteData)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CPPTKPForm>() {
                    @Override
                    public void accept(CPPTKPForm cpptkpForm) throws Exception {
                        if (cpptkpForm.getRealisasiTindakLanjut() != null)
                            setExistingData(cpptkpForm.getRealisasiTindakLanjut());
                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review_hasil_tindak_lanjut, container, false);
        ButterKnife.bind(this, view);

        historyLaporanAwalButton.setVisibility(isReviewWithHistory ? View.VISIBLE : View.GONE);
        historyRencanaTindakLanjutButton.setVisibility(isReviewWithHistory ? View.VISIBLE : View.GONE);
        historyLaporanApprovalButton.setVisibility(isReviewWithHistory ? View.VISIBLE : View.GONE);

        return view;
    }

    private void setExistingData(DataRealisasiTindakLanjut dataRealisasiTindakLanjut) {
        fieldHasilPencegahanPerbaikan.setText(dataRealisasiTindakLanjut.getHasilPencegahanDanPerbaikan());
        if (dataRealisasiTindakLanjut.getListDokumentasiHasil().size() > 0) {
            attachmentArea.setVisibility(View.VISIBLE);
            int thumbnailSize = getResources().getDimensionPixelSize(R.dimen.attachment_thumbnail_size);

            for (final String path : dataRealisasiTindakLanjut.getListDokumentasiHasil()) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        thumbnailSize,
                        thumbnailSize
                );
                params.setMargins(0, 0, getResources().getDimensionPixelSize(R.dimen.attachment_margin), 0);
                SimpleDraweeView simpleDraweeView = FormHelper.getImageThumbnailViewerDrawee(this, thumbnailSize, path);
                attachmentContainer.addView(simpleDraweeView, attachmentContainer.getChildCount() - 1, params);
            }
        } else {
            attachmentArea.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.history_laporan_awal_btn)
    void onClickViewHistoryLaporanAwal() {
        FormHelper.launchReviewFormActivity(
                getActivity(), formId, FormStatus.FORM_PENGECEKAN, false, isUsingRemoteData);
    }

    @OnClick(R.id.history_rencana_tindak_lanjut_btn)
    void onClickViewHistoryRencanaTindakLanjut() {
        FormHelper.launchReviewFormActivity(
                getActivity(), formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT, false, isUsingRemoteData);
    }

    @OnClick(R.id.history_approval)
    void onClickViewHistoryApproval() {
        FormHelper.launchReviewFormActivity(
                getActivity(), formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI, false, isUsingRemoteData);
    }

}
