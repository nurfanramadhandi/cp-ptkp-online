package id.wika.cpptkponline.view.form;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataFormPelaporan;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import id.wika.cpptkponline.data.FormDataCatalogue;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.endpoint.RemoteFormStatus;
import id.wika.cpptkponline.data.endpoint.request.PostFormRequestGenerator;
import id.wika.cpptkponline.data.endpoint.response.RoleUnitKerjaResponse;
import id.wika.cpptkponline.data.model.LingkupTemuan;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.presenter.DictionaryViewModel;
import id.wika.cpptkponline.presenter.FormViewModel;
import id.wika.cpptkponline.view.widget.ClickToSelectEditText;
import id.wika.cpptkponline.view.widget.ClickToSelectEditTextListAdapter;
import id.wika.cpptkponline.view.widget.StringClickToSelectEditTextListAdapter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class FormPengecekanFragment extends Fragment implements CPPTKPFormView {

    private DateTimeHelper dateTimeHelper = new DateTimeHelper();

    private CPPTKPForm formData;

    @BindView(R.id.user_fullname)
    TextView userFullname;

    @BindView(R.id.user_position)
    TextView userPosition;

    @BindView(R.id.form_date)
    TextView formDate;

    @BindView(R.id.field_project_name)
    TextView namaProyek;

    @BindView(R.id.field_lokasi_penyimpangan)
    TextView lokasiPenyimpangan;

    @BindView(R.id.field_bentuk_penyimpangan)
    TextView bentukPenyimpangan;

    @BindView(R.id.attachment_area)
    View attachmentArea;

    @BindView(R.id.attachment_container)
    LinearLayout attachmentContainer;

    @BindView(R.id.field_kategori)
    TextInputLayout fieldKategori;

    @BindView(R.id.field_lingkup_temuan)
    TextInputLayout fieldLingkupTemuan;

    @BindView(R.id.field_detail_lingkup_temuan)
    TextInputLayout fieldDetailLingkupTemuan;

    @BindView(R.id.field_divisi_penanggungjawab)
    TextInputLayout fieldDivisiPenanggungJawab;

    @BindView(R.id.field_nama_penanggungjawab)
    TextInputLayout fieldNamaPenanggungJawab;

    private DictionaryViewModel dictionaryViewModel;
    private User user;

    public static FormPengecekanFragment newInstance(int formId) {
        Bundle args = new Bundle();
        args.putInt(FormHelper.EXTRA_KEY_FORM_ID, formId);

        FormPengecekanFragment fragment = new FormPengecekanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public FormPengecekanFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int formId = getArguments().getInt(FormHelper.EXTRA_KEY_FORM_ID);
        user = User.load(getActivity());
        dictionaryViewModel = new DictionaryViewModel();

        FormViewModel formViewModel = ViewModelProviders
                .of(getActivity())
                .get(FormViewModel.class);
        formViewModel.getFormData(formId)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CPPTKPForm>() {
                    @Override
                    public void accept(CPPTKPForm cpptkpForm) throws Exception {
                        formData = cpptkpForm;
                        setContentView(formData);
                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_pengecekan, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    private void setContentView(CPPTKPForm formData) {
        if (formData != null && formData.getRemoteFormStatus().equals("draft")) {
            setExistingData(formData.getFormPelaporan());
            setupFieldKategori();
            setupFieldLingkupTemuan();
            setupFieldPenanggungJawab();
        }

    }

    private void setExistingData(DataFormPelaporan data) {

        userFullname.setText(data.getPenemu().getFullname());
        userPosition.setText(data.getPenemu().getJabatan());
        formDate.setText(dateTimeHelper.printInIndonesian(data.getFormDate()));
        namaProyek.setText(data.getNamaProyek());
        lokasiPenyimpangan.setText(data.getLokasiPenyimpangan());
        bentukPenyimpangan.setText(data.getBentukPenyimpangan());

        if (data.getAttachmentPaths().size() > 0) {
            attachmentArea.setVisibility(View.VISIBLE);
            int thumbnailSize = getResources().getDimensionPixelSize(R.dimen.attachment_thumbnail_size);

            for (final String path : data.getAttachmentPaths()) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        thumbnailSize,
                        thumbnailSize
                );
                params.setMargins(0, 0, getResources().getDimensionPixelSize(R.dimen.attachment_margin), 0);

                SimpleDraweeView simpleDraweeView = FormHelper.getImageThumbnailViewerDrawee(this, thumbnailSize, path);
                attachmentContainer.addView(simpleDraweeView, attachmentContainer.getChildCount() - 1, params);
            }
        } else {
            attachmentArea.setVisibility(View.GONE);
        }
    }

    private void setupFieldKategori() {

        ((ClickToSelectEditText) fieldKategori.getEditText())
                .setAdapter(new StringClickToSelectEditTextListAdapter(getActivity(), FormDataCatalogue.getListFieldKategori()));

    }

    private void setupFieldLingkupTemuan() {

        List<LingkupTemuan> listProyek = dictionaryViewModel.getLingkupTemuanJenisRemote(user);

        ClickToSelectEditText editText = (ClickToSelectEditText) fieldLingkupTemuan.getEditText();
        editText.setAdapter(new ClickToSelectEditTextListAdapter<LingkupTemuan>(getActivity(), listProyek) {
            @Override
            public String getItemLabelToSet(LingkupTemuan selectedItem) {
                return selectedItem.getTitle();
            }
        });

        editText.setOnItemSelectedListener(new ClickToSelectEditText.OnItemSelectedListener<LingkupTemuan>() {
            @Override
            public void onItemSelectedListener(LingkupTemuan item, int selectedIndex) {
                if (item.getTitle().length() > 0) {
                    setupFieldDetailLingkupTemuan(item);
                } else {
                    fieldDetailLingkupTemuan.setVisibility(View.GONE);
                }
            }
        });
    }

    private void setupFieldDetailLingkupTemuan(LingkupTemuan item) {

        List<String> listProyek = dictionaryViewModel.getLingkupTemuanRemote(user, item.getTitle());

        ((ClickToSelectEditText) fieldDetailLingkupTemuan.getEditText())
                .setAdapter(new StringClickToSelectEditTextListAdapter(getActivity(), listProyek));
        fieldDetailLingkupTemuan.setVisibility(View.VISIBLE);
    }

    private void setupFieldPenanggungJawab() {
        List<RoleUnitKerjaResponse> listProyek = dictionaryViewModel.getDivisiAllRemote(user);

        ClickToSelectEditText editText = (ClickToSelectEditText) fieldDivisiPenanggungJawab.getEditText();
        editText.setAdapter(new ClickToSelectEditTextListAdapter<RoleUnitKerjaResponse>(getActivity(), listProyek) {
            @Override
            public String getItemLabelToSet(RoleUnitKerjaResponse selectedItem) {
                return selectedItem.nama;
            }
        });

        editText.setOnItemSelectedListener(new ClickToSelectEditText.OnItemSelectedListener<RoleUnitKerjaResponse>() {
            @Override
            public void onItemSelectedListener(RoleUnitKerjaResponse item, int selectedIndex) {

                setupFieldNamaPenanggungJawab(user, item.getKode());
            }
        });
    }

    private void setupFieldNamaPenanggungJawab(User user, String divisi) {

        String kodeProyek= namaProyek.getText().toString();
        List<User> listProyek = dictionaryViewModel.getUserByDivisiRemote(user, divisi, kodeProyek);
        fieldNamaPenanggungJawab.setVisibility(View.VISIBLE);
        fieldNamaPenanggungJawab.getEditText().setText("");

        ((ClickToSelectEditText) fieldNamaPenanggungJawab.getEditText())
                .setAdapter(new ClickToSelectEditTextListAdapter<User>(getActivity(), listProyek) {
                    @Override
                    public String getItemLabelToSet(User selectedItem) {
                        return selectedItem.getFullname();
                    }
                });
    }

    @Override
    public boolean isValid() {
        return FormHelper.isFormFieldContentValid(fieldKategori)
                && FormHelper.isFormFieldContentValid(fieldLingkupTemuan)
                && isLingkupTemuanExtraValid()
                && FormHelper.isFormFieldContentValid(fieldDivisiPenanggungJawab)
                && FormHelper.isFormFieldContentValid(fieldNamaPenanggungJawab);
    }

    private boolean isLingkupTemuanExtraValid() {
        return fieldDetailLingkupTemuan.getVisibility() != View.VISIBLE
                || FormHelper.isFormFieldContentValid(fieldDetailLingkupTemuan);
    }

    @Override
    public CPPTKPForm getFormData() {

        formData.setFormStatus(FormStatus.FORM_PENGECEKAN);
        formData.setRemoteFormStatus(RemoteFormStatus.PENGECEKAN);
        formData.setNeedResponse(false);

        DataFormPelaporan formPelaporan = formData.getFormPelaporan();
        String kategori = FormHelper.getTextFromField(fieldKategori);
        formPelaporan.setKategoriForm(FormHelper.parseKategoriToConstant(kategori));
        formPelaporan.setLingkupTemuan(FormHelper.getTextFromField(fieldLingkupTemuan));
        formPelaporan.setDetailLingkupTemuan(FormHelper.getTextFromField(fieldDetailLingkupTemuan));

        formData.setFormPelaporan(formPelaporan);

        DataRencanaTindakLanjut rencanaTindakLanjut = new DataRencanaTindakLanjut();
        User penanggungJawab = new User();
        penanggungJawab.setFullname(FormHelper.getTextFromField(fieldNamaPenanggungJawab));
        penanggungJawab.setDivisi(FormHelper.getTextFromField(fieldDivisiPenanggungJawab));
        rencanaTindakLanjut.setPenanggungJawab(penanggungJawab);
        rencanaTindakLanjut.setRelatedFormId(formData.getFormId());

        formData.setRencanaTindakLanjut(rencanaTindakLanjut);

        return formData;
    }

    @Override
    public PostFormRequestGenerator.Strategy getPostFormStrategy(int formStatus) {
        return PostFormRequestGenerator.Strategy.POST_PENGECEKAN;
    }
}
