/*
Copyright 2014 David Morrissey

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package id.wika.cpptkponline.view.annotation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Originally imported from SubsamplingScaleImageView Extension example
 * <p>
 * Edited by mamang_kompii on 29/09/17.
 */

public class FreehandView extends SubsamplingScaleImageView implements OnTouchListener {

    private PointF vPrevious;
    private PointF vStart;

    private int strokeWidth;

    private List<LineDrawingObject> drawingObjects = new ArrayList<>();
    private LineDrawingObject currentDrawingObject;
    private int strokeColor = Color.RED;

    public FreehandView(Context context, AttributeSet attr) {
        super(context, attr);
        initialise();
    }

    public FreehandView(Context context) {
        this(context, null);
    }

    private void initialise() {
        setOnTouchListener(this);
        float density = getResources().getDisplayMetrics().densityDpi;
        strokeWidth = (int) (density / 60f);
    }

    public void setStrokeColor(int strokeColor) {
        this.strokeColor = strokeColor;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        boolean consumed = false;
        int touchCount = event.getPointerCount();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_1_DOWN:
                vStart = new PointF(event.getX(), event.getY());
                vPrevious = new PointF(event.getX(), event.getY());
                break;
            case MotionEvent.ACTION_POINTER_2_DOWN:
                // Abort any current drawing, user is zooming
                vStart = null;
                vPrevious = null;
                break;
            case MotionEvent.ACTION_MOVE:
                PointF sCurrentF = viewToSourceCoord(event.getX(), event.getY());
                if (sCurrentF == null) {
                    break;
                }
                PointF sCurrent = new PointF(sCurrentF.x, sCurrentF.y);
                PointF sStart = vStart == null ? null : new PointF(viewToSourceCoord(vStart).x, viewToSourceCoord(vStart).y);

                if (touchCount == 1 && vStart != null) {
                    float vDX = Math.abs(event.getX() - vPrevious.x);
                    float vDY = Math.abs(event.getY() - vPrevious.y);
                    if (vDX >= strokeWidth * 5 || vDY >= strokeWidth * 5) {
                        if (currentDrawingObject == null) {
                            currentDrawingObject = new LineDrawingObject(strokeWidth, strokeColor);
                            currentDrawingObject.addDrawingPoint(sStart);
                        }
                        currentDrawingObject.addDrawingPoint(sCurrent);
                        vPrevious.x = event.getX();
                        vPrevious.y = event.getY();
                    }
                    consumed = true;
                    invalidate();
                } else if (touchCount == 1) {
                    // Consume all one touch drags to prevent odd panning effects handled by the superclass.
                    consumed = true;
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                invalidate();
                vPrevious = null;
                vStart = null;
                if (currentDrawingObject != null) {
                    drawingObjects.add(currentDrawingObject);
                }
                currentDrawingObject = null;
        }
        // Use parent to handle pinch and two-finger pan.
        return consumed || super.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Don't draw anything before image is ready.
        if (!isReady()) {
            return;
        }

        for (LineDrawingObject drawingObject : drawingObjects) {
            drawObject(canvas, drawingObject);
        }

        if (currentDrawingObject != null) {
            drawObject(canvas, currentDrawingObject);
        }
    }

    public Bitmap drawAnnotationToBitmap(Bitmap bitmap) {
        Canvas canvas = new Canvas(bitmap);
        canvas.save();

        float scale = 1f;

        switch (getAppliedOrientation()) {
            case 0:
            case 360:
                scale = (float) bitmap.getWidth() / getWidth();
                break;
            case 90:
                canvas.translate(0, bitmap.getHeight());
                canvas.rotate(-90);
                scale = (float) bitmap.getWidth() / getHeight();
                break;
            case 180:
                canvas.translate(bitmap.getWidth(), bitmap.getHeight());
                canvas.rotate(180);
                scale = (float) bitmap.getWidth() / getWidth();
                break;
            case 270:
                canvas.translate(bitmap.getWidth(), 0);
                canvas.rotate(90);
                scale = (float) bitmap.getWidth() / getHeight();
                break;
        }

        for (LineDrawingObject drawingObject : drawingObjects) {
            Path vPath = new Path();
            PointF vPrev = drawingObject.getStartingDrawingPoint();
            vPath.moveTo(vPrev.x, vPrev.y);
            for (int i = 1; i < drawingObject.getDrawingPointSize(); i++) {
                PointF vPoint = drawingObject.getDrawingPoint(i);
                vPath.quadTo(vPrev.x, vPrev.y, (vPoint.x + vPrev.x) / 2, (vPoint.y + vPrev.y) / 2);
                vPrev = vPoint;
            }
            Paint paint = drawingObject.getPaint();
            paint.setStrokeWidth(paint.getStrokeWidth() * scale);
            canvas.drawPath(vPath, paint);
        }

        canvas.restore();

        return bitmap;
    }

    private void drawObject(Canvas canvas, LineDrawingObject drawingObject) {
        if (drawingObject.isDrawable()) {
            Path vPath = new Path();
            PointF vPrev = sourceToViewCoord(drawingObject.getStartingDrawingPoint());
            vPath.moveTo(vPrev.x, vPrev.y);
            for (int i = 1; i < drawingObject.getDrawingPointSize(); i++) {
                PointF vPoint = sourceToViewCoord(drawingObject.getDrawingPoint(i));
                vPath.quadTo(vPrev.x, vPrev.y, (vPoint.x + vPrev.x) / 2, (vPoint.y + vPrev.y) / 2);
                vPrev = vPoint;
            }
            canvas.drawPath(vPath, drawingObject.getPaint());
        }
    }

    public void undoDrawing() {
        int size = drawingObjects.size();
        if (size > 0) {
            drawingObjects.remove(size - 1);
            invalidate();
        }
    }

}
