package id.wika.cpptkponline.view.history;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import butterknife.ButterKnife;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.FormSummary;
import id.wika.cpptkponline.helper.FormHelper;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 19/09/17 - 22:57
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved.
 ******************************************************************************/
public class HistoryActivity extends AppCompatActivity implements HistoryFragment.ChangeListener {

    static boolean statusHistory;
    public static String INTENT_EXTRA_PARAM_STATUS = "history_param_status";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);

        HistoryFragment fragment = new HistoryFragment();
        Bundle arguments = new Bundle();
        arguments.putBoolean(HistoryFragment.FRAGMENT_EXTRA_PARAM_STATUS, getIntent().getBooleanExtra(INTENT_EXTRA_PARAM_STATUS, false));
        fragment.setArguments(arguments);
        fragment.setListener(this);

        changeFragment(fragment);

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    public void changeFragment(Fragment f) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, f);

        if (!(f instanceof HistoryFragment)) {
            ft.addToBackStack(null);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (f.getArguments().getString("title") == null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("isHistory", statusHistory);
            f.setArguments(bundle);
        }
        ft.commit();
    }

    public static void start(Context context, boolean status) {
        statusHistory = status;
        Intent starter = new Intent(context, HistoryActivity.class);
        starter.putExtra(INTENT_EXTRA_PARAM_STATUS, status);
        context.startActivity(starter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onChange(Fragment f) {
        changeFragment(f);
    }

}
