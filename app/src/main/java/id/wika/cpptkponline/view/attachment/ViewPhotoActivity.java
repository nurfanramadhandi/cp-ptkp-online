package id.wika.cpptkponline.view.attachment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.facebook.common.util.UriUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.helper.ImageHelper;
import id.wika.cpptkponline.view.dialog.PhotoDeleteConfirmationDialogFragment;

public class ViewPhotoActivity extends AppCompatActivity
        implements PhotoDeleteConfirmationDialogFragment.InteractionListener {

    public static final String EXTRA_KEY_CAN_DELETE = "extraCanDelete";

    @BindView(R.id.image_placeholder)
    ZoomableDraweeView imageViewer;

    @BindView(R.id.delete_button)
    View deleteButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_photo);
        ButterKnife.bind(this);

        boolean isCanDelete = getIntent().getBooleanExtra(EXTRA_KEY_CAN_DELETE, false);
        deleteButton.setVisibility(isCanDelete ? View.VISIBLE : View.GONE);

        GenericDraweeHierarchy hierarchy = new GenericDraweeHierarchyBuilder(getResources())
                .setActualImageScaleType(ScalingUtils.ScaleType.FIT_CENTER)
                .build();

        imageViewer.setHierarchy(hierarchy);
        imageViewer.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                if (view.getHeight() > 0 && view.getHeight() > 0) {
                    imageViewer.removeOnLayoutChangeListener(this);
                    setPic();
                }
            }
        });
    }

    private void setPic() {
        String path = getIntent().getStringExtra(ImageHelper.EXTRA_KEY_IMAGE_FILE_PATH);
        Uri photoURI = Uri.parse(path);
        if (!UriUtil.isNetworkUri(photoURI)) {
            photoURI = FileProvider.getUriForFile(this,
                    "id.wika.cppptkponline.fileprovider",
                    new File(path));
        }
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(photoURI)
                .setResizeOptions(new ResizeOptions(imageViewer.getWidth(), imageViewer.getHeight()))
                .build();
        imageViewer.setController(
                Fresco.newDraweeControllerBuilder()
                        .setOldController(imageViewer.getController())
                        .setImageRequest(request)
                        .build());
    }

    @OnClick(R.id.back_button)
    void onCancelButtonClick() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.delete_button)
    void onDeleteButtonClick() {
        PhotoDeleteConfirmationDialogFragment dialogFragment = new PhotoDeleteConfirmationDialogFragment();
        dialogFragment.show(getFragmentManager(), PhotoDeleteConfirmationDialogFragment.TAG);
    }

    @Override
    public void onConfirmDeletePhoto() {
        Intent intent = new Intent();
        intent.putExtras(getIntent().getExtras());
        setResult(RESULT_OK, intent);
        finish();
    }
}
