package id.wika.cpptkponline.view.review;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataFormPelaporan;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.helper.StringHelper;
import id.wika.cpptkponline.presenter.ReviewViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ReviewFormPelaporanFragment extends Fragment {

    private DateTimeHelper dateTimeHelper = new DateTimeHelper();

    @BindView(R.id.nomor_form_title)
    View nomorFormTitle;

    @BindView(R.id.nomor_form)
    TextView nomorForm;

    @BindView(R.id.user_fullname)
    TextView userFullname;

    @BindView(R.id.user_position)
    TextView userPosition;

    @BindView(R.id.form_date)
    TextView formDate;

    @BindView(R.id.field_project_name)
    TextView namaProyek;

    @BindView(R.id.field_lokasi_penyimpangan)
    TextView lokasiPenyimpangan;

    @BindView(R.id.field_bentuk_penyimpangan)
    TextView bentukPenyimpangan;

    @BindView(R.id.attachment_area)
    View attachmentArea;

    @BindView(R.id.attachment_container)
    LinearLayout attachmentContainer;

    @BindView(R.id.field_kategori)
    TextView kategoriForm;

    @BindView(R.id.field_lingkup_temuan)
    TextView lingkupTemuan;

    @BindView(R.id.field_lingkup_temuan_extra_title)
    TextView extraLingkupTemuanTitle;

    @BindView(R.id.field_lingkup_temuan_extra_content)
    TextView extraLingkupTemuanContent;

    @BindView(R.id.extended_form_field_qa)
    View extendedFormField;

    @BindView(R.id.extended_form_penanggung_jawab)
    View extendedPJField;

    @BindView(R.id.field_divisi_penanggungjawab)
    TextView divisiPenanggungJawab;

    @BindView(R.id.field_nama_penanggungjawab)
    TextView namaPenanggungJawab;

    public static Fragment newInstance(int formId, boolean isUsingRemoteData) {
        Bundle args = new Bundle();
        args.putInt(FormHelper.EXTRA_KEY_FORM_ID, formId);
        args.putBoolean(FormHelper.EXTRA_KEY_REVIEW_WITH_REMOTE_DATA, isUsingRemoteData);

        ReviewFormPelaporanFragment fragment = new ReviewFormPelaporanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ReviewFormPelaporanFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int formId = getArguments().getInt(FormHelper.EXTRA_KEY_FORM_ID);
        boolean isUsingRemoteData = getArguments().getBoolean(FormHelper.EXTRA_KEY_REVIEW_WITH_REMOTE_DATA);

        ReviewViewModel reviewViewModel = ViewModelProviders
                .of(getActivity())
                .get(ReviewViewModel.class);
        reviewViewModel.getFormReviewData(formId, FormStatus.FORM_PENGECEKAN, isUsingRemoteData)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CPPTKPForm>() {
                    @Override
                    public void accept(CPPTKPForm cpptkpForm) throws Exception {
                        DataFormPelaporan formPelaporan = cpptkpForm.getFormPelaporan();
                        if (formPelaporan != null) {
                            setExistingData(formPelaporan);
                        }
                        DataRencanaTindakLanjut rencanaTindakLanjut = cpptkpForm.getRencanaTindakLanjut();
                        if (rencanaTindakLanjut != null) {
                            setExistingData(rencanaTindakLanjut);
                        }
                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_review_form_pelaporan, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    private void setExistingData(DataFormPelaporan data) {

        String noTemuan = data.getNoTemuan();
        if (StringHelper.isNotBlank(noTemuan)) {
            nomorFormTitle.setVisibility(View.VISIBLE);
            nomorForm.setText(noTemuan);
            nomorForm.setVisibility(View.VISIBLE);
        } else {
            nomorFormTitle.setVisibility(View.GONE);
            nomorForm.setVisibility(View.GONE);
        }

        userFullname.setText(data.getPenemu().getFullname());
        userPosition.setText(data.getPenemu().getJabatan());
        formDate.setText(dateTimeHelper.printInIndonesian(data.getFormDate()));
        namaProyek.setText(data.getNamaProyek());
        lokasiPenyimpangan.setText(data.getLokasiPenyimpangan());
        bentukPenyimpangan.setText(data.getBentukPenyimpangan());

        if (data.getAttachmentPaths().size() > 0) {
            attachmentArea.setVisibility(View.VISIBLE);
            int thumbnailSize = getResources().getDimensionPixelSize(R.dimen.attachment_thumbnail_size);

            for (final String path : data.getAttachmentPaths()) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        thumbnailSize,
                        thumbnailSize
                );
                params.setMargins(0, 0, getResources().getDimensionPixelSize(R.dimen.attachment_margin), 0);

                SimpleDraweeView simpleDraweeView = FormHelper.getImageThumbnailViewerDrawee(this, thumbnailSize, path);
                attachmentContainer.addView(simpleDraweeView, attachmentContainer.getChildCount() - 1, params);
            }
        } else {
            attachmentArea.setVisibility(View.GONE);
        }

        String kategoriFormStr = data.getKategoriForm();
        if (StringHelper.isNotBlank(kategoriFormStr)) {
            kategoriForm.setText(FormHelper.getFormCategoryString(kategoriFormStr));
            lingkupTemuan.setText(data.getLingkupTemuan());

            String dataExtraLingkupTemuanDesc = data.getDetailLingkupTemuan();
            if (StringHelper.isNotBlank(dataExtraLingkupTemuanDesc)) {
                extraLingkupTemuanContent.setText(data.getDetailLingkupTemuan());
                extraLingkupTemuanTitle.setVisibility(View.VISIBLE);
                extraLingkupTemuanContent.setVisibility(View.VISIBLE);
            } else {
                extraLingkupTemuanTitle.setVisibility(View.GONE);
                extraLingkupTemuanContent.setVisibility(View.GONE);
            }
        } else {
            extendedFormField.setVisibility(View.GONE);
        }
    }

    private void setExistingData(DataRencanaTindakLanjut rencanaTindakLanjut) {
        User penanggungJawab = rencanaTindakLanjut.getPenanggungJawab();
        if (penanggungJawab != null) {
            namaPenanggungJawab.setText(penanggungJawab.getFullname());
            divisiPenanggungJawab.setText(penanggungJawab.getDivisi());
            extendedPJField.setVisibility(View.VISIBLE);
        } else {
            extendedPJField.setVisibility(View.GONE);
        }
    }
}
