package id.wika.cpptkponline.view.home.list;

import android.support.v7.widget.SearchView;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 19/September/2017 - 15:56
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public class RecordQueryListener implements SearchView.OnQueryTextListener {
    private FormListSummaryRecyclerViewAdapter recordAdapter;

    public RecordQueryListener(FormListSummaryRecyclerViewAdapter recordAdapter) {
        this.recordAdapter = recordAdapter;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        recordAdapter.getAdapterFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        recordAdapter.getAdapterFilter().filter(newText);
        return false;
    }
}