package id.wika.cpptkponline.view.dashboard;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.endpoint.response.GetGrafikResponse;
import id.wika.cpptkponline.data.endpoint.response.GetGrafikRootCouseResponse;
import id.wika.cpptkponline.injection.AppComponent;
import id.wika.cpptkponline.injection.DaggerDashboardViewComponent;
import id.wika.cpptkponline.injection.DashboardViewModule;
import id.wika.cpptkponline.presenter.DashboardPresenter;
import id.wika.cpptkponline.presenter.DictionaryViewModel;
import id.wika.cpptkponline.presenter.PresenterFactory;
import id.wika.cpptkponline.view.DashboardView;
import id.wika.cpptkponline.view.impl.BaseActivity;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 2:39:34 PM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public final class DashboardActivity extends BaseActivity<DashboardPresenter, DashboardView> implements DashboardView {
    @Inject
    PresenterFactory<DashboardPresenter> mPresenterFactory;

    // Your presenter is available using the mPresenter variable
    private DictionaryViewModel dictionaryViewModel;
    private User user;
    private Integer selectedYear = DateTime.now().getYear();

    @BindView(R.id.chart1)
    PieChart mChart1;

    @BindView(R.id.chart2)
    PieChart mChart2;

    @BindView(R.id.chart3)
    PieChart mChart3;

    @BindView(R.id.chart4)
    PieChart mChart4;

    @BindView(R.id.chart_title_1)
    TextView chart1Title;

    @BindView(R.id.chart_title_2)
    TextView chart2Title;

    @BindView(R.id.chart_title_3)
    TextView chart3Title;

    @BindView(R.id.chart_title_4)
    TextView chart4Title;

    private ArrayList<Integer> years = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        user = User.load(this);
        ButterKnife.bind(this);
        dictionaryViewModel = new DictionaryViewModel();

        setupFilterData();
        setupCharts();
    }

    private void setupFilterData() {
        DateTime now = DateTime.now();
        for (DateTime d = now.minusYears(5); d.isBefore(now); d = d.plusYears(1)) {
            years.add(d.getYear());
        }

        years.add(now.getYear());
    }

    private void setupCharts() {
        setupChart(mChart1);
        setupChart(mChart2);
        setupChart(mChart3);
        setupChart(mChart4);
    }

    private void setupChart(PieChart mChart) {
        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
//        mChart.setExtraOffsets(5, 10, 5, 5);
        mChart.setExtraLeftOffset(5);
        mChart.setExtraRightOffset(5);
//        mChart.setExtraTopOffset(-10);
        mChart.setDrawHoleEnabled(false);
        mChart.setRotationAngle(0);

        mChart.getLegend().setEnabled(false);

        // enable rotation of the chart by touch
        mChart.setRotationEnabled(false);
        mChart.setHighlightPerTapEnabled(true);

        // add a selection listener
        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry entry, Highlight highlight) {
                if (entry == null)
                    return;

                Toast.makeText(DashboardActivity.this, "Value: " + entry.getY() + ", index: " + highlight.getX()
                        + ", DataSet index: " + highlight.getDataSetIndex(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {
                Log.i("PieChart", "nothing selected");
            }
        });

        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        mChart.setEntryLabelColor(Color.BLACK);
        mChart.setEntryLabelTextSize(12f);

    }

    @Override
    protected void onStart() {
        super.onStart();
        rerenderChart();
    }

    @Override
    protected void setupComponent(@NonNull AppComponent parentComponent) {
        DaggerDashboardViewComponent.builder()
                .appComponent(parentComponent)
                .dashboardViewModule(new DashboardViewModule())
                .build()
                .inject(this);
    }

    @NonNull
    @Override
    protected PresenterFactory<DashboardPresenter> getPresenterFactory() {
        return mPresenterFactory;
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, DashboardActivity.class);
//        starter.putExtra();
        context.startActivity(starter);
    }

    private void setData(PieChart mChart, List<GetGrafikResponse.Datum> dataList) {
        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (GetGrafikResponse.Datum data : dataList) {
            entries.add(new PieEntry(data.getJumlah(), data.getLingkupTemuan(), getResources().getDrawable(R.drawable.ic_history)));
        }

        PieDataSet dataSet = new PieDataSet(entries, "Jenis Lingkup Temuan");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
//        data.setValueTypeface(mTfLight);
        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }

    private void setDataRoot(PieChart mChart, List<GetGrafikRootCouseResponse.Datum> dataList) {
        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (GetGrafikRootCouseResponse.Datum data : dataList) {
            if (data.getRootCauseCategory() != null)
                entries.add(new PieEntry(data.getJumlah(), data.getRootCauseCategory(), getResources().getDrawable(R.drawable.ic_history)));
        }

        PieDataSet dataSet = new PieDataSet(entries, "Kategori Root Cause");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
//        data.setValueTypeface(mTfLight);
        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }


    @Override
    public void clearChart1() {
        mChart1.clear();
    }

    @Override
    public void clearChart2() {
        mChart2.clear();
    }

    @Override
    public void clearChart3() {
        mChart3.clear();
    }

    @Override
    public void clearChart4() {
        mChart4.clear();
    }

    @Override
    public void setChart1Title(String title) {
        chart1Title.setText(title);
    }

    @Override
    public void setChart2Title(String title) {
        chart2Title.setText(title);
    }

    @Override
    public void setChart3Title(String title) {
        chart3Title.setText(title);
    }

    @Override
    public void setChart4Title(String title) {
        chart4Title.setText(title);
    }

    @Override
    public void renderChart1(List<GetGrafikRootCouseResponse.Datum> dataList) {
        setDataRoot(mChart1, dataList);
    }

    @Override
    public void renderChart2(List<GetGrafikResponse.Datum> dataList) {
        setData(mChart2, dataList);
    }

    @Override
    public void renderChart3(List<GetGrafikResponse.Datum> dataList) {
        setData(mChart3, dataList);
    }

    @Override
    public void renderChart4(List<GetGrafikResponse.Datum> dataList) {
        setData(mChart4, dataList);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context context() {
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_filter) {
            showMenuFilter();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showMenuFilter() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        ArrayAdapter<Integer> arrayAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, years);

        builder.setTitle("Filter Tahun")
                .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        selectedYear = years.get(i);
                        rerenderChart();
                    }
                });

        builder.show();
    }

    private void rerenderChart() {
        if (mPresenter != null) {
            mPresenter.renderChart1(selectedYear);
            mPresenter.renderChart2(selectedYear);
            mPresenter.renderChart3(selectedYear);
            mPresenter.renderChart4(selectedYear);
        }
    }
}
