package id.wika.cpptkponline.view;

/**
 * Created by apple on 9/10/17.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import id.wika.cpptkponline.data.ServiceGenerator;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.endpoint.LoginEndpointInterface;
import id.wika.cpptkponline.data.endpoint.TokenWrapper;
import id.wika.cpptkponline.data.endpoint.response.RoleUnitKerjaResponse;
import id.wika.cpptkponline.view.home.HomeActivity;
import id.wika.cpptkponline.view.login.LoginActivity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;


public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_splash_screen);
        setVisible(false);
        final User session = User.load(this);
        if (session != null){
            LoginEndpointInterface loginAPI = ServiceGenerator.createService(LoginEndpointInterface.class);

            loginAPI.cekToken(TokenWrapper.getWrappedToken(session.getToken()))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<RoleUnitKerjaResponse>() {

                        @Override
                        protected void onStart() {
                            super.onStart();
                        }

                        @Override
                        public void onNext(RoleUnitKerjaResponse loginResponse) {
                            Intent intent = new Intent(SplashScreen.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                            finishAndStartActivity(intent);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(SplashScreen.this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            User.clear(SplashScreen.this);
                            FirebaseMessaging.getInstance().unsubscribeFromTopic(session.getRoleId());

                            Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                            finishAndStartActivity(intent);
                        }

                        @Override
                        public void onComplete() {
                        }
                    });

        } else {
            Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            finishAndStartActivity(intent);
        }
    }

    private void finishAndStartActivity(Intent intent) {
        startActivity(intent);
        finish();
    }
}