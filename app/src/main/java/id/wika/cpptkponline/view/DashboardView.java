package id.wika.cpptkponline.view;

import android.support.annotation.UiThread;

import java.util.List;

import id.wika.cpptkponline.data.PieChartData;
import id.wika.cpptkponline.data.endpoint.response.GetGrafikResponse;
import id.wika.cpptkponline.data.endpoint.response.GetGrafikRootCouseResponse;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 2:39:34 PM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

@UiThread
public interface DashboardView extends BaseView{
    void renderChart1(List<GetGrafikRootCouseResponse.Datum> dataList);
    void renderChart2(List<GetGrafikResponse.Datum> dataList);
    void renderChart3(List<GetGrafikResponse.Datum> dataList);
    void renderChart4(List<GetGrafikResponse.Datum> dataList);

    void clearChart1();
    void clearChart2();
    void clearChart3();
    void clearChart4();

    void setChart1Title(String title);
    void setChart2Title(String title);
    void setChart3Title(String title);
    void setChart4Title(String title);
}