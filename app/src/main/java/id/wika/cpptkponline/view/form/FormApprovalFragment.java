package id.wika.cpptkponline.view.form;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataApproval;
import id.wika.cpptkponline.data.DataFormPelaporan;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.data.endpoint.RemoteFormStatus;
import id.wika.cpptkponline.data.endpoint.request.PostFormRequestGenerator;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.presenter.FormViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class FormApprovalFragment extends Fragment implements CPPTKPFormView {

    private CPPTKPForm formData;
    private User user;

    private boolean isApproved = true;
    private DateTime formDate = DateTime.now();

    private int formId;

    @BindView(R.id.attachment_container)
    LinearLayout attachmentContainer;

    public static FormApprovalFragment newInstance(int formId) {
        Bundle args = new Bundle();
        args.putInt(FormHelper.EXTRA_KEY_FORM_ID, formId);
        FormApprovalFragment fragment = new FormApprovalFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public FormApprovalFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = User.load(getActivity());

        if (getArguments() != null) {
            formId = getArguments().getInt(FormHelper.EXTRA_KEY_FORM_ID);

            FormViewModel formViewModel = ViewModelProviders
                    .of(getActivity())
                    .get(FormViewModel.class);
            formViewModel.getFormData(formId)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<CPPTKPForm>() {
                        @Override
                        public void accept(CPPTKPForm cpptkpForm) throws Exception {
                            formData = cpptkpForm;
                            setContentView(formData);
                        }
                    });
        }
    }

    private Set<String> pathSet = new HashSet<>();

    private void setContentView(CPPTKPForm formData) {
        DataFormPelaporan dataFormPelaporan = formData.getFormPelaporan();

        if (dataFormPelaporan.getAttachmentPaths() != null && dataFormPelaporan.getAttachmentPaths().size() > 0) {
            int thumbnailSize = getResources().getDimensionPixelSize(R.dimen.attachment_thumbnail_size);
            int margin = getResources().getDimensionPixelSize(R.dimen.attachment_margin);

            for (final String path : dataFormPelaporan.getAttachmentPaths()) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(0, 0, margin, 0);

                View view = FormHelper.getCheckableImageThumbnail(
                        getActivity(),
                        thumbnailSize,
                        path,
                        pathSet);
                attachmentContainer.addView(view, attachmentContainer.getChildCount() - 1, params);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_approval, container, false);
        ButterKnife.bind(this, view);

        setupStaticInfo(view);
        return view;
    }

    private void setupStaticInfo(View view) {
        ((TextView) view.findViewById(R.id.user_fullname)).setText(user.getFullname());
        ((TextView) view.findViewById(R.id.user_position)).setText(user.getJabatan());

        DateTimeHelper dateTimeHelper = new DateTimeHelper();
        ((TextView) view.findViewById(R.id.approval_date)).setText(dateTimeHelper.printInIndonesian(formDate));
    }

    @OnClick(R.id.history_laporan_awal_btn)
    void onClickViewHistoryLaporanAwal() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_PENGECEKAN);
    }

    @OnClick(R.id.history_rencana_tindak_lanjut_btn)
    void onClickViewHistoryRencanaTindakLanjut() {
        FormHelper.launchReviewFormActivityWithNonHistoricalLocalData(
                getActivity(), formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT);
    }

    @OnCheckedChanged(R.id.approval_positive)
    void onApproveRadioCheckedChanged(boolean checked) {
        if (checked) {
            isApproved = true;
        }
    }

    @OnCheckedChanged(R.id.approval_negative)
    void onRejectRadioCheckedChanged(boolean checked) {
        if (checked) {
            isApproved = false;
        }
    }

    @Override
    public boolean isValid() {
        return pathSet.size() > 0;
    }

    @Override
    public CPPTKPForm getFormData() {
        formData.setFormStatus(isApproved ? FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI : FormStatus.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN);
        formData.setRemoteFormStatus(RemoteFormStatus.APPROVAL);
        formData.setNeedResponse(false);

        DataApproval approval = new DataApproval(user, formDate, isApproved);
        approval.setRelatedFormId(formData.getFormId());
        approval.setApprovedAttachmentPaths(new ArrayList<>(pathSet));

        formData.setApproval(approval);

        return formData;
    }

    @Override
    public PostFormRequestGenerator.Strategy getPostFormStrategy(int formStatus) {
        return PostFormRequestGenerator.Strategy.POST_APPROVAL;
    }
}
