package id.wika.cpptkponline.view.impl;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

import id.wika.cpptkponline.App;
import id.wika.cpptkponline.injection.AppComponent;
import id.wika.cpptkponline.presenter.BasePresenter;
import id.wika.cpptkponline.presenter.PresenterFactory;

import java.util.concurrent.atomic.AtomicBoolean;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:34:19 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public abstract class BaseActivity<P extends BasePresenter<V>, V> extends AppCompatActivity {
    /**
     * Will be true if presenter wasn't loaded when {@link #onStart()} is reached
     */
    private final AtomicBoolean mNeedToCallStart = new AtomicBoolean(false);
    /**
     * The presenter for this view
     */
    @Nullable
    protected P mPresenter;
    /**
     * Is this the first start of the activity (after onCreate)
     */
    private boolean mFirstStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirstStart = true;

        injectDependencies();

        mPresenter = getPresenterFactory().create();

    }

    private void injectDependencies() {
        setupComponent(((App) getApplication()).getAppComponent());
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mPresenter == null) {
            mNeedToCallStart.set(true);
        } else {
            doStart();
        }
    }

    /**
     * Call the presenter callbacks for onStart
     */
    @SuppressWarnings("unchecked")
    private void doStart() {
        assert mPresenter != null;

        mPresenter.onViewAttached((V) this);

        mPresenter.onStart(mFirstStart);

        mFirstStart = false;
    }

    @Override
    protected void onStop() {
        if (mPresenter != null) {
            mPresenter.onStop();

            mPresenter.onViewDetached();
        }

        super.onStop();
    }

    /**
     * Get the mPresenterFactory factory implementation for this view
     *
     * @return the mPresenterFactory factory
     */
    @NonNull
    protected abstract PresenterFactory<P> getPresenterFactory();

    /**
     * Setup the injection component for this view
     *
     * @param appComponent the app component
     */
    protected abstract void setupComponent(@NonNull AppComponent appComponent);
}
