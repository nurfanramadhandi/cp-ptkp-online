package id.wika.cpptkponline.view.review;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.CPPTKPForm;
import id.wika.cpptkponline.data.DataRencanaTindakLanjut;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.User;
import id.wika.cpptkponline.helper.DateTimeHelper;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.helper.StringHelper;
import id.wika.cpptkponline.presenter.ReviewViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ReviewRencanaTindakLanjutFragment extends Fragment {

    private int formId;
    private boolean isReviewWithHistory;
    boolean isUsingRemoteData;

    @BindView(R.id.history_laporan_awal_btn)
    View historyLaporanAwalButton;

    @BindView(R.id.field_nama_penanggungjawab)
    TextView namaPenanggungJawab;

    @BindView(R.id.field_divisi_penanggungjawab)
    TextView divisiPenanggungJawab;

    @BindView(R.id.field_jabatan_penanggungjawab)
    TextView jabatanPenanggungJawab;

    @BindView(R.id.field_penyebab_utama_penyimpangan)
    TextView penyebabUtamaPenyimpangan;

    @BindView(R.id.field_keputusan_jangka_pendek)
    TextView keputusanJangkaPendek;

    @BindView(R.id.field_keputusan_jangka_pendek_extra_title)
    TextView detailKeputusanJangkaPendekTitle;

    @BindView(R.id.field_keputusan_jangka_pendek_extra)
    TextView detailKeputusanJangkaPendek;

    @BindView(R.id.field_waktu_keputusan_jangka_pendek)
    TextView waktuKeputusanJangkaPendek;

    @BindView(R.id.field_keputusan_jangka_panjang)
    TextView keputusanJangkaPanjang;

    @BindView(R.id.field_waktu_keputusan_jangka_panjang)
    TextView waktuKeputusanJangkaPanjang;

    public static Fragment newInstance(int formId, boolean isShowingHistory, boolean isUsingRemoteData) {
        Bundle args = new Bundle();
        args.putInt(FormHelper.EXTRA_KEY_FORM_ID, formId);
        args.putBoolean(FormHelper.EXTRA_KEY_REVIEW_WITH_REMOTE_DATA, isUsingRemoteData);
        args.putBoolean(FormHelper.EXTRA_KEY_IS_REVIEW_WITH_HISTORY, isShowingHistory);

        ReviewRencanaTindakLanjutFragment fragment = new ReviewRencanaTindakLanjutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        formId = getArguments().getInt(FormHelper.EXTRA_KEY_FORM_ID);
        isUsingRemoteData = getArguments().getBoolean(FormHelper.EXTRA_KEY_REVIEW_WITH_REMOTE_DATA);
        isReviewWithHistory = getArguments().getBoolean(FormHelper.EXTRA_KEY_IS_REVIEW_WITH_HISTORY);

        ReviewViewModel reviewViewModel = ViewModelProviders
                .of(getActivity())
                .get(ReviewViewModel.class);
        reviewViewModel.getFormReviewData(formId, FormStatus.FORM_RENCANA_TINDAK_LANJUT, isUsingRemoteData)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CPPTKPForm>() {
                    @Override
                    public void accept(CPPTKPForm cpptkpForm) throws Exception {
                        if (cpptkpForm.getRencanaTindakLanjut() != null) {
                            setExistingData(cpptkpForm.getRencanaTindakLanjut());
                        }
                    }
                });
    }


    public ReviewRencanaTindakLanjutFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review_rancangan_tindak_lanjut, container, false);
        ButterKnife.bind(this, view);

        historyLaporanAwalButton.setVisibility(isReviewWithHistory ? View.VISIBLE : View.GONE);

        return view;
    }

    private void setExistingData(DataRencanaTindakLanjut dataRencanaTindakLanjut) {
        User penanggungJawab = dataRencanaTindakLanjut.getPenanggungJawab();
        if (penanggungJawab != null) {
            namaPenanggungJawab.setText(penanggungJawab.getFullname());
            divisiPenanggungJawab.setText(penanggungJawab.divisi);
            jabatanPenanggungJawab.setText(penanggungJawab.jabatan);
        } else {
            namaPenanggungJawab.setText("No Data");
            divisiPenanggungJawab.setText("No Data");
            jabatanPenanggungJawab.setText("No Data");
        }
        penyebabUtamaPenyimpangan.setText(dataRencanaTindakLanjut.getPenyebabUtamaPenyimpangan());
        keputusanJangkaPendek.setText(dataRencanaTindakLanjut.getKeputusanJangkaPendek());

        String detailKeputusanJangkaPendekStr = dataRencanaTindakLanjut.getDetailKeputusanJangkaPendek();
        if (StringHelper.isNotBlank(detailKeputusanJangkaPendekStr)) {
            detailKeputusanJangkaPendekTitle.setText(dataRencanaTindakLanjut.getTitleDetailKeputusanJangkaPendek());
            detailKeputusanJangkaPendek.setText(detailKeputusanJangkaPendekStr);
        } else {
            detailKeputusanJangkaPendekTitle.setVisibility(View.GONE);
            detailKeputusanJangkaPendek.setVisibility(View.GONE);
        }

        DateTimeHelper dateTimeHelper = new DateTimeHelper();
        waktuKeputusanJangkaPendek.setText(dateTimeHelper.printInIndonesian(
                dataRencanaTindakLanjut.getTargetWaktuKeputusanJangkaPendek()));

        keputusanJangkaPanjang.setText(dataRencanaTindakLanjut.getKeputusanJangkaPanjang());
        waktuKeputusanJangkaPanjang.setText(dateTimeHelper.printInIndonesian(
                dataRencanaTindakLanjut.getTargetWaktuKeputusanJangkaPanjang()));
    }

    @OnClick(R.id.history_laporan_awal_btn)
    void onClickHistoryLaporanAwalButton() {
        FormHelper.launchReviewFormActivity(
                getActivity(), formId, FormStatus.FORM_PENGECEKAN, false, isUsingRemoteData);
    }
}
