package id.wika.cpptkponline.view.history;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.FormSummary;
import id.wika.cpptkponline.data.filter.ResumeFillterType;
import id.wika.cpptkponline.helper.FormHelper;
import id.wika.cpptkponline.helper.FormSummarySortComparatorFactory;
import id.wika.cpptkponline.injection.AppComponent;
import id.wika.cpptkponline.injection.DaggerHistoryViewComponent;
import id.wika.cpptkponline.injection.HistoryViewModule;
import id.wika.cpptkponline.presenter.HistoryPresenter;
import id.wika.cpptkponline.presenter.PresenterFactory;
import id.wika.cpptkponline.view.HistoryView;
import id.wika.cpptkponline.view.home.list.FormListItem;
import id.wika.cpptkponline.view.home.list.FormListSummaryRecyclerViewAdapter;
import id.wika.cpptkponline.view.home.list.RecordQueryExpandListener;
import id.wika.cpptkponline.view.home.list.RecordQueryListener;
import id.wika.cpptkponline.view.impl.BaseFragment;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : Sep 20, 2017 8:51:54 AM
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/


public final class HistoryFragment extends BaseFragment<HistoryPresenter, HistoryView> implements HistoryView {
    public static final String FRAGMENT_EXTRA_PARAM_STATUS = "param_status";
    @Inject
    PresenterFactory<HistoryPresenter> mPresenterFactory;
    @BindView(R.id.form_list)
    RecyclerView formList;
    Unbinder unbinder;
    private FormListSummaryRecyclerViewAdapter adapter;
    @NonNull
    private ChangeListener listener;
    private boolean isRefreshing = false;
    private boolean status = false;
    private boolean isHistory;

    // Your presenter is available using the mPresenter variable

    public interface ChangeListener {
        void onChange(Fragment f);
    }

    public HistoryFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isHistory = getArguments().getBoolean("isHistory");
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        if (getArguments() != null) {
            status = getArguments().getBoolean(FRAGMENT_EXTRA_PARAM_STATUS);
        }

        unbinder = ButterKnife.bind(this, view);
        initFormList();
        if (isHistory) ((HistoryActivity) getActivity()).getSupportActionBar().setTitle("History");
        else ((HistoryActivity) getActivity()).getSupportActionBar().setTitle("Tracking");
        return view;
    }

    public void setListener(ChangeListener changeListener) {
        listener = changeListener;
    }

    private void initFormList() {
        adapter = new FormListSummaryRecyclerViewAdapter(new ArrayList<FormListItem>());
        adapter.setListener(createListener());
        formList.setAdapter(adapter);
        formList.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private FormListSummaryRecyclerViewAdapter.OnItemClickListener createListener() {
        return new FormListSummaryRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                FormListItem item = ((FormListSummaryRecyclerViewAdapter) formList.getAdapter()).getItem(position);
                FormSummary summary = (FormSummary) item.getData();

                HistoryDetailFragment fragment = new HistoryDetailFragment();
                Bundle arguments = new Bundle();
                arguments.putInt("id", summary.getRemoteFormId());
                arguments.putString("title", summary.getTitle());
                arguments.putString("subtitle", summary.getSubTitle());
                fragment.setArguments(arguments);

                if (isHistory)
                    handleOpenDataReview(summary);
                else
                    listener.onChange(fragment);

            }
        };
    }

    private void handleOpenDataReview(FormSummary summary) {
        FormHelper.launchReviewFormActivity(
                getActivity(),
                summary.getFormId(),
                summary.getStatus(),
                true,
                true);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Your code here
        // Do not call mPresenter from here, it will be null! Wait for onStart
    }

    @Override
    protected void setupComponent(@NonNull AppComponent parentComponent) {
        DaggerHistoryViewComponent.builder()
                .appComponent(parentComponent)
                .historyViewModule(new HistoryViewModule())
                .build()
                .inject(this);
    }

    @NonNull
    @Override
    protected PresenterFactory<HistoryPresenter> getPresenterFactory() {
        return mPresenterFactory;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_history_main_menu, menu);
        setSearchView(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setSearchView(Menu menu) {
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new RecordQueryListener(adapter));

        MenuItemCompat.setOnActionExpandListener(item, new RecordQueryExpandListener(adapter));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                if (mPresenter != null) {
                    mPresenter.getListData();
                    isRefreshing = true;
                }
                break;
            case R.id.action_filter:
                showMenuFilter();
                return true;
            case R.id.action_sort:
                showSortMenu();
                return true;
        }
        return true;

    }

    private void showMenuFilter() {
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
        String[] sports = {
                "ALL"
                , "FORM_AWAL".replace("_", " ")
                , "FORM_PENGECEKAN".replace("_", " ")
                , "FORM_RENCANA_TINDAK_LANJUT_DISETUJUI".replace("_", " ")
                , "FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN".replace("_", " ")
                , "FORM_RENCANA_TINDAK_LANJUT".replace("_", " ")
                , "FORM_REALISASI_TINDAK_LANJUT".replace("_", " ")
                , "FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT".replace("_", " ")
                , "LAPORAN_TINDAK_LANJUT_STATUS_OPEN".replace("_", " ")
                , "LAPORAN_TINDAK_LANJUT_STATUS_CLOSED".replace("_", " ")
        };
        b.setTitle("Filter Status")
                .setItems(sports, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (mPresenter != null) {
                            switch (which) {
                                case 0:
                                    mPresenter.setResumeFillterType(ResumeFillterType.ALL);
                                    break;
                                case 1:
                                    mPresenter.setResumeFillterType(ResumeFillterType.FORM_AWAL);
                                    break;
                                case 2:
                                    mPresenter.setResumeFillterType(ResumeFillterType.FORM_PENGECEKAN);
                                    break;
                                case 3:
                                    mPresenter.setResumeFillterType(ResumeFillterType.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI);
                                    break;
                                case 4:
                                    mPresenter.setResumeFillterType(ResumeFillterType.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN);
                                    break;
                                case 5:
                                    mPresenter.setResumeFillterType(ResumeFillterType.FORM_RENCANA_TINDAK_LANJUT);
                                    break;
                                case 6:
                                    mPresenter.setResumeFillterType(ResumeFillterType.FORM_REALISASI_TINDAK_LANJUT);
                                    break;
                                case 7:
                                    mPresenter.setResumeFillterType(ResumeFillterType.FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT);
                                    break;
                                case 8:
                                    mPresenter.setResumeFillterType(ResumeFillterType.LAPORAN_TINDAK_LANJUT_STATUS_OPEN);
                                    break;
                                case 9:
                                    mPresenter.setResumeFillterType(ResumeFillterType.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED);
                                    break;
                            }
                            mPresenter.getListData();
                        }
                    }
                });

        b.show();

    }

    private void showSortMenu() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String[] sortTypes = {
                "Tanggal Update (ASC)",
                "Tanggal Update (DESC)",
                "Nomor Form (ASC)",
                "Nomor Form (DESC)"
        };

        builder.setTitle("Urutkan berdasarkan")
                .setItems(sortTypes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mPresenter != null) {
                            mPresenter.setSortType(getSelectedSortType(which));
                            mPresenter.getListData();
                        }
                    }

                    private FormSummarySortComparatorFactory.SORT_TYPE getSelectedSortType(int which) {
                        switch (which) {
                            case 0:
                                return FormSummarySortComparatorFactory.SORT_TYPE.DATE_ASC;
                            case 1:
                                return FormSummarySortComparatorFactory.SORT_TYPE.DATE_DESC;
                            case 2:
                                return FormSummarySortComparatorFactory.SORT_TYPE.REMOTE_FORM_ID_ASC;
                            case 3:
                                return FormSummarySortComparatorFactory.SORT_TYPE.REMOTE_FORM_ID_DESC;
                            default:
                                return FormSummarySortComparatorFactory.SORT_TYPE.DATE_DESC;
                        }
                    }
                });

        builder.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void loadData(List<FormListItem> formListItems) {
        adapter.setData(formListItems);
        adapter.notifyDataSetChanged();
        if (isRefreshing) {
            Toast.makeText(getActivity(), "Data Loaded", Toast.LENGTH_SHORT).show();
            isRefreshing = false;
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context context() {
        return null;
    }
}
