package id.wika.cpptkponline.view.dialog;

import android.view.View;
import android.widget.Toast;

/**
 * Created by mamang_kompii on 8/30/17.
 */

public class DiscardFormDialogFragment extends GeneralConfirmationDialogFragment {

    public static final String TAG = "DiscardFormDialogFragment";

    @Override
    protected boolean isTitleLabelVisible() {
        return false;
    }

    @Override
    protected String getTitleText() {
        return "";
    }

    @Override
    protected String getDescriptionText() {
        return "Apakah Anda ingin keluar dari halaman ini?";
    }

    @Override
    protected String getNegativeButtonText() {
        return "BATAL";
    }

    @Override
    protected boolean isNeutralButtonVisible() {
        return false;
    }

    @Override
    protected String getNeutralButtonText() {
        return "SIMPAN DRAF";
    }

    @Override
    protected String getPositiveButtonText() {
        return "KELUAR";
    }

    @Override
    void onNegativeButtonClicked() {
        dismiss();
    }

    @Override
    void onNeutralButtonClicked() {
        Toast.makeText(getActivity(), "Form telah disimpan sebagai draf", Toast.LENGTH_SHORT).show();
        onPositiveButtonClicked();
    }

    @Override
    void onPositiveButtonClicked() {
        dismiss();
        getActivity().finish();
    }
}
