package id.wika.cpptkponline.view.home.list;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collection;
import java.util.List;

import id.wika.cpptkponline.R;
import id.wika.cpptkponline.data.FormStatus;
import id.wika.cpptkponline.data.FormSummary;
import id.wika.cpptkponline.helper.FormHelper;

/**
 * Created by mamang_kompii on 8/25/17.
 */

public class FormListSummaryRecyclerViewAdapter extends RecyclerView.Adapter {

    private List<FormListItem> data;
    private List<FormListItem> dataTemp;
    private OnItemClickListener listener;
    private final AdapterFilter adapterFilter;

    public FormListSummaryRecyclerViewAdapter(List<FormListItem> data) {
        this.data = data;
        this.adapterFilter = new AdapterFilter(this);
    }

    public FormListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        FormListItem item = data.get(position);
        return item.getItemType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case FormListItem.HEADER_ITEM_TYPE:
                View v1 = inflater.inflate(R.layout.form_date_header_list_item, parent, false);
                viewHolder = new FormDateViewHolder(v1);
                break;

            case FormListItem.RECORD_ITEM_TYPE:
                View v2 = inflater.inflate(R.layout.form_summary_list_item, parent, false);
                viewHolder = new FormSummaryViewHolder(v2);
                break;

            default:
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        FormListItem item = data.get(position);

        switch (holder.getItemViewType()) {
            case FormListItem.HEADER_ITEM_TYPE:
                renderHeaderItemType(((FormDateViewHolder) holder), item.getData());
                break;
            case FormListItem.RECORD_ITEM_TYPE:
                renderRecordItemType(((FormSummaryViewHolder) holder), item);
                break;
            default:
                break;
        }
    }

    private void renderHeaderItemType(FormDateViewHolder holder, Object item) {
        holder.getTextView().setText(item.toString());
    }

    private String getActionBarTitle(int incomingFormStatus) {

        if (incomingFormStatus == FormStatus.FORM_RENCANA_TINDAK_LANJUT) {
            return "Tahapan CP/PTKP: Rencana Tindak Lanjut";
        } else if (incomingFormStatus == FormStatus.FORM_AWAL) {
            return "Tahapan CP/PTKP: Pelaporan Awal";
        } else if (incomingFormStatus == FormStatus.FORM_PENGECEKAN) {
            return "Tahapan CP/PTKP: Pengecekan";
        } else if (incomingFormStatus == FormStatus.FORM_REALISASI_TINDAK_LANJUT) {
            return "Tahapan CP/PTKP: Realisasi Tindak Lanjut";
        } else if (incomingFormStatus == FormStatus.FORM_VERIFIKASI_REALISASI_TINDAK_LANJUT) {
            return "Tahapan CP/PTKP: Verifikasi Tindak Lanjut";
        } else if (incomingFormStatus == FormStatus.FORM_RENCANA_TINDAK_LANJUT_DIBATALKAN) {
            return "Tahapan CP/PTKP: Approval (Batal)";
        } else if (incomingFormStatus == FormStatus.FORM_RENCANA_TINDAK_LANJUT_DISETUJUI) {
            return "Tahapan CP/PTKP: Approval (Disetujui)";
        } else if (incomingFormStatus == FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_CLOSED) {
            return "Tahapan CP/PTKP: Temuan (Closed)";
        } else if (incomingFormStatus == FormStatus.LAPORAN_TINDAK_LANJUT_STATUS_OPEN) {
            return "Tahapan CP/PTKP: Temuan (Open)";
        } else {
            return "Tahapan CP/PTKP: Pelaporan";
        }
    }

    private void renderRecordItemType(final FormSummaryViewHolder holder, final FormListItem item) {

        FormSummary summary = ((FormSummary) item.getData());
        holder.getStatusImageView().setImageResource(getResourceByStatus(summary.getStatus()));

        SpannableStringBuilder title;
        SpannableString subtitle;
        String IDTitle = summary.noTemuan;

        if (IDTitle == null)
            IDTitle = String.valueOf(summary.getRemoteFormId());
        else
            IDTitle = summary.getNoTemuan();

        if (summary.getTitle() == null) title = new SpannableStringBuilder("-");
        else title = new SpannableStringBuilder("No. " + IDTitle + " @");

        if (summary.getSubTitle() == null) subtitle = new SpannableString("");
        else subtitle = new SpannableString(summary.getSummary());


        subtitle.setSpan(new StyleSpan(Typeface.ITALIC), 0, subtitle.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        title.append(subtitle);
        holder.getTitleTextView().setText(title);
        holder.getSummaryTextView().setText(summary.getSubTitle() + " - " + summary.getTitle());
        String statusString = getActionBarTitle(summary.getStatus());
        holder.getSummaryStatusTextView().setText(statusString);
        holder.getExtraStatusView().setVisibility(summary.isNeedAttention() ? View.VISIBLE : View.GONE);

        if (listener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(holder.itemView, holder.getAdapterPosition());
                }
            });
        }
    }

    private int getResourceByStatus(int status) {
        return FormHelper.getFormStatusIcon(status);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public void setData(List<FormListItem> data) {
        this.data = data;
        this.adapterFilter.setRecordModelList((List<FormListItem>) data);
        this.notifyDataSetChanged();
    }

    private static class FormDateViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        public FormDateViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView;
        }

        public TextView getTextView() {
            return textView;
        }
    }

    private static class FormSummaryViewHolder extends RecyclerView.ViewHolder {

        private ImageView statusImageView;
        private TextView titleTextView;
        private TextView summaryTextView;
        private TextView summaryStatusTextView;
        private View extraStatusView;

        public FormSummaryViewHolder(View itemView) {
            super(itemView);

            statusImageView = itemView.findViewById(R.id.status_image);
            titleTextView = itemView.findViewById(R.id.item_title);
            summaryTextView = itemView.findViewById(R.id.item_summary);
            extraStatusView = itemView.findViewById(R.id.extra_status);
            summaryStatusTextView = itemView.findViewById(R.id.item_summary_status);
        }

        public ImageView getStatusImageView() {
            return statusImageView;
        }

        public TextView getTitleTextView() {
            return titleTextView;
        }

        public TextView getSummaryTextView() {
            return summaryTextView;
        }

        public TextView getSummaryStatusTextView() {
            return summaryStatusTextView;
        }

        public View getExtraStatusView() {
            return extraStatusView;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void startCollapsingFilter() {
        dataTemp = data;
    }

    public void stopCollapsingFilter() {
        if (dataTemp != null) {
            setData(dataTemp);
        }
    }

    public AdapterFilter getAdapterFilter() {
        return adapterFilter;
    }

    public void setFilterData(Collection<FormListItem> metaTranscriptModelCollection) {
        this.data = (List<FormListItem>) metaTranscriptModelCollection;
        this.notifyDataSetChanged();
    }
}
