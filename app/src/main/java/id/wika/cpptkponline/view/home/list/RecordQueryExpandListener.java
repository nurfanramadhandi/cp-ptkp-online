package id.wika.cpptkponline.view.home.list;

import android.support.v4.view.MenuItemCompat;
import android.view.MenuItem;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Date    : 19/September/2017 - 15:56
 * Email   : ardhimaarik2@gmail.com
 * Project : Cp Ptkp Online
 *
 * Copyright (c) 2017 "Kirra Dev" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public class RecordQueryExpandListener implements MenuItemCompat.OnActionExpandListener {
    private FormListSummaryRecyclerViewAdapter adapter;

    public RecordQueryExpandListener(FormListSummaryRecyclerViewAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        adapter.startCollapsingFilter();
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        adapter.stopCollapsingFilter();
        return true;
    }
}
